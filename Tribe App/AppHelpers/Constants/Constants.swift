//
//  Constants.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 23/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//


import UIKit
import Firebase

//to set the environment
let isDebugEnabled = true
//to set logs
let isLogEnabled = false


struct mapZoomLevel{
    static let max:Float = 19.0
    static let min:Float = 5.0
}

struct appColor{

        static let blueColor = UIColor(red: 83.0/255.0, green: 168.0/255.0, blue: 253.0/255.0, alpha: 1.0)
         static let white = UIColor.white
        static let black = UIColor.black
        static let appgrayBackground = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)

    
    //static let yellow = UIColor(red: 249.0/255.0, green: 166.0/255.0, blue: 2.0/255.0, alpha: 1.0)

    static let gradientRedStart = UIColor(red: 255.0/255.0, green: 88.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    static let gradientRedEnd = UIColor(red: 255.0/255.0, green: 88.0/255.0, blue: 137.0/255.0, alpha: 1.0)
    static let redShadow = UIColor(red: 255.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)
    static let bgColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let green = UIColor.color(r: 133, g: 194, b: 38)
    static let inactiveWhite = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 0.8)
    static let brown = UIColor(red: 248.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    static let gradientStart = UIColor(red: 44.0/255.0, green: 190.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    static let gradientEnd = UIColor(red: 54.0/255.0, green: 72.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let blue = UIColor.color(r: 51, g: 74, b: 219)
    static let gray = UIColor.color(r: 169, g: 167, b: 168)
    static let boxGray = UIColor.color(r: 148, g: 148, b: 148)
    static let splash = UIColor.color(r: 42, g: 121, b: 195)
    static let payoutRed = UIColor.color(r: 248, g: 24, b: 73)
    static let payoutBlue = UIColor.color(r: 69, g: 111, b: 245)
    static let lightGray = UIColor.groupTableViewBackground
    static let red = UIColor.black//UIColor.color(r:221, g:18, b:123)
}


struct gradientTextColor{
    static let start = UIColor(red: 35.0/255.0, green: 97.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 42.0/255.0, green: 65.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 55.0/255.0, green: 32.0/255.0, blue: 208.0/255.0, alpha: 1.0)
}


let warningMessageShowingDuration = 1.25

extension UIColor{
    class func color(r:CGFloat,g:CGFloat,b:CGFloat,alpha:CGFloat? = nil) -> UIColor{
        if let alp = alpha{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alp)
        }else{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
        }
    }
}


/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
    public static let USER_DID_ADD_NEW_CARD_NOTIFICATION = NSNotification.Name("UserDidAddNewCardNotification")
    public static let USER_DID_UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UserDidUpdateProfileNotification")
    public static let FIRInstanceIDTokenRefreshNotification = NSNotification.Name.MessagingRegistrationTokenRefreshed
    public static let DROP_OFF_ADDRESS_DID_SET_NOTIFICATION = NSNotification.Name("DropOffAddressDidSelected")
    public static let TIME_UP_NOTIFICATION = NSNotification.Name("TimeUpNotification")
    public static let STOP_CHECKING_NEAR_BY_DRIVER_NOTIFICATION = NSNotification.Name("StopCheckingNearByDriver")
    public static let RIDE_ACCEPTED_NOTIFICATION = NSNotification.Name("RideAcceptNotification")
    public static let RIDE_CANCELLED_NOTIFICATION = NSNotification.Name("RideCancelledNotification")
    public static let RIDE_START_NOTIFICATION = NSNotification.Name("RideStartedNotification")
    public static let RIDE_END_NOTIFICATION = NSNotification.Name("RideEndedNotification")
    public static let RIDE_REJECTED_NOTIFICATION = NSNotification.Name("RideRejectedNotification")
    public static let DRIVER_REACHED_NOTIFICATION = NSNotification.Name("DriverReachedNotification")
    public static let DISMISS_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("DismissPaymentModeNotification")
    public static let SELECT_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("SelectPaymentMode")
    public static let BACK_TO_PAYMENT_MODE_NOTIFICATION = NSNotification.Name("BackToPaymentMode")
    public static let PAYMENT_SUCCESS_BY_USER_NOTIFICATION = NSNotification.Name("PaymentSuccessfullPayedByUser")
    public static let AFTER_PAYMENT_SUCCESS_BY_USER_NOTIFICATION = NSNotification.Name("AfterPaymentSuccessfullPayedByUser")
    public static let CartingKidzsSlideNavigationControllerDidClose = NSNotification.Name("SlideNavigationControllerDidClose")
    public static let RIDE_UPDATED_NOTIFICATION = NSNotification.Name("RideUpdatedNotification")
    public static let RIDE_CANCELLED_BY_DRIVER_NOTIFICATION = NSNotification.Name("RideCancelledByDriverNotification")
    public static let USER_BLOCKED_BY_ADMIN_NOTIFICATION = NSNotification.Name("user_blocked")
    public static let USER_UNBLOCKED_BY_ADMIN_NOTIFICATION = NSNotification.Name("user_unblocked")
    
    public static let LANGUAGE_CHANGE_NOTIFICATION = NSNotification.Name("LanguageChangeNotification")
    public static let DROP_CHANGE_OLD_USER_NOTIFICATION = NSNotification.Name("ride_extended")
    public static let SINGLE_USER_NOTIFICATION = NSNotification.Name("single_notification")
    public static let BULK_USER_NOTIFICATION = NSNotification.Name("bulk_notification")
    public static let OFFER_USER_NOTIFICATION = NSNotification.Name("offer_notification")
    public static let WALLET_AMOUNT_UPDATE_NOTIFICATION = NSNotification.Name("walletAmountUpdateNotification")
    public static let SCHEDULE_RIDE_ACCEPTED_NOTIFICATION = NSNotification.Name("scehdule_ride_accepted")
    public static let REMOVE_SCHEDULE_RIDE_NOTIFICATION = NSNotification.Name("remove_schedule_ride")
    
    
    
}


let kNotificationType = "notification_type"
let kRideAcceptedNotification = "ride_accepted"
let kRideRequestNotification = "ride_request"
let kRideCancelledNotification = "cancel_ride"
let kRideStartedNotification = "ride_started"
let kRideEndedNotification = "ride_ended"
let kDriverReachedNotification = "driver_reached"
let kDriverCancelledNotification = "driver_cancelled"
let kUserBlockedByAdmin = "user_blocked"
let kUserUnBlockedByAdmin = "user_unblocked"
let kDropChangeNewUserNotification = "ride_extended"
let kSingleUserNotification = "single_notification"
let kBulkUserNotification = "bulk_notification"
let kOfferUserNotification = "offer"
let kScheduleRideAccept = "scehdule_ride_accepted"
let kRemoveScheduleRide = "remove_schedule_ride"



enum NotificationType:String{
    case bookingCreatedByCustomerNotification = "customer_create_booking"
    case bookingCreatedByBusinessNotification = "business_create_booking"
    case bookingCancelledByBusinessNotification = "booking_cancel_by_business"
    case bookingCancelledByCustomerNotification = "booking_cancel_by_customer"
    case bookingCompletedByNotification = "booking_complete_by_business"
}


let kUserDefaults = UserDefaults.standard
/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/

let kDeviceToken = "DeviceToken"
let kFirstLaunchAfterReset = "firstLaunchAfterReset"

//let kSupportEmail = "nakulsharma.1296@gmail.com"
//let kReportUsEmail = "nakulsharma.1296@gmail.com"

//let tollFreeNumber = "1-888-202-3625"
let appID = "1450626631"
//let adminCommission = 0.04

let rountingNumberDigit = 9
let accountNumberDigit = 12
let ssnDigit = 9

enum AddressCategory:String{
    case home = "Home"
    case work = "Work"
    case other = ""
    init(rawValue:String) {
        if rawValue.lowercased() == "home"{
            self = .home
        }else if rawValue.lowercased() == "work"{
            self = .work
        }else{
            self = .other
        }
    }
    func getIcon() -> UIImage{
        if self == .home{
            return #imageLiteral(resourceName: "home")
        }else if self == .work{
            return #imageLiteral(resourceName: "work")
        }else{
            return #imageLiteral(resourceName: "favorite_sel")
        }
    }
}


enum ErrorCode:Int{
    case success
    case failure
    case forceUpdate
    case normalUpdate
    case sessionExpire
    case previousDues

    init(rawValue:Int) {
        if rawValue == 102{
            self = .forceUpdate
        }else if rawValue == 101{
            self = .normalUpdate
        }else if rawValue == 345{
            self = .sessionExpire
        }else if rawValue == 303{
            self = .previousDues
        }else if ((rawValue >= 200) && (rawValue < 300)){
            self = .success
        }else{
            self = .failure
        }
    }
}


enum SelectLanguage: String {
    case English = "English"
    case Portuguese = "Portuguese"
    
}


enum SelectPaymentOption: String {
    case byCash = "cash"
    case byWallet = "wallet"
    case byCard = "card"

}


enum payedPaymenttype: String {
    case cash = "by_cash"
    case wallet = "by_wallet"
    case card = "online"
    
}
    


enum PayoutStatus:String{
    case paid
    case pending

    func getColor() -> UIColor{
        if self == .paid{
            return appColor.green
        }else{
            return appColor.payoutRed
        }
    }
    
    init(rawValue:String) {
        if rawValue.lowercased() == "paid"{
            self = .paid
        }else{
            self = .pending
        }
    }
}


enum TicketStatus:String{
    case open = "open"
    case closed = "closed"
    case resolved = "resolved"
    case ouvrir = "ouvrir"
    case fermé = "fermé"
    case résolu = "résolu"
    init(rawValue:String) {
        if rawValue.lowercased() == "open"{
            self = .open
        }else if rawValue.lowercased() == "closed"{
            self = .closed
        }else if rawValue.lowercased() == "ouvrir" {
            self = .ouvrir
        }else if rawValue.lowercased() == "fermé" {
            self = .fermé
        }else if rawValue.lowercased() == "résolu" {
            self = .résolu
        }else{
            self = .resolved
        }
    }
    func getColor() -> UIColor{
        if (self == .open) || (self == .ouvrir){
            return UIColor(red: 179.0/255.0, green: 0, blue: 27.0/255.0, alpha: 1.0)
        }else if ((self == .closed) || (self == .fermé)) {
            return UIColor(red: 16.0/255.0, green: 122.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        }
        else{
            return UIColor(red: 16.0/255.0, green: 122.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        }
    }
}

/*================== API URLs ====================================*/
//"http://192.168.1.93:3000/"//
//let inviteLinkUrl = "https://hlsapp.ch/"//"http://45.58.45.156:3002/"
//let appLink = isDebugEnabled ? "http://test.hlsapp.ch/" : "https://hlsapp.ch/"
//let WEBSITE_URL = isDebugEnabled ? "http://test.hlsapp.ch/" : "https://hlsapp.ch/"
let apiVersion = "1"
let BASE_URL = isDebugEnabled ?"http://45.58.45.156:3004/" :"http://45.58.45.156:3004/"
let actionCableBaseUrl = "http://45.58.45.156:3004/"

enum api: String {
    case base
    case aboutUs
    case login
    case forgotPassword
    case signUp
    case searchCarByCategory
    case allCarCategory
    case rateDriver
    case userProfile
    case userRideHistory
    case userOnGoingRide
    case editProfile
    case allCars
    case rideRequest
    case changeRideRequest
    case rideDetails
    case addRatings
    case costEstimation
    case preCostEstimation
    case emergencyContact
    case driverLocation
    case termsAndConditions
    case userCancelRide
    case logout
    case allContact
    case removeContact
    case resetPassword
    case addCard
    case defaultCard
    case getCards
    case removeCard
    case payRide
    case newSearchUrl
    case newUserCancelRide
    case cancelRideForServer
    case newRideRequestUrl
    case rideLater
    case updateDeviceToken
    case isExistingContact
    case firebaseDatabase
    
    case getChildren
    case addChild
    case updateChild
    case deleteChild
    
    case addParent
    case FAQ
    case addSupportQuery
    case mailInvoiceForRide
    
    //favorite places/recent
    case addFavoritePlace
    case deleteFavoritePlace
    case updateFavoritePlace
    case listFavoritePlace
    case walletDetail
    case addTransaction
    case allSupportsTickets
    case addCommentToSupport
    case ticketView
    case cancelReasons
    case lastTwoRecentPlaces
    case authenticateUser
    case addTip
    case userFavouriteDriversList
    case addFavouriteDriver
    case updateDropAddress
    case allOffersList
    case removeScheduleRide
    case walletUserHistory

    
    
    func url() -> String {
        switch self {
        case .base: return BASE_URL
        case .signUp: return "\(BASE_URL)api/v\(apiVersion)/users"
        case .login: return "\(BASE_URL)api/v\(apiVersion)/login"
        case .isExistingContact: return "\(BASE_URL)api/v\(apiVersion)/check/existing/contact"
        case .updateDeviceToken: return "\(BASE_URL)api/v\(apiVersion)/update/device/token"
        case .logout: return "\(BASE_URL)api/v\(apiVersion)/logout"
        case .editProfile: return "\(BASE_URL)api/v\(apiVersion)/edit/profile"
        case .FAQ: return "\(BASE_URL)api/v\(apiVersion)/faqs"
        case .allSupportsTickets: return "\(BASE_URL)api/v\(apiVersion)/ticket/list"
        case .addSupportQuery: return "\(BASE_URL)api/v\(apiVersion)/new/ticket"
        case .addCommentToSupport: return "\(BASE_URL)api/v\(apiVersion)/ticket/reply"
        case .ticketView: return "\(BASE_URL)api/v\(apiVersion)/ticket/view"
        case .forgotPassword: return "\(BASE_URL)api/v\(apiVersion)/recover/password"
        case .resetPassword: return "\(BASE_URL)api/v\(apiVersion)/update/password"

        case .aboutUs: return "http://www.tecorb.com/"
        
        case .userProfile: return  "\(BASE_URL)api/v\(apiVersion)/profile"
        case .allContact: return "\(BASE_URL)api/v\(apiVersion)/contacts"
        case .emergencyContact: return "\(BASE_URL)api/v\(apiVersion)/add/emergency/contact"
        case .removeContact: return "\(BASE_URL)api/v\(apiVersion)/remove/contact"
        case .addCard: return "\(BASE_URL)api/v\(apiVersion)/add/card"
        case .defaultCard: return "\(BASE_URL)api/v\(apiVersion)/default/card"
        case .getCards: return "\(BASE_URL)api/v\(apiVersion)/cards"
        case .removeCard: return "\(BASE_URL)api/v\(apiVersion)/remove/card"
        case .payRide: return "\(BASE_URL)api/v\(apiVersion)/pay/pending/ride"
        case .authenticateUser: return "\(BASE_URL)api/v\(apiVersion)/authenticate/user"

        case .newSearchUrl: return "\(BASE_URL)api/v\(apiVersion)/search/car"
        case .allCarCategory: return "\(BASE_URL)api/v\(apiVersion)/car/categories"
        case .userRideHistory: return "\(BASE_URL)api/v\(apiVersion)/customer/booking/history"
        case .lastTwoRecentPlaces: return "\(BASE_URL)api/v\(apiVersion)/last/two/recent/places"
        case .listFavoritePlace: return "\(BASE_URL)api/v\(apiVersion)/recent/places/list"
        case .rideDetails: return "\(BASE_URL)api/v\(apiVersion)/ride/details"
        case .updateFavoritePlace: return "\(BASE_URL)api/v\(apiVersion)/update/recent/place"
        case .addFavoritePlace: return "\(BASE_URL)api/v\(apiVersion)/add/recent/place"
        case .deleteFavoritePlace: return "\(BASE_URL)api/v\(apiVersion)/delete/recent/place/"
        case .cancelReasons: return "\(BASE_URL)api/v\(apiVersion)/ride/cancel/reasons"
            
        case .newUserCancelRide: return "\(BASE_URL)api/v\(apiVersion)/customer/cancel/ride"
        case .userOnGoingRide: return "\(BASE_URL)api/v\(apiVersion)/user/booked/rides"
        case .cancelRideForServer: return "\(BASE_URL)api/v\(apiVersion)/server/cancel/ride"
        case .preCostEstimation: return "\(BASE_URL)api/v\(apiVersion)/estimated_cost"
        case .rateDriver: return "\(BASE_URL)api/v\(apiVersion)/customer/rate/driver"
        case .newRideRequestUrl: return "\(BASE_URL)api/v\(apiVersion)/ride/request"
        case .mailInvoiceForRide: return "\(BASE_URL)api/v\(apiVersion)/ride/invoice"
        case .addTip: return "\(BASE_URL)api/v\(apiVersion)/user/pay/tip"
        case .userFavouriteDriversList: return "\(BASE_URL)api/v\(apiVersion)/favourite/drivers"
        case .addFavouriteDriver: return "\(BASE_URL)api/v\(apiVersion)/favourite/driver"
        case .updateDropAddress : return "\(BASE_URL)api/v\(apiVersion)/change/ride/drop"
            
        case .allOffersList : return "\(BASE_URL)api/v\(apiVersion)/offer/list"
        case .removeScheduleRide : return "\(BASE_URL)api/v\(apiVersion)/cancel/schedule/ride"
            
            
            
            
        case .searchCarByCategory: return "\(BASE_URL)search_car"
        case .allCars: return "\(BASE_URL)all_cars"
        case .rideRequest: return "\(BASE_URL)ride_request"
        case .changeRideRequest: return "\(BASE_URL)change_ride_request"
        case .addRatings: return "\(BASE_URL)add_rating"
        case .costEstimation: return "\(BASE_URL)fair_price"
        case .driverLocation: return "\(BASE_URL)driver_location"
        case .termsAndConditions: return "\(BASE_URL)term_condition"
        case .getChildren: return "\(BASE_URL)api/v\(apiVersion)/child/show"
        case .addChild: return "\(BASE_URL)api/v\(apiVersion)/add/child"
        case .updateChild: return "\(BASE_URL)api/v\(apiVersion)/edit/child"
        case .deleteChild: return "\(BASE_URL)api/v\(apiVersion)/delete/child/"
        case .addParent: return "\(BASE_URL)api/v\(apiVersion)/edit/user/details"
        case .rideLater: return "\(BASE_URL)api/v\(apiVersion)/ride/later"
        case .userCancelRide: return "\(BASE_URL)api/v\(apiVersion)/customer/cancel/ride"
            
            //add id here (get type)
            
        case .walletDetail: return "\(BASE_URL)api/v\(apiVersion)/wallet/detail"
        case .addTransaction: return "\(BASE_URL)api/v\(apiVersion)/add/transaction"
        case .walletUserHistory: return "\(BASE_URL)api/v\(apiVersion)/wallet/history"

        case .firebaseDatabase: return isDebugEnabled ? "https://tribe-application.firebaseio.com" : "https://tribe-application.firebaseio.com"
        }
    }
}



/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "Facebook"
    case google = "Google"
}

/*======================== CONSTANT MESSAGES ==================================*/
enum warningMessage : String{
    case updateVersion = "updateVersion"
    case title = "Important Message"
    case pendingRideTitle = "pendingRideTitle"

    case setUpPassword = "setUpPassword"
    case invalidPassword = "invalidPassword"
    case invalidPhoneNumber = "invalidPhoneNumber"
    case invalidFirstName = "invalidFirstName"
    case invalidLastName = "invalidLastName"
    case invalidEmailAddress = "invalidEmailAddress"
    
    case emailCanNotBeEmpty = "emailCanNotBeEmpty"
    case restYourPassword = "restYourPassword"
    case changePassword = "changePassword"
    case logoutMsg = "logoutMsg"
    case networkIsNotConnected = "networkIsNotConnected"
    case functionalityPending = "functionalityPending"
    case enterPassword = "enterPassword"
    case validPassword = "validPassword"
    
    case enterOldPassword = "enterOldPassword"
    case validOldPassword = "validOldPassword"
    case enterNewPassword = "enterNewPassword"
    case validNewPassword = "validNewPassword"
    case confirmPassword = "confirmPassword"
    case passwordDidNotMatch = "passwordDidNotMatch"
    case cardDeclined = "cardDeclined"
    case enterCVV = "enterCVV"
    case enterValidCVV = "enterValidCVV"
    case cardHolderName = "cardHolderName"
    case expMonth = "expMonth"
    case expYear = "expYear"
    case validExpMonth = "validExpMonth"
    case validExpYear = "validExpYear"
    case validCardNumber = "validCardNumber"
    case cardNumber = "cardNumber"
    case sessionExpired = "sessionExpired"
    func messageString()->String{
        //
        switch self {
            
        case .updateVersion:
            return  warningMessage.updateVersion.rawValue.localizedString
        case .title:
            return  warningMessage.title.rawValue.localizedString
        case .pendingRideTitle:
            return  warningMessage.pendingRideTitle.rawValue.localizedString
        case .setUpPassword:
            return  warningMessage.setUpPassword.rawValue.localizedString
        case .invalidPassword:
            return  warningMessage.invalidPassword.rawValue.localizedString
        case .invalidPhoneNumber:
            return  warningMessage.invalidPhoneNumber.rawValue.localizedString
        case .invalidFirstName:
            return  warningMessage.invalidFirstName.rawValue.localizedString
        case .invalidLastName:
            return  warningMessage.invalidLastName.rawValue.localizedString
        case .invalidEmailAddress:
            return  warningMessage.invalidEmailAddress.rawValue.localizedString
            
        case .emailCanNotBeEmpty:
            return  warningMessage.emailCanNotBeEmpty.rawValue.localizedString
        case .restYourPassword:
            return  warningMessage.restYourPassword.rawValue.localizedString
        case .changePassword:
            return  warningMessage.changePassword.rawValue.localizedString
        case .logoutMsg:
            return  warningMessage.logoutMsg.rawValue.localizedString
        case .networkIsNotConnected:
            return  warningMessage.networkIsNotConnected.rawValue.localizedString
        case .functionalityPending:
            return  warningMessage.functionalityPending.rawValue.localizedString
        case .enterPassword:
            return  warningMessage.enterPassword.rawValue.localizedString
        case .validPassword:
            return  warningMessage.validPassword.rawValue.localizedString
        case .enterOldPassword:
            return  warningMessage.enterOldPassword.rawValue.localizedString
        case .validOldPassword:
            return  warningMessage.validOldPassword.rawValue.localizedString
        case .enterNewPassword:
            return  warningMessage.enterNewPassword.rawValue.localizedString
        case .validNewPassword:
            return  warningMessage.validNewPassword.rawValue.localizedString
        case .confirmPassword:
            return  warningMessage.confirmPassword.rawValue.localizedString
        case .passwordDidNotMatch:
            return  warningMessage.passwordDidNotMatch.rawValue.localizedString
        case .cardDeclined:
            return  warningMessage.cardDeclined.rawValue.localizedString
        case .enterCVV:
            return  warningMessage.enterCVV.rawValue.localizedString
        case .enterValidCVV:
            return  warningMessage.enterValidCVV.rawValue.localizedString
        case .cardHolderName:
            return  warningMessage.cardHolderName.rawValue.localizedString
        case .expMonth:
            return  warningMessage.expMonth.rawValue.localizedString
        case .expYear:
            return  warningMessage.expYear.rawValue.localizedString
        case .validExpMonth:
            return  warningMessage.validExpMonth.rawValue.localizedString
        case .validExpYear:
            return  warningMessage.validExpYear.rawValue.localizedString
        case .validCardNumber:
            return  warningMessage.validCardNumber.rawValue.localizedString
        case .cardNumber:
            return  warningMessage.cardNumber.rawValue.localizedString
        case .sessionExpired:
            return  warningMessage.sessionExpired.rawValue.localizedString
            //
        }
        //
    }
}





/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let stripeKey =  isDebugEnabled ? "pk_test_IS3Nmd85YwpaKFdJs7AWxhTR" : "pk_live_0S0mwMPzTPoidmEDPoZ0pMqp"
let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.461178826649-mhlcdlkefqppmvj02vk4b45d5d2eh7gd"
let GOOGLE_API_KEY = "AIzaSyAVVb0SYD1oAuVHT5lyofdK2BHNzM9MS1A"//"AIzaSyC7qOidgjd9BL9gToTdtGuOQBNRwu_aTpg" 
let kGoogleClientId = "461178826649-mhlcdlkefqppmvj02vk4b45d5d2eh7gd.apps.googleusercontent.com"
let firebaseDatabaseUrl = isDebugEnabled ? "https://tribe-application.firebaseio.com" : "https://tribe-application.firebaseio.com"
/*============== PRINTING IN DEBUG MODE ==================*/
func print_debug <T>(_ object : T){
    if isLogEnabled{
        print(object)
    }
}


func print_log <T>(_ object : T){
//    NSLog("\(object)")
}



enum fontSize : CGFloat {
    case small = 12.0
    case smallMedium = 14.0
    case medium = 15.0
    case lagreMedium = 16.0
    case large = 17.0
    case xLarge = 18.0
    case xXLarge = 20.0
    case xXXLarge = 26.0
    case xXXXLarge = 32.0
}

enum fonts {
    enum OpenSans : String {
        case regular = "Roboto-Regular"
        case semiBold = "OpenSans-Semibold"
        case bold = "Roboto-Bold"
        case medium = "Roboto-Medium"
        case light = "Roboto-Light"
        func font(_ size : fontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        }
    }
}

//uses of fonts
//let fo = fonts.OpenSans.regular.font(.xXLarge)


/*==============================================*/






