
//
//  RideService.swift
//  TaxiApp
//
//  Created by TecOrb on 03/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation


class RideService {
    static let sharedInstance = RideService()
    func getAllCategoryFromServer(completionBlock:@escaping (_ succes:Bool, _ carCategories:Array<CarCategory>?, _ message: String) -> Void){
        if !AppSettings.shared.isLoggedIn{
            return
        }

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allCarCategory.url()) headers :\(head)")
        Alamofire.request(api.allCarCategory.url(),method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all categories json is:\n\(json)")
                    let parser = CarCategoryParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.categories,parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    func getAllCategoryFromServer(_ latitude:Double,longitude:Double,completionBlock:@escaping (_ succes:Bool, _ carCategories:Array<CarCategory>?, _ message: String) -> Void){
        if !AppSettings.shared.isLoggedIn{
            return
        }
        let params = ["current_latitude":"\(latitude)","current_longitude":"\(longitude)"]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allCarCategory.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.allCarCategory.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all categories json is:\n\(json)")
                    let parser = CarCategoryParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.categories, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func rateDriverWithRide(_ rideID:String,rating:Double,userID:String,completionBlock:@escaping (_ succes:Bool, _ ride:Ride?, _ message: String) -> Void){
        let params = ["ride_id":rideID,"rating":"\(rating)","user_id":userID]
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.rateDriver.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.rateDriver.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("rating json is:\n\(json)")
                    let parser = RideParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.ride, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    
    
    func searchCarsByCategory(_ categoryID:String, latitude:Double,longitude:Double,userID:String,completionBlock:@escaping (_ success:Bool, _ cars: Array<Car>?, _ message: String) -> Void){

        var params = ["latitude":"\(latitude)","longitude":"\(longitude)"]
        params.updateValue(categoryID, forKey: "category_id")
        params.updateValue(userID, forKey: "user_id")
        
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.searchCarByCategory.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.searchCarByCategory.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all cars json is:\n\(json)")
                    let parser = CarParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.cars, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    func newSearchCarsByCategory(_ categoryID:String, latitude:Double,longitude:Double,userID:String,isFromCategoryPage:Bool,completionBlock:@escaping (_ success:Bool, _ cars: Array<Car>?, _ message: String) -> Void){
        var params = ["latitude":"\(latitude)","longitude":"\(longitude)"]
        params.updateValue(categoryID, forKey: "category_id")
        params.updateValue(userID, forKey: "user_id")
        if isFromCategoryPage {
            if categoryID.isEmpty {
                return
            }
        }
        
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.newSearchUrl.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.newSearchUrl.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all cars json is:\n\(json)")
                    let parser = CarParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock(parser.errorCode == .success,parser.cars, parser.responseMessage)
                    }
                }else{
                    completionBlock(false, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func getEstimatedCostForCategory(_ categoryID:String, distance:Double,time:Double,completionBlock:@escaping (_ success:Bool, _ estimatedCost:Double?, _ message: String) -> Void){
        var params = ["distance":"\(distance)","time":"\(time)"]
        params.updateValue(categoryID, forKey: "category_id")

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.preCostEstimation.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.preCostEstimation.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cost estimation json is:\n\(json)")
                    let costParser = CostEstimationParser(json: json)
                    if costParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if costParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((costParser.errorCode == .success), costParser.estimatedCost, costParser.responseMessage)
                    }
                }else{
                    completionBlock(false, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }

            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func getApproxFares(_ distance:Double,time:Double,offerCode: String,completionBlock:@escaping (_ success:Bool, _ approxFares:Array<ApproxFare>?, _ message: String, _ wallet: Wallet?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue("\(distance)", forKey: "distance")
        params.updateValue("\(time)", forKey: "time")
        
        if offerCode.trimmingCharacters(in: .whitespaces).count != 0 {
            params.updateValue(offerCode, forKey: "offer_code")
        }
        let head =  AppSettings.shared.prepareHeader(withAuth: true)

        print_debug("hitting \(api.preCostEstimation.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.preCostEstimation.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("approx fares json is:\n\(json)")
                    let parser = ApproxFareParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.approxFares, parser.message, parser.wallet)
                    }
                }else{
                    completionBlock(false, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString, nil)
                }

            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, nil, error.localizedDescription, nil)
            }
        }
    }



    func getAllCarsFromServer(completionBlock:@escaping (_ success:Bool, _ cars:Array<Car>?, _ message: String) -> Void){
        
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allCars.url()) with and headers :\(head)")
        Alamofire.request(api.allCars.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all cars json is:\n\(json)")
                    let parser = CarParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.cars, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    
    func getRideHistoryFromServer(_ userID: String, pageNumber:Int,perPage: Int,historyType:String, completionBlock:@escaping (_ success: Bool, _ ride:Array<Ride>?,_ totalPage:Int, _ message: String) -> Void){
        var params = ["role":"user", "page":"\(pageNumber)", "per_page":"\(perPage)"]
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(historyType, forKey: "key")

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.userRideHistory.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.userRideHistory.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride history json is:\n\(json)")
                    let rideParser = RideParser(json: json)
                    if rideParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if rideParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((rideParser.errorCode == .success), rideParser.rides,rideParser.totalPages, rideParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,0,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,0,error.localizedDescription)
            }
        }
    }


    func getOnGoingRides(completionBlock:@escaping (_ success: Bool, _ ride:Array<Ride>?,_ totalPage:Int, _ message: String) -> Void){

        var head = AppSettings.shared.prepareHeader(withAuth: true)
        head.updateValue("Basic cm9vdDpwYXNzd29yZA==", forKey: "Authorization")
        print_debug("hitting \(api.userOnGoingRide.url()) with and headers :\(head)")
        Alamofire.request(api.userOnGoingRide.url(), method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug(" Booked ride history json is:\n\(json)")
                    let rideParser = RideParser(json: json)
//                    if rideParser.errorCode == .forceUpdate{
//                        AppSettings.shared.showForceUpdateAlert()
//                    }else if rideParser.errorCode == .sessionExpire{
//                        //AppSettings.shared.showSessionExpireAndProceedToLandingPage()
//                    }else{
                        completionBlock((rideParser.errorCode == .success), rideParser.rides,rideParser.totalPages, rideParser.responseMessage)
//                    }
                }else{
                    completionBlock(false,nil,0,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,0,error.localizedDescription)
            }
        }
    }
    
    func getRideDetails(_ rideID:String,completionBlock:@escaping (_ success: Bool, _ ride:Ride?, _ message: String) -> Void){
        let params = ["ride_id":rideID]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.rideDetails.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.rideDetails.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride json is:\n\(json)")
                    let rideParser = RideParser(json: json)
                    if rideParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if rideParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((rideParser.errorCode == .success), rideParser.ride, rideParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    func rideRequest(_ userID: String,carCategory:String, pickUpAddress:String,pickUpCoordinates: CLLocationCoordinate2D, dropOffAddress:String, dropOffCoordinates: CLLocationCoordinate2D,time: Double,distance:Double,scheduledDate:Date?,paymentType:String,offerCode:String,completionBlock:@escaping (_ errorCode:ErrorCode, _ ride:Ride?, _ message:String) -> Void){
        var params = Dictionary<String,Any>()
        var apiUrl = api.newRideRequestUrl
        if let date = scheduledDate{
            params.updateValue(Int64(date.toMillis()), forKey: "schedule_time")
            apiUrl = api.rideLater
        }else{
            
//            if offerCode.trimmingCharacters(in: .whitespaces).count != 0 {
//                params.updateValue(offerCode, forKey: "offer_code")
//            }
        }
//        if isCashSelect {
            params.updateValue( paymentType, forKey: "payment_type")
//
//        }else{
//            params.updateValue( "wallet", forKey: "payment_type")
//
//        }
        

        

        
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(carCategory, forKey: "category_id")
        params.updateValue("\(pickUpAddress)", forKey: "start_location")
        params.updateValue("\(dropOffAddress)", forKey: "end_location")
        params.updateValue("\(pickUpCoordinates.latitude)", forKey: "start_lat")
        params.updateValue("\(pickUpCoordinates.longitude)", forKey: "start_lng")
        params.updateValue("\(dropOffCoordinates.latitude)", forKey: "end_lat")
        params.updateValue("\(dropOffCoordinates.longitude)", forKey: "end_lng")
        params.updateValue("\(time)", forKey: "time")
        params.updateValue("\(distance)", forKey: "distance")

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(apiUrl.url()) with param \(params) and headers :\(head)")
        Alamofire.request(apiUrl.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride json is:\n\(json)")
                    let rideParser = RideParser(json: json)
                    if rideParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if rideParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock(rideParser.errorCode,rideParser.ride,rideParser.responseMessage)
                    }
                }else{
                    completionBlock(ErrorCode.failure, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(ErrorCode.failure,nil, error.localizedDescription)
            }
        }
    }
    
    func cancelRide(_ rideID:String,reason:String, isForServerUse:Bool=false, completionBlock:@escaping (_ success:Bool, _ ride:Ride?, _ message:String) -> Void){
        let params = ["ride_id":rideID, "reason":reason]

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        let url = isForServerUse ? api.cancelRideForServer.url() : api.newUserCancelRide.url()
        print_debug("hitting \(url) with param \(params) and headers :\(head)")
        Alamofire.request(url,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride Cancel json is:\n\(json)")
                    let parser = RideParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.ride,parser.responseMessage)
                    }
                }else{
                    completionBlock(false, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil, error.localizedDescription)
            }
        }
    }
    
    
    
    func getRideLocationUpdate(_ rideID:String,carID:String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["ride_id":rideID,"car_id": carID]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.driverLocation.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.driverLocation.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("driver location json is:\n\(json)")
                    if let _resCode = json["code"].int as Int?{
                        if _resCode == 200{
                            if let dict = json["car_location"].dictionaryObject as [String:AnyObject]?{
                                let locationModel = RideLocationModel(dictionary: dict)
                                completionBlock(locationModel)
                            }else{
                                completionBlock(nil)
                            }
                        }else{
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure:
                completionBlock(nil)
            }
        }
    }
    




    func mailInvoice(for rideID:String, email:String, completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){
        let params = ["ride_id":rideID,"email":email]
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        Alamofire.request(api.mailInvoiceForRide.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print("InvoiceMailJson>>>\(json)")
                    let parser = RideParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.responseMessage)
                    }
                }else{
                    completionBlock(false, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, error.localizedDescription)
            }
        }
    }

    func getCancelReasons(completionBlock:@escaping ( _ succes:Bool,  _ reasons:Array<CancelReason>?, _ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.cancelReasons.url()) headers :\(head)")
        Alamofire.request(api.cancelReasons.url(),method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("all cancell reason json is:\n\(json)")
                    let parser = CancelReasonParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.reasons,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    func addTipFromUser(_ rideID:String,amount:String,completionBlock:@escaping (_ succes:Bool, _ message: String) -> Void){
        let params = ["ride_id":rideID,"amount":amount]
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addTip.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.addTip.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("forgotpassword json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        var errorCode : ErrorCode = .failure
                        errorCode = ErrorCode(rawValue: responseCode)
                        
                        if errorCode == .forceUpdate {
                            //                            AppSettings.shared.hideLoader()
                            AppSettings.shared.showForceUpdateAlert()
                        }else if errorCode == .sessionExpire{
                            AppSettings.shared.hideLoader()
                            AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                        }else if errorCode == .success{
                            completionBlock(true,json["message"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? "")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? "")
                    }
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,error.localizedDescription)
            }
        }
    }
    
    
    
    
    
    
    
    func updateDropAddress(of rideId:String,currentAddress:String,currentCoordinate:CLLocationCoordinate2D,newAddress:String,newAddressCoordinate:CLLocationCoordinate2D,distanceBeforeChange:Double,distanceAfterChange:Double, timeBeforeChange:Double, timeAfterChange:Double, completionBlock:@escaping (_ success: Bool, _ ride:Ride?, _ message: String) -> Void){
        var params = ["ride_id":rideId]
        params.updateValue(currentAddress, forKey: "change_location")
        params.updateValue("\(currentCoordinate.latitude)", forKey: "change_lat")
        params.updateValue("\(currentCoordinate.longitude)", forKey: "change_lng")
        params.updateValue("\(distanceBeforeChange)", forKey: "before_point_distance")
        params.updateValue("\(timeBeforeChange)", forKey: "before_point_time")
        params.updateValue(newAddress, forKey: "end_location")
        params.updateValue("\(newAddressCoordinate.latitude)", forKey: "end_lat")
        params.updateValue("\(newAddressCoordinate.longitude)", forKey: "end_lng")
        params.updateValue("\(distanceAfterChange)", forKey: "after_point_distance")
        params.updateValue("\(timeAfterChange)", forKey: "after_point_time")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.updateDropAddress.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.updateDropAddress.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("updated json is:\n\(json)")
                    let rideParser = RideParser(json: json)
                    if rideParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if rideParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((rideParser.errorCode == .success), rideParser.ride, rideParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    
    
    ///Remove schedule ride
    
    
    
    func removeScheduleRide(_ rideID:String, completionBlock:@escaping (_ success:Bool, _ ride:Ride?, _ message:String) -> Void){
        let params = ["schedule_ride_id":rideID]
        
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(String(describing: api.removeScheduleRide.url)) with param \(params) and headers :\(head)")
        Alamofire.request(api.removeScheduleRide.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride Cancel json is:\n\(json)")
                    let parser = RideParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.ride,parser.responseMessage)
                    }
                }else{
                    completionBlock(false, nil, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil, error.localizedDescription)
            }
        }
    }

}
