//
//  WalletService.swift
//  JO CAB
//
//  Created by Parikshit on 27/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation


class WalletService {
    static let sharedInstance = WalletService()

    func getUserWalletDetail(completionBlock:@escaping (_ succes:Bool, _ card: Bank?,_ walletBalance: Double, _ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.walletDetail.url()) headers :\(head)")
        Alamofire.request(api.walletDetail.url(),method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("wallet detail json is:\n\(json)")
                    let parser = WalletParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.bank,parser.walletBalance, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,0.0, response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                completionBlock(false,nil,0.0,error.localizedDescription)
            }
        }
        
    }


    func addTransaction(_ transactionID:String,amount:String,image: UIImage?,completionBlock:@escaping (_ success:Bool,_ message:String)-> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["transaction_id":transactionID,"amount":amount,"role":"user"]
        print_debug("hitting \(api.addTransaction.url()) headers :\(head) or Parama \(params)")

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = image{
                    if let data = pimage.jpegData(compressionQuality: 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.addTransaction.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("add transaction json is:\n\(json)")
                                let parser = SupportTicketParser(json: json)
                                if parser.errorCode == .forceUpdate{
                                    
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                    
                                }else if parser.errorCode == .sessionExpire{
                                    
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                    
                                }else{
                                    completionBlock((parser.errorCode == .success),parser.message)
                                }
                            }else{
                                completionBlock(false,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                            }
                        case .failure(let error):
                            let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                            print_debug(message)
                            completionBlock(false,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,encodingError.localizedDescription)
                }
        }
        )
        
    }

    func addFavouriteDriverForUser(_ rideID:String,driverId:String,isFavorite:Bool,completionBlock:@escaping (_ succes:Bool, _ ride:FavouriteDrivers?, _ message: String) -> Void){
        
        var params = Dictionary<String,String>()
        params.updateValue(rideID, forKey: "ride_id")
        params.updateValue(driverId, forKey: "driver_id")
        params.updateValue(isFavorite ? "true" : "false", forKey: "is_favourite")
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addFavouriteDriver.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.addFavouriteDriver.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("add favourite Driver json is:\n\(json)")
                    let parser = FavouriteDriverParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success), parser.driver, parser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    

    func getFavouriteDriverForUser(pageNumber:Int,perPage: Int, completionBlock:@escaping (_ success: Bool, _ ride:Array<FavouriteDrivers>?, _ message: String) -> Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.userFavouriteDriversList.url())  and headers :\(head)")
        Alamofire.request(api.userFavouriteDriversList.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("favouritesDriver json is:\n\(json)")
                    let driverParser = FavouriteDriverParser(json: json)
                    if driverParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if driverParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        
                        completionBlock((driverParser.errorCode == .success), driverParser.drivers, driverParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    
    func getAllOfferList(pageNumber:Int,perPage: Int, completionBlock:@escaping (_ success: Bool, _ offers:Array<OfferModel>?, _ message: String) -> Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allOffersList.url())  and headers :\(head)")
        Alamofire.request(api.allOffersList.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("favouritesDriver json is:\n\(json)")
                    let offerParser = OfferParser(json: json)
                    if offerParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if offerParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        
                        completionBlock((offerParser.errorCode == .success), offerParser.offers, offerParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    
    func walletUserHistory(keyWord:String, pageNumber:Int,perPage: Int,completionBlock:@escaping (_ success:Bool,_ cards: Array<WalletHistoryModel>?,_ message:String,_ totalPages:Int) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["keyword":keyWord,"role":"user", "page":"\(pageNumber)", "per_page":"\(perPage)"]
        
        print_debug("hitting \(api.walletUserHistory.url()) with and headers :\(head) and params :\(params)")
        Alamofire.request(api.walletUserHistory.url(),method: .post,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("wallet history json is:\n\(json)")
                    let historyWalletParser = HistoryWalletParser(json: json)
                    if historyWalletParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if historyWalletParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        completionBlock((historyWalletParser.errorCode == .success),historyWalletParser.walletHistories,historyWalletParser.responseMessage, historyWalletParser.totalPages)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString, 0)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription, 0)
            }
        }
    }
    
}
