//
//  PlaceService.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 02/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class PlaceService {
    static let sharedInstance = PlaceService()
    fileprivate init() {}
    
    func getPlaceForUser(completionBlock:@escaping (_ success:Bool,_ recentPlaces: Array<FavoritePlace>?, _ favoritePlaces: Array<FavoritePlace>?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.listFavoritePlace.url()) with and headers :\(head)")
        Alamofire.request(api.listFavoritePlace.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("places json is:\n\(json)")
                    let parser = FavoritePlaceParser(json: json)
                    if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        completionBlock((parser.errorCode == .success), parser.recentPlaces, parser.favourites, parser.message)
                    }
                }else{
                    completionBlock(false,nil,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,nil,error.localizedDescription)
            }
        }
    }

    func removePlace(_ id:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){
        if id.trimmingCharacters(in: .whitespaces) == ""{
            return
        }
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue(id, forKey: "place_id")
        print_debug("hitting \(api.deleteFavoritePlace.url()) and headers :\(head) with params : \(params)")
        Alamofire.request(api.deleteFavoritePlace.url(),method: .post,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("places json is:\n\(json)")
                    let parser = FavoritePlaceParser(json: json)
                    if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(parser.errorCode == .success, parser.message)
                    }
                }else{
                    completionBlock(false,"Oops! Something went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,error.localizedDescription)
            }
        }
    }


    func addFavoritePlace(_ place : String, category: String, coordinate: CLLocationCoordinate2D, completionBlock:@escaping (_ success:Bool,_ place: FavoritePlace?,_ message:String) -> Void){

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue(place, forKey: "place")
        params.updateValue(category, forKey: "category")
        params.updateValue("\(coordinate.latitude)", forKey: "latitude")
        params.updateValue("\(coordinate.longitude)", forKey: "longitude")

        print_debug("hitting \(api.addFavoritePlace.url()) with and headers :\(head)")
        Alamofire.request(api.addFavoritePlace.url(),method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("added place json is:\n\(json)")
                    let parser = FavoritePlaceParser(json: json)
                    if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(parser.errorCode == .success, parser.place, parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func updateFavoritePlace(_ placeId : String, category:String, isFavorite: Bool, completionBlock:@escaping (_ success:Bool, _ place: FavoritePlace?, _ message:String) -> Void){

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue(placeId, forKey: "place_id")
        params.updateValue(category, forKey: "category")
        params.updateValue(isFavorite ? "true" : "false", forKey: "is_favourite")

        print_debug("hitting \(api.updateFavoritePlace.url()) with and headers :\(head)")
        Alamofire.request(api.updateFavoritePlace.url(), method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("updated place json is:\n\(json)")
                    let parser = FavoritePlaceParser(json: json)
                    if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(parser.errorCode == .success, parser.place, parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    func getLastTwoRecentPlaces(completionBlock:@escaping (_ success:Bool,_ recentPlaces: Array<FavoritePlace>?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.lastTwoRecentPlaces.url()) with and headers :\(head)")
        Alamofire.request(api.lastTwoRecentPlaces.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Last two places json is:\n\(json)")
                    let parser = FavoritePlaceParser(json: json)
                    if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        completionBlock((parser.errorCode == .success), parser.lastTwoPlaces, parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
}
