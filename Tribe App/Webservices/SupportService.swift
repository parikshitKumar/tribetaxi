//
//  SupportService.swift
//  GPDock
//
//  Created by TecOrb on 07/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SupportService {
    static let sharedInstance = SupportService()
    fileprivate init() {}


    func getAllQueriesForUser( userID:String,completionBlock:@escaping ( _ success:Bool, _ supportTickets:Array<SupportQuery>?,_  message:String) -> Void){
        //var params = Dictionary<String,String>()
        // params.updateValue(userID, forKey: "user_id")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allSupportsTickets.url()) and headers :\(head)")
        Alamofire.request(api.allSupportsTickets.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("tickets json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if supportParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((supportParser.errorCode == .success),supportParser.supports,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went".localizedString)
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    
    func addCommentForSupportQuery( userID:String, supportID:String, message: String, completionBlock:@escaping ( _ success:Bool, _ comment:SupportMessage?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(supportID, forKey: "support_id")
        params.updateValue(message, forKey: "message")
        
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addCommentToSupport.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.addCommentToSupport.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("comment json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if supportParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((supportParser.errorCode == .success),supportParser.comment,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,response.result.error?.localizedDescription ?? "Oops! Something wrong went".localizedString)
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    //    "support_id":"10",
    //    "page":"1",
    //    "per_page":"10",
    //    "sort":"DESC"
    
    func getDetailsForSupportWith(supportId:String,page:String,perPage:String,sort:String,completionBlock:@escaping ( _ success:Bool, _ supportTicket: SupportQuery?,_ message:String) -> Void){
        
        var params = Dictionary<String,String>()
        
        params.updateValue(supportId, forKey: "support_id")
        params.updateValue(page, forKey: "page")
        params.updateValue(perPage, forKey: "per_page")
        params.updateValue(sort, forKey: "sort")
        
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.ticketView.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.ticketView.url(),method: .post ,parameters: params, headers:head).responseJSON  { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ticket details json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if supportParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((supportParser.errorCode == .success),supportParser.support,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went".localizedString)
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    
    
    func getFAQsFromServer(completionBlock:@escaping (_ success:Bool, _ faqModels:Array<FAQModel>?, _ message:String) -> Void) {
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        Alamofire.request(api.FAQ.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("faqs json: \(json)")
                    let parser = FAQParser(json: json)
                    if parser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else if parser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((parser.errorCode == .success),parser.faqs,parser.message)
                    }
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Something went wrong".localizedString)
                }
            case .failure(let error):
                let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                print_debug(message)
                completionBlock(false,nil,error.localizedDescription)
            }
        }
        
    }
    
    
    func sendEmailToSupport(_ userID:String,message:String,image: UIImage?,completionBlock:@escaping (_ success:Bool,_ support:SupportQuery? ,_ message:String)-> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["message":message]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = image{
                    if let data = pimage.jpegData(compressionQuality: 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.addSupportQuery.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("write to us json is:\n\(json)")
                                let parser = SupportTicketParser(json: json)
                                if parser.errorCode == .forceUpdate{
                                    
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                    
                                }else if parser.errorCode == .sessionExpire{
                                    
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                    
                                }else{
                                    completionBlock((parser.errorCode == .success),parser.support,parser.message)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong".localizedString)
                            }
                        case .failure(let error):
                            let message:String = AppSettings.shared.getFullError(errorString: error.localizedDescription, andresponse: response.data)
                            print_debug(message)
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )
        
    }

}
