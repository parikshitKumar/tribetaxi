//
//  IntroScreenViewController.swift
//  Rolling Rim
//
//  Created by Parikshit on 08/04/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit


class IntroScreenViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var aCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
//    @IBOutlet weak var copyActionButton : UIButton!
//    @IBOutlet weak var referCodeLabel: UILabel!
//
//    @IBOutlet weak var showDescOneLabel: UILabel!
//    @IBOutlet weak var showDescTwoLabel: UILabel!
//    @IBOutlet weak var tapToCopyCode: UILabel!
//    @IBOutlet weak var inviteButton: UIButton!
    
    
    
    
    
    let tempImage = ["tutorial_one","tutorial_two","tutorial_three"]
    var currentPage: Int = 0
    var titleArray = ["LOREM IPSUM ONE","LOREM IPSUM TWO","LOREM IPSUM THREE","LOREM IPSUM FOUR"]
    var descArray = ["Why Ride with Rolling Rim?\r\nRide with your favorite Driver and favorite Taxi Company\r\nCity Verified Taxi Service at your fingertips","Track your Ride and know where your Loved Ones are,have\r\nPeace of mind on every ride","Let's get rolling with Rolling Rim"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        aCollectionView.register(UINib(nibName: "IntroScreenCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "IntroScreenCollectionViewCell")
        aCollectionView.dataSource = self
        aCollectionView.delegate = self
        self.aCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.pageControl.numberOfPages = 3
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickBookMemangaExpress(_ sender: UIButton){
//        let signInVC = AppStoryboard.Main.viewController(SignInViewController.self)
//        self.navigationController?.pushViewController(signInVC, animated: true)
        AppSettings.shared.proceedToLoginModule()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroScreenCollectionViewCell", for: indexPath) as!  IntroScreenCollectionViewCell
        cell.backImage.image = UIImage(named:"\(tempImage[indexPath.item])")
//        cell.titleLabel.text = self.titleArray[indexPath.item]
        cell.descLabel.text = self.descArray[indexPath.item]

        //self.setupViewForItem(cell)
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSize = collectionView.bounds.size
        //        cellSize.width -= collectionView.contentInset.left
        //        cellSize.width -= collectionView.contentInset.right
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let aCell = cell as? IntroScreenCollectionViewCell else {
            return
        }
        self.pageControl.currentPage =  self.currentPage
        aCell.layoutIfNeeded()
        aCell.setNeedsLayout()
    }
    
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == aCollectionView{
            let pageWidth = scrollView.frame.width
            self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
            self.pageControl.currentPage = self.currentPage
        }
        
    }


    
}

