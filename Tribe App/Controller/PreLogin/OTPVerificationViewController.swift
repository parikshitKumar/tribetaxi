//
//  OTPVerificationViewController.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase

enum appAction {
    case forgotPassword,login,signUp
}

class OTPVerificationViewController: UIViewController {
    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var resendbutton: UIButton!
    @IBOutlet weak var userMobileNo: UILabel!

    
   // @IBOutlet weak var didNotReceivedCodeLabel: UILabel!

    var verificationID : String!
    var otpString :String = ""
    var fullName:String?
    var contact:String!
    var countryCode:String!
    var email:String!
    var password:String?
    var userAccessToken: String?
    var appAction:appAction!
    var titleView : NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "ENTER OTP".localizedString
        self.submitButton.setTitle("Submit".localizedString, for: .normal)
        self.resendbutton.setTitle("Resend OTP".localizedString, for: .normal)
        //self.loginWithPasswordButton.setTitle("Login with Password".localizedString, for: .normal)
        self.userMobileNo.text = countryCode + contact
        self.setupOTPView()
        self.resendbutton.isEnabled = true
        self.resendbutton.alpha = 1.0
        self.toggleResend()
        self.perform(#selector(toggleResend), with: nil, afterDelay: 60)
    }

    func setupOTPView(){
        self.otpView.otpFieldFont = fonts.OpenSans.semiBold.font(.xXXLarge)
        otpView.otpFieldSize = self.otpView.frame.size.width * 1/8
        otpView.otpFieldSeparatorSpace = 10
        otpView.otpFieldsCount = 6
        otpView.otpFieldDefaultBorderColor = .black
        otpView.otpFieldEnteredBorderColor = .black
        otpView.otpFieldBorderWidth = 2
        otpView.shouldAllowIntermediateEditing = false
        otpView.otpFieldDisplayType = .underlinedBottom
//        otpView.otpFieldDefaultBackgroundColor = appColor.blackOne
//        otpView.otpFieldEnteredBackgroundColor = appColor.blackOne
        otpView.otpFieldTextColor = appColor.red
        otpView.cursorColor = appColor.red
        otpView.delegate = self
        otpView.initalizeUI()
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 25)
    }

    @objc func toggleResend(){
        self.resendbutton.isEnabled = !self.resendbutton.isEnabled
       // self.didNotReceivedCodeLabel.alpha = (self.didNotReceivedCodeLabel.alpha == 1.0) ? 0.2 : 1.0
        self.resendbutton.alpha = (self.resendbutton.alpha == 1.0) ? 0.2 : 1.0
    }


    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: fonts.OpenSans.semiBold.font(.xXLarge),NSAttributedString.Key.foregroundColor:UIColor.black]

    }
    
    
    
    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        self.navigationController?.pop(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickResend(_ sender: UIButton){
        self.otpView.initalizeUI()
        self.sendOTPOn(countryCode: self.countryCode, phoneNumber: self.contact)
    }

    

    
    @IBAction func onClickSubmitButtton(_ sender: UIButton) {
        NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code".localizedString)
    }

    func sendOTPOn(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Resending..")
        self.otpString = ""
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
            return
        }
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }

            self.toggleResend()
            self.perform(#selector(self.toggleResend), with: nil, afterDelay: 60)
            guard let verificationToken = verificationID else{
                return
            }
            self.verificationID = verificationToken
        }
    }




    func verifyOTP(otp:String,authID:String){
        self.view.endEditing(true)
        if self.appAction == .signUp{
            guard let name = self.fullName else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your full name".localizedString)
                return
            }
            guard let myPassword = self.password else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your password".localizedString)
                return
            }
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)

           Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.initalizeUI()
                    })
                    return
                }else{
                    //create user's account
                    AppSettings.shared.updateLoader(withStatus: "Creating..".localizedString)
                    self.signUpUserWith(name: name, contact: self.contact, countryCode: self.countryCode, email: self.email, password: myPassword)
                }
            }
        }else if appAction == .forgotPassword{

            guard let accessToken = self.userAccessToken else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:"Sorry!, we are having problem in order to recognize you. Please try later".localizedString, completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)
            Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.initalizeUI()
                    })
                    return
                }else{
                    AppSettings.shared.hideLoader()
                   self.openResetPasswordScreen(contact: self.contact, countryCode: self.countryCode, accessToken: accessToken)
                }
            }
        }
        self.otpView.initalizeUI()
    }

    func openResetPasswordScreen(contact: String, countryCode: String,accessToken:String){
        let resetPswdVC = AppStoryboard.Main.viewController(ResetPasswordViewController.self)
        resetPswdVC.countryCode = countryCode
        resetPswdVC.phoneNumber = contact
        resetPswdVC.accessToken = accessToken
        self.navigationController?.pushViewController(resetPswdVC, animated: true)
    }

    


    
   //for signup////

    func signUpUserWith(name : String, contact : String, countryCode : String, email: String, password : String){
        LoginService.sharedInstance.registerUserWith(name, mobile: countryCode+contact, email: email, password: password, countryCode: countryCode) { (success, resUser, message) in
            if success{
                if let user = resUser{
                    AppSettings.shared.hideLoader()
                    LoginService.sharedInstance.updateDeviceTokeOnServer()
                    AppSettings.shared.proceedToHome()
                    FireBaseUser.registerUser(withName: user.fullName, email: user.contact+"_user@tribeapp.com", password: "tribeapp_\(user.contact)", completion: { (status) in
                        if status{
                            AppSettings.shared.hideLoader()
                            LoginService.sharedInstance.updateDeviceTokeOnServer()
                            AppSettings.shared.proceedToHome()
                        }else{
                            AppSettings.shared.hideLoader()
                            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong!!".localizedString)
                        }
                    })
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }

    }
}


extension OTPVerificationViewController: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        if hasEntered{
            self.verifyOTP(otp: self.otpString, authID: self.verificationID)
        }
        return hasEntered
    }

    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }

    func enteredOTP(otpString: String) {
        self.otpString = otpString
    }
}


////////////



