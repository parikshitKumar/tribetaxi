//
//  SuccessfulResetPassword.swift
//  Pepel Congo
//
//  Created by Parikshit on 19/02/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class SuccessfulResetPassword: UIViewController {
//    @IBOutlet weak var passwordLabel: UILabel!
//    @IBOutlet weak var successfulLabel: UILabel!
//    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var logInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.passwordLabel.text = "PASSWORD RESET".localizedString
//        self.successfulLabel.text = "SUCCESSFUL".localizedString
//        self.descLabel.text = "you have successful reset your password. please use your new password when logging in".localizedString
        self.logInButton.setTitle("Log In".localizedString, for: .normal)
        CommonClass.makeViewCircularWithCornerRadius(self.logInButton, borderColor: .clear, borderWidth: 0, cornerRadius: 25)


        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        self.navigationController?.popToRoot(true)

    }
    @IBAction func onClickLogInBiutton(_ sender: UIButton) {
        self.navigationController?.popToRoot(true)
        
    }

}
