//
//  PreLoginViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 25/06/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import EAIntroView

class PreLoginViewController: UIViewController {
    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var bookCartingKidzsButton: UIButton!


    var selectedIndex: UInt = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //NotificationCenter.default.addObserver(self, selector: #selector(changuageChangeUpdate(_:)), name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil)
       // self.setupNavigationViews()
        self.navigationController?.makeTransparent()
        self.showIntro()
        CommonClass.makeViewCircularWithCornerRadius(self.bookCartingKidzsButton, borderColor: .black, borderWidth: 1, cornerRadius: 4)
        self.bookCartingKidzsButton.setTitle("Book Memanga Express", for: .normal)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }


    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBookCartingKidzs(_ sender: UIButton){
        let signInVC = AppStoryboard.Main.viewController(SignInViewController.self)
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @objc func changuageChangeUpdate(_ notification:Notification) {
        self.showIntro()
        self.bookCartingKidzsButton.setTitle("Book Memanga Express", for: .normal)

    }

}




extension PreLoginViewController : EAIntroDelegate{
    func showIntro() {
        //AppSettings.shared.isIntroShown = true
        let sampleDescription1 = "Text Demo1"
        let sampleDescription2 = "Text Demo2"
        let sampleDescription3 = "Text Demo3"
        let sampleDescription4 = "Text Demo4"
        
        let page1 = EAIntroPage()
        page1.title = "Memanga Express"
        page1.titleColor = UIColor.white
        page1.titleFont = fonts.OpenSans.semiBold.font(.xXLarge)
        page1.descFont = fonts.OpenSans.regular.font(.large)
        page1.desc = sampleDescription1;
        page1.descColor = UIColor.white
        page1.titleIconView = UIImageView(image: #imageLiteral(resourceName: "logo"))
         page1.showTitleView = true

        page1.bgImage = #imageLiteral(resourceName: "tutorial_one")

        let page2 = EAIntroPage()
        page2.title = "Memanga Express"
        page2.titleColor = UIColor.white
        page2.titleFont = fonts.OpenSans.semiBold.font(.xXLarge)
        page2.descFont = fonts.OpenSans.regular.font(.large)
        page2.desc = sampleDescription2;
        page2.descColor = UIColor.white
        page2.titleIconView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        page2.showTitleView = true

        page2.bgImage = #imageLiteral(resourceName: "tutorial_two")

        let page3 = EAIntroPage()
        page3.title = "Memanga Express"
        page3.titleColor = UIColor.white
        page3.titleFont = fonts.OpenSans.semiBold.font(.xXLarge)
        page3.descFont = fonts.OpenSans.regular.font(.large)
        page3.desc = sampleDescription3;
        page3.descColor = UIColor.white
        page3.titleIconView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        page3.showTitleView = true
        page3.bgImage = #imageLiteral(resourceName: "tutorial_three")
        
        
        let page4 = EAIntroPage()
        page4.title = "Memanga Express"
        page4.titleColor = UIColor.white
        page4.titleFont = fonts.OpenSans.semiBold.font(.xXLarge)
        page4.descFont = fonts.OpenSans.regular.font(.large)
        page4.desc = sampleDescription4;
        page4.descColor = UIColor.white
        page4.titleIconView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        page4.showTitleView = true
        page4.bgImage = #imageLiteral(resourceName: "tutorial_four")

        let intro = EAIntroView(frame: self.introView.bounds, andPages: [page1,page2,page3,page4])
        intro?.pageControl.currentPageIndicatorTintColor = appColor.white
        intro?.pageControl.pageIndicatorTintColor = appColor.blueColor
        intro?.tapToNext = false
        intro?.swipeToExit = false
        intro?.skipButton.setTitle("", for: .normal)
        intro?.delegate = self
        intro?.show(in: self.introView, animateDuration: 0.3, withInitialPageIndex: self.selectedIndex)
    }

    func intro(_ introView: EAIntroView!, pageEndScrolling page: EAIntroPage!, with pageIndex: UInt) {
//        let title = (pageIndex == 2) ? "Get Started" : "Skip"
//        introView.skipButton.setTitle(title, for: .normal)
        self.selectedIndex = pageIndex
    }

    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
//        AppSettings.shared.isIntroShown = true
    }

}

