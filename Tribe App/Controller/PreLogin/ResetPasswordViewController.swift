//
//  ResetPasswordViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 03/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    @IBOutlet weak var passwordTextField : FloatLabelTextField!
    @IBOutlet weak var reenterPasswordTextField : FloatLabelTextField!

    @IBOutlet weak var submitButton: UIButton!


    var phoneNumber:String!
    var countryCode:String!
    var accessToken:String!
    var titleView : NavigationTitleView!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.passwordTextField.placeholder = "New Password".localizedString
        self.reenterPasswordTextField.placeholder = "Re-enter Password".localizedString

        self.submitButton.setTitle("Submit".localizedString, for: .normal)
        self.navigationItem.title = "RESET PASSWORD".localizedString
//        self.addViewPassword(textField: self.passwordTextField)
    }
    

    override func viewWillLayoutSubviews() {
        self.decorateViews()
        //self.setupLeftBarButtons()


    }
    
    
    func addViewPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    @IBAction func showPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.passwordTextField.isSecureTextEntry = !sender.isSelected
    }
    


    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: .clear, borderWidth: 0, cornerRadius: 25)
//        CommonClass.sharedInstance.setPlaceHolder(self.passwordTextField, placeHolderString: "New Password".localizedString, withColor: .white)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmitButton(_ sender: UIButton){
        let password = passwordTextField.text!
        let rePassword = reenterPasswordTextField.text!
        if password.isEmpty {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter password".localizedString, completionBlock: {})
            return
        }
        
        if rePassword.isEmpty {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter re-enter password".localizedString, completionBlock: {})
            return
        }


        let validation = self.validateParams(password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        
        if password != rePassword {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Password and re-enter password should be same".localizedString, completionBlock: {})
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        AppSettings.shared.showLoader(withStatus: "Resetting..")
        let contact = (self.countryCode+self.phoneNumber).removingWhitespaces()
        self.resetPassword(contact: contact, accessToken: self.accessToken, password: password)
    }

    func resetPassword(contact:String,accessToken:String,password:String){

        LoginService.sharedInstance.resetPassword(contact: contact, accessToken: accessToken, password: password) { (success, done, message) in
            AppSettings.shared.hideLoader()
            if success{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message, completionBlock: {
                    let otpVC = AppStoryboard.Main.viewController(SuccessfulResetPassword.self)
                    self.navigationController?.pushViewController(otpVC, animated: true)
                })
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func validateParams(password:String) -> (success:Bool,message:String){
        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,warningMessage.validPassword.messageString())
        }
        return (true,"")
    }

}
