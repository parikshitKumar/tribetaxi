//
//  LogInViewController.swift
//  JO CAB
//
//  Created by Parikshit on 25/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
import CountryPickerView
import Firebase

class SignUpViewController: UIViewController {
    @IBOutlet weak var loginTableView: UITableView!

    var languagePicker :UIPickerView!
    var languageArray = ["English","Portuguese"]
    var languageCode = ["en","pt-PT"]
    var countryFlag = [#imageLiteral(resourceName: "support_blue"),#imageLiteral(resourceName: "remove")]
    var toolBar:UIToolbar!
    
    var selectedCountry: Country!
    var titleView : NavigationTitleView!
    var changeLanguageView: LanguageChangeView!
    var selectLanguageView: SelectLanguageView!
    var fullName = "";var email="";var password="";var phoneNumber="";var countryCode="";
    var selectButton: Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectButton = true

        self.navigationItem.title = "SIGN UP".localizedString
        self.loginTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.loginTableView.dataSource = self
        self.loginTableView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        self.loginTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: fonts.OpenSans.semiBold.font(.xXLarge),NSAttributedString.Key.foregroundColor:UIColor.white]

    }
    
    
    
    
    
    func setRightBarButtons() {
        self.selectLanguageView = SelectLanguageView.instanceFromNib()
        self.selectLanguageView.frame = CGRect(x:self.view.frame.size.width - 140 ,y: 0, width:140, height: 44)
//        self.prevSelectLanguage()
        self.setupLanguagePicker(textField: self.selectLanguageView.selectLanguageTextField)
        
        let selectLanguageBarButton = UIBarButtonItem(customView: selectLanguageView)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([selectLanguageBarButton], animated: false)
        
    }
    
    
//    func prevSelectLanguage() {
//        if AppSettings.shared.selectedLanguageName == SelectLanguage.Portuguese.rawValue {
//            AppSettings.shared.selectedLanguageCode = "pt-PT"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "portuguese_flag")
//            self.selectLanguageView.selectLabel.text = "Portugues"
//        }else{
//            AppSettings.shared.selectedLanguageCode = "en"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "english_flag")
//            self.selectLanguageView.selectLabel.text = "English"
//        }
//    }
    
    
    
    
    func reloadView() {
        titleView.titleLabel.text = "SIGN UP".localizedString
        self.fullName = ""
        self.email = ""
        self.password = ""
        self.phoneNumber = ""
        self.countryCode = ""
        self.loginTableView.reloadData()

    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onclickBackButton(_ sender : UIButton){
       // NotificationCenter.default.post(name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil, userInfo: nil)
        self.navigationController?.pop(true)
        
    }
    

    @IBAction func onToggleCheckBoxButton(_ sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
            self.selectButton = false
        }else{
            sender.isSelected = true
            self.selectButton = true

        }
        
        //sender.isSelected = !sender.isSelected
        
    }
    
    
//    @IBAction func onClickTermsandCondition(_ sender: UIButton) {
//        let termsOfUses = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
//        termsOfUses.isPrivacyPolicy = false
//        self.navigationController?.pushViewController(termsOfUses, animated: true)
//    }
//
    @IBAction func onClickLoginInButton(_ sender: UIButton){
        NotificationCenter.default.post(name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil, userInfo: nil)
        self.navigationController?.pop(true)
    }
    

    
    @IBAction func onClickSignUpButton(_ sender: UIButton){
        self.view.endEditing(true)
        


        let validation = self.validateParams(fullName: fullName, countryCode: countryCode, phoneNumber: phoneNumber, email: email, password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        
        if !selectButton{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "You must agree to the terms and conditions")
            return
        }
        //self.sendOTPOn(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, email: email, password: password)
        AppSettings.shared.showLoader(withStatus: "Please wait..")

        self.contactVerifyBeforeSignUP(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, email: email, password: password)

    }
    
    
    
    func contactVerifyBeforeSignUP(countryCode:String,phoneNumber:String,fullName:String,email:String,password:String){
        LoginService.sharedInstance.contactVerifyBeforeSinUp(contact: (countryCode+phoneNumber).removingWhitespaces()) { (success, isExists, resToken, message) in
            if success{
                if isExists{
                    AppSettings.shared.hideLoader()
                    self.sendOTPOn(countryCode: countryCode, phoneNumber: phoneNumber, fullName: fullName, email: email, password: password)
                    
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "We didn't recognize the number you've entered".localizedString)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                
            }
        }
    }
    
    
    func validateParams(fullName:String,countryCode:String,phoneNumber: String, email: String, password:String) -> (success:Bool,message:String){
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your user name".localizedString)
        }
        
        if !CommonClass.validateEmail(email){
            return (false,"Please enter a valid email".localizedString)
        }
        
        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code".localizedString)
        }
        
        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your mobile number".localizedString)
        }
        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid mobile number".localizedString)
        }
        
        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Mobile number should not be less than".localizedString + " \(self.selectedCountry.minDigit)" + " in length".localizedString)
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Mobile number should not be more than".localizedString +  " \(self.selectedCountry.maxDigit)" + " in length".localizedString)
        }
        
        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,warningMessage.validPassword.messageString())
        }
        
        return (true,"")
    }
    
    
    func sendOTPOn(countryCode:String,phoneNumber:String, fullName:String, email: String, password:String){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
        
        AppSettings.shared.showLoader(withStatus: "Sending OTP..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {})
                return
            }
            //proceed to otp varification
            guard let verificationToken = verificationID else{
                return
            }
           
            self.openOTPScreen(verificationID: verificationToken, fullName: fullName, contact: phoneNumber, countryCode: countryCode, email: email, password: password)
        }
    }
    
    func openOTPScreen(verificationID: String, fullName: String, contact: String, countryCode: String, email: String, password: String, appAction: appAction = .signUp){
        let otpVC = AppStoryboard.Main.viewController(OTPVerificationViewController.self)
        otpVC.appAction = appAction
        otpVC.verificationID = verificationID
        otpVC.contact = contact
        otpVC.countryCode = countryCode
        otpVC.email = email
        otpVC.fullName = fullName
        otpVC.password = password
        self.navigationController?.pushViewController(otpVC, animated: true)
        
    }

}

extension SignUpViewController: UITableViewDataSource, UITableViewDelegate,UserContactCellDelegate {
    func contact(contactCell cell: UserInfoTableViewCell, didSelectCountry selectedCountry: Country) {
        self.selectedCountry = selectedCountry
        self.countryCode = selectedCountry.phoneCode
        self.loginTableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 100
        switch indexPath.row {
        case 0:
           
            height =  ((self.view.frame.height) * 500/1138) - 64
        case 1:
            height =  200
        case 2:
            height =  200
        default:
            break
        }
        return height
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoTableViewCell", for: indexPath) as! UserInfoTableViewCell
            
            
            cell.fullNameTextField.placeholder = "User name".localizedString
            cell.emailTextField.placeholder = "Email".localizedString
            cell.phoneNumberTextField.placeholder = "Mobile number".localizedString
            cell.passwordTextField.placeholder = "Password".localizedString
            
            cell.showNameLabel.text = "User name".localizedString
            cell.showEmailLabel.text = "Email".localizedString
            cell.showMobileLabel.text = "Mobile".localizedString
            cell.showPasswordLabel.text = "Password".localizedString

            cell.viewController = self

            cell.fullNameTextField.keyboardType = .default
            cell.fullNameTextField.isSecureTextEntry = false
            cell.fullNameTextField.text = fullName
            cell.fullNameTextField.autocapitalizationType = .words
        
            cell.emailTextField.keyboardType = .emailAddress
            cell.emailTextField.isSecureTextEntry = false
            cell.emailTextField.text = email
            cell.emailTextField.autocapitalizationType = .none
            
            cell.phoneNumberTextField.keyboardType = .numberPad
            cell.phoneNumberTextField.isSecureTextEntry = false
            cell.phoneNumberTextField.text = phoneNumber
            cell.phoneNumberTextField.autocapitalizationType = .none
            
            cell.passwordTextField.keyboardType = .default
            cell.passwordTextField.isSecureTextEntry = true
            cell.passwordTextField.text = password
            cell.passwordTextField.autocapitalizationType = .none

            cell.fullNameTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
            cell.fullNameTextField.delegate = self
            cell.emailTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
            cell.emailTextField.delegate = self
            cell.phoneNumberTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
            cell.phoneNumberTextField.delegate = self
            cell.passwordTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
            cell.passwordTextField.delegate = self
            
            cell.delegate = self
            cell.countryPickerSetUp(with: self.selectedCountry)

            
//            cell.countryCodeButton.addTarget(self, action: #selector(onClickCountryCodeButton(_:)), for: .touchUpInside)
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoSubmitTableViewCell", for: indexPath) as! InfoSubmitTableViewCell
            cell.signUpButton.setTitle("Sign Up".localizedString, for: .normal)
            cell.logInLabel.text = "LOG IN".localizedString

            cell.loginButton.addTarget(self, action: #selector(onClickLoginInButton(_:)), for: .touchUpInside)
            cell.signUpButton.addTarget(self, action: #selector(onClickSignUpButton(_:)), for: .touchUpInside)
//            cell.termsAndConditionButton.addTarget(self, action: #selector(onClickTermsandCondition(_:)), for: .touchUpInside)

            return cell
        default:
            break

        }
        return UITableViewCell()
        
    }
    
    
}

extension SignUpViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.loginTableView) as IndexPath?{
            if let cell = loginTableView.cellForRow(at: indexPath) as? UserInfoTableViewCell{
                    if  textField == cell.fullNameTextField{
                        fullName = textField.text!
                        
                    }else if textField == cell.emailTextField {
                        email = textField.text!

                    }else if textField == cell.phoneNumberTextField {
                        phoneNumber = textField.text!

                    }else if textField == cell.passwordTextField {
                        password = textField.text!

                     }
                        
                        
                }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.loginTableView) as IndexPath?{
            if let cell = loginTableView.cellForRow(at: indexPath) as? UserInfoTableViewCell{
                if  textField == cell.fullNameTextField{
                    fullName = textField.text!
                    
                }else if textField == cell.emailTextField {
                    email = textField.text!
                    
                }else if textField == cell.phoneNumberTextField {
                    phoneNumber = textField.text!
                    
                }else if textField == cell.passwordTextField {
                    password = textField.text!
                    
                }
                
                
            }
        }
    }
    
    @objc  func textDidChanged(_ textField: UITextField) -> Void {
        if let indexPath = textField.tableViewIndexPath(self.loginTableView) as IndexPath?{
            if let cell = loginTableView.cellForRow(at: indexPath) as? UserInfoTableViewCell{
                if  textField == cell.fullNameTextField{
                    fullName = textField.text!
                    
                }else if textField == cell.emailTextField {
                    email = textField.text!
                    
                }else if textField == cell.phoneNumberTextField {
                    phoneNumber = textField.text!
                    
                }else if textField == cell.passwordTextField {
                    password = textField.text!
                    
                }
                
                
            }
        }
    }
}


extension SignUpViewController: UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languageArray.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60.0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 60))
        
        let myImageView = UIImageView(frame: CGRect(x: 50, y: 10, width: 40, height: 40))
        
        var rowString = String()
        switch row {
        case 0:
            rowString = "English"
            myImageView.image = UIImage(named:"english_flag")
        case 1:
            rowString = "Portuguese"
            myImageView.image = UIImage(named:"portuguese_flag")
        case 2: break
        default:
            rowString = "Error: too many rows"
            myImageView.image = nil
        }
        let myLabel = UILabel(frame: CGRect(x: 120, y: 0, width: pickerView.bounds.width - 90, height: 60))
        myLabel.font = UIFont.init(name: "OpenSans-SemiBold", size: 17.0)!
        myLabel.text = rowString
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        
        return myView
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.languageArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
    }
    
    func setupLanguagePicker(textField:UITextField){
        self.languagePicker = UIPickerView()
        self.languagePicker.backgroundColor = UIColor.lightText
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickDone(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickCancel(_:)))
        cancelButton.tintColor = UIColor.white
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [cancelButton, spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.barStyle = UIBarStyle.default
        toolbar.barTintColor = appColor.red
        toolbar.tintColor = UIColor.white
        textField.inputView = languagePicker
        textField.inputAccessoryView = toolbar;
        languagePicker.dataSource = self
        languagePicker.delegate = self
    }
    @IBAction func onClickDone(_ sender: UIBarButtonItem){
        let row = self.languagePicker.selectedRow(inComponent: 0)
        let unitSelected = languageArray[row]
//        self.resetLanguageView(language: unitSelected)
        self.selectLanguageView.selectLanguageTextField.resignFirstResponder()
        
    }
    
    
//    func resetLanguageView(language: String) {
//        if language == SelectLanguage.Portuguese.rawValue {
//            AppSettings.shared.selectedLanguageCode = "pt-PT"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "portuguese_flag")
//            self.selectLanguageView.selectLabel.text = "Portugues"
//        }else{
//            AppSettings.shared.selectedLanguageCode = "en"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "english_flag")
//            self.selectLanguageView.selectLabel.text = "English"
//        }
//        self.reloadView()
//        NotificationCenter.default.post(name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil, userInfo: nil)
//
//
//    }
    
    
    
    @IBAction func onClickCancel(_ sender: UIBarButtonItem){
        self.selectLanguageView.selectLanguageTextField.resignFirstResponder()
    }
    
    
}




