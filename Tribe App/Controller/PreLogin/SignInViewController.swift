//
//  LoginViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 25/06/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView
import Firebase

class SignInViewController: UIViewController {
 
    
    @IBOutlet weak var countryCodeLabel : UILabel!
    @IBOutlet weak var phoneNumberTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var signUPButton: UIButton!
    
    @IBOutlet weak var showMobileLabel: UILabel!
    @IBOutlet weak var showPasswordLabel: UILabel!
    @IBOutlet weak var backView: UIView!

    var languagePicker :UIPickerView!
    var languageArray = ["English","Portuguese"]
    var languageCode = ["en","pt-PT"]
    var countryFlag = [#imageLiteral(resourceName: "tick"),#imageLiteral(resourceName: "support_blue")]
    var toolBar:UIToolbar!

    var cpv : CountryPickerView!
    var selectedCountry: Country!
    @objc var showPassword = false
    
    var titleView : NavigationTitleView!
    var changeLanguageView: LanguageChangeView!
    var selectLanguageView: SelectLanguageView!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        //NotificationCenter.default.addObserver(self, selector: #selector(changuageChangeUpdate(_:)), name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil)
        self.countryPickerSetUp()
        self.navigationController?.navigationBar.isHidden = false
       self.navigationItem.title = "LOG IN".localizedString
        self.reloadView()
        self.setupNavigationViews()
        self.decorateViews()
        self.addViewPassword(textField: passwordTextField )
        CommonClass.makeCircularTopRadius(self.backView, cornerRadius: 15)

    }
    
    func reloadView() {
        self.showMobileLabel.text = "Mobile".localizedString
        self.showPasswordLabel.text = "Password".localizedString
        
        self.phoneNumberTextField.text = ""
        self.passwordTextField.text = ""

        self.forgetPasswordButton.setTitle("Forgot Password?".localizedString, for: .normal)
        self.signUPButton.setTitle("Create Account".localizedString, for: .normal)
        self.LoginButton.setTitle("Log In".localizedString, for: .normal)
        self.addViewPassword(textField: self.passwordTextField)
        
//        CommonClass.sharedInstance.setPlaceHolder(self.phoneNumberTextField, placeHolderString: "Mobile number".localizedString, withColor: .white)
//        CommonClass.sharedInstance.setPlaceHolder(self.passwordTextField, placeHolderString: "Password".localizedString, withColor: .white)
    }
    
    override func viewWillLayoutSubviews() {
        self.decorateViews()
        //self.setupLeftBarButtons()
        CommonClass.makeCircularTopRadius(self.backView, cornerRadius: 15)
    }
    
    @objc func changuageChangeUpdate(_ notification:Notification) {
      self.reloadView()
//        self.prevSelectLanguage()
    }
    
   
    func addViewPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }

    @IBAction func showPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.passwordTextField.isSecureTextEntry = !sender.isSelected
    }

    
    func setRightBarButtons() {
        self.selectLanguageView = SelectLanguageView.instanceFromNib()
        self.selectLanguageView.frame = CGRect(x:self.view.frame.size.width - 140 ,y: 0, width:140, height: 44)
//        self.prevSelectLanguage()
        self.setupLanguagePicker(textField: self.selectLanguageView.selectLanguageTextField)

        let selectLanguageBarButton = UIBarButtonItem(customView: selectLanguageView)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([selectLanguageBarButton], animated: false)
        
    }
    
    
//    func prevSelectLanguage() {
//        if AppSettings.shared.selectedLanguageName == SelectLanguage.Portuguese.rawValue {
//            AppSettings.shared.selectedLanguageCode = "pt-PT"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "portuguese_flag")
//            self.selectLanguageView.selectLabel.text = "Portugues"
//        }else{
//            AppSettings.shared.selectedLanguageCode = "en"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "english_flag")
//            self.selectLanguageView.selectLabel.text = "English"
//        }
//    }

    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.LoginButton, borderColor: .clear, borderWidth: 2, cornerRadius: 25)


    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }


    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.makeTransparent()
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: fonts.OpenSans.semiBold.font(.xXLarge),NSAttributedString.Key.foregroundColor:UIColor.white]

    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickCountryCodeButton(_ sender: UIButton){
        cpv.showCountriesList(from: self)
    }

    @IBAction func onClickSignUpButton(_ sender: UIButton){
//        AppSettings.shared.proceedToHome()

        let signUpVC = AppStoryboard.Main.viewController(SignUpViewController.self)
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }

    @IBAction func onClickForgotPasswordButton(_ sender: UIButton){
        let forgotPasswordVC = AppStoryboard.Main.viewController(ForgotPasswordViewController.self)
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }
    @IBAction func onClickLoginInButton(_ sender: UIButton){

        let countryCode = self.selectedCountry.phoneCode

        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your mobile number".localizedString, completionBlock: {})
            return
        }

        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your password".localizedString, completionBlock: {})
            return
        }
        let validation = self.validateParams(countryCode: countryCode, phoneNumber: phoneNumber, password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        self.signInUserWith(contact: phoneNumber, countryCode: countryCode, password: password)

    }


    
    
    func signInUserWith(contact : String, countryCode : String, password : String){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.networkIsNotConnected.messageString(), completionBlock: {})
            return
        }
        
        LoginService.sharedInstance.loginWith(countryCode+contact, password: password) { (success, resUser, message) in
            if success{
                AppSettings.shared.hideLoader()
                LoginService.sharedInstance.updateDeviceTokeOnServer()
                AppSettings.shared.proceedToHome()
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    
    
    

    func validateParams(countryCode:String,phoneNumber: String,password:String) -> (success:Bool,message:String){
        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code".localizedString)
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your mobile number".localizedString)
        }

        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid mobile number".localizedString)
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Mobile number should not be less than".localizedString + " \(self.selectedCountry.minDigit)" + " in length".localizedString)
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Mobile number should not be more than".localizedString +  " \(self.selectedCountry.maxDigit)" + " in length".localizedString)
        }

        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,"Please enter a valid password".localizedString)
        }

        return (true,"")
    }
    

}




extension SignInViewController: CountryPickerViewDataSource,CountryPickerViewDelegate{

    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }

        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.OpenSans.regular.font(.medium)
        cpv.countryDetailsLabel.textColor = UIColor.clear
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = false
        //cpv.showCountryFlagInView = false
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv as Any, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv as Any, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv as Any, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv as Any, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        self.selectedCountry = cpv.selectedCountry
        self.countryCodeLabel.text = self.selectedCountry.phoneCode
    }
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }


    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country".localizedString
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil
//        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: nil, action: nil)
//        barButton.tintColor = appColor.blue
//        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.countryCodeLabel.text = self.selectedCountry.phoneCode

        //self.phoneNumberTextField.becomeFirstResponder()
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }


}


extension SignInViewController: UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languageArray.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60.0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 60))
        
        let myImageView = UIImageView(frame: CGRect(x: 50, y: 10, width: 40, height: 40))
        
        var rowString = String()
        switch row {
        case 0:
            rowString = "English"
            myImageView.image = UIImage(named:"english_flag")
        case 1:
            rowString = "Portuguese"
            myImageView.image = UIImage(named:"portuguese_flag")
        case 2: break
        default:
            rowString = "Error: too many rows"
            myImageView.image = nil
        }
        let myLabel = UILabel(frame: CGRect(x: 120, y: 0, width: pickerView.bounds.width - 90, height: 60))
        myLabel.font = UIFont.init(name: "OpenSans-SemiBold", size: 17.0)!
        myLabel.text = rowString
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        
        return myView
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.languageArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        
    }
    
    func setupLanguagePicker(textField:UITextField){
        self.languagePicker = UIPickerView()
        self.languagePicker.backgroundColor = UIColor.lightText
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickDone(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onClickCancel(_:)))
        cancelButton.tintColor = UIColor.white
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [cancelButton, spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.barStyle = UIBarStyle.default
        toolbar.barTintColor = appColor.red
        toolbar.tintColor = UIColor.white
        textField.inputView = languagePicker
        textField.inputAccessoryView = toolbar;
        languagePicker.dataSource = self
        languagePicker.delegate = self
    }
    @IBAction func onClickDone(_ sender: UIBarButtonItem){
        let row = self.languagePicker.selectedRow(inComponent: 0)
        let unitSelected = languageArray[row]
//         self.resetLanguageView(language: unitSelected)
        self.selectLanguageView.selectLanguageTextField.resignFirstResponder()

    }
    
    
//    func resetLanguageView(language: String) {
//        if language == SelectLanguage.Portuguese.rawValue {
//            AppSettings.shared.selectedLanguageCode = "pt-PT"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "portuguese_flag")
//            self.selectLanguageView.selectLabel.text = "Portugues"
//        }else{
//            AppSettings.shared.selectedLanguageCode = "en"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "english_flag")
//            self.selectLanguageView.selectLabel.text = "English"
//        }
//        self.reloadView()
//
//    }
    

    
    @IBAction func onClickCancel(_ sender: UIBarButtonItem){
        self.selectLanguageView.selectLanguageTextField.resignFirstResponder()
    }
    
    
}


