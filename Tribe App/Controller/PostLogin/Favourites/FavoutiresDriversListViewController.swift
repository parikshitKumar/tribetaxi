//
//  FavoutiresDriversListViewController.swift
//  Rolling Rim
//
//  Created by Parikshit on 22/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class FavoutiresDriversListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var aTableView:UITableView!
    
    
    var page = 1
    var perPage = 100
    //    var totalPage = 0
    var isLoading = false
    var drivers = [FavouriteDrivers]()
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FAVOURITES"
        self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.aTableView.addSubview(refreshControl)
        self.loadFavouriteDrivers(page, perPage: perPage)

    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        
        self.aTableView.tableFooterView = nil
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        self.isLoading = true
        
        WalletService.sharedInstance.getFavouriteDriverForUser(pageNumber: page, perPage: perPage) { (success, resDrivers, responseMessage) in
            self.isLoading = false
            self.drivers.removeAll()
            refreshControl.endRefreshing()
            if let someDriver = resDrivers{
                self.drivers.append(contentsOf: someDriver)
            }
            
            self.aTableView.reloadData()
        }
    }
    
    @IBAction func onClickMenuButton(_ sender:UIButton) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()

    }
    
    func loadFavouriteDrivers(_ page: Int,perPage:Int) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        WalletService.sharedInstance.getFavouriteDriverForUser(pageNumber: page, perPage: perPage) { (success, resDrivers, responseMessage) in
            self.isLoading = false
            if let someDrivers = resDrivers{
                if someDrivers.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.drivers = someDrivers
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            
            self.aTableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.drivers.count == 0) ? 1 : self.drivers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.drivers.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isLoading ? "Loading..".localizedString : "No favourite driver found\nPull down to refresh".localizedString
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteDriversCell", for: indexPath) as! FavouriteDriversCell
         let driver = self.drivers[indexPath.row]
        cell.userImageView.sd_setImage(with: URL(string:driver.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:driver.driver.fullName))
        cell.nameLabel.text = driver.driver.fullName.capitalized
        cell.mobileLabel.text = driver.driver.contact
        cell.emailLabel.text = driver.driver.email
        cell.favButton.addTarget(self, action: #selector(onClickRemoveFavouriteDriver(_:)), for: .touchUpInside)
        return cell
        }
    }
    
    
    @IBAction func onClickRemoveFavouriteDriver(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.aTableView) as IndexPath?{
            let driver = drivers[indexPath.row]
            self.askToRemoveDriver(driver: driver, atIndexPath: indexPath)
        }
    }
    
    func askToRemoveDriver(driver : FavouriteDrivers,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.messageString(), message: "Would you really want to remove favourite driver".localizedString, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete".localizedString, style: .destructive){[weak driver](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeFavDriver(driver, indexPath: indexPath)
        }
        
        let cancelAction = UIAlertAction(title: "Nope".localizedString, style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    func removeFavDriver(_ myDriver:FavouriteDrivers?,indexPath: IndexPath) -> Void {
        guard let driver = myDriver else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        WalletService.sharedInstance.addFavouriteDriverForUser(driver.rideID, driverId: driver.driver.ID, isFavorite: false) { (success, resFav, responseMassage) in

            AppSettings.shared.hideLoader()
            if success{
                self.drivers.remove(at: indexPath.row)
                self.aTableView.reloadData()

//                self.aTableView.deleteRows(at: [indexPath], with: .automatic)

            }
//            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "")
        }
        
    }
    
}

