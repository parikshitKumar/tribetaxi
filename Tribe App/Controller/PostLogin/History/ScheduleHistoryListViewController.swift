//
//  ScheduleHistoryListViewController.swift
//  HLS Taxi
//
//  Created by Parikshit on 15/07/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ScheduleHistoryListViewController: UIViewController {
    @IBOutlet weak var historyTableView: UITableView!
    var user : User!
    var ridesArray = [Ride]()
    var pageNumber = 1
    var totalRecords = 0
    var recordsPerPage = 15
    var isNewDataLoading = false
    var canLoadMore = true
    let historyType = "schedule"
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "MY RIDES".localizedString
        
        NotificationCenter.default.addObserver(self, selector: #selector(rideDidUpdate(_:)), name: .RIDE_UPDATED_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rideDidCancelledByDriver(_:)), name: .RIDE_CANCELLED_BY_DRIVER_NOTIFICATION, object: nil)
        
        
        self.user = User.loadSavedUser()
        self.historyTableView.addSubview(refreshControl)
        self.historyTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
        
        historyTableView.dataSource = self
        historyTableView.delegate = self
        self.loadRideHistoryFor(self.pageNumber, perPage: self.recordsPerPage, type: self.historyType)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
        self.historyTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    
    
    
    
    func loadRideHistoryFor(_ pageNumber:Int, perPage:Int,type:String) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        self.isNewDataLoading = true
        if pageNumber == 1{
            ridesArray.removeAll()
            historyTableView.reloadData()
        }
        if pageNumber > 1{
            self.historyTableView.addFooterSpinner()
        }
        RideService.sharedInstance.getRideHistoryFromServer(self.user.ID, pageNumber: pageNumber, perPage: perPage, historyType: type) { (success, resRides,resTotalRecords, message)  in
            self.isNewDataLoading = false
            self.totalRecords = resTotalRecords
            self.historyTableView.removeFooterSpinner()
            if success{
                if let newRideArray = resRides{
                    if newRideArray.count == 0{
                        if self.pageNumber > 1{
                            self.pageNumber = self.pageNumber - 1
                        }
                    }
                    self.ridesArray.append(contentsOf: newRideArray)
                }else{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
            self.historyTableView.reloadData()
        }
    }
    
    func loadNextBatch(indexPath:IndexPath) {
        if self.ridesArray.count >= self.totalRecords {
            self.historyTableView.tableFooterView = nil
            return
        }
        if ridesArray.count >= self.recordsPerPage && canLoadMore {
            if !isNewDataLoading && ((ridesArray.count - indexPath.row) <= self.recordsPerPage/2){
                if AppSettings.isConnectedToNetwork{
                    isNewDataLoading = true
                    self.pageNumber+=1
                    self.loadRideHistoryFor(self.pageNumber, perPage: self.recordsPerPage, type: self.historyType)
                }
            }
        }
    }
    
    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = true
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }
    
    func showBill(ride:Ride){
        let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
        billingVC.ride = ride
        billingVC.fromHistory = true
        self.navigationController?.pushViewController(billingVC, animated: true)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !AppSettings.isConnectedToNetwork{
            refreshControl.endRefreshing()
            return
        }
        self.pageNumber = 1
        self.isNewDataLoading = true
        self.ridesArray.removeAll()
        refreshControl.endRefreshing()
        self.historyTableView.reloadData()
        RideService.sharedInstance.getRideHistoryFromServer(self.user.ID, pageNumber: pageNumber, perPage: self.recordsPerPage, historyType: self.historyType) { (success, resRides,resTotalRecords, message)  in
            self.isNewDataLoading = false
            self.totalRecords = resTotalRecords
            self.ridesArray.removeAll()
            refreshControl.endRefreshing()
            if success{
                if let someRides = resRides{
                    self.ridesArray.append(contentsOf: someRides)
                }
                self.historyTableView.reloadData()
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                self.historyTableView.reloadData()
            }
        }
        
    }
    
}

extension ScheduleHistoryListViewController:RideScheduledViewControllerDelegate{
    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //    func showRideScheduled() -> Void {
    //        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
    //        rideScheduledVC.delegate = self
    //        self.present(rideScheduledVC, animated: false, completion: {
    //        })
    //    }
    
    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}




extension ScheduleHistoryListViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.ridesArray.count == 0) ? 1 : self.ridesArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.ridesArray.count == 0) ? 140 : 175
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // this will turn on `masksToBounds` just before showing the cell
        cell.layoutIfNeeded()
             if let cCell = cell as? InCompletedRideCell{
            CommonClass.makeViewCircularWithCornerRadius(cCell.backView, borderColor: .lightGray, borderWidth: 0, cornerRadius: 15)
            cCell.backView.dropShadow(shadowRadius:10,shadowOffset: CGSize(width: 0, height: 10))
        }
    
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.ridesArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isNewDataLoading ? "Loading..".localizedString : "No schedule ride found\nPull down to refresh".localizedString
            return cell
        }else{
            var cell : UITableViewCell!

            let ride = ridesArray[indexPath.row]
            cell = self.scheduledRideCell(tableView, rowForIndexPath: indexPath, withRide: ride)

            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            DispatchQueue.main.async {
                if self.ridesArray.count < self.totalRecords{
                    self.loadNextBatch(indexPath: indexPath)
                }
            }
            return cell
        }
        
    }
    
    
    
    
    
    
    func scheduledRideCell(_ tableView:UITableView, rowForIndexPath indexPath :IndexPath, withRide ride: Ride)-> InCompletedRideCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "InCompletedRideCell", for: indexPath) as! InCompletedRideCell
        let tempDate = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        let  isEmptyDate =  CommonClass.sharedInstance.splitDateInLanguage(date: tempDate)
        if isEmptyDate != "" {
            let dateAndTime = CommonClass.sharedInstance.splitBetweenDateAndTimeInLanguage(date: tempDate)
            cell.dateTimeLabel.text = dateAndTime.0
            cell.timeLabel.text = dateAndTime.1
            
        }else{
            cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        }

        cell.carCategoryLabel.text = ride.carCategory.categoryName
        cell.crnLabel.text = "CRN\(ride.ID)"
        if ride.driver.ID != ""{
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            cell.driverIcon.image = UIImage(named: "profile")
        }
        cell.pickUpAddressLabel.text = ride.startLocation
        cell.dropAddressLabel.text = ride.endLocation
        CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.blueColor, borderWidth: 1)
        cell.cancelButton.addTarget(self, action: #selector(onClickRemoveScheduleRide(_:)), for: .touchUpInside)

        cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        cell.showFromLabel.text = "From".localizedString
        cell.showToLabel.text = "To".localizedString
        
        let currentStatus = ride.getCurrentStatus()
        if currentStatus == .cancelled {
            cell.cancelButton.isHidden = true
            cell.rideStatusLabel.text = "CANCELLED".localizedString

        }else{
            cell.cancelButton.isHidden = false
            cell.rideStatusLabel.text = "SCHEDULED".localizedString


        }


        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    
    
    

    
    
    
    
    @objc func rideDidUpdate(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let ride = userInfo["ride"] as? Ride{
                for index in 0..<self.ridesArray.count{
                    if ride.ID == self.ridesArray[index].ID{
                        self.ridesArray[index] = ride
                        break
                    }
                }
                self.historyTableView.reloadData()
            }
        }
    }
    
    @objc func rideDidCancelledByDriver(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let ride = userInfo["ride"] as? Ride{
                for index in 0..<self.ridesArray.count{
                    if ride.ID == self.ridesArray[index].ID{
                        self.ridesArray[index] = ride
                        break
                    }
                }
                //                for index in 0..<self.ridesArray.count{
                //                    if ride.ID == self.ridesArray[index].ID{
                //                        self.ridesArray.remove(at: index)
                //                        break
                //                    }
                //                }
                self.historyTableView.reloadData()
            }
        }
    }
    
    
    
    @IBAction func onClickRemoveScheduleRide(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.historyTableView) as IndexPath?{
            let resRide = ridesArray[indexPath.row]
            self.askToRemoveRide(ride: resRide, atIndexPath: indexPath)
        }
    }
    
    func askToRemoveRide(ride : Ride,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.messageString(), message: "Would you really want to remove this schedule ride".localizedString, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete".localizedString, style: .destructive){[weak ride](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeScheduleRide(ride, indexPath: indexPath)
        }
        
        let cancelAction = UIAlertAction(title: "Nope".localizedString, style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    
    
    func removeScheduleRide(_ myRide:Ride?,indexPath: IndexPath) -> Void {
        guard let ride = myRide else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        
        RideService.sharedInstance.removeScheduleRide(ride.ID){ (success, resRide, responseMassage) in
            
            AppSettings.shared.hideLoader()
            if success{
                //self.ridesArray.remove(at: indexPath.row)
                if let tempRide = resRide{
                    self.rideCancelByUser(ride: tempRide)
                }
                
                self.historyTableView.reloadData()
                //NotificationCenter.default.post(name: .REMOVE_SCHEDULE_RIDE_NOTIFICATION, object: nil, userInfo: nil)

                //                self.aTableView.deleteRows(at: [indexPath], with: .automatic)
                
            }
            //            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "")
        }
        
    }
    
     func rideCancelByUser(ride:Ride) {
        for index in 0..<self.ridesArray.count{
            if ride.ID == self.ridesArray[index].ID{
                self.ridesArray[index] = ride
                break
            }
        }
        self.historyTableView.reloadData()
        
    }
    
    
}







