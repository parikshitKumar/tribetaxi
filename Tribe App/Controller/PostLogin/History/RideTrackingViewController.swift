//
//  RideTrackingViewController.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 23/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import SnapKit
import ActionCableClient
import Firebase

//import SwiftyGif

class RideTrackingViewController: UIViewController,CLLocationManagerDelegate {
    var ride: Ride!
    var fromHistory:Bool = false
    var proceededToBill : Bool = false
    var driverMarker:DriverMarker!
    var timeMarker:TimeMarker!
    var myTimer: Timer!

    var oldLocationCoordinate2D : CLLocationCoordinate2D!
    //for edit address
    var isLocationCalled : Bool = false
    var currentCoordinate2D:CLLocationCoordinate2D!
    var locationManager = CLLocationManager()
    @IBOutlet weak var editDropAddressButton: UIButton!
    var dropMarker:GMSMarker!



    var polyline = GMSPolyline()
    var path = GMSMutablePath()


    @IBOutlet weak var dottedImageView: UIImageView!
    @IBOutlet weak var fromAddressLabel: UILabel!
    @IBOutlet weak var toAddressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!

    //outlets for driver view
    @IBOutlet weak var callDriverButton: UIButton!
    @IBOutlet weak var cancelRideButton: UIButton!
//    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var ratingView: NKFloatRatingView!

    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var driverRatingLabel: UILabel!
    
    
    @IBOutlet weak var showFromLabel: UILabel!
    @IBOutlet weak var showToLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var rideView: UIView!



    
    static var ChannelIdentifier = "RideChannel"
    
//    let client = ActionCableClient(url: URL(string:"ws:" + actionCableBaseUrl + "cable")!)
    let client = ActionCableClient(url: URL(string: actionCableBaseUrl + "cable")!)

    var channel: Channel?
    var trackChannel : Channel?
    static var trackChannelIdentifier = "TribeTaxiChannel"
    var markerParser =  DriverParser()
    var OldUserMarker:GMSMarker!
    
    
    var chatRemoveRef: DatabaseReference!
    var chatRemoveHandle: DatabaseHandle?
    
    var isFromAppDelegate: Bool = false



    override func viewDidLoad() {
        super.viewDidLoad()
//        self.chatButton.isHidden = true
//        NotificationCenter.default.addObserver(self, selector: #selector(rideDidUpdateDropChange(_:)), name: .DROP_CHANGE_OLD_USER_NOTIFICATION, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachedRideFromNotification(_:)), name: .DRIVER_REACHED_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startRideFromNotification(_:)), name: .RIDE_START_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EndedRideFromNotification(_:)), name: .RIDE_END_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelDriverFromNotification(_:)), name: .RIDE_CANCELLED_BY_DRIVER_NOTIFICATION, object: nil)

        self.setupClient()

        self.navigationController?.navigationBar.isHidden = false
        self.showFromLabel.text = "From".localizedString
        self.showToLabel.text = "To".localizedString
        self.cancelRideButton.setTitle("CANCEL THE RIDE".localizedString, for: .normal)
        self.callDriverButton.setTitle("CALL".localizedString, for: .normal)
        self.locationManagerSetup()


        self.oldLocationCoordinate2D = CLLocationCoordinate2D(latitude: ride.driver.location.latitude, longitude: ride.driver.location.longitude)
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        self.mapView.isTrafficEnabled = false
        self.setRideDetails(ride: self.ride)
        self.setTitleForTracking(ride: ride)
        self.decorateView()
        self.addDropOffMarker(ride: self.ride)

        self.resetBoundOfMap(ride: self.ride)
        self.addDriverMarker(on: oldLocationCoordinate2D,and: "\(ride.driver.ID)")
        self.drawRouteFirstTimeBeforeStart(ride: self.ride)
        self.updateActionCableMarker()

        self.mapView.setMinZoom(mapZoomLevel.min, maxZoom: mapZoomLevel.max)

        if self.ride.isStarted || self.ride.isDriverReached{
            self.addPickUpMarker(ride: self.ride)
        }else{
            self.handleTimeUpdates()
        }
        if self.ride.isStarted{
//            self.chatButton.isHidden = true
        }
        self.setEditDropAddressButton()
        
        self.changeDateAftergetRideDetail()

    }
    
    
    func changeDateAftergetRideDetail() {
        self.getCurrentRideDetailStatus{(res) in
            if res{
                self.setRideDetails(ride: self.ride)
                self.addDropOffMarker(ride: self.ride)
                self.resetBoundOfMap(ride: self.ride)

            }
        }
                
    }
    
    
    
    @objc func rideDidUpdateDropChange(_ notification:Notification) {
        self.getCurrentRideDetailStatus{(res) in
            if res{
                //self.reloadScreenDetail()
                self.setRideDetails(ride: self.ride)
                self.addDropOffMarker(ride: self.ride)
                self.resetBoundOfMap(ride: self.ride)
                self.updateActionCableMarker()

            }
            
        }
        
    }
    
    func setEditDropAddressButton() {
        self.editDropAddressButton.isHidden = !self.ride.isStarted
    }
    //MARK:- CoreLocation handling
    func locationManagerSetup() {
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self;
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            self.locationManager.startUpdatingLocation()
        }else if status == .denied || status == .restricted{
            self.showGotoLocationSettingAlert()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationCalled{
            isLocationCalled = true
            guard let currentCD = locations.last?.coordinate else{return}
            self.currentCoordinate2D = currentCD
        }
    }
    
    
    func showGotoLocationSettingAlert(){
        let alert = UIAlertController(title: "Opps".localizedString, message: "Location access seems disabled\r\nGo to settings to enabled".localizedString, preferredStyle: UIAlertController.Style.alert)
        let okayAction = UIAlertAction(title: "Okay".localizedString, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        let settingsAction = UIAlertAction(title: "Settings".localizedString, style: .default) { (action) in
            let url = UIApplication.openSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }
        
        alert.addAction(okayAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getCurrentRideDetailStatus(completion:@escaping (_ done: Bool)->Void){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        if !isFromAppDelegate {
            AppSettings.shared.showLoader(withStatus: "Loading..")
            }
        RideService.sharedInstance.getRideDetails(self.ride.ID) { (success,resRide,message)  in
            if success{
                AppSettings.shared.hideLoader()
                if let ride = resRide{
                    self.ride = ride
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":ride])
                    if self.ride.isDriverReject{
                        self.trackNewCancelledByDriver(isRideRejectAfterAccept: true)
                    }
                    completion(true)
                    
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                    completion(false)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                completion(false)
                
            }
        }
    }
    

    func drawRouteFirstTimeBeforeStart(ride:Ride){
        let toCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        let fromCoordinate = CLLocationCoordinate2D(latitude: ride.driver.location.latitude, longitude: ride.driver.location.longitude)
        self.drawPath(startLocation: fromCoordinate, endLocation: toCoordinate)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.ride.isStarted{
//            self.chatButton.isHidden = true
        }
    }

    deinit {
        self.deleteTimer(timer: self.myTimer)
        NotificationCenter.default.removeObserver(self)
    }

    func deleteTimer(timer:Timer?){

    }
    func showBill(ride:Ride){
        AppSettings.shared.hideLoader()
        if (UIApplication.shared.keyWindow?.rootViewController?.topViewController is BillViewController ) {
            return
        }else{
            if self.navigationController == nil {
                let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
                billingVC.ride = ride
                billingVC.fromHistory = false
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: billingVC, withSlideOutAnimation: false, andCompletion: {
                    
                })
            }else{
                let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
                billingVC.ride = ride
                billingVC.fromHistory = false
                self.navigationController?.pushViewController(billingVC, animated: true)
            }
        }
    }
    
    @objc func getRideBookingDetailsAndProceedToBill(){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        AppSettings.shared.showLoader(withStatus: "Getting Bill..")
        RideService.sharedInstance.getRideDetails(self.ride.ID) { (success,resRide,message)  in
            if success{
                if let ride = resRide{
                    self.ride = ride
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":ride])
                    self.showBill(ride: ride)
                    self.mapView.clear()
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
        }
    }
    @objc func calculateBill(){
        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":ride])
        self.showBill(ride: ride)
    }
    func setRideDetails(ride:Ride){
        self.fromAddressLabel.text = self.ride.startLocation
        self.toAddressLabel.text = self.ride.endLocation

        //driver details
        self.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        self.driverNameLabel.text = ride.driver.fullName.capitalized
        self.carNameLabel.text = ride.car.name.capitalized
        self.carNumberLabel.text = ride.car.number.uppercased()
        self.otpLabel.text = "OTP :" + "\(ride.OTP)"
        self.driverRatingLabel.text = String(format: "%0.1f",ride.driver.rating)
        self.ratingView.rating = Float(ride.driver.rating)
    }

    func setTitleForTracking(ride:Ride){
        if !ride.isDriverReached && !ride.isStarted{
            self.title = "Pick up is Arriving".localizedString
        }
        if ride.isDriverReached{
            self.title = "Pick up Arrived".localizedString
        }
        if ride.isStarted{
            self.title = "Enjoy Your Ride".localizedString
        }
        self.setEditDropAddressButton()
//        self.editDropAddressButton.isHidden = true

    }

    func decorateView(){
        CommonClass.makeViewCircularWithCornerRadius(otpLabel, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(driverIcon, borderColor: appColor.blueColor, borderWidth: 1, cornerRadius: driverIcon.frame.size.width/6)
//        CommonClass.makeViewCircularWithCornerRadius(callDriverButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
//        CommonClass.makeViewCircularWithCornerRadius(cancelRideButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
//        CommonClass.makeViewCircularWithCornerRadius(moreButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)

    }
    
    @IBAction func onClickCurrentLocation(_ sender: UIButton){
        guard let localDriverMarker = self.driverMarker else{return}
        self.mapView.animate(toLocation: localDriverMarker.position)
    }
    


    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.deleteTimer(timer: self.myTimer)
        if fromHistory{
            self.navigationController?.pop(true)
        }else{
            self.navigationController?.popToRoot(true)
        }
        mapView.clear()
    }

    override func viewDidLayoutSubviews() {
        //self.dottedImageView.image = UIImage.drawDottedImage(width: 8, height: dottedImageView.frame.size.height, color: appColor.redColor)
        CommonClass.makeViewCircularWithRespectToHeight(self.cancelRideButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeCircularTopRadius(self.rideView, cornerRadius: 15)
        CommonClass.makeCircularBottomRadius(self.headerView, cornerRadius: 15)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationViews()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        if !self.ride.isDriverReached{
            self.myTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(handleTimeUpdatesWithTimer), userInfo: nil, repeats: true)
        }
        
        guard let vcs = self.navigationController?.viewControllers else{return}
        let filteredVC = vcs.filter { (vc) -> Bool in
            return (vc is HomeViewController)
        }
        
        if let homeVC = filteredVC.first as? HomeViewController{
//            homeVC.removeFirebaseHandlers()
        }
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
//        self.channel?.onUnsubscribed = {
//            print("Channel has been unsubscribed!")
//        }
//        self.channel?.unsubscribe()
//
//        self.trackChannel?.onUnsubscribed = {
//            print("Channel has been unsubscribed!")
//        }
//        self.trackChannel?.unsubscribe()hˆ
        
        self.removePath()
        self.deleteTimer(timer: self.myTimer)
        guard let timer = self.myTimer else {
            return
        }
        timer.invalidate()
        self.myTimer = nil
       //mapView.clear()

    }
    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickCallDriver(_ sender: UIButton){
        self.dialPhone(phoneNumber: "+"+self.ride.driver.contact)
    }

    func dialPhone(phoneNumber:String){
        let dial = "telprompt://" + "\(phoneNumber)"
        self.showPhoneApp(dial: dial)
    }

    func showPhoneApp(dial: String)  {
        let url = URL(string:dial)!
        if (UIApplication.shared.canOpenURL(url)){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Cann't able to call right now\r\nPlease try later!".localizedString)
        }
    }
    
    
    @IBAction func onClickSmsDrivers(_ sender:UIButton) {
//        let smsDrivers = AppStoryboard.Wallet.viewController(ChatViewController.self)
//        smsDrivers.ride = self.ride
//        self.navigationController?.pushViewController(smsDrivers, animated: true)
        
    }
    

    @IBAction func onClickCancelRide(_ sender: UIButton){
        
        
        
//        let vc = ChatViewController()
//        vc.ride = self.ride
//        let nav = AppSettings.shared.getNavigation(vc: vc)
//        nav.navigationBar.isHidden = false
//        nav.modalPresentationStyle = .overFullScreen
//        self.present(nav, animated: true, completion: nil)
        
        
        
        if self.ride.isStarted{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Sorry! You cann't cancel ride once started. If you wish to end the ride, please ask driver to end the ride.".localizedString)
            return
        }
        self.openSelectCancelReason()
    }

    func openSelectCancelReason() {
        let reasonVC = AppStoryboard.History.viewController(SelectCancelReasonViewController.self)
        reasonVC.delegate = self
        reasonVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        let nav = UINavigationController(rootViewController: reasonVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickMore(_ sender: UIButton){
        NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.messageString())
    }

    func cancelRequest(rideID:String,reason:String,completionBlock: @escaping (_ finished : Bool, _ message:String)->Void){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        RideService.sharedInstance.cancelRide(rideID, reason: reason) { (success, resRide, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.removePath()
                // self.deleteTimer(timer: self.myTimer)
                self.ride.isUserCancelled = true
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
            }
            completionBlock(success,message)
        }
    }
}
//MARK:- makers and mapview handling
extension RideTrackingViewController: SelectCancelReasonViewControllerDelegate{
    func cancelRide(_ viewController: SelectCancelReasonViewController, didCancelledRideWithReason reason: String) {
        viewController.dismiss(animated: false, completion: nil)
        self.cancelRequest(rideID: self.ride.ID, reason: reason) { (cancelled,message) in
            self.removeActionCableClient()
            if self.ride.ID != "" {
                self.removeDriverChatFromFirebase(rideID: self.ride.ID)
            }
            // self.deleteTimer(timer: self.myTimer)
            if self.fromHistory{
                self.navigationController?.pop(true)
            }else{
                self.navigationController?.popToRoot(true)
            }
            self.mapView.clear()
        }
    }
    
    func addPickUpMarker(ride:Ride) -> Void {
        let coordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.addMarkerforCoordinate(coordinate: coordinate, image: #imageLiteral(resourceName: "uber_pin_green"), address: ride.startLocation)
        self.removeTimeMarker()
    }
    
    func addDropOffMarker(ride:Ride) -> Void {
        let position = CLLocationCoordinate2D(latitude: ride.endLatitude, longitude: ride.endLongitude)
        if self.dropMarker != nil{
            self.dropMarker.map = nil
        }
        self.dropMarker = GMSMarker(position: position)
        self.dropMarker.icon = #imageLiteral(resourceName: "pin")
        if isDebugEnabled{
            self.dropMarker.title = ride.endLocation
        }
        self.dropMarker.isTappable = true
        self.dropMarker.title = ride.endLocation
        self.dropMarker.map = self.mapView;
    }

    func addMarkerforCoordinate(coordinate : CLLocationCoordinate2D?,image: UIImage,address: String) {
        if let position = coordinate{
            let driverMarker = GMSMarker(position: position)
            driverMarker.icon = image
            driverMarker.isTappable = true
            driverMarker.title = address
            driverMarker.map = self.mapView;
        }
    }
    
    



    func resetBoundOfMap(ride:Ride) -> Void {
        let path = GMSMutablePath()
        let pickCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        if self.oldLocationCoordinate2D != nil{
            path.add(self.oldLocationCoordinate2D)
        }
        path.add(pickCoordinate)

        if self.ride.isDriverReached{
            let dropCoordinate = CLLocationCoordinate2D(latitude: ride.endLatitude, longitude: ride.endLongitude)
            path.add(dropCoordinate)
        }

        let bounds = GMSCoordinateBounds(path: path)
        let camera = self.mapView.camera(for: bounds, insets:UIEdgeInsets(top: 40, left: 15, bottom: 15, right: 15))
        self.mapView.camera = camera!;
        self.mapView.setMinZoom(mapZoomLevel.min, maxZoom: mapZoomLevel.max)
    }
}

//MARK:- Notification Handling
extension RideTrackingViewController{
    
    func handleTimeUpdates(){
        //        if self.ride.isDriverReached || self.ride.isStarted{
        //            self.deleteTimer(timer: self.myTimer)
        //            return
        //        }
        let fromCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.calculateDriverArrivingTime(from: self.oldLocationCoordinate2D, to: fromCoordinate)
    }
    
    @objc func handleTimeUpdatesWithTimer(){
        if self.ride.isDriverReached || self.ride.isStarted{
            guard let timer = self.myTimer else {
                return
            }
            timer.invalidate()
            self.myTimer = nil
            return
        }
        let fromCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        self.calculateDriverArrivingTime(from: self.oldLocationCoordinate2D, to: fromCoordinate)
    }
    
    
    func calculateDriverArrivingTime(from driverLocation:CLLocationCoordinate2D,to pickUpCoordinate: CLLocationCoordinate2D){
        self.getTravelTimeAndDistance(from: pickUpCoordinate, to: driverLocation) { (duration) in
            self.addTimeMarker(time: duration, position: pickUpCoordinate)
        }
    }
    
    func addTimeMarker(time:String,position:CLLocationCoordinate2D){
        if timeMarker != nil{
            timeMarker.map = nil
        }
        self.timeMarker = TimeMarker(time: time)
        self.timeMarker.position = position
        if isDebugEnabled{
            self.timeMarker.title = "Pick location".localizedString
        }
        self.timeMarker.zIndex = 10
        self.timeMarker.map = self.mapView
    }
    
    func removeTimeMarker(){
        if timeMarker != nil{
            timeMarker.map = nil
            timeMarker = nil
        }
    }
    
    func getTravelTimeAndDistance(from driverLocation:CLLocationCoordinate2D,to pickUpCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: String)->Void)->Void{
        
        let origin = "\(driverLocation.latitude),\(driverLocation.longitude)"
        let destination = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let rows = json["rows"].arrayValue
                if rows.count > 0{
                    let arrValues = rows.first!["elements"].arrayValue.first!
                    var duration = "..."
                    if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                        if let durationString = durationDict["text"] as? String{
                            duration = durationString
                        }
                    }
                    completion(duration)
                }
            }catch{}
        }
    }
    

    

    
    func addDriverMarker(on position:CLLocationCoordinate2D, and driverId: String){
        let aDriver = ActionCableDriver()
        aDriver.driverID = driverId
        aDriver.latitude = position.latitude
        aDriver.longitude = position.longitude
        aDriver.latitude = position.latitude
        aDriver.longitude = position.longitude
        if self.driverMarker != nil{
            self.driverMarker.map = nil
        }
        self.oldLocationCoordinate2D = position
        self.driverMarker = DriverMarker(driver: aDriver)
        self.driverMarker.position = position
        self.driverMarker.appearAnimation = .pop
        self.driverMarker.map = self.mapView
        self.driverMarker.zIndex = 10
        self.driverMarker.rotation = self.getInitailBearing()
    }
    
    func getInitailBearing() -> Double{
        let strat = CLLocationCoordinate2D(latitude: self.ride.driver.location.previousLatitude, longitude: self.ride.driver.location.previousLongitude)
        let end = CLLocationCoordinate2D(latitude: self.ride.driver.location.latitude, longitude: self.ride.driver.location.longitude)
        let brng = self.bearingFromCoordinate(from: strat, to: end)
        return brng
    }
    

    
    func removeDriverMarker(key:String){
        guard let marker = driverMarker else{return}
        if key == marker.driver.driverID{
            self.driverMarker.map = nil
        }
    }
    
    func updateDriverMarker(key:String,newPosition:CLLocationCoordinate2D){
        guard let marker = self.driverMarker else{
            self.addDriverMarker(on: newPosition, and: key)
            return
        }
        
        if key == marker.driver.driverID{
            guard let oldLocation = self.oldLocationCoordinate2D else{
                return
            }
            let oldLocationOfMarker = CLLocation(latitude: oldLocation.latitude, longitude: oldLocation.latitude)
            let newLocationOfMarker = CLLocation(latitude: newPosition.latitude, longitude: newPosition.latitude)
            let distance = oldLocationOfMarker.distance(from: newLocationOfMarker)
            //            if distance >= 200.0{
            //                print_debug("distance Tracking: \(distance)")
            //                let newBearing = self.bearingFromCoordinate(from: oldLocation, to: newPosition)
            //                self.resetMarker(markerID: key, to: newPosition, bearing: newBearing)
            //            }else{
            self.updateDriverMarkerLocation(marker: marker, from: oldLocation, to: newPosition)
            //}
        }
    }
    
    func updateDriverMarkerLocation(marker:DriverMarker, from oldLocation: CLLocationCoordinate2D,to newLocation: CLLocationCoordinate2D) -> Void {
        self.moveMarker(marker, to: newLocation)
    }
    
    func resetMarker(markerID: String, to destination: CLLocationCoordinate2D, bearing:Double){
        let aDriver = ActionCableDriver()
        aDriver.driverID = markerID
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        removeDriverMarker(key: markerID)
        
        let driverMarker = DriverMarker(driver: aDriver)
        driverMarker.position = destination
        driverMarker.appearAnimation = .pop
        driverMarker.rotation = bearing
        if self.mapView != nil{
            driverMarker.map = self.mapView
        }
        self.driverMarker = driverMarker
    }
    
    func moveMarker(_ marker : GMSMarker, to destination:CLLocationCoordinate2D){
        guard let start = self.oldLocationCoordinate2D else{return}
        let startRotation = marker.rotation
        let animator = ValueAnimator.animate("some", from: 0, to: 1, duration: 1.5) { (prop, value) in
            let v = value.value
            let newPosition = self.getInterpolation(fraction: v, startPoint: start, endPoint: destination)
            marker.position = newPosition
            let newBearing = self.bearingFromCoordinate(from: start, to: newPosition)
            let rotation = self.getRotation(fraction: v, start: startRotation, end: newBearing)
            marker.rotation = rotation
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isFlat = true
            self.oldLocationCoordinate2D = newPosition
        }
        //self.oldLocationCoordinate2D = newPosition
        animator.resume()
        self.makeRideCenter(position: marker.position)
        
        
    }
    
    
    private func angleFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let deltaLongitude = seccond.longitude - first.longitude
        let deltaLatitude = seccond.latitude - first.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude/deltaLongitude)
        if deltaLongitude > 0 {return angle}
        else if deltaLongitude < 0 {return angle + .pi}
        else if deltaLatitude == 0 {return .pi}
        return 0.0
    }
    
    
    func bearingFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let pi = Double.pi
        let lat1 = first.latitude*pi/180.0
        let long1 = first.longitude*pi/180.0
        let lat2 = seccond.latitude*pi/180.0
        let long2 = seccond.longitude*pi/180.0
        
        let diffLong = long2 - long1
        let y = sin(diffLong)*cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(diffLong)
        var bearing = atan2(y, x)
        bearing = bearing.toDegree(fromRadian: bearing)
        bearing = (bearing+360).truncatingRemainder(dividingBy: 360)
        return bearing
    }
    
    
    private func getRotation(fraction:Double, start: Double, end: Double) -> Double{
        let normailizedEnd = end - start
        let normailizedEndAbs = ((normailizedEnd+360)).truncatingRemainder(dividingBy: 360)
        let direction = (normailizedEndAbs > 180) ? -1 : 1 //-1 for anticlockwise and 1 for closewise
        let rotation = (direction > 0) ? normailizedEndAbs : (normailizedEndAbs-360)
        let result = fraction*rotation+start
        let finalResult = (result+360).truncatingRemainder(dividingBy: 360)
        return finalResult
    }
    
    private func getInterpolation(fraction:Double, startPoint:CLLocationCoordinate2D, endPoint:CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        let latitude = (endPoint.latitude - startPoint.latitude) * fraction + startPoint.latitude
        var longDelta = endPoint.longitude - startPoint.longitude
        if abs(longDelta) > 180{
            longDelta -= longDelta*360
        }
        let longitude = (longDelta*fraction) + startPoint.longitude
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    func makeRideCenter(position:CLLocationCoordinate2D){
        if !self.mapView.projection.contains(position){
            self.mapView.animate(toLocation: position)
        }
    }
    
    
}




extension RideTrackingViewController{

    func updateTravelledPath(currentLoc: CLLocationCoordinate2D){
        if self.ride.isDriverReached{self.removePath();return}
        var index = 0
        for i in 0..<self.path.count(){
            let pathLat = Double(self.path.coordinate(at: i).latitude).rounded(toPlaces: 3)
            let pathLong = Double(self.path.coordinate(at: i).longitude).rounded(toPlaces: 3)
            let currentLat = Double(currentLoc.latitude).rounded(toPlaces: 3)
            let currentLong = Double(currentLoc.longitude).rounded(toPlaces: 3)
            if currentLat == pathLat && currentLong == pathLong{
                index = Int(i)
                break   //Breaking the loop when the index found
            }
        }

        //Creating new path from the current location to the destination
        let newPath = GMSMutablePath()
        for i in index..<Int(self.path.count()){
            newPath.add(self.path.coordinate(at: UInt(i)))
        }

        if index == 0{
            let toCoordinate = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
            self.drawPath(startLocation: currentLoc, endLocation: toCoordinate)
            return
        }
        self.path = newPath
        self.polyline.map = nil
        self.polyline = GMSPolyline(path: self.path)
        self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        self.polyline.strokeWidth = 3.0
        self.polyline.map = self.mapView
    }

    func removePath(){
        self.polyline.map = nil
        self.path = GMSMutablePath()
    }

    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D){
        let origin = "\(startLocation.latitude.rounded(toPlaces: 10)),\(startLocation.longitude.rounded(toPlaces: 10))"
        let destination = "\(endLocation.latitude.rounded(toPlaces: 10)),\( endLocation.longitude.rounded(toPlaces: 10))"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GOOGLE_API_KEY)"

        Alamofire.request(url).responseJSON { response in
            let json = JSON(response.data!)
            let routes = json["routes"].arrayValue
            for route in routes{
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue

                self.path = GMSMutablePath.init(fromEncodedPath: points!)!
                self.polyline.path = self.path
                self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
                self.polyline.strokeWidth = 3.0
                self.polyline.map = self.mapView
            }
        }
    }
}





extension RideTrackingViewController {
    
    func setupClient() -> Void {
        
        self.client.willConnect = {
            print_debug("Will Connect")
        }
        
        self.client.onConnected = {
            print_debug("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
            self.client.connect()
            self.channel?.subscribe()
//            self.trackChannel?.subscribe()
            
            
            
        }
        
        self.client.willReconnect = {
            print_debug("Reconnecting to \(self.client.url)")
            return true
        }
        
        let room_identifier = ["room_id": "Ride_\(self.ride.ID)","ride_id":self.ride.ID,"role":"user"] as [String : Any]
//         self.channel = client.create(RideTrackingViewController.ChannelIdentifier, identifier: room_identifier)
        
        self.channel = client.create(RideTrackingViewController.ChannelIdentifier, identifier: room_identifier, autoSubscribe: true, bufferActions: true)
        
        self.channel?.onSubscribed = {
            print_debug("Subscribed to \(RideTrackingViewController.ChannelIdentifier)")
        }
        
        self.channel?.onReceive = {(data: Any?, error: Error?) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            let JSONObject = JSON(data!)
            print_debug("status Json\(JSONObject)")
            
            let   statusKeyParser = StatusKeyParser(json: JSONObject)
            if (statusKeyParser.isAdminCancelled) {
              self.trackCancelByAdmin()
//                self.chatButton.isHidden = true
                if self.ride.ID != "" {
                self.removeDriverChatFromFirebase(rideID: self.ride.ID)
                }
            }else if (statusKeyParser.isDriverCancelled){
                self.trackNewCancelledByDriver(isRideRejectAfterAccept: false)
                
            }else if (statusKeyParser.isReached && !statusKeyParser.isStarted && !statusKeyParser.isEnded) {
                self.trackNewDriverReached()
            }else if (statusKeyParser.isReached && statusKeyParser.isStarted && !statusKeyParser.isEnded) {
                self.trackNewRideStarted()
                if self.ride.ID != "" {
                    self.removeDriverChatFromFirebase(rideID: self.ride.ID)
                }

            }else if (statusKeyParser.isReached && statusKeyParser.isStarted && statusKeyParser.isEnded) {
                self.trackNewRideEnded()

            }
            
        }

        
        let track_room_identifier = ["room_id": "Track_\(self.ride.driver.ID)","driver_id": "\(self.ride.driver.ID)","role":"driver"] as [String : Any]

        
//            self.trackChannel = self.client.create(RideTrackingViewController.trackChannelIdentifier, identifier: track_room_identifier)
        self.trackChannel = self.client.create(RideTrackingViewController.trackChannelIdentifier, identifier: track_room_identifier, autoSubscribe: true, bufferActions: true)

        
        self.trackChannel?.onSubscribed = {
            print_debug("Subscribed to \(RideTrackingViewController.trackChannelIdentifier)")
        }
        
        self.trackChannel?.onReceive = {(data: Any?, error: Error?) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
//            if (self.ride.isStarted == true) && self.ride.isDriverReached && self.ride.rideStatus{
//                print_debug("Track not read")
//
//            }else{
            let JSONObject = JSON(data!)
            print_debug("Track Json\(JSONObject)")
            
            if (JSONObject.dictionaryObject as Dictionary<String,AnyObject>?) != nil{
                let driverParser = DriverParser(json: JSONObject)
                self.markerParser = driverParser
               self.updateActionCableMarker()

                
           // }
            
            }
                //else{
//                let driver = self.markerParser.trackDriver
//
//                        self.removeDriverMarker(key: driver.driverID)
//                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong!".localizedString, completionBlock: {
//
//                            self.client.disconnect()
//                        self.client.onDisconnected = {(error: ConnectionError?) in
//                        print("Disconected with error: \(String(describing: error))")
//                    }
//
//                    guard let timer = self.myTimer else {
//                        return
//                    }
//                    timer.invalidate()
//                    self.myTimer = nil
//                    if self.fromHistory{
//                        self.navigationController?.pop(true)
//                        }else{
//                    self.navigationController?.popToRoot(true)
//                }
//            })
//
//        }
        }
        
        self.client.connect()
    }
    
    
    func trackNewDriverReached(){
        if  self.ride.isDriverReached == true {
            return
        }

        self.removePath()
        self.deleteTimer(timer: self.myTimer)
        self.addPickUpMarker(ride: self.ride)
        if !self.ride.isDriverReached{
            //AppDelegate.getAppDelegate().playSound()
        }
        self.ride.isDriverReached = true
        self.removePath()
        self.setTitleForTracking(ride: self.ride)
        self.resetBoundOfMap(ride: self.ride)
        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
        
        guard let timer = self.myTimer else {
            return
        }
        timer.invalidate()
        self.myTimer = nil
        
    }
    
    
    func trackCancelByAdmin(){
        
        if (UIApplication.shared.keyWindow?.rootViewController?.topViewController is SelectCancelReasonViewController ) {
            self.dismiss(animated: true, completion: nil)
            
        }
        self.removePath()
        self.title = "RIDE CANCELLED".localizedString
        self.ride.isDriverReject = true
        self.ride.isUserCancelled = true
        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
        NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your ride is cancelled by Admin\nYou can book another ride".localizedString, completionBlock: {
            
            self.removeActionCableClient()
            
            if self.fromHistory{
                self.navigationController?.pop(true)
            }else{
                self.navigationController?.popToRoot(true)
            }
            guard let timer = self.myTimer else {
                return
            }
            timer.invalidate()
            self.myTimer = nil
            self.mapView.clear()
        })
        
        
    }
    
    
    func trackNewCancelledByDriver(isRideRejectAfterAccept:Bool){
        if !isRideRejectAfterAccept {
        if (self.ride.isDriverReject == true) || (self.ride.isUserCancelled == true) {
            return
        }
      
        }
        if (UIApplication.shared.keyWindow?.rootViewController?.topViewController is SelectCancelReasonViewController ) {
            self.dismiss(animated: true, completion: nil)
            
        }

        if self.ride.ID != "" {
            self.removeDriverChatFromFirebase(rideID: self.ride.ID)
        }
        self.removePath()
        self.title = "RIDE CANCELLED".localizedString
        self.ride.isDriverReject = true
        self.ride.isUserCancelled = true
        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
        NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your ride is cancelled by driver\nYou can book another ride".localizedString, completionBlock: {
 
            self.removeActionCableClient()
//            self.dismiss(animated: true, completion: nil)
            if self.fromHistory{
                self.navigationController?.pop(true)
            }else{
                self.navigationController?.popToRoot(true)
            }
            guard let timer = self.myTimer else {
                return
            }
            timer.invalidate()
            self.myTimer = nil
            self.mapView.clear()
        })
        
        
    }

    func trackNewRideStarted(){
        
        if (self.ride.isStarted == true) && self.ride.isDriverReached {
            return
        }
        self.removePath()
        
        if timeMarker != nil{
            timeMarker.map = nil
            timeMarker = nil
            self.addPickUpMarker(ride: self.ride)
        }
        self.ride.isDriverReached = true
        self.ride.isStarted = true
        self.setTitleForTracking(ride: self.ride)
        self.resetBoundOfMap(ride: self.ride)
        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
        
        guard let timer = self.myTimer else {
            return
        }
        timer.invalidate()
        self.myTimer = nil
        
    }
    
    
    func trackNewRideEnded(){
        if (self.ride.isStarted == true) && self.ride.isDriverReached && self.ride.rideStatus{
            return
        }
        self.ride.isDriverReached = true
        self.ride.isStarted = true
        self.ride.rideStatus = true
        
        self.deleteTimer(timer: self.myTimer)
        self.removePath()
        
        self.perform(#selector(self.getRideBookingDetailsAndProceedToBill), with: nil, afterDelay: 1.5)
        self.removeActionCableClient()

        guard let timer = self.myTimer else {
            return
        }
        timer.invalidate()
        self.myTimer = nil
        
        
        ///
    }
    
    
    
    func updateActionCableMarker(){
        
        let driver = self.markerParser.trackDriver
        if self.ride.driver.ID != driver.driverID {
            return
        }

            if driver.driverID != "" {
        
                if self.ride.driver.ID == driver.driverID{
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    self.updateDriverMarker(key: driver.driverID, newPosition: position)
                    if !self.ride.isDriverReached{
                        self.updateTravelledPath(currentLoc: position)
                    }
                }else{
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    self.addDriverMarker(on: position,and: driver.driverID)
                    if !(self.ride.isDriverReached || self.ride.isStarted){
                        self.handleTimeUpdates()
                    }

                }
            }
//               else{
//                self.removeDriverMarker(key: driver.driverID)
//                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Something went wrong!".localizedString, completionBlock: {
//
//                    self.client.disconnect()
//                    self.client.onDisconnected = {(error: ConnectionError?) in
//                        print("Disconected with error: \(String(describing: error))")
//                    }
//
//                    guard let timer = self.myTimer else {
//                        return
//                    }
//                    timer.invalidate()
//                    self.myTimer = nil
//                    if self.fromHistory{
//                        self.navigationController?.pop(true)
//                    }else{
//                        self.navigationController?.popToRoot(true)
//                    }
//                })
//            }
    
        
    }
    
    func removeActionCableClient(){
        self.channel?.onUnsubscribed = {
            print_debug("Channel has been unsubscribed!")
        }
        self.trackChannel?.onUnsubscribed = {
            print_debug("Channel has been unsubscribed!")
        }
        self.channel?.unsubscribe()
        self.trackChannel?.unsubscribe()
        self.client.disconnect()
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
        }
    }
    
    
    
    @objc func startRideFromNotification(_ notification: Notification){
        print_debug("start track notification is working")
        if self.ride.ID == ""  {
            return
        }
 
               self.trackNewRideStarted()
        
    }
    
    
    @objc func reachedRideFromNotification(_ notification: Notification){
        print_debug("reached Tack notification is working")
        if self.ride.ID == ""  {
            return
        }
        self.trackNewDriverReached()

 
        
    }
    
    
    @objc func EndedRideFromNotification(_ notification: Notification){
        print_debug("ended Tack notification is working")
        if self.ride.ID == ""  {
            return
        }
        self.trackNewRideEnded()
        
    }
    
    
    @objc func cancelDriverFromNotification(_ notification: Notification){
        print_debug("Driver Cancel notification is working")
        if self.ride.ID == ""  {
            return
        }
        self.trackNewCancelledByDriver(isRideRejectAfterAccept: false)
        
    }


    
}



extension RideTrackingViewController: EditDropAddressViewControllerDelegate {
    func editPickAndDropAddressAddressViewControllerr(viewController: EditDropAddressViewController, didPickSelect pickCoordinate: CLLocationCoordinate2D, pickAddress: String, didDropSelect dropCoordinate: CLLocationCoordinate2D, dropAddress: String) {
        
    }
    
    func editDropAddressViewControllerr(viewController: EditDropAddressViewController, didSelect dropCoordinate: CLLocationCoordinate2D, dropAddress: String) {
        viewController.dismiss(animated: true, completion: nil)
        guard let currentLocation = self.locationManager.location else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Couldn't found your current location".localizedString)
            return
        }
        self.proceedToGetCurrentAddress(currentLocationCoordinate: currentLocation.coordinate, endLocationCoordinate: dropCoordinate, endAddress: dropAddress)
    }
    
    
    
   
    
}


extension RideTrackingViewController{

    
    
    @IBAction func onClickEditDrop(_ sender: UIButton){
        self.openAddressPicker()
    }
    
    
    
    func openAddressPicker(){
        let addressPickerVC = AppStoryboard.Booking.viewController(EditDropAddressViewController.self)
        addressPickerVC.delegate = self
        addressPickerVC.pickUpAddress = self.ride.startLocation
        addressPickerVC.pickUpCoordinates = CLLocationCoordinate2D(latitude: ride.startLatitude, longitude: ride.startLongitude)
        
        addressPickerVC.dropOffAddress = self.ride.endLocation
        addressPickerVC.dropOffCoordinates = CLLocationCoordinate2D(latitude: ride.endLatitude, longitude: ride.endLongitude)
        let navigation = AppSettings.shared.getNavigation(vc: addressPickerVC)

        self.present(navigation, animated: true, completion: nil)
    }
    

    func proceedToGetCurrentAddress(currentLocationCoordinate: CLLocationCoordinate2D, endLocationCoordinate: CLLocationCoordinate2D, endAddress:String){
        
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        AppSettings.shared.showLoader(withStatus: "Getting address..")
        self.nameOfPlaceWithCoordinates(coordinate: currentLocationCoordinate) { (currentAddress) in
            if currentAddress.count == 0{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Couldn't get your current address!".localizedString)
                return
            }
            AppSettings.shared.updateLoader(withStatus: "Getting distance..")
            self.getTravelTimeAndDistance(from: self.ride.dropChangedAt.coordinate2D, to: currentLocationCoordinate, completion: { (timeBeforeChanged, distanceBeforeChanged) in
                
                self.getTravelTimeAndDistance(from: currentLocationCoordinate, to: endLocationCoordinate, completion: { (timeAfterChanged, distanceAfterChanged) in
                    
                    //Now go to update address service
                    self.updateDropAddressOfRide(rideId: self.ride.ID, currentAddress: currentAddress, currentCoordinate: currentLocationCoordinate, newAddress: endAddress, newAddressCoordinate: endLocationCoordinate, distanceBeforeChnage: distanceBeforeChanged, distanceAfterChange: distanceAfterChanged, timeBeforeChange: timeBeforeChanged, timeAfterChange: timeAfterChanged)
                })
            })
            
        }
        
    }
    
    
    func getTravelTimeAndDistance(from sourceCoordinate:CLLocationCoordinate2D,to destinationCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: Double,_ distance: Double)->Void)->Void{
        
        let origin = "\(sourceCoordinate.latitude),\(sourceCoordinate.longitude)"
        let destination = "\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let rows = json["rows"].arrayValue
                print_debug("time and distance :\(rows)")
                if rows.count > 0{
                    let arrValues = rows.first!["elements"].arrayValue.first!
                    var distance = 0.0
                    if let distanceDict = arrValues["distance"].dictionaryObject as [String:AnyObject]?{
                        if let distancef = distanceDict["value"] as? Double{
                            distance = distancef
                        }
                    }
                    
                    var duration = 0.0
                    if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                        if let durationf = durationDict["value"] as? Double{
                            duration = durationf
                        }
                    }
                    completion(duration,distance)
                }
            }catch{NKToastHelper.sharedInstance.showErrorAlert(self,message:"Error occurred while calculating time".localizedString)}
        }
    }
        
        func updateDropAddressOfRide(rideId: String, currentAddress: String, currentCoordinate: CLLocationCoordinate2D, newAddress: String, newAddressCoordinate: CLLocationCoordinate2D, distanceBeforeChnage: Double, distanceAfterChange: Double, timeBeforeChange: Double, timeAfterChange: Double){
            if !AppSettings.isConnectedToNetwork{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
                return
            }
            AppSettings.shared.updateLoader(withStatus: "Please wait..")
            RideService.sharedInstance.updateDropAddress(of: rideId, currentAddress: currentAddress, currentCoordinate: currentCoordinate, newAddress: newAddress, newAddressCoordinate: newAddressCoordinate, distanceBeforeChange: distanceBeforeChnage, distanceAfterChange: distanceAfterChange, timeBeforeChange: timeBeforeChange, timeAfterChange: timeAfterChange) { (success, resRide, message) in
                AppSettings.shared.hideLoader()
                if success{
                    if let updatedRide = resRide{
                        self.ride = updatedRide
                        self.setRideDetails(ride: self.ride)
                        self.addDropOffMarker(ride: self.ride)
                        
                        self.resetBoundOfMap(ride: self.ride)
                        NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    }else{
                        NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }
            
        }
        

        func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
            let geocoder = GMSGeocoder()
            var currentAddress = ""
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    let filteredLines = lines.filter({ (line) -> Bool in
                        return !line.isEmpty
                    })
                    currentAddress = filteredLines.joined(separator: ",")
                    complition(currentAddress)
                }
            }
        }
        
        
    
    func removeDriverChatFromFirebase(rideID:String){
        self.chatRemoveRef = Database.database().reference()
        self.chatRemoveRef.keepSynced(false)
        self.chatRemoveHandle = self.chatRemoveRef.observe(.childAdded, with: { (snapshot) in
            let chat = snapshot.key
            if chat == "Chat_Ride_\(self.ride.ID)" {
                let deleteDriver = self.chatRemoveRef.child(chat)
                deleteDriver.removeValue()
            }
        })
        
    }
    
}
    

