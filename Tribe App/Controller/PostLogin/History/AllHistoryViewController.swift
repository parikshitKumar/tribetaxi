//
//  AllHistoryViewController.swift
//  HLS Taxi
//
//  Created by Parikshit on 15/07/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class AllHistoryViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var scrolling: UIScrollView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var instantButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var tapingView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var shadowView: UIView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MY RIDES".localizedString
        self.setupNavigationViews()
        self.instantButton.setTitle("INSTANT".localizedString, for: .normal)
        self.scheduleButton.setTitle("SCHEDULE".localizedString, for: .normal)
        
        self.scrolling.isPagingEnabled = true
        self.scrolling.bounces = false
        self.toggleRateButton(button: instantButton)
        let allVc = AppStoryboard.History.viewController(MyRidesViewController.self)
        self.addChild(allVc)
        let receivedVc = AppStoryboard.History.viewController(ScheduleHistoryListViewController.self)
        self.addChild(receivedVc)
        
        self.perform(#selector(LoadScollView), with: nil, afterDelay: 0.5)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
    CommonClass.makeViewCircularWithCornerRadius(self.instantButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.instantButton.frame.size.height/2)
    CommonClass.makeViewCircularWithCornerRadius(self.scheduleButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.scheduleButton.frame.size.height/2)
        self.shadowView.dropShadow(shadowColor:.lightGray,shadowRadius:10,shadowOffset: CGSize(width: 0, height: 10))
       self.tapingView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 30)
        CommonClass.makeCircularBottomRadius(self.backView, cornerRadius: 15)
       // self.backView.dropShadow(shadowColor:.lightGray,shadowRadius:10,shadowOffset: CGSize(width: 0, height: 10))

    }
    
    
    
    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.makeBlackBackGround()
    }
    
    
    //    func setRightNavigationButton(){
    //        let  button = UIButton(frame: CGRect(x:0, y:0, width:70, height:30))
    //        button.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    //        if AppSettings.shared.selectedLanguageName == SelectLanguage.French.rawValue {
    //            button.setImage(#imageLiteral(resourceName: "support_french_button"), for: .normal)
    //        }else{
    //            button.setImage(#imageLiteral(resourceName: "support_button"), for: .normal)
    //        }
    //        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
    //        let viewWalletBarButton = UIBarButtonItem(customView: button)
    //        self.navigationItem.setRightBarButtonItems(nil, animated: false)
    //        self.navigationItem.setRightBarButtonItems([viewWalletBarButton], animated: false)
    //    }
    
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    
    
    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        //        self.navigationController?.pop(true)
        SlideNavigationController.sharedInstance().toggleLeftMenu()
        NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

    }
    
    
    func toggleRateButton(button:UIButton){
        if button == instantButton{
            self.selectButton(button: instantButton)
            self.unSelectButton(button: scheduleButton)
            
            
        }else{
            self.unSelectButton(button: instantButton)
            self.selectButton(button: scheduleButton)
            
        }
        
    }
    
    func selectButton(button:UIButton) {
        button.isSelected = true
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = appColor.blueColor
    }
    
    
    func unSelectButton(button:UIButton) {
        button.isSelected = false
        button.setTitleColor(appColor.red, for: .normal)
        button.backgroundColor = UIColor.white

        
    }
    
}


//let kScreenWidth = UIScreen.main.bounds.size.width

extension AllHistoryViewController: UIScrollViewDelegate {
    
    @IBAction func onClickInstantButton(_ sender: UIButton){
        self.toggleRateButton(button: instantButton)
        self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
//        self.raceTo(CGPoint(x:self.instantButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    @IBAction func onClickScheduleButton(_ sender: UIButton){
        self.toggleRateButton(button: scheduleButton)
        self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
//        self.raceTo(CGPoint(x:self.scheduleButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    
    
    @objc func LoadScollView() {
        scrolling.delegate = nil
        scrolling.contentSize = CGSize(width:kScreenWidth * 2, height:scrolling.frame.size.height)
        for i in 0 ..< self.children.count {
            self.loadScrollViewWithPage(i)
        }
        scrolling.delegate = self
    }
    
    
    func loadScrollViewWithPage(_ page: Int) {
        if page < 0 {
            return
        }
        if page >= self.children.count {
            return
        }
        var allVC : MyRidesViewController
        var addedVC : ScheduleHistoryListViewController
        
        var frame: CGRect = scrolling.frame
        switch page {
        case 0:
            allVC = self.children[page] as! MyRidesViewController
            allVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            allVC.view.frame = frame
            scrolling.addSubview(allVC.view!)
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            
        case 1:
            addedVC = self.children[page] as! ScheduleHistoryListViewController
            addedVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            addedVC.view.frame = frame
            scrolling.addSubview(addedVC.view!)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            addedVC.view.setNeedsLayout()
            addedVC.view.layoutIfNeeded()
            
            
            
            
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageCurrent = Int(floor(scrolling.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleRateButton(button: instantButton)
            self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
//            self.raceTo(CGPoint(x:self.instantButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        case 1:
            self.toggleRateButton(button: scheduleButton)
            self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
//            self.raceTo(CGPoint(x:self.scheduleButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        default:
            break
        }
    }
    
//    func raceTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
//        var stopPoint: CGPoint = destination
//        if withSnapBack {
//            let diffx = destination.x - moovingView.frame.origin.x
//            let diffy = destination.y - moovingView.frame.origin.y
//            if diffx < 0 {
//                stopPoint.x -= 10.0
//            }
//            else if diffx > 0 {
//                stopPoint.x += 10.0
//            }
//
//            if diffy < 0 {
//                stopPoint.y -= 10.0
//            }
//            else if diffy > 0 {
//                stopPoint.y += 10.0
//            }
//        }
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationDuration(0.2)
//        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
//        moovingView.frame = CGRect(x:stopPoint.x, y:stopPoint.y, width: moovingView.frame.size.width,height: moovingView.frame.size.height)
//        UIView.commitAnimations()
//        let firstDelay = 0.1
//        let startTime = firstDelay * Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: .now() + startTime) {
//            UIView.beginAnimations(nil, context: nil)
//            UIView.setAnimationDuration(0.1)
//            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
//            self.moovingView.frame = CGRect(x:destination.x, y:destination.y,width: self.moovingView.frame.size.width, height: self.moovingView.frame.size.height)
//            UIView.commitAnimations()
//        }
//    }
    
    
    func raceScrollTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - scrolling.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        if isleft {
            scrolling.contentOffset = CGPoint(x:destination.x - 5, y:destination.y)
        }
        else {
            scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
        }
        
        UIView.commitAnimations()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            if isleft {
                self.scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
            }
            else {
                self.scrolling.contentOffset = CGPoint(x:destination.x - 5,y: destination.y)
            }
            UIView.commitAnimations()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0){() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.easeInOut)
                self.scrolling.contentOffset = CGPoint(x:destination.x, y:destination.y)
                UIView.commitAnimations()
            }
        }
    }
}

