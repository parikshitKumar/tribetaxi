//
//  SelectCancelReasonViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 24/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

protocol SelectCancelReasonViewControllerDelegate {
    func cancelRide(_ viewController: SelectCancelReasonViewController, didCancelledRideWithReason reason: String)
}

class SelectCancelReasonViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    //let reasons = ["Expected a shorter wait time","Unable to contact driver","Driver denied duty","Cab is not moving in my direction","My reason is not listed"]
    var reasons = Array<CancelReason>()
    var delegate : SelectCancelReasonViewControllerDelegate?
    @IBOutlet weak var containnerView: UIView!

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var cancelRideButton : UIButton!
    @IBOutlet weak var donotCancelRideButton : UIButton!
    @IBOutlet weak var showCancelReasonList: UILabel!

    var selectedIndex = 0
    var selectedReason : String!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.backgroundColor = appColor.blackOne
//        self.tableView.backgroundColor = appColor.blackOne
        self.getCancelReason()
        //selectedReason = reasons[selectedIndex].reason
        self.cancelRideButton.setTitle("CANCEL RIDE".localizedString, for: .normal)
        self.donotCancelRideButton.setTitle("DON'T CANCEL".localizedString, for: .normal)
        self.showCancelReasonList.text = "SELECT A REASON".localizedString

        self.tableView.dataSource = self
        self.tableView.delegate = self
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    override func viewDidLayoutSubviews() {
//        CommonClass.makeViewCircularWithRespectToHeight(self.cancelRideButton, borderColor: (cancelRideButton.titleLabel?.textColor)!, borderWidth: 1)
//        CommonClass.makeViewCircularWithRespectToHeight(self.donotCancelRideButton, borderColor: (donotCancelRideButton.titleLabel?.textColor)!, borderWidth: 1)
//    }

    func getCancelReason(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        AppSettings.shared.showLoader(withStatus: "Loading..")
        RideService.sharedInstance.getCancelReasons { (success, resReasons, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.reasons.removeAll()
                if let someReasons = resReasons{
                    self.reasons.append(contentsOf: someReasons)
                    self.selectedReason = self.reasons[self.selectedIndex].reason
                    self.tableView.reloadData()
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }

    }

    @IBAction func onClickCancelRide(_ sender: UIButton){
        delegate?.cancelRide(self, didCancelledRideWithReason: selectedReason)
    }

    @IBAction func onClickDonotCancelRide(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasons.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelReasonCell", for: indexPath) as! CancelReasonCell
        cell.button.isSelected = (indexPath.row == selectedIndex)
        cell.reasonLabel.text = reasons[indexPath.row].reason
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == indexPath.row{
            return
        }else{
            selectedIndex = indexPath.row
            selectedReason = reasons[indexPath.row].reason
            tableView.reloadData()
        }
    }

}
