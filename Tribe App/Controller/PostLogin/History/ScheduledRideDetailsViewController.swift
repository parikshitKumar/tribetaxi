//
//  ScheduledRideDetailsViewController.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 23/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class ScheduledRideDetailsViewController: UIViewController,SelectCancelReasonViewControllerDelegate {
    let heights:[CGFloat] = [190,75,70,70,95]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var mailLable: UILabel!
    @IBOutlet weak var supportLabel: UILabel!


    var ride: Ride!
    var user : User!
    var navigationTitleView: RideDetailNavigationTitleView!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppSettings.shared.hideLoader()
        self.user = User.loadSavedUser()
        self.registerCells()
        self.setUpNavigationTitle()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        let status = self.ride.getCurrentStatus()
        self.supportLabel.text = "SUPPORT".localizedString

        if status == .cancelled{//!self.ride.isUserCancelled {
            mailLable.textColor = appColor.gray
            self.mailLable.text = "MAIL INVOICE".localizedString
            mailButton.setImage(#imageLiteral(resourceName: "mail_invoice_unsel"), for: UIControl.State())
        }else{
            
            if self.ride.scheduleTime < Date().toMillis() && self.ride.requestStatus == false{
                mailLable.textColor = appColor.gray
                mailButton.setImage(#imageLiteral(resourceName: "mail_invoice_unsel"), for: UIControl.State())
                mailButton.isUserInteractionEnabled = false
                
            }else{
                
                self.mailLable.text = "CANCEL RIDE".localizedString
                mailLable.textColor = appColor.black
                mailButton.setImage(#imageLiteral(resourceName: "close"), for: UIControl.State())
            }

        }
    }
    
    override func viewWillLayoutSubviews() {
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
    
    
    
    func setUpNavigationTitle(){
        self.navigationTitleView = RideDetailNavigationTitleView.instanceFromNib()
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width-80, height: 44)
        self.navigationTitleView.frame = frame
        self.navigationTitleView.crnLabel.text = "CRN"+self.ride.ID
        //        self.navigationTitleView.titleLabel.text = (self.ride.rideType == .instant) ? self.ride.createdAt : self.ride.scheduledAt
        if AppSettings.shared.selectedLanguageName == SelectLanguage.Portuguese.rawValue {
            let temp = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
            let  convertDate = CommonClass.sharedInstance.splitDate(date: temp)
            self.navigationTitleView.titleLabel.text = convertDate
        }else{
            self.navigationTitleView.titleLabel.text = (self.ride.rideType == .instant) ? self.ride.createdAt : self.ride.scheduledAt
        }
        
        self.navigationItem.titleView = self.navigationTitleView
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func registerCells(){
        self.tableView.register(UINib(nibName: "RidePaymentDetailsCell", bundle: nil), forCellReuseIdentifier:"RidePaymentDetailsCell")
        self.tableView.register(UINib(nibName: "RideAddressDetailsCell", bundle: nil), forCellReuseIdentifier:"RideAddressDetailsCell")
        self.tableView.register(UINib(nibName: "RideDistanceAndFareCell", bundle: nil), forCellReuseIdentifier:"RideDistanceAndFareCell")
        self.tableView.register(UINib(nibName: "CarNameAndCategoryCell", bundle: nil), forCellReuseIdentifier:"CarNameAndCategoryCell")
        self.tableView.register(UINib(nibName: "RideDetailsDriverCell", bundle: nil), forCellReuseIdentifier:"RideDetailsDriverCell")
        self.tableView.register(UINib(nibName: "RideMapCell", bundle: nil), forCellReuseIdentifier:"RideMapCell")
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        supportVC.ride = self.ride
        self.navigationController?.pushViewController(supportVC, animated: true)
    }

    @IBAction func onClickCancelRide(_ sender: UIButton){
        let status = self.ride.getCurrentStatus()
        if status == .scheduled{
            if self.ride.scheduleTime > Date().toMillis() && self.ride.requestStatus == false{
                self.openSelectCancelReason()
            }
        }else{return}
    }
    
    func showAlertForEmail(){
        let alertController = UIAlertController(title: "Enter Email Address".localizedString, message: "", preferredStyle: .alert)

        let sendAction = UIAlertAction(title: "SEND".localizedString, style: .default) { (aciton) in

            guard let emailTextField = alertController.textFields?.first else{
                return
            }
            guard let email = emailTextField.text else{
                return
            }

            if !CommonClass.isValidEmailAddress(email.trimmingCharacters(in: .whitespaces)){
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.invalidEmailAddress.messageString())
                return
            }
            self.sendInvoice(ride: self.ride, email: email)

        }

        let cancelAction = UIAlertAction(title: "CANCEL".localizedString, style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }

        alertController.addTextField { (textField) in
            textField.placeholder = "Email".localizedString
            textField.keyboardType = .emailAddress
        }

        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }


    func sendInvoice(ride:Ride,email:String){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        ride.mailInvoice(email){ (success, message) in
            AppSettings.shared.hideLoader()
            NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
        }
    }


    func openSelectCancelReason() {
        let reasonVC = AppStoryboard.History.viewController(SelectCancelReasonViewController.self)
        reasonVC.delegate = self
        reasonVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        let nav = UINavigationController(rootViewController: reasonVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    func cancelRequest(rideID:String,reason:String,completionBlock: @escaping (_ finished : Bool, _ message:String)->Void){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        RideService.sharedInstance.cancelRide(rideID, reason: reason) { (success, resRide, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.ride.isUserCancelled = true
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
            }
            completionBlock(success,message)
        }
    }

    func cancelRide(_ viewController: SelectCancelReasonViewController, didCancelledRideWithReason reason: String) {
        viewController.dismiss(animated: false, completion: nil)
        self.cancelRequest(rideID: self.ride.ID, reason: reason) { (cancelled,message) in
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: message, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
    }



}

extension ScheduledRideDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.heights.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heights[indexPath.row]
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideMapCell", for: indexPath) as! RideMapCell
            cell.mapImageView.setIndicatorStyle(.gray)
            cell.mapImageView.setShowActivityIndicator(true)
            let height = self.heights[indexPath.row]-4
            let width = self.view.frame.width
            cell.mapImageView.sd_setImage(with: ride.getRideMapUrl(CGSize(width:width,height:height)))
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideDetailsDriverCell", for: indexPath) as! RideDetailsDriverCell

            if ride.isUserCancelled{
                cell.cancelledLabel.text = "CANCELLED".localizedString
                cell.cancelledLabel.textColor = appColor.black

            }else{
                if self.ride.scheduleTime < Date().toMillis() && self.ride.requestStatus == false{
                    cell.cancelledLabel.text = "NO DRIVER ASSIGNED YET".localizedString
                }else{
                    cell.cancelledLabel.text = " "
                }
            }
            cell.cancelledLabel.textColor = appColor.black
            cell.ratingView.isHidden = true
            cell.ratingView.isUserInteractionEnabled = (self.ride.rating == 0.0)
            //cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: #imageLiteral(resourceName: "placeholder"))

            cell.driverName.text = "SCHEDULED RIDE".localizedString
            cell.driverName.textColor = appColor.blueColor

            cell.ratingView.rating = Float(ride.rating)
            CommonClass.makeViewCircular(cell.driverIcon, borderColor: appColor.blueColor, borderWidth: 1)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarNameAndCategoryCell", for: indexPath) as! CarNameAndCategoryCell

            cell.carIcon.sd_setImage(with: URL(string:ride.car.image) ?? URL(string: api.base.url())!, placeholderImage: UIImage(named: ride.car.carCategory.categoryName.lowercased()))
//            cell.categoryLabel.text = self.ride.car.name.capitalized
//            cell.carName.text = self.ride.carCategory.categoryName.capitalized

            cell.categoryLabel.text = self.ride.car.name.capitalized
            cell.carName.text = self.ride.car.carCategory.categoryName.capitalized
            CommonClass.makeViewCircular(cell.carIcon, borderColor: appColor.blueColor, borderWidth: 1)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideDistanceAndFareCell", for: indexPath) as! RideDistanceAndFareCell
            cell.fareLabel.text = self.ride.isUserCancelled ? Rs+String(format: "%.2f", 0) : Rs+String(format: "%.2f", ride.estimatedCost.totalPrice)
            cell.timeLabel.text = " "
            cell.distanceLabel.text = " "
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RideAddressDetailsCell", for: indexPath) as! RideAddressDetailsCell
            cell.pickUpTimeLabel.text = (self.ride.startTime.count == 0) ? " " : self.ride.startTime
            cell.dropOffTimeLabel.text = (self.ride.endTime.count == 0) ? " " : self.ride.endTime
            cell.pickUpAddressLabel.text = self.ride.startLocation
            cell.dropOffAddressLabel.text = self.ride.endLocation
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let mycell = cell as? CarNameAndCategoryCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
        }else if let mycell = cell as? RideDetailsDriverCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
        }else if let mycell = cell as? RideAddressDetailsCell{
            mycell.setNeedsLayout()
            mycell.layoutIfNeeded()
            //mycell.dotedImageView.image = UIImage.drawDottedImage(width: 2, height: mycell.dotedImageView.frame.size.height, color: UIColor.white)
        }

    }




    func timeStringFromRideTime(seconds : Double) -> String {
        var timeStr = ""
        let secondsInAMinute : Int = 60
        let minutesInAnHour : Int = 60
        let secondsInAnHour : Int = 3600

        let secs = Int(seconds)
        if secs >= 3600{
            let hrs = Int(secs/secondsInAnHour)
            let remSec = secs%secondsInAnHour
            let mms = remSec%minutesInAnHour
            var hhStr = ""
            var mmStr = ""
            if hrs > 0{
                hhStr = "\(hrs) Hrs "
            }
            if mms > 0{
                mmStr = "\(mms) Min"
            }
            timeStr = hhStr+mmStr
        }else if secs >= 60{
            let mms = Int(secs/secondsInAMinute)
            let scs = secs%secondsInAMinute
            var mmStr = ""
            var scStr = ""
            if mms > 0{
                mmStr = "\(mms) Min "
            }
            if scs > 0{
                scStr = "\(scs) Sec"
            }
            timeStr = mmStr+scStr
        }else{
            timeStr = String(format: "%.0f Sec",seconds)
        }
        return timeStr
    }
}





