//
//  FareBreakUpsViewController.swift


import UIKit

let tax:Double = 0//10

class FareBreakUpsViewController: UIViewController {
    //var fare:Double!
    var category: CarCategory!
    var approxFare : ApproxFare!
    @IBOutlet weak var categoryNameLabel: UILabel!
   // @IBOutlet weak var carsNameLabel: UILabel!
    @IBOutlet weak var rideFareLabel: UILabel!
    @IBOutlet weak var taxesLabel: UILabel!
    @IBOutlet weak var totalFareLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    
    @IBOutlet weak var showRideFareLabel:  UILabel!
    @IBOutlet weak var showIncludeTaxLabel:  UILabel!
    @IBOutlet weak var showTotalFareLabel:  UILabel!
    @IBOutlet weak var showNoteLabel:  UILabel!
    @IBOutlet weak var gotITButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var offerPriceLabel:  UILabel!
    var isTapFairDetail:Bool = false



    override func viewDidLoad() {
        super.viewDidLoad()
        self.showRideFareLabel.text = "Ride Fare".localizedString
        self.showIncludeTaxLabel.text = "Excluding Taxes".localizedString
        self.showTotalFareLabel.text = "TOTAL FARE".localizedString
        self.showNoteLabel.text = "NOTE:".localizedString
        self.remarkLabel.text = "This is the approx fare your ride will charge. If you change your drop or route it may change.".localizedString
        self.gotITButton.setTitle("GOT IT".localizedString, for: .normal)
        
        self.categoryNameLabel.text = approxFare.categoryName.uppercased()
       // self.carsNameLabel.text = " "//category.carsName.capitalized
        self.rideFareLabel.text = Rs + String(format: "%0.2f",approxFare.fare)
        let actualCost = (100*approxFare.fare)/(100+tax)
        let includingTaxes = approxFare.fare - actualCost
        self.taxesLabel.text =  Rs + String(format: "%0.2f",includingTaxes)
        
        
        if self.isTapFairDetail {
            self.offerPriceLabel.text =  Rs + String(format: "%0.2f",approxFare.offerPrice)
            let netPrice = self.approxFare.fare - self.approxFare.offerPrice
            self.totalFareLabel.text = Rs + String(format: "%0.2f",netPrice)

        }else{
            self.offerPriceLabel.text =  Rs + "0.0"
            self.totalFareLabel.text = Rs + String(format: "%0.2f",approxFare.fare)

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickDetailsButton(_ sender: UIButton){
        self.showCategoryFare(approxFare: approxFare)
    }

    func showCategoryFare(approxFare:ApproxFare){
        let categoryFareVC = AppStoryboard.Booking.viewController(CategoryFareViewController.self)
        categoryFareVC.approxFare = approxFare
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        let nav = UINavigationController(rootViewController: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }



    @IBAction func onClickGotIt(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
