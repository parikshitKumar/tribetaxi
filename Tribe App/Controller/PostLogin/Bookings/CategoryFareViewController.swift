//
//  CategoryFairViewController.swift


import UIKit

class CategoryFareViewController: UIViewController {
    //var category: CarCategory!
    var approxFare: ApproxFare!
    @IBOutlet weak var categoryNameLabel: UILabel!
   // @IBOutlet weak var carsNameLabel: UILabel!
    @IBOutlet weak var baseFareLabel: UILabel!
    @IBOutlet weak var minimumFareLabel: UILabel!
    @IBOutlet weak var ratePerKMLabel: UILabel!
    @IBOutlet weak var rideTimeRateLabel: UILabel!
    
    @IBOutlet weak var showBasePriceLabel: UILabel!
    @IBOutlet weak var showMinPriceLabel: UILabel!
    @IBOutlet weak var showPerKMLabel: UILabel!
    @IBOutlet weak var showPerMinLabel: UILabel!
    @IBOutlet weak var showNoteLabel: UILabel!
    @IBOutlet weak var showNoteDescLabel: UILabel!
    @IBOutlet weak var gotITButton: UIButton!
    @IBOutlet weak var backView: UIView!


    //@IBOutlet weak var remarkLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBasePriceLabel.text = "Base Price".localizedString
        self.showMinPriceLabel.text = "Minimum Price".localizedString
        self.showPerKMLabel.text = "+ Per KM".localizedString
        self.showPerMinLabel.text = "+ Per Minute".localizedString
        self.showNoteLabel.text = "NOTE:".localizedString
        self.showNoteDescLabel.text = "This is the approx fare your ride will charge. If you change your drop or route it may change.".localizedString
        self.gotITButton.setTitle("GOT IT".localizedString, for: .normal)

        self.categoryNameLabel.text = "FARE BREAKDOWNS".uppercased().localizedString
        //self.carsNameLabel.text = category.carsName.capitalized
        self.baseFareLabel.text = Rs + String(format: "%0.2f",approxFare.baseFair)
        self.minimumFareLabel.text = Rs + String(format: "%0.2f",approxFare.minimumFare)
        self.ratePerKMLabel.text = Rs + String(format: "%0.2f",approxFare.pricePerKM)
        self.rideTimeRateLabel.text = Rs + String(format: "%0.2f",approxFare.rideTimeFair)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }


    @IBAction func onClickGotIt(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
