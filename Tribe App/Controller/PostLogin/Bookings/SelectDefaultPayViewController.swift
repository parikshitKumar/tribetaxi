//
//  SelectDefaultPayViewController.swift


import UIKit



protocol SelectDefaultPayViewControllerDelegate {
    func selectPay(_ viewController: SelectDefaultPayViewController,didSelectIndex:Int ,didSuccess success:Bool)
}

class SelectDefaultPayViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var showSelectPaymentLabel:UILabel!
    @IBOutlet weak var submitButton:UIButton!

    
    var selectedIndex: Int = 0

    var delegate : SelectDefaultPayViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.showSelectPaymentLabel.text = "SELECT PAYMENT METHOD".localizedString
        self.submitButton.setTitle("SUBMIT".localizedString, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDefaultPayCell", for: indexPath) as! SelectDefaultPayCell
        cell.selectButton.isSelected = (self.selectedIndex == indexPath.row)
        if indexPath.row == 0 {
            cell.selectLabel.text = "Pay by wallet"
            
        }else{
            cell.selectLabel.text = "Pay by cash"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
       aTableView.reloadData()
    }
    
    
    @IBAction func onClickSubmitButton(_ sender:UIButton) {
        self.delegate?.selectPay(self, didSelectIndex: self.selectedIndex, didSuccess: true)
        self.dismiss(animated: true, completion: nil)

    }
    
    
    @IBAction func onClickCancekButton(_ sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    

}

