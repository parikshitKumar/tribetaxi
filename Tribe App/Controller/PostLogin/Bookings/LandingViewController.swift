//
//  LandingViewController.swift



import UIKit
import GoogleMaps
import SDWebImage
import Alamofire
import SwiftyJSON
import DGActivityIndicatorView

import ActionCableClient
import SnapKit


class LandingViewController: UIViewController{
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var centerImageView: UIImageView!

    @IBOutlet weak var aTableView:UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var roundView: UIView!

    var selectedAddressType: AddressType = .from
    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var pickUpCoordinates: CLLocationCoordinate2D?
    var dropOffCoordinates: CLLocationCoordinate2D?
    var pickUpAddress: String?
    var dropOffAddress: String?
    var mapViewWillMove = true
    var mapViewWillMoveByLocationButton = false
    var mapViewMovedByGesture = false
    var isLocationCalled = false

    var driverMarkers = [DriverMarker]()
    var user : User!
    var onGoingRideView: OnGoingRideView!
    var currentRide:Ride?
    var shouldAddShadow:Bool = true
    var activityIndicator : DGActivityIndicatorView!
    var recentPlaces = [FavoritePlace]()
    
    var markerParser =  DriverParser()

    /////////
    // Create the Room Channel
    
    static var ChannelIdentifier = "TribeTaxiChannel"
    
    let client = ActionCableClient(url: URL(string:"\(actionCableBaseUrl)cable")!)
//    let client = ActionCableClient(url: URL(string:"ws:" + actionCableBaseUrl + "cable")!)


    var channel: Channel?
    
    var name: String?
    


    override func viewDidLoad() {
        super.viewDidLoad()
       NotificationCenter.default.addObserver(self, selector: #selector(rideDidUpdate(_:)), name: .RIDE_UPDATED_NOTIFICATION, object: nil)
        self.user = User.loadSavedUser()
       setupClient()

        self.title = "TAXI".localizedString
        self.navigationController?.navigationBar.isHidden = false
        self.registerCells()

        self.locationManagerSetup()
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
//        self.mapView.isMyLocationEnabled = false

        self.setRightNavigationButton()
        
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.showIndicator()
                }

    }
    

    
    
    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
        NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

    }

    func resetBounds(){

    }
    
    
    func showIndicator() {
        activityIndicator = DGActivityIndicatorView(type: .ballScale, tintColor: appColor.gray, size: self.view.frame.size.width*30/100)
        self.view.addSubview(activityIndicator)
       activityIndicator.center = centerImageView.center
        self.view.bringSubviewToFront(activityIndicator)
        activityIndicator.startAnimating()

    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       setupClient()
        self.driverMarkers.removeAll()
        self.markerParser.drivers.removeAll()
        
        self.setupNavigationViews()
        self.isLocationCalled = false
        self.locationManager.startUpdatingLocation()
        self.getOnGoingRides()
        self.getRecentPlaces()
        
//        guard let pickLocation = self.pickUpCoordinates else{
//            self.isLocationCalled = false
//            self.locationManager.startUpdatingLocation()
//            return
//        }



    }
    
    func mapViewLoad(){
        if AppDelegate.getAppDelegate().currentLocation != nil{
            self.pickUpCoordinates = AppDelegate.getAppDelegate().currentLocation.coordinate
//            setupClient()
                    guard let pickLocation = self.pickUpCoordinates else{
                        self.isLocationCalled = false
                        self.locationManager.startUpdatingLocation()
                        return
                    }
            self.getAllCarsByCategoryFromServer(latitude: pickLocation.latitude, longitude: pickLocation.longitude)

        }else{
                        self.isLocationCalled = false
                        self.locationManager.startUpdatingLocation()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.mapViewLoad()

        guard let pickLocation = self.pickUpCoordinates else{
            self.isLocationCalled = false
            self.locationManager.startUpdatingLocation()
            return
        }
        self.getAllCarsByCategoryFromServer(latitude: pickLocation.latitude, longitude: pickLocation.longitude)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        CommonClass.makeCircularTopRadius(self.aTableView, cornerRadius: 15.0)
        CommonClass.makeCircularBottomRadius(self.roundView, cornerRadius: 15.0)
        self.setRightNavigationButton()
//        self.showIndicator()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//
        self.channel?.onUnsubscribed = {
            print_debug("Channel has been unsubscribed!")
        }
        self.channel?.unsubscribe()
        self.client.disconnect()
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
        }
        self.removeAllDriverMarker()
        mapView.clear()
//        activityIndicator.stopAnimating()

        
    }
    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setRightNavigationButton(){
        let  button = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        button.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        let notificationImage = UIImage(named: "notification_icon")
        button.setImage(notificationImage, for: .normal)
        let viewWalletBarButton = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([viewWalletBarButton], animated: false)
    }
    


    @objc func rideDidUpdate(_ notification:Notification) {
        self.removeRideView()
        self.getOnGoingRides()
    }
    func addRideView(with ride: Ride){
        self.currentRide = ride
        if self.onGoingRideView != nil{
            self.onGoingRideView.removeFromSuperview()
            self.onGoingRideView = nil
        }
        self.onGoingRideView = OnGoingRideView.instanceFromNib()
        let eframe = CGRect(x: 6, y:self.roundView.frame.center.y + 20, width: self.view.frame.size.width-12, height: 103)
        self.onGoingRideView.frame = eframe
        self.onGoingRideView.setupRideDetails(ride: ride)
        self.onGoingRideView.cancelButton.addTarget(self, action: #selector(onClickCandelOfCurrentRideview(_:)), for: .touchUpInside)
        self.view.addSubview(self.onGoingRideView)
        self.view.bringSubviewToFront(onGoingRideView)
        self.addGestureRecognizerOnOnGoingRideView()
        CommonClass.makeViewCircularWithCornerRadius(self.onGoingRideView.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        self.animateCurrentRideView()
//        self.animateWithAddShadow()
        self.onGoingRideView.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 2, shadowOpacity: 0.5, shadowColor: self.shouldAddShadow ? .clear : .lightGray)

    }

    func animateCurrentRideView(){
        let lframe = CGRect(x: 6, y: self.roundView.frame.center.y + 20, width: self.view.frame.size.width-12, height: 103)
        UIView.animate(withDuration: 0.5, delay: 0.01, usingSpringWithDamping: 0.5, initialSpringVelocity: 2, options: .curveEaseInOut, animations: {
            self.onGoingRideView.frame = lframe
        }, completion: {(done) in
        })
    }

    @IBAction func onClickCandelOfCurrentRideview(_ sender: UIButton){
        self.removeRideView()
    }
    func addGestureRecognizerOnOnGoingRideView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(currentRideViewTapped(_:)))
        self.onGoingRideView.isUserInteractionEnabled = true
        self.onGoingRideView.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func currentRideViewTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        guard let ride = self.currentRide else {
            return
        }
//        self.getRideBookingDetails(rideID: ride.ID)
        self.openRideTracking(ride: ride)
    }
    
    
    
//    func getRideBookingDetails(rideID:String){
//        if !AppSettings.isConnectedToNetwork {
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
//            return
//        }
//
//        RideService.sharedInstance.getRideDetails(rideID) { (success,resRide,message)  in
//            if success{
//                if let ride = resRide{
//                    self.openRideTracking(ride: ride)
//
//                }else{
//                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
//                }
//            }else{
//                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
//            }
//        }
//    }



    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = false
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }

    

    func getOnGoingRides(){
        let appDelegate = AppDelegate.getAppDelegate()
        if !(appDelegate.window?.rootViewController?.topViewController is LandingViewController) {
            return
        }

        RideService.sharedInstance.getOnGoingRides { (success, resRides, count, message) in
            if var rides = resRides,rides.count > 0{
                rides.sort(by: { (ride1, ride2) -> Bool in
                    return ride1.ID < ride2.ID
                })
                guard let currentRide = rides.last else{return}
                self.addRideView(with: currentRide)
            }
        }
    }
    func removeRideView(){
        if self.onGoingRideView != nil{
            self.onGoingRideView.removeFromSuperview()
            self.onGoingRideView = nil
        }
    }





    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- CoreLocation handling
    func locationManagerSetup() {
        locationManager.delegate = self;
        mapView.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.isMyLocationEnabled = true
    }



    @IBAction func onClickCurrentLocation(_ sender: UIButton){
        guard let location = self.mapView.myLocation else {return}
        self.mapViewWillMoveByLocationButton = true
        self.mapView.animate(toLocation: location.coordinate)
    }

    @IBAction func onClickNavigationTitle(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    @IBAction func onClickDestinationAddress(_ sender: UIButton){
        isLocationCalled = true
        self.openAddressPicker()
//        self.navigateScheduleView()


    }
    
    
    @IBAction func onClickScheduleButton(_ sender: UIButton){
        isLocationCalled = true
        self.navigateScheduleView()
        // NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
        
    }
    
    func navigateScheduleView(){
        let categoryFareVC = AppStoryboard.Booking.viewController(ScheduleBookingViewController.self)
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let nav = UINavigationController(rootViewController: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func onClickPickUpAddressButton(_ sender: UIButton) {
        //self.openAddressPicker()
        NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)

    }

    func openAddressPicker(){
        let addressPickerVC = AppStoryboard.Booking.viewController(AddressPickerViewController.self)
        addressPickerVC.dropOffAddress = self.dropOffAddress
        addressPickerVC.dropOffCoordinates = self.dropOffCoordinates
        addressPickerVC.pickUpAddress = self.pickUpAddress
        addressPickerVC.pickUpCoordinates = self.pickUpCoordinates
        self.navigationController?.pushViewController(addressPickerVC, animated: true)
    }
    
    
    func animateHeaderAndRideView(_ isHide:Bool) {
        if !mapViewWillMove{
            return
        }
        if isHide{
            UIView.animate(withDuration: 0.2, animations: {
                
                self.aTableView.frame.origin.y = self.view.frame.size.height
                
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                if #available(iOS 11.0, *) {
                    self.aTableView.frame.origin.y = self.view.frame.size.height - (self.aTableView.frame.size.height + self.view.safeAreaInsets.bottom)
                } else {
                    self.aTableView.frame.origin.y = self.view.frame.size.height - self.aTableView.frame.size.height
                    
                }
            })
        }
    }}




///Last Two RecentPlaces


extension LandingViewController{
    func getRecentPlaces() {
        PlaceService.sharedInstance.getLastTwoRecentPlaces { (success, response, message) in
            if success {
                if let resPlaces = response {
                    self.recentPlaces = resPlaces
                    print_debug("CountRecentPlaces>>>\(self.recentPlaces.count)")
                    self.aTableView.reloadData()
                    self.setDynamicTableViewHeight()
                    
                }
                
                
            }
        }
    }
    
    
    func setDynamicTableViewHeight() {
        let viewHeight = 70//self.view.frame.width*150/1138
        switch self.recentPlaces.count {
        case 0:
            UIView.animate(withDuration: 0.3) {
                self.tableViewHeight.constant = CGFloat(viewHeight + 20)
            }
        case 1:
            UIView.animate(withDuration: 0.3){
                self.tableViewHeight.constant = CGFloat(viewHeight*2 + 20)
            }
        case 2:
            UIView.animate(withDuration: 0.3) {
                self.tableViewHeight.constant = CGFloat(viewHeight*3 + 20)
            }
        default:
            UIView.animate(withDuration: 0.3) {
                self.tableViewHeight.constant = CGFloat(viewHeight + 20)
            }
        }
        self.aTableView.reloadData()
        
        
    }
    

    
    func navigateToHomeViewController() {
        isLocationCalled = true
        if shouldMoveForward() {
        let homeVC = AppStoryboard.Booking.viewController(HomeViewController.self)
        homeVC.pickUpCoordinates = self.pickUpCoordinates!
        homeVC.dropOffCoordinates = self.dropOffCoordinates!
        homeVC.pickUpAddress = self.pickUpAddress!
        homeVC.dropOffAddress = self.dropOffAddress!
        homeVC.isFromLandingPage = true
        self.navigationController?.pushViewController(homeVC, animated: true)
        }else{
            self.openAddressPicker()
        }
    }
    
    
    func shouldMoveForward() -> Bool {
        if self.pickUpCoordinates == nil {
            return false
        }
        if self.pickUpAddress == nil {
            return false
        }
        if self.dropOffCoordinates == nil {
            return false
        }
        
        if self.dropOffAddress == nil {
            return false
        }
        
        return ((self.pickUpCoordinates != nil) && (self.dropOffCoordinates != nil) && (self.pickUpAddress != nil) && (self.dropOffAddress != nil))
    }

    
}



extension LandingViewController:UITableViewDataSource,UITableViewDelegate {
    
    func registerCells(){
        
        let destinationNib = UINib(nibName: "LandingDestinationTableViewCell", bundle: nil)
        self.aTableView.register(destinationNib, forCellReuseIdentifier: "LandingDestinationTableViewCell")
        let selectAddressNib = UINib(nibName: "SelectDestinationTableViewCell", bundle: nil)
        self.aTableView.register(selectAddressNib, forCellReuseIdentifier: "SelectDestinationTableViewCell")
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.tableViewHeight.constant = 230
        self.aTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.recentPlaces.count == 0 ? 0 : self.recentPlaces.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                if indexPath.section == 0{
                    return 90//self.view.frame.width*150/1138
                }else{
                    return 70//self.view.frame.width*150/1138
                }
        //return  self.view.frame.width*150/1138
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LandingDestinationTableViewCell", for: indexPath) as! LandingDestinationTableViewCell
            cell.destinationButton.addTarget(self, action: #selector(onClickDestinationAddress(_:)), for: .touchUpInside)
            cell.destinationLabel.text = "|  Enter Destination".localizedString
            cell.userNameLabel.text = self.user.fullName
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDestinationTableViewCell", for: indexPath) as! SelectDestinationTableViewCell
            let place = self.recentPlaces[indexPath.row]
            if  place.category.isEmpty{
                cell.categoryLabel.text = "Other"
            }else{
                cell.categoryLabel.text = place.category.capitalized
            }
            cell.addressLabel.text = place.place
            cell.categoryImage.image = UIImage(named: "search_address")

//            if place.category.lowercased() == "home" {
//                cell.categoryImage.image = UIImage(named: "home")
//
//            }else if place.category.lowercased() == "work" {
//                cell.categoryImage.image = UIImage(named: "work")
//
//            }else{
//                cell.categoryImage.image = UIImage(named: "default_location")
//
//            }
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{return}
        if self.recentPlaces.count == 0{return}
        let place = self.recentPlaces[indexPath.row]
        self.dropOffAddress = place.place
        self.dropOffCoordinates = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        self.navigateToHomeViewController()
    }
    
    
}





//MARK:- LocationManagerDelegate and GMSMapViewDelegate
extension LandingViewController: CLLocationManagerDelegate, GMSMapViewDelegate{

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            self.locationManager.startUpdatingLocation()
        }else if status == .denied || status == .restricted{
            self.showGotoLocationSettingAlert()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationCalled{
            isLocationCalled = true
            if let location = locations.last{
                currentLocation = location
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                print("locationCordinate>>>\(self.currentLocation)")

           // }
                self.refreshCustomerLocationWithCoordinate(location.coordinate)
                self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                    self.pickUpCoordinates = location.coordinate
                    self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                        self.pickUpAddress = address

                    })
                })
            }
        }
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        let fromLocation = mapViewWillMoveByLocationButton
        mapViewWillMove = gesture || fromLocation
        self.mapViewWillMoveByLocationButton = false
        animateHeaderAndRideView(true)

    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !self.mapViewWillMove{
            self.mapViewWillMove = true
            return
        }
        pickUpCoordinates = position.target
        
        
//        self.setUpNavigationTitle(title: "Getting address..")
        
        self.nameOfPlaceWithCoordinates(coordinate: position.target, complition: { (address) in
            self.pickUpAddress = address

//            self.setUpNavigationTitle(title: address)
        })
        self.getAllCarsByCategoryFromServer(latitude: position.target.latitude, longitude: position.target.longitude)
        animateHeaderAndRideView(false)

    }
    
    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        //self.mapView.animate(toLocation: coordinate)
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.mapView.mapType = .normal
        self.mapView.camera = camera
    }
    
    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        var currentAddress = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                let filteredLines = lines.filter({ (line) -> Bool in
                    return !line.isEmpty
                })
                currentAddress = filteredLines.joined(separator: ",")
                complition(currentAddress)
            }
        }
    }
//
    func getAllCarsByCategoryFromServer(latitude:Double,longitude:Double) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        let appDelegate = AppDelegate.getAppDelegate()

        if !(appDelegate.window?.rootViewController?.topViewController is LandingViewController) {
            return
        }
        if (appDelegate.window?.rootViewController?.topViewController is HomeViewController) {
            return
        }

        RideService.sharedInstance.newSearchCarsByCategory("", latitude: latitude, longitude: longitude, userID: self.user.ID, isFromCategoryPage: false) { (success, resCars, message) in
            //self.updateMarkerWithNewHandlers()
            self.firstTimeAddMArker()
            if !success{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
        
    }

    func showGotoLocationSettingAlert(){
        let alert = UIAlertController(title: "Opps".localizedString, message: "Location access seems disabled\r\nGo to settings to enabled".localizedString, preferredStyle: UIAlertController.Style.alert)
        let okayAction = UIAlertAction(title: "Okay".localizedString, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }

        let settingsAction = UIAlertAction(title: "Settings".localizedString, style: .default) { (action) in
            let url = UIApplication.openSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }

        alert.addAction(okayAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
    }

}

extension LandingViewController{

    
    func isDriverMarkersContains(key:String) -> Bool{
        let filteredArray = self.driverMarkers.filter { (driverMarker) -> Bool in
            return driverMarker.driver.driverID == key
        }
        return filteredArray.count != 0
    }
    
    func indexForDriverMarker(driverMarker:DriverMarker) -> Int?{
        var result : Int?
        for (index,dM) in self.driverMarkers.enumerated(){
            if dM.driver.driverID == driverMarker.driver.driverID{
                result = index
                break;
            }
        }
        return result
    }
    
    func removeDriverMarker(key:String){
        for (index,dM) in self.driverMarkers.enumerated(){
            if key == dM.driver.driverID{
                dM.map = nil
                self.driverMarkers.remove(at: index)
                break;
            }
        }
    }
    
    func removeAllDriverMarker(){
        mapView.clear()
        self.driverMarkers.removeAll()
    }
    
    func updateDriverMarker(key:String,newPosition:CLLocationCoordinate2D){
        for dM in self.driverMarkers{
            if key == dM.driver.driverID{
                let oldLocation = dM.position
                let oldLocationOfMarker = CLLocation(latitude: oldLocation.latitude, longitude: oldLocation.latitude)
                let newLocationOfMarker = CLLocation(latitude: newPosition.latitude, longitude: newPosition.latitude)
                let distance = oldLocationOfMarker.distance(from: newLocationOfMarker)
                if distance >= 200.0{
                    print_debug("distance in landing page : \(distance)")
                    let newBearing = self.bearingFromCoordinate(from: oldLocation, to: newPosition)
                    self.resetMarker(markerID: key, to: newPosition, bearing: newBearing)
                }else{
                    self.updateDriverMarkerLocation(marker: dM, from: oldLocation, to: newPosition)
                }
                break;
            }
        }
    }
    
    
    func resetMarker(markerID: String, to destination: CLLocationCoordinate2D, bearing:Double){
        let aDriver = ActionCableDriver()
        aDriver.driverID = markerID
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        removeDriverMarker(key: markerID)
        
        let driverMarker = DriverMarker(driver: aDriver)
        driverMarker.position = destination
        driverMarker.appearAnimation = .pop
        driverMarker.rotation = bearing
        if self.mapView != nil{
            driverMarker.map = self.mapView
        }
        self.driverMarkers.append(driverMarker)
        self.resetBounds()
    }
    
    func updateDriverMarkerLocation(marker:DriverMarker, from oldLocation: CLLocationCoordinate2D,to newLocation: CLLocationCoordinate2D) -> Void {
        self.moveMarker(marker, fromSource: oldLocation, to: newLocation) { (updatedLocation) in
        }
    }
}




extension LandingViewController{
    func moveMarker(_ marker : DriverMarker,fromSource source:CLLocationCoordinate2D, to destination:CLLocationCoordinate2D,completionBlock:@escaping (_ movedMarkerPosition:CLLocationCoordinate2D) -> Void){
        let start = source
        let startRotation = marker.rotation
        
        let animator = ValueAnimator.animate(" ", from: 0, to: 1, duration: 1.5) { (prop, value) in
            let v = value.value
            let newPosition = self.getInterpolation(fraction: v, startPoint: start, endPoint: destination)
            marker.position = newPosition
            
            let newBearing = self.bearingFromCoordinate(from: start, to: newPosition)
            let rotation = self.getRotation(fraction: v, start: startRotation, end: newBearing)
            marker.rotation = rotation
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isFlat = true
            marker.driver.latitude = start.latitude
            marker.driver.longitude = start.longitude
            marker.driver.latitude = newPosition.latitude
            marker.driver.longitude = newPosition.longitude
            completionBlock(start)
        }
        animator.resume()
        
    }
    
    
    private func angleFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let deltaLongitude = seccond.longitude - first.longitude
        let deltaLatitude = seccond.latitude - first.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude/deltaLongitude)
        if deltaLongitude > 0 {return angle}
        else if deltaLongitude < 0 {return angle + .pi}
        else if deltaLatitude == 0 {return .pi}
        return 0.0
    }
    
    
    func bearingFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let pi = Double.pi
        let lat1 = first.latitude*pi/180.0
        let long1 = first.longitude*pi/180.0
        let lat2 = seccond.latitude*pi/180.0
        let long2 = seccond.longitude*pi/180.0
        
        let diffLong = long2 - long1
        let y = sin(diffLong)*cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(diffLong)
        var bearing = atan2(y, x)
        bearing = bearing.toDegree(fromRadian: bearing)
        bearing = (bearing+360).truncatingRemainder(dividingBy: 360)
        return bearing
    }
    
    
    private func getRotation(fraction:Double, start: Double, end: Double) -> Double{
        let normailizedEnd = end - start
        let normailizedEndAbs = ((normailizedEnd+360)).truncatingRemainder(dividingBy: 360)
        let direction = (normailizedEndAbs > 180) ? -1 : 1 //-1 for anticlockwise and 1 for closewise
        let rotation = (direction > 0) ? normailizedEndAbs : (normailizedEndAbs-360)
        let result = fraction*rotation+start
        let finalResult = (result+360).truncatingRemainder(dividingBy: 360)
        return finalResult
    }
    
    private func getInterpolation(fraction:Double, startPoint:CLLocationCoordinate2D, endPoint:CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        let latitude = (endPoint.latitude - startPoint.latitude) * fraction + startPoint.latitude
        var longDelta = endPoint.longitude - startPoint.longitude
        if abs(longDelta) > 180{
            longDelta -= longDelta*360
        }
        let longitude = (longDelta*fraction) + startPoint.longitude
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}


extension LandingViewController {
    
    func setupClient() -> Void {
        print(self.client)
        self.client.willConnect = {
            print_debug("Will Connect")
        }
        
        self.client.onConnected = {
            print_debug("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
            self.client.connect()
        }
        
        
        self.client.willReconnect = {
            print_debug("Reconnecting to \(self.client.url)")
            return true
        }
        self.channel?.onSubscribed = {
            print_debug("Subscribed to \(LandingViewController.ChannelIdentifier)")
        }

        let room_identifier = ["room_id" : "User_\(self.user.ID)","customer_id": "\(self.user.ID)","role":"user"] as [String : Any]
        print("rommIdentifiew>>>>>>>>>\(room_identifier)")
        self.channel = client.create(LandingViewController.ChannelIdentifier, identifier: room_identifier, autoSubscribe: true, bufferActions: true)

 
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {

        guard let pickLocation = self.pickUpCoordinates else{
            self.isLocationCalled = false
            self.locationManager.startUpdatingLocation()
            return
        }
        self.getAllCarsByCategoryFromServer(latitude: pickLocation.latitude, longitude: pickLocation.longitude)

        }
        
        self.channel?.onReceive = {(data: Any?, error: Error?) in
            if let error = error {
                print_debug(error.localizedDescription)
                return
            }
            do {
                let JSONObject = JSON(data!)

                //let json = try JSON.init(data: data as! Data)
                let driverParser = DriverParser(json: JSONObject)
                self.markerParser = driverParser
                self.checkObjectTypeFromJson(from: JSONObject)
               print_debug("jsonBroadCast>>>\(JSONObject)")
                
            } catch let error {
                print_debug("Failed to load: \(error.localizedDescription)")
            }

        }
        

        
        self.client.connect()
    }
    
    
    
    func checkObjectTypeFromJson(from json: JSON) {
        if (json["drivers"].dictionaryObject as Dictionary<String,AnyObject>?) != nil{
            self.updateActionMarlerHandler()
            
        }else if (json["drivers"].arrayObject as? [[String:AnyObject]]) != nil {
            self.firstTimeAddMArker()
            
        }
        

    }

    func updateActionMarlerHandler(){
        let driver = self.markerParser.driver
    
            if driver.isAddedDriver {
                if self.isDriverMarkersContains(key: driver.driverID){
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    self.updateDriverMarker(key: driver.driverID, newPosition: position)
                }else{
            let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
            let aDriver = ActionCableDriver()
            aDriver.driverID = driver.driverID
            aDriver.latitude = position.latitude
            aDriver.longitude = position.longitude
            aDriver.previousLatitude = position.latitude
            aDriver.previousLongitude = position.longitude
            
            let driverMarker = DriverMarker(driver: aDriver)
            driverMarker.position = position
            driverMarker.appearAnimation = .pop
            if isDebugEnabled{
                driverMarker.title = aDriver.driverID
                driverMarker.isTappable = true
            }
            if self.mapView != nil{
                driverMarker.map = self.mapView
            }
            self.driverMarkers.append(driverMarker)
            self.resetBounds()
        }
            }else{
                if self.isDriverMarkersContains(key: driver.driverID){
                    self.removeDriverMarker(key: driver.driverID)
                    self.resetBounds()
                }
            }
      
    }
    
    
    
    func firstTimeAddMArker(){
        let drivers = self.markerParser.drivers
        if drivers.isEmpty {
            self.removeAllDriverMarker()
            return
        }
        for driver in drivers {
            
            if driver.isAddedDriver {
                if self.isDriverMarkersContains(key: driver.driverID){
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    self.updateDriverMarker(key: driver.driverID, newPosition: position)
                }else{
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    let aDriver = ActionCableDriver()
                    aDriver.driverID = driver.driverID
                    aDriver.latitude = position.latitude
                    aDriver.longitude = position.longitude
                    aDriver.previousLatitude = position.latitude
                    aDriver.previousLongitude = position.longitude
                    
                    let driverMarker = DriverMarker(driver: aDriver)
                    driverMarker.position = position
                    driverMarker.appearAnimation = .pop
                    if isDebugEnabled{
                        driverMarker.title = aDriver.driverID
                        driverMarker.isTappable = true
                    }
                    if self.mapView != nil{
                        driverMarker.map = self.mapView
                    }
                    self.driverMarkers.append(driverMarker)
                    self.resetBounds()
                }
            }else{
                if self.isDriverMarkersContains(key: driver.driverID){
                    self.removeDriverMarker(key: driver.driverID)
                    self.resetBounds()
                }
            }
        }
        
    }
    
    
    
    
    func removeUpdateActionMarkerHandler() {
       let driver = self.markerParser.driver
        if self.isDriverMarkersContains(key: driver.driverID){
            self.removeDriverMarker(key: driver.driverID)
            self.resetBounds()
        }
        
    }
    
    
    
}



