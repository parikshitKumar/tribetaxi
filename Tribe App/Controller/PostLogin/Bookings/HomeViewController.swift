//
//  HomeViewController.swift

//


import UIKit
import GoogleMaps
import SDWebImage
import GooglePlaces
import Alamofire
import SwiftyJSON
import ActionCableClient
import SnapKit

let kMapZoomLevel:Float = 14.0

class HomeViewController: UIViewController {
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var footerView: UIView!

    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var rideButton: UIButton!
    //@IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var showBookYourRideLabel: UILabel!


   // @IBOutlet weak var centerPinImage: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var pickUpAddressLabel:UILabel!
    @IBOutlet weak var dropAddressLabel:UILabel!
    @IBOutlet weak var editButton: UIButton!


    var carCategories = Array<CarCategory>()
    var approxFares = Array<ApproxFare>()
    var selectedCategoryIndex: Int = 0
    var cars = Array<Car>()
    var selectedAddressType: AddressType = .from

    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var selectedCategory : CarCategory!
    var pickUpCoordinates: CLLocationCoordinate2D!
    var dropOffCoordinates: CLLocationCoordinate2D!
    var pickUpAddress: String!
    var dropOffAddress: String!
    var ridingDate : Date?
    var mapViewWillMove = true
    var mapViewWillMoveByLocationButton = false
    var mapViewMovedByGesture = false
    var isLocationCalled = false
    var shouldOpenAddChild = false

    var dateTextField : UITextField!
    var datePicker : UIDatePicker!
    var addressMarker:AddressMarker!
    var addressPickUpMarker:AddressMarker!
    var wallet = Wallet()
//    var confirmRideView: ConfirmRideView!
    var selectApproxFare = ApproxFare()
    var isPresentConfirmRideView: Bool = false
    var isFromLandingPage: Bool = false
    var walletButton = UIButton()


    
    var driverMarkers = [DriverMarker]()
    var user : User!

    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSMutablePath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0

    var ride = Ride()

    var time:Double = 0.0
    var distance: Double = 0.0
    var refreshTime : Double!
    weak var refreshTimer: Timer?
    var markerParser =  DriverParser()
    var selectOffer = OfferModel()
    var selectOfferIndex:Int = -1
    
    
    static var ChannelIdentifier = "TribeTaxiChannel"
    
//    let client = ActionCableClient(url: URL(string:"ws://rolling-rim.iwebmobileapp.com:3005/cable")!)
//    let client = ActionCableClient(url: URL(string:"ws:" + actionCableBaseUrl + "cable")!)
    let client = ActionCableClient(url: URL(string:"\(actionCableBaseUrl)cable")!)

    var channel: Channel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.setAddressInfo()
//        self.moreButton.isUserInteractionEnabled = false
        self.moreButton.setTitle("Schedule".uppercased(), for: .normal)
        self.rideButton.setTitle("Get Taxi".uppercased(), for: .normal)

        let nib = UINib(nibName: "CategoryCell", bundle: nil)
        self.categoryCollectionView.register(nib, forCellWithReuseIdentifier: "CategoryCell")
        let catFlowLayout = UICollectionViewFlowLayout()
        catFlowLayout.minimumLineSpacing = 1
        catFlowLayout.minimumInteritemSpacing = 10
        catFlowLayout.scrollDirection = .horizontal
        categoryCollectionView.collectionViewLayout = catFlowLayout
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        dateTextField = UITextField()
        self.view.addSubview(dateTextField)
//        self.setUpDatePicker()
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
//        self.waitngTimeLabel.text = self.pickUpAddress
//        self.addressLabel.text = self.dropOffAddress
        if !self.isFromLandingPage {
        SlideNavigationController.sharedInstance().viewControllers.remove(at: SlideNavigationController.sharedInstance().viewControllers.count-2)
        }

    }

    override func viewWillLayoutSubviews() {
        self.decorateView()
       CommonClass.makeCircularBottomRadius(self.headerView, cornerRadius: 15)
        CommonClass.makeCircularTopRadius(self.footerView, cornerRadius: 15)

    }
    
    func setAddressInfo(){
        self.pickUpAddressLabel.text = self.pickUpAddress
        self.dropAddressLabel.text = self.dropOffAddress

    }
    
    func openAddressPicker(homeAddressType:String){
        let addressPickerVC = AppStoryboard.Booking.viewController(EditDropAddressViewController.self)
        addressPickerVC.delegate = self
        addressPickerVC.pickUpAddress = self.pickUpAddress
        addressPickerVC.pickUpCoordinates = self.pickUpCoordinates
        
        addressPickerVC.dropOffAddress = self.dropOffAddress
        addressPickerVC.dropOffCoordinates = self.dropOffCoordinates
        addressPickerVC.homeAddressType = homeAddressType
        addressPickerVC.delegate = self
        let navigation = AppSettings.shared.getNavigation(vc: addressPickerVC)
        self.present(navigation, animated: true, completion: nil)
    }
    
    
    
    
    
//    func confirmRideDetailView(){
//        self.isPresentConfirmRideView = true
//        self.confirmRideView = ConfirmRideView.instanceFromNib()
//        self.confirmRideView.frame = CGRect(x:0, y:self.footerView.frame.origin.y, width: self.footerView.frame.size.width, height: self.footerView.frame.height)
//        self.confirmRideView.cateroryCarLabel.text = self.selectedCategory.categoryName
//        self.confirmRideView.showWalletLabel.isHidden = true
//        self.confirmRideView.wallletAmountLabel.isHidden = true
//
////        self.confirmRideView.wallletAmountLabel.text = Rs + String(format: "%0.2f", self.wallet.point)
//        CommonClass.makeViewCircularWithCornerRadius(self.confirmRideView.confirmButton, borderColor: .clear, borderWidth: 0, cornerRadius: 5)
//
//        self.confirmRideView.fairDetailsButton.addTarget(self, action: #selector(onShowFareBreakUps(_:)), for: .touchUpInside)
//        self.confirmRideView.confirmButton.addTarget(self, action: #selector(onClickConfirmButton(_:)), for: .touchUpInside)
//        self.view.addSubview(self.confirmRideView)
//        self.view.bringSubviewToFront(self.confirmRideView)
//
//    }
    
//    @IBAction func onClickConfirmButton(_ sender: UIButton) {
//        if (self.user.countryCode == "91") || (self.user.countryCode == "1") {
//            self.navigateToPaymentSelectOption()
//
//        }else{
//            self.checkPointForBooking(isCashSelected: true)
//
//        }
//
//    }
    
    @IBAction func onClickEditAddressButton(_ sender:UIButton) {
     self.openAddressPicker()
        
    }
    
    func openAddressPicker(){
        let addressPickerVC = AppStoryboard.Booking.viewController(EditDropAddressViewController.self)
        addressPickerVC.delegate = self
        addressPickerVC.pickUpAddress = self.pickUpAddress
        addressPickerVC.pickUpCoordinates = self.pickUpCoordinates
        addressPickerVC.dropOffAddress = self.dropOffAddress
        addressPickerVC.dropOffCoordinates = self.dropOffCoordinates
        addressPickerVC.homeAddressType = "home"
        let navigation = AppSettings.shared.getNavigation(vc: addressPickerVC)
        
        self.present(navigation, animated: true, completion: nil)
    }
    


    @objc func refreshCategories(){
        if self.refreshTime == nil{
            self.refreshTime = Date().toMillis()
        }
        if (Date().toMillis()-59999) < self.refreshTime{
            return
        }

        guard let pickLocation = self.pickUpCoordinates else {return}
        let userLocation = CLLocation(latitude: pickLocation.latitude, longitude: pickLocation.longitude)
        self.getCarCategoriesFromServer(userLocation, shouldRefreshOnlyCategories: false, shouldRefreshApproxFare: false)
    }

    func invalidateTimerAndSetItAgain(){
        self.refreshTime = Date().toMillis()
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
        self.refreshTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(refreshCategories), userInfo: nil, repeats: true)
    }

    func addMarkers(){
        self.addPickupMarker(at: self.pickUpCoordinates,title: self.pickUpAddress)
        self.addMarker(at: self.dropOffCoordinates,title: self.dropOffAddress)
        resetBounds()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.driverMarkers.removeAll()
//        self.markerParser.drivers.removeAll()
        self.removeActionCable()
        if self.refreshTime != nil{
        self.refreshTimer?.invalidate()
        self.refreshTimer = nil
        }
        
    

        mapView.clear()
    }
    
    
    func removeActionCable() {
        self.channel?.onUnsubscribed = {
            print("Channel has been unsubscribed!")
        }
        self.channel?.unsubscribe()
        self.client.disconnect()
        self.client.onDisconnected = {(error: ConnectionError?) in
            print("Disconected with error: \(String(describing: error))")
        }
    }
    

    @IBAction func onClickBackButton(_ sender: UIButton){
        if isPresentConfirmRideView {
            self.isPresentConfirmRideView = false
            self.selectOffer = OfferModel()
            self.selectOfferIndex = -1
            return
        }
        mapView.clear()
        NotificationCenter.default.removeObserver(self)
        self.removeActionCable()
        if self.refreshTime != nil{
            self.refreshTimer?.invalidate()
            self.refreshTimer = nil
        }
        self.navigationController?.popToRoot(false)

    }


    func decorateView(){
        
        CommonClass.makeViewCircularWithCornerRadius(self.moreButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.moreButton.frame.size.height/2)
        CommonClass.makeViewCircularWithCornerRadius(self.rideButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.rideButton.frame.size.height/2)
//        self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 1.0))
    }


    

    
    func resetBounds(){
        let path = GMSMutablePath()
        path.add(self.pickUpCoordinates)
        path.add(self.dropOffCoordinates)

        for driver in driverMarkers{
            path.add(driver.position)
        }

        let bounds = GMSCoordinateBounds(path: path)
        let camera = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: self.headerView.frame.size.height + 100, left: 40, bottom: self.footerView.frame.size.height+50, right: 40))
        self.mapView.animate(with: camera)
    }
    
    func addMarker(at position: CLLocationCoordinate2D, title:String){
        let marker = GMSMarker(position: position)
        marker.appearAnimation = .pop
        marker.title = title
        marker.icon = #imageLiteral(resourceName: "pin")
        marker.map = self.mapView
        

    }
    func addPickupMarker(at position: CLLocationCoordinate2D, title:String){
        let marker = GMSMarker(position: position)
        marker.appearAnimation = .pop
        marker.title = title
        marker.icon = #imageLiteral(resourceName: "uber_pin_green")
        marker.map = self.mapView
        
//        if addressPickUpMarker != nil{
//            addressPickUpMarker.map = nil
//
//        }
//
//
//
//        let pickUpMarkerImg: UIImage = UIImage(named: "uber_pin_green")!
//
//        self.addressPickUpMarker = AddressMarker(address: title, image: pickUpMarkerImg, infoMode: "left")
//        self.addressPickUpMarker.position = position
//        self.addressPickUpMarker.zIndex = 10
//        self.addressPickUpMarker.map = self.mapView
        

//        self.removeAddressMarker()

        
    }
    
    func removeAddressMarker(){
        if addressMarker != nil{
            addressMarker.map = nil
            addressMarker = nil
        }
    }
    
    func removeAddressPickUpMarker(){
        if addressPickUpMarker != nil{
            addressPickUpMarker.map = nil
            addressPickUpMarker = nil
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.removeAllDriverMarker()
//        self.driverMarkers.removeAll()
        self.markerParser.drivers.removeAll()
        
        footerView.isHidden = (self.carCategories.count == 0)
        guard let pickLocation = self.pickUpCoordinates else {return}
//        let userLocation = CLLocation(latitude: pickLocation.latitude, longitude: pickLocation.longitude)

        setupClient()

//        self.getCarCategoriesFromServer(userLocation, shouldRefreshOnlyCategories: false, shouldRefreshApproxFare: true)
        guard let dropLocation = self.dropOffCoordinates else {return}

        self.addMarkers()
        self.resetBounds()
        self.drawPath(startLocation: pickUpCoordinates, endLocation: dropLocation)
    }

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    func setUpDatePicker() -> Void {
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        self.datePicker.minimumDate = Date.init(timeInterval: (1*60*60 + 5*60), since: Date())
        self.datePicker.date =  Date.init(timeInterval: (1*60*60 + 5*60), since: Date())
        self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = appColor.blueColor
        toolBar.sizeToFit()
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = appColor.blueColor
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localizedString, style: .plain, target: self, action: #selector(HomeViewController.doneClick))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localizedString, style: .plain, target: self, action: #selector(HomeViewController.cancelClick))
        cancelButton.tintColor = UIColor.white
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dateTextField.inputView = self.datePicker
        dateTextField.inputAccessoryView = toolBar
    }

    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        self.ridingDate = datePicker.date//dateFormatter1.string(from: datePicker.date)
        dateTextField.text = dateFormatter1.string(from: datePicker.date)
        dateTextField.resignFirstResponder()
        self.navigateToPaymentSelectOption()
    }

    @objc func cancelClick() {
        dateTextField.resignFirstResponder()
    }

    //MARK:- RideNow and RideLate actions
    @IBAction func onClickMore(_ sender: UIButton){

//
//        self.ridingDate = nil
//        self.datePicker.minimumDate = Date.init(timeInterval: (1*60*60 + 5*60), since: Date())
//        self.datePicker.date =  Date.init(timeInterval: (1*60*60 + 5*60), since: Date())
//        self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
//        dateTextField.becomeFirstResponder()
        
        self.navigateScheduleView()
    }

    @IBAction func onClickRideNow(_ sender: UIButton){
        self.ridingDate = nil
        self.navigateToPaymentSelectOption()
    }
    

    func navigateScheduleView(){
        let categoryFareVC = AppStoryboard.Booking.viewController(ScheduleBookingViewController.self)
        categoryFareVC.delegate = self
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let nav = UINavigationController(rootViewController: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }


    func checkPointForBooking(paymentType: String){
        if self.pickUpCoordinates == nil{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter pick up location".localizedString)
            return
        }

        if self.dropOffCoordinates == nil{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter drop location".localizedString)
            return
        }

        if self.pickUpAddress == nil{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter pick up location".localizedString)
            return
        }

        if self.dropOffAddress == nil{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter drop location".localizedString)
            return
        }
        self.confirmRideRequest(paymentType: paymentType) {(resRide) in
            //nothing to do
        }

    }

    func navigateToBookingScreen(rideDate: Date?, pickAddress: String, dropAddress: String, pickCoordinates: CLLocationCoordinate2D, dropCoordinate: CLLocationCoordinate2D){


    }
    
    
    
    func navigateToPaymentSelectOption() {
        let categoryFareVC = AppStoryboard.Booking.viewController(SelectDefaultPayViewController.self)
        categoryFareVC.delegate = self
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        let nav = AppSettings.shared.getNavigation(vc: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }
}




//MARK:- CollectionView handling
extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("kubhitvnucmoricp9,orcgotopvcm>>>>>\(carCategories.count)")
        return carCategories.count
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        let category = carCategories[indexPath.item]
        let imageName = category.categoryName.lowercased()
        cell.categoryIcon.image = UIImage(named: imageName)
        cell.categoryNameLabel.text = category.categoryName
        if category.time == ""{
            cell.timeLabel.text = "..."
        }else if category.time.contains("no".lowercased()){
            cell.timeLabel.text = "No ".localizedString //+ category.categoryName.capitalized
        }else{
            cell.timeLabel.text = category.time
        }

//        if category.approxFare == 0.0{
//            cell.fareLabel.text = Rs+"..."
//        }else{
//            let str = String(format: "%.2f", category.approxFare)
//            cell.fareLabel.text = Rs+str
//        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let categoryCell = cell as? CategoryCell {
            let category = carCategories[indexPath.item]
            
            if selectedCategoryIndex == indexPath.item{
                let selectImage = "_"+category.ID + "_sel"
                categoryCell.categoryIcon.image = UIImage(named: selectImage)
            }else{
                let imageName = "_"+category.ID
                categoryCell.categoryIcon.image = UIImage(named: imageName)
            }
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = (collectionView.frame.size.width)/4
            let height = (collectionView.frame.size.height)
            return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 10, bottom: 1, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedCategoryIndex == indexPath.item{
            if (self.approxFares.count != 0){
                if self.approxFares.count == self.carCategories.count{
                    
                    self.showFareBreakUpsDialog(approxFare: self.approxFares[indexPath.item], isTapFairDetail: false)
                    self.selectApproxFare = self.approxFares[indexPath.item]
                }
            }
            return
        }
        
        self.selectedCategory = self.carCategories[indexPath.item]
        self.selectedCategoryIndex = indexPath.item
        
        self.rideButton.setTitle("Get ".uppercased()+self.selectedCategory.categoryName.uppercased(), for: .normal)
        
        guard let pickCoordinates = self.pickUpCoordinates else {
            return
        }
//        self.driverMarkers.removeAll()
        //self.markerParser.drivers.removeAll()

//        self.mapView.clear()
            self.firstTimeAddMArker()
        if  !self.selectedCategory.ID.isEmpty{
        self.getAllCarsByCategoryFromServerHomePage(catetegoryID: self.selectedCategory.ID, latitude: pickCoordinates.latitude, longitude: pickCoordinates.longitude)
        }
        self.removeAllDriverMarker()
//        self.driverMarkers.removeAll()
        self.markerParser.drivers.removeAll()
        
        self.firstTimeAddMArker()

        collectionView.reloadData()
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }

}



//MARK:- LocationManagerDelegate and GMSMapViewDelegate
extension HomeViewController: CLLocationManagerDelegate, GMSMapViewDelegate{
    
    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D){
        let origin = "\(String(format: "%0.10f", startLocation.latitude)),\(String(format: "%0.10f", startLocation.longitude))"
        let destination = "\(String(format: "%0.10f", endLocation.latitude)),\(String(format: "%0.10f", endLocation.longitude))"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GOOGLE_API_KEY)"

        Alamofire.request(url).responseJSON { response in
            let json = JSON(response.data!)
            let routes = json["routes"].arrayValue

            for route in routes{
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                self.path = GMSMutablePath.init(fromEncodedPath: points!)!
                self.polyline.path = self.path
                self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
                self.polyline.strokeWidth = 3.0
                self.polyline.map = self.mapView
            }
        }
    }


    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        let fromLocation = mapViewWillMoveByLocationButton
        mapViewWillMove = gesture || fromLocation
        self.mapViewWillMoveByLocationButton = false
    }


    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !self.mapViewWillMove{
            self.mapViewWillMove = true
            return
        }
    }


    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.mapView.mapType = .normal
        self.mapView.camera = camera
    }

    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        var currentAddress = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                let filteredLines = lines.filter({ (line) -> Bool in
                    return !line.isEmpty
                })
                currentAddress = filteredLines.joined(separator: ",")
                complition(currentAddress)
            }
        }
    }


    func getCarCategoriesFromServer(_ location: CLLocation, shouldRefreshOnlyCategories:Bool = false, shouldRefreshApproxFare:Bool = false) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        let appDelegate = AppDelegate.getAppDelegate()
        
        if !(appDelegate.window?.rootViewController?.topViewController is HomeViewController) {
            return
        }

        //weak var weakSelf = self
        self.invalidateTimerAndSetItAgain()
        RideService.sharedInstance.getAllCategoryFromServer(location.coordinate.latitude, longitude: location.coordinate.longitude) { (success, resCategories, message) in
            if success{
                if let someCategories = resCategories{
                    self.carCategories.removeAll()
                    self.carCategories.append(contentsOf: someCategories)
                    self.carCategories.sort(by: { (cat1, cat2) -> Bool in
                        return cat1.ID < cat2.ID
                    })
                    if self.selectedCategory == nil{
                        self.selectedCategory = self.carCategories.first ?? CarCategory()
                        self.rideButton.setTitle("Get ".uppercased().localizedString+self.selectedCategory.categoryName.uppercased(), for: .normal)
                    }
                    
                    self.prepareRequestToTravelTime()
                    if self.approxFares.count == 0{
                        self.prepareForApproxFare()
                    }else{
                        self.setApproxFaresOnCategories(fares: self.approxFares)
                    }

                    self.footerView.isHidden = (self.carCategories.count == 0)
                    self.categoryCollectionView.reloadData()

                    if !shouldRefreshOnlyCategories{
                        if  !self.selectedCategory.ID.isEmpty {
                        self.getAllCarsByCategoryFromServerHomePage(catetegoryID: self.selectedCategory.ID, latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        }
                    }
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }

            if self.carCategories.count == 0{
                self.footerView.isHidden = true
            }else{
                self.footerView.isHidden = false
            }
        }
    }

    func getTravelTimeAndDistance(from pickUpCoordinate:CLLocationCoordinate2D,to dropOffCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: Double,_ distance: Double)->Void)->Void{

        let origin = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
        let destination = "\(dropOffCoordinate.latitude),\(dropOffCoordinate.longitude)"

        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
        Alamofire.request(url).responseJSON { response in
            do{
                let json = try JSON(data: response.data!)
                let rows = json["rows"].arrayValue
                if rows.count > 0{
                    let arrValues = rows.first!["elements"].arrayValue.first!
                    var distance = 0.0
                    if let distanceDict = arrValues["distance"].dictionaryObject as [String:AnyObject]?{
                        if let distancef = distanceDict["value"] as? Double{
                            distance = distancef
                        }
                    }
                    var duration = 0.0
                    if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                        if let durationf = durationDict["value"] as? Double{
                            duration = durationf
                        }
                    }
                    completion(duration,distance)
                }
            }catch{NKToastHelper.sharedInstance.showErrorAlert(self,message:"Error occurred while calculating time".localizedString)}
        }
    }

    func getAllCarsByCategoryFromServerHomePage(catetegoryID: String,latitude:Double,longitude:Double) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        print("lip,h vfiughvbiuntoiy>>" + catetegoryID)
        if catetegoryID.isEmpty{
            return
        }
       // weak var weakSelf = self

        RideService.sharedInstance.newSearchCarsByCategory(catetegoryID, latitude: latitude, longitude: longitude, userID: self.user.ID, isFromCategoryPage: true) { (success, resCars, message) in
            if (AppDelegate.getAppDelegate().window?.rootViewController?.topViewController is HomeViewController) {
//                self.updateMarkerWithNewHandler()
            }else{
            }
        }
    }

    func prepareRequestToTravelTime() -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        var destinations = [CLLocationCoordinate2D]()
        for carCategory in self.carCategories{
            destinations.append(CLLocationCoordinate2D(latitude: carCategory.nearestCarLatitude, longitude: carCategory.nearestCarLongitude))
        }
        if destinations.count==0{
            return
        }
        guard let pickCoordnates = self.pickUpCoordinates else {
            return
        }
        self.getArrivalTimeOfAllCategory(from: pickCoordnates, to: destinations)
    }

    func showGotoLocationSettingAlert(){
        let alert = UIAlertController(title: "Opps".localizedString, message: "Location access seems disabled\r\nGo to settings to enabled".localizedString, preferredStyle: UIAlertController.Style.alert)
        let okayAction = UIAlertAction(title: "Okay".localizedString, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }

        let settingsAction = UIAlertAction(title: "Settings".localizedString, style: .default) { (action) in
            let url = UIApplication.openSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }

        alert.addAction(okayAction)
        alert.addAction(settingsAction)

        self.present(alert, animated: true, completion: nil)
    }

}

extension HomeViewController{
    
    //here we goes for firebase handling
    
    func removeAllDriverMarker(){
        for marker in self.driverMarkers{
            self.removeDriverMarker(key: marker.driver.driverID)
        }
    }
    
    

    
    
    func isDriverMarkersContains(key:String) -> Bool{
        let filteredArray = self.driverMarkers.filter { (driverMarker) -> Bool in
            return driverMarker.driver.driverID == key
        }
        return filteredArray.count != 0
    }
    
    func indexForDriverMarker(driverMarker:DriverMarker) -> Int?{
        var result : Int?
        for (index,dM) in self.driverMarkers.enumerated(){
            if dM.driver.driverID == driverMarker.driver.driverID{
                result = index
                break;
            }
        }
        return result
    }
    
    func removeDriverMarker(key:String){
        for (index,dM) in self.driverMarkers.enumerated(){
            if key == dM.driver.driverID{
                dM.map = nil
                self.driverMarkers.remove(at: index)
                break;
            }
        }
    }
    
    func updateDriverMarker(key:String,newPosition:CLLocationCoordinate2D){
        for dM in self.driverMarkers{
            if key == dM.driver.driverID{
                let oldLocation = dM.position
                let oldLocationOfMarker = CLLocation(latitude: oldLocation.latitude, longitude: oldLocation.latitude)
                let newLocationOfMarker = CLLocation(latitude: newPosition.latitude, longitude: newPosition.latitude)
                let distance = oldLocationOfMarker.distance(from: newLocationOfMarker)
                if distance >= 200.0{
                    print_debug("distance in home : \(distance)")
                    let newBearing = self.bearingFromCoordinate(from: oldLocation, to: newPosition)
                    self.resetMarker(markerID: key, to: newPosition, bearing: newBearing)
                }else{
                    self.updateDriverMarkerLocation(marker: dM, from: oldLocation, to: newPosition)
                }
                break;
            }
        }
    }
    
    
    
    
    
    func updateDriverMarkerLocation(marker:DriverMarker, from oldLocation: CLLocationCoordinate2D,to newLocation: CLLocationCoordinate2D) -> Void {
        self.moveMarker(marker, fromSource: oldLocation, to: newLocation) { (updatedLocation) in}
    }
    
    func getArrivalTimeOfAllCategory(from userLocationCoordinate: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D]) -> Void {
        var destinationsStr = ""
        let sourceStr = "\(userLocationCoordinate.latitude),\(userLocationCoordinate.longitude)"
        for dest in destinations{
            let destStr = "\(dest.latitude)%2C\(dest.longitude)"
            if destinationsStr == ""{
                destinationsStr = destStr
            }else{
                destinationsStr = "\(destinationsStr)%7C\(destStr)"
            }
        }
        
        getTravelTime(from: sourceStr, to: destinationsStr) { (timeArray) in
            for t in 0..<timeArray.count{
                self.carCategories[t].time = timeArray[t]
                if self.carCategories[t].ID == self.selectedCategory.ID{
                    self.selectedCategory.time = timeArray[t]
                }
            }
            self.categoryCollectionView.reloadData()
        }
    }
    
    func resetMarker(markerID: String, to destination: CLLocationCoordinate2D, bearing:Double){
        let aDriver = ActionCableDriver()
        aDriver.driverID = markerID
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        aDriver.latitude = destination.latitude
        aDriver.longitude = destination.longitude
        removeDriverMarker(key: markerID)
        
        let driverMarker = DriverMarker(driver: aDriver)
        driverMarker.position = destination
        driverMarker.appearAnimation = .pop
        driverMarker.rotation = bearing
        if self.mapView != nil{
            driverMarker.map = self.mapView
        }
        self.driverMarkers.append(driverMarker)
        self.resetBounds()
    }
    
    
    func prepareForApproxFare(){
        self.getTravelTimeAndDistance(from: self.pickUpCoordinates, to: self.dropOffCoordinates) { (time, distance) in
            self.distance = distance
            self.time = time
            self.getApproxFare(distance: distance, time: time, offerCode: self.selectOffer.coupanCode)
        }
    }
    
    func setApproxFaresOnCategories(fares: Array<ApproxFare>){
        
        if fares.count != self.carCategories.count{return}
        for t in 0..<fares.count{
            self.carCategories[t].approxFare = fares[t].fare
            if self.carCategories[t].ID == self.selectedCategory.ID{
                self.selectedCategory.approxFare = fares[t].fare
            }
        }
        self.categoryCollectionView.reloadData()
    }
    
    func getApproxFare(distance:Double, time:Double,offerCode:String){
        RideService.sharedInstance.getApproxFares(distance, time: time, offerCode: offerCode) { (success, resFares, message,wallet)  in
            if success{
                if let someFares = resFares{
                    self.wallet = wallet!
                    self.walletButton.setTitle( Rs + String(format: "%0.1f", self.wallet.point) + " ", for: .normal)
                    self.approxFares.removeAll()
                    self.approxFares.append(contentsOf: someFares)
                    self.approxFares.sort(by: { (apporxFare1, apporxFare2) -> Bool in
                        return apporxFare1.categoryID < apporxFare2.categoryID
                    })
                    self.setApproxFaresOnCategories(fares: self.approxFares)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    func getTravelTime(from source:String,to destination: String,completion:@escaping (_ times: [String])->Void)->Void{
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(destination)&destinations=\(source)&key=\(GOOGLE_API_KEY)"
        
        Alamofire.request(url).responseJSON { response in
            do{
                let json =  try JSON(data: response.data!)
                print_debug("traver time json : \n \(json)")
                let rows = json["rows"].arrayValue
                var timeArray = [String]()
                for row in rows{
                    if let targetDict = row["elements"].arrayValue.first{
                        if let notFound = targetDict["status"].string as String?,notFound != "OK"{
                            timeArray.append("no car")
                        }else if let durationDict = targetDict["duration"].dictionaryObject as [String:AnyObject]?{
                            if let durationf = durationDict["text"] as? String{
                                timeArray.append(durationf)
                            }
                        }
                    }
                }
                completion(timeArray)
            }catch{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Error occurred while calculating time".localizedString)
            }
        }
    }
}





extension HomeViewController{
    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}


class PlaceMark: GMSMarker {
    var place:Car
    init(place: Car) {
        self.place = place
        super.init()
        self.position = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        groundAnchor = CGPoint(x: 0.5, y: 1)
    }
}

extension HomeViewController{
    func moveMarker(_ marker : DriverMarker,fromSource source:CLLocationCoordinate2D, to destination:CLLocationCoordinate2D,completionBlock:@escaping (_ movedMarkerPosition:CLLocationCoordinate2D) -> Void){
        let start = source
        let startRotation = marker.rotation
        let animator = ValueAnimator.animate(marker.driver.driverID, from: 0, to: 1, duration: 1.5) { (prop, value) in
            let v = value.value
            let newPosition = self.getInterpolation(fraction: v, startPoint: start, endPoint: destination)
            marker.position = newPosition
            let newBearing = self.bearingFromCoordinate(from: start, to: newPosition)
            let rotation = self.getRotation(fraction: v, start: startRotation, end: newBearing)
            marker.rotation = rotation
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.isFlat = true
            completionBlock(start)
        }
        animator.resume()
        
    }
    
    
    private func angleFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let deltaLongitude = seccond.longitude - first.longitude
        let deltaLatitude = seccond.latitude - first.latitude
        let angle = (.pi * 0.5) - atan(deltaLatitude/deltaLongitude)
        if deltaLongitude > 0 {return angle}
        else if deltaLongitude < 0 {return angle + .pi}
        else if deltaLatitude == 0 {return .pi}
        return 0.0
    }
    
    
    func bearingFromCoordinate(from first : CLLocationCoordinate2D, to seccond:CLLocationCoordinate2D) -> Double {
        let pi = Double.pi
        let lat1 = first.latitude*pi/180.0
        let long1 = first.longitude*pi/180.0
        let lat2 = seccond.latitude*pi/180.0
        let long2 = seccond.longitude*pi/180.0
        
        let diffLong = long2 - long1
        let y = sin(diffLong)*cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(diffLong)
        var bearing = atan2(y, x)
        bearing = bearing.toDegree(fromRadian: bearing)
        bearing = (bearing+360).truncatingRemainder(dividingBy: 360)
        return bearing
    }
    
    
    private func getRotation(fraction:Double, start: Double, end: Double) -> Double{
        let normailizedEnd = end - start
        let normailizedEndAbs = ((normailizedEnd+360)).truncatingRemainder(dividingBy: 360)
        let direction = (normailizedEndAbs > 180) ? -1 : 1 //-1 for anticlockwise and 1 for closewise
        let rotation = (direction > 0) ? normailizedEndAbs : (normailizedEndAbs-360)
        let result = fraction*rotation+start
        let finalResult = (result+360).truncatingRemainder(dividingBy: 360)
        return finalResult
    }
    
    private func getInterpolation(fraction:Double, startPoint:CLLocationCoordinate2D, endPoint:CLLocationCoordinate2D) -> CLLocationCoordinate2D{
        let latitude = (endPoint.latitude - startPoint.latitude) * fraction + startPoint.latitude
        var longDelta = endPoint.longitude - startPoint.longitude
        if abs(longDelta) > 180{
            longDelta -= longDelta*360
        }
        let longitude = (longDelta*fraction) + startPoint.longitude
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension HomeViewController: CheckingRidesViewControllerDelegate, RideScheduledViewControllerDelegate{
    

    func verifyPaymentMethodsAdded(){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message)  in
            if success{
                if let someCards = resCards{
                    if someCards.count == 0{
                        AppSettings.shared.hideLoader()
                        self.navigateToAddNewCard()
                    }else{
//                        self.checkPointForBooking(isCashSelected: false)
                        self.checkPointForBooking(paymentType: SelectPaymentOption.byWallet.rawValue)

                    }
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func confirmRideRequest(paymentType:String,completion:@escaping (Ride?)->Void) {

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        //
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rideRequest(self.user.ID,carCategory:self.selectedCategory.ID, pickUpAddress: self.pickUpAddress!, pickUpCoordinates: self.pickUpCoordinates!, dropOffAddress: dropOffAddress!, dropOffCoordinates: self.dropOffCoordinates!, time: self.time, distance: distance,scheduledDate: self.ridingDate, paymentType: paymentType, offerCode: self.selectOffer.coupanCode) { (errorCode,resRide,message) in
            AppSettings.shared.hideLoader()
            if errorCode == .success{
                if let aRide = resRide{
                    self.ride = aRide
                    if self.ride.rideType == .scheduled{
                        self.showRideScheduled()
                    }else{
                        self.showChekingNearByDrivers(rideId: self.ride.ID)
                        self.removeActionCable()
                        if self.refreshTime != nil{
                            self.refreshTimer?.invalidate()
                            self.refreshTimer = nil
                        }
                        
                        self.isPresentConfirmRideView = false
                        self.selectOffer = OfferModel()
                        self.selectOfferIndex = -1

                    }
                    completion(aRide)
                }else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)

                }
            }else if errorCode == .previousDues{

                self.isPresentConfirmRideView = false
                
//                self.selectOffer = OfferModel()
//                self.selectOfferIndex = self.selectOfferIndex


                guard let aRide = resRide else{
                    completion(nil)
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    return
                }
                self.ride = aRide
                let amountDue = Rs + String(format: "%0.2f", self.ride.cost.totalPrice)
                let paymentMessage = "You have a ride that is not paid".localizedString + " \(amountDue)" + ", please complete your dues first to book another ride".localizedString
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.pendingRideTitle, message: paymentMessage,completionBlock: {
                    
                    let status = self.ride.getCurrentStatus()
                    if status == .paymentPending{
//                        self.navigationController?.popToRoot(true)
                        self.proceedToPayFor(ride: self.ride)
                    }
                })
                completion(nil)

            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)

            }
        }
    }
    
    func proceedToPayFor(ride:Ride){
//        let payForRide = AppStoryboard.Billing.viewController(PaymentOptionViewController.self)
//        payForRide.user = self.user
//        payForRide.ride = ride
//        self.navigationController?.pushViewController(payForRide, animated: true)
    }

    func proceedToScheduleRideFor(ride:Ride){
//        let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
//        billingVC.ride = ride
//        self.navigationController?.pushViewController(billingVC, animated: true)
    }

    @IBAction func onShowFareBreakUps(_ sender: UIButton){
//        let offersVC = AppStoryboard.Wallet.viewController(OffersListViewController.self)
//         self.navigationController?.pushViewController(offersVC, animated: true)
        

            if (self.approxFares.count != 0){
                if self.approxFares.count == self.carCategories.count{
                    var temp:Bool = false
                    if selectOfferIndex == -1{
                       temp = false
                    }else{
                        temp = true

                    }

                    self.showFareBreakUpsDialog(approxFare: self.approxFares[self.selectedCategoryIndex], isTapFairDetail: temp)
                }
            }



    }
    

    func showFareBreakUpsDialog(approxFare: ApproxFare,isTapFairDetail:Bool) -> Void {
        let fareBreakUpsVC = AppStoryboard.Booking.viewController(FareBreakUpsViewController.self)
        fareBreakUpsVC.approxFare = approxFare
        fareBreakUpsVC.isTapFairDetail = isTapFairDetail
        fareBreakUpsVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        let nav = UINavigationController(rootViewController: fareBreakUpsVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    func navigateToAddNewCard(){
//        let addNewPaymentOptionVC = AppStoryboard.MyCards.viewController(AddCardViewController.self)
//        addNewPaymentOptionVC.shouldBackToBooking = true
//        addNewPaymentOptionVC.delegate = self
//        let nav = AppSettings.shared.getNavigation(vc: addNewPaymentOptionVC)
//        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        nav.navigationBar.isHidden = false
//        self.present(nav, animated: true, completion: nil)
    }




    func checkingRidesViewController(viewController: CheckingRidesViewController, shouldGotoTrackRideWithRide ride: Ride) {
        viewController.dismiss(animated: false, completion: nil)
        self.openRideTracking(ride: ride)
    }

    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRoot(true)
    }

    func openRideTracking(ride:Ride){
        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
        rideTrackingVC.ride = ride
        rideTrackingVC.fromHistory = false
        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
    }

    func cancelRide(rideID: String,reason: String) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        RideService.sharedInstance.cancelRide(self.ride.ID, reason: reason) { (success,resRide,message)  in
            if success{
                if let aRide = resRide{
                    self.ride = aRide
                    NotificationCenter.default.post(name: .RIDE_CANCELLED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    self.ride = Ride()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func showChekingNearByDrivers(rideId:String) -> Void {
        let checkingVC = AppStoryboard.Booking.viewController(CheckingRidesViewController.self)
        checkingVC.delegate = self
        checkingVC.rideID = rideId
        checkingVC.pickupAddress = self.pickUpAddress
        let nav = AppSettings.shared.getNavigation(vc: checkingVC)
        nav.navigationBar.isHidden = true

        self.present(nav, animated: false, completion: {})
    }

    func showRideScheduled() -> Void {
        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
        rideScheduledVC.delegate = self
        self.present(rideScheduledVC, animated: false, completion: {})
    }

}






extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print_debug("Unable to find style.json")
            }
        } catch {
            print_debug("One or more of the map styles failed to load. \(error)")
        }
    }
}

class DriverModel {

    var id: String?
    var latitude: Double?
    var longitude: Double?

    init(id: String?, latitude: Double?, longitude: Double?){
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
    }
}


extension HomeViewController {
    
    func setupClient() -> Void {
        
        self.client.willConnect = {
            print_debug("Will Connect")
        }
        
        self.client.onConnected = {
            print_debug("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
//            self.setupClient()
            self.client.connect()

        }
        
        self.client.willReconnect = {
            print_debug("Reconnecting to \(self.client.url)")
            return true
        }
        
        let room_identifier = ["room_id" : "User_\(self.user.ID)","customer_id": "\(self.user.ID)","role":"user"] as [String : Any]
        self.channel = client.create(HomeViewController.ChannelIdentifier, identifier: room_identifier, autoSubscribe: true, bufferActions: true)
        
        self.channel?.onSubscribed = {
            print_debug("Subscribed to \(HomeViewController.ChannelIdentifier)")
        }
        
        
        guard let pickLocation = self.pickUpCoordinates else {return}
        let userLocation = CLLocation(latitude: pickLocation.latitude, longitude: pickLocation.longitude)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.getCarCategoriesFromServer(userLocation, shouldRefreshOnlyCategories: false, shouldRefreshApproxFare: true)

        }

        
        self.channel?.onReceive = {(data: Any?, error: Error?) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            //            let JSONObject = JSON(data!)
            //            print("json>>>\(JSONObject)")
            do {
                let JSONObject = JSON(data!)
                
                //let json = try JSON.init(data: data as! Data)
                let driverParser = DriverParser(json: JSONObject)
                self.markerParser = driverParser
                self.checkObjectTypeFromJson(from: JSONObject)
                print_debug("jsonBroadCastFromHomePAge>>>>\(JSONObject)")
                
            } catch let error {
                print_debug("Failed to load: \(error.localizedDescription)")
            }
            
        }
        
        
        
        self.client.connect()
    }
    
    
    
    func checkObjectTypeFromJson(from json: JSON) {
        if (json["drivers"].dictionaryObject as Dictionary<String,AnyObject>?) != nil{
            self.updateActionMarlerHandler()
            
        }else if (json["drivers"].arrayObject as? [[String:AnyObject]]) != nil {
            self.firstTimeAddMArker()
            
        }
        
        
    }
    
    func updateActionMarlerHandler(){
        let driver = self.markerParser.driver
        
        if driver.isAddedDriver {
            if self.isDriverMarkersContains(key: driver.driverID){
                let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                self.updateDriverMarker(key: driver.driverID, newPosition: position)
            }else{
                //self.getCarCategoriesFromServer(CLLocation(latitude: self.pickUpCoordinates.latitude, longitude: self.pickUpCoordinates.longitude), shouldRefreshOnlyCategories: true,shouldRefreshApproxFare: false)

                let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                let aDriver = ActionCableDriver()
                aDriver.driverID = driver.driverID
                aDriver.latitude = position.latitude
                aDriver.longitude = position.longitude
                aDriver.previousLatitude = position.latitude
                aDriver.previousLongitude = position.longitude
                
                let driverMarker = DriverMarker(driver: aDriver)
                driverMarker.position = position
                driverMarker.appearAnimation = .pop
                if isDebugEnabled{
                    driverMarker.title = aDriver.driverID
                    driverMarker.isTappable = true
                }
                if self.mapView != nil{
                    driverMarker.map = self.mapView
                }
                self.driverMarkers.append(driverMarker)
                self.resetBounds()
            }
        }else{
            if self.isDriverMarkersContains(key: driver.driverID){
               // self.getCarCategoriesFromServer(CLLocation(latitude: self.pickUpCoordinates.latitude, longitude: self.pickUpCoordinates.longitude), shouldRefreshOnlyCategories: true,shouldRefreshApproxFare: false)

                self.removeDriverMarker(key: driver.driverID)
                self.resetBounds()
            }
        }
        
    }
    
    
    
    func firstTimeAddMArker(){
       // self.markerParser.drivers.removeAll()
        let drivers = self.markerParser.drivers
        if drivers.isEmpty {
            //self.driverMarkers.removeAll()
            self.removeAllDriverMarker()
            return
        }
        for driver in drivers {
            if driver.isAddedDriver {
                if self.isDriverMarkersContains(key: driver.driverID){
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    self.updateDriverMarker(key: driver.driverID, newPosition: position)
                }else{
                    self.getCarCategoriesFromServer(CLLocation(latitude: self.pickUpCoordinates.latitude, longitude: self.pickUpCoordinates.longitude), shouldRefreshOnlyCategories: true,shouldRefreshApproxFare: false)
                    let position = CLLocationCoordinate2D(latitude: driver.latitude.rounded(toPlaces: 7), longitude: driver.longitude.rounded(toPlaces: 7))
                    let aDriver = ActionCableDriver()
                    aDriver.driverID = driver.driverID
                    aDriver.latitude = position.latitude
                    aDriver.longitude = position.longitude
                    aDriver.previousLatitude = position.latitude
                    aDriver.previousLongitude = position.longitude
                    
                    let driverMarker = DriverMarker(driver: aDriver)
                    driverMarker.position = position
                    driverMarker.appearAnimation = .pop
                    if isDebugEnabled{
                        driverMarker.title = aDriver.driverID
                        driverMarker.isTappable = true
                    }
                    if self.mapView != nil{
                        driverMarker.map = self.mapView
                    }
                    self.driverMarkers.append(driverMarker)
                    self.resetBounds()
                }
            }else{
                self.getCarCategoriesFromServer(CLLocation(latitude: self.pickUpCoordinates.latitude, longitude: self.pickUpCoordinates.longitude), shouldRefreshOnlyCategories: true,shouldRefreshApproxFare: false)
                if self.isDriverMarkersContains(key: driver.driverID){
                    self.removeDriverMarker(key: driver.driverID)
                    self.resetBounds()
                }
            }
        }
        
    }
    
    
    
    
    func removeUpdateActionMarkerHandler() {
        let driver = self.markerParser.driver
        if self.isDriverMarkersContains(key: driver.driverID){
            self.removeDriverMarker(key: driver.driverID)
            self.resetBounds()
        }
        
    }
    
    
    
}


extension HomeViewController: SelectDefaultPayViewControllerDelegate {
    func selectPay(_ viewController: SelectDefaultPayViewController, didSelectIndex: Int, didSuccess success: Bool) {
        if success {
            if didSelectIndex == 0 {
//                self.verifyPaymentMethodsAdded()
                self.checkPointForBooking(paymentType: SelectPaymentOption.byWallet.rawValue)


            }else{
                self.checkPointForBooking(paymentType: SelectPaymentOption.byCash.rawValue)

            }
            
        }
    }
    
    
}



extension HomeViewController :EditDropAddressViewControllerDelegate {
    func editDropAddressViewControllerr(viewController: EditDropAddressViewController, didSelect dropCoordinate: CLLocationCoordinate2D, dropAddress: String) {
        
    }
    
    func editPickAndDropAddressAddressViewControllerr(viewController: EditDropAddressViewController, didPickSelect pickCoordinate: CLLocationCoordinate2D, pickAddress: String, didDropSelect dropCoordinate: CLLocationCoordinate2D, dropAddress: String) {
        viewController.dismiss(animated: true, completion: nil)
        if pickUpCoordinates == nil && dropOffCoordinates == nil{
            return
        }
        self.pickUpCoordinates = pickCoordinate
        self.pickUpAddress = pickAddress
        self.dropOffCoordinates = dropCoordinate
        self.dropOffAddress = dropAddress
        self.approxFares.removeAll()
        self.setAddressInfo()

    }
    
    
}

extension Double {
    // Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



extension HomeViewController:ScheduleBookingViewControllerDelegate{
    func selectScheduleDate(_ viewController: ScheduleBookingViewController, didSelectDate date: Date, didsuccess success: Bool, didSelectPaymentOption paymentOption: String) {
        viewController.dismiss(animated: false, completion: nil)
        if success{
            self.ridingDate = date
            self.checkPointForBooking(paymentType: SelectPaymentOption.byCash.rawValue)
            
        }
    }

    
}
