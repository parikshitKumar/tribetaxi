//
//  CheckingRidesViewController.swift


import UIKit
import GoogleMaps
import DGActivityIndicatorView
import ActionCableClient
import SnapKit
import SwiftyJSON

protocol CheckingRidesViewControllerDelegate {
    func checkingRidesViewController(viewController: CheckingRidesViewController,shouldGotoTrackRideWithRide ride:Ride) -> Void
}

enum CheckingRidesCancelReason:String {
    case fromCloseButton = "from close button"
    case timeExceed = "time exceed"
}


enum RideTrackingKeys:String{
    case isCancelledByDriver = "is_cancelled_by_driver"
    case isReached = "is_reached"
    case isEnded = "is_ride_ended"
    case isStarted = "is_started"
}

class CheckingRidesViewController: UIViewController,DriverDetailOnConfirmRideViewDelegate {
    @IBOutlet weak var mapImageView: UIImageView!

    @IBOutlet weak var driverCheckingView: UIView!
    @IBOutlet weak var noRideView: UIView!
    @IBOutlet weak var bookingConfirmView: UIView!
    @IBOutlet weak var tipLabel: UILabel!
    
    @IBOutlet weak var showBookingConfirmLabel: UILabel!
    @IBOutlet weak var showBookingConfirmDescLabel: UILabel!
    
//    @IBOutlet weak var showCheckingDriverNearLabel: UILabel!
//    @IBOutlet weak var showQuickTipsLabel: UILabel!
    
    @IBOutlet weak var showNoRideLabel: UILabel!
    @IBOutlet weak var choseAnotherRideLabel: UILabel!


    
    var activityIndicator : DGActivityIndicatorView!
    var delegate : CheckingRidesViewControllerDelegate?
    var pickupAddress = ""
    var rideID = ""
    var rideRequestStatus = false // please check it while deleting the group i.e. true for accepting and false for not found
    var requestTimer : Timer!
    let quickTips = ["Enjoy a trip with Tribe","You ride more, save more","Start to save money!"]

    //firebase component

    var visibleTipIndex = 0
    
    static var ChannelIdentifier = "RideChannel"
    
//    let client = ActionCableClient(url: URL(string:"ws:" + actionCableBaseUrl + "cable")!)
    let client = ActionCableClient(url: URL(string: actionCableBaseUrl + "cable")!)

    
    var channel: Channel?
    var user : User!
    
    var rideStatusParser =  StatusKeyParser()


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(startReached(_:)), name: .RIDE_ACCEPTED_NOTIFICATION, object: nil)

        self.user = User.loadSavedUser()
        self.setupClient()

        activityIndicator = DGActivityIndicatorView(type: .ballScale, tintColor: appColor.blueColor, size: self.view.frame.size.width*3/4)
        self.driverCheckingView.addSubview(activityIndicator)
        activityIndicator.center = mapImageView.center
        self.driverCheckingView.bringSubviewToFront(activityIndicator)
        activityIndicator.startAnimating()
        //self.firebaseHandling()
//        self.startTimer()
//        self.showTips()
//        self.showDriverChekingView()

    }


    @objc func startReached(_ notification: Notification){
        if self.rideID == "" {
            return
        }

        if (UIApplication.shared.keyWindow?.rootViewController?.topViewController is CheckingRidesViewController ) {
            self.navigationController?.pop(true)
            if !self.rideRequestStatus  {
                print_debug("RIDE REACHED notification is working")

                                self.rideRequestStatus = true
                               self.showBookingConfirmedRideView()
            }
            
        }
 

        
    }
    
    
    func onTap(view: DriverDetailOnConfirmRideView) {
        dismiss(animated: true, completion: nil)
    }

    @objc func showTips(){
        UIView.transition(with: self.tipLabel, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            let item = self.visibleTipIndex % self.quickTips.count
            self.tipLabel.text = self.quickTips[item].localizedString
        }) { (done) in
            self.visibleTipIndex = self.visibleTipIndex+1
            self.perform(#selector(self.showTips), with: nil, afterDelay: 4.0)
        }
    }

    func addDriverInfoView(ride:Ride){
        let driverInfoView = DriverDetailOnConfirmRideView.instanceFromNib()
        let height = self.view.frame.size.height*208/1138
        driverInfoView.driverImage.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        driverInfoView.registrationAndOTP.text = ride.car.number+"  OTP:" + "\(ride.OTP)"
        driverInfoView.driverNameLabel.text = "\(ride.driver.fullName.capitalized)" + " is on the way".localizedString

        driverInfoView.timeLabel.text = ride.createdAt

        driverInfoView.delegate = self
        driverInfoView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height)
        driverInfoView.alpha = 0.0
        driverInfoView.layoutSubviews()
        self.view.addSubview(driverInfoView)
        driverInfoView.layoutSubviews()
        self.view.bringSubviewToFront(driverInfoView)
        driverInfoView.layoutSubviews()

        let originY:CGFloat = 54
        let frame = CGRect(x: 0, y: originY, width: self.view.frame.size.width, height: height)
        self.driverCheckingView.backgroundColor = .clear
        UIView.animate(withDuration: 0.2) {
            driverInfoView.frame = frame
            driverInfoView.alpha = 1.0
            driverInfoView.setNeedsLayout()
            driverInfoView.layoutIfNeeded()
            CommonClass.makeViewCircular(driverInfoView.driverImage, borderColor: appColor.blueColor, borderWidth: 1)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.delegate?.checkingRidesViewController(viewController: self, shouldGotoTrackRideWithRide: ride)
        }
    }



    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func showDriverChekingView(){
        self.noRideView.isHidden = true
        self.bookingConfirmView.isHidden = true
        self.driverCheckingView.isHidden = false
        self.view.bringSubviewToFront(self.driverCheckingView)
    }

    func showNoRideView(){
        let driverViews = self.view.subviews.filter { (view) -> Bool in
            return view is DriverDetailOnConfirmRideView
        }
        for driverView in driverViews{
            driverView.removeFromSuperview()
        }

        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        self.bookingConfirmView.isHidden = true
        self.driverCheckingView.isHidden = true
        self.noRideView.isHidden = false
        self.showNoRideLabel.text =  "Sorry, No rides available now".localizedString
        self.choseAnotherRideLabel.text = "CHOOSE ANOTHER RIDE".localizedString
        self.view.bringSubviewToFront(self.noRideView)
    }

    func showBookingConfirmedRideView(){
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        self.driverCheckingView.isHidden = true
        self.noRideView.isHidden = true
        self.bookingConfirmView.isHidden = false
        self.showBookingConfirmLabel.text = "BOOKING CONFIRMED".localizedString
        self.showBookingConfirmDescLabel.text = "Your booking has been confirmed!".localizedString

        self.view.bringSubviewToFront(self.bookingConfirmView)
        self.getRideBookingDetails()
    }

    func getRideBookingDetails(){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        RideService.sharedInstance.getRideDetails(rideID) { (success,resRide,message)  in
            if success{
                if let ride = resRide{
                    self.addDriverInfoView(ride: ride)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
        }
    }




    func startTimer() -> Void {
        //self.requestTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(stopTimer), userInfo: nil, repeats: true)
    }

    func cancelRequestBeforeLeave(rideID:String,reason:CheckingRidesCancelReason,completionBlock: @escaping (_ finished : Bool)->Void){
        RideService.sharedInstance.cancelRide(rideID, reason: reason.rawValue,isForServerUse: true) { (success, resRide, message) in
            AppSettings.shared.hideLoader()
            completionBlock(success)
        }
    }

    @objc func stopTimer() -> Void {
        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        self.requestTimer = nil
        self.cancelRequestBeforeLeave(rideID: self.rideID, reason: .fromCloseButton, completionBlock: {(finished) in
            self.activityIndicator.stopAnimating()
        })

        self.showNoRideView()
    }

    override func viewDidLayoutSubviews() {
        activityIndicator.center = mapImageView.center
        activityIndicator.frame = mapImageView.frame
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

         // self.removeActionCable()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.setupNavigationViews()
//    }
    
    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.makeBlackBackGround()
    }
    
    
    func removeActionCable() {
        self.channel?.onUnsubscribed = {
            print("Channel has been unsubscribed!")
        }
        self.channel?.unsubscribe()
        self.client.disconnect()
        self.client.onDisconnected = {(error: ConnectionError?) in
            print("Disconected with error: \(String(describing: error))")
        }
    }

    @IBAction func onClickCloseButton(_ sender: UIButton){
        self.dismiss(animated: false, completion: nil)

        if self.requestTimer != nil{if self.requestTimer.isValid{self.requestTimer.invalidate()}}
        activityIndicator.stopAnimating()
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        self.cancelRequestBeforeLeave(rideID: self.rideID, reason: .fromCloseButton, completionBlock: {(finished) in
            if finished{
                self.removeActionCable()

                self.dismiss(animated: false, completion: nil)
            }
        })

    }

    @IBAction func onClickCloseButtonOfNoRideView(_ sender: UIButton){
        self.removeActionCable()

        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func onClickAnotherRideButtonOfNoRideView(_ sender: UIButton){
        self.removeActionCable()
        self.dismiss(animated: false, completion: nil)
    }


    
}


extension CheckingRidesViewController {
    
    func setupClient() -> Void {
        
        self.client.willConnect = {
            print_debug("Will Connect")
        }
        
        self.client.onConnected = {
            print_debug("Connected to \(self.client.url)")
        }
        
        self.client.onDisconnected = {(error: ConnectionError?) in
            print_debug("Disconected with error: \(String(describing: error))")
            //            self.setupClient()
            self.client.connect()
            
        }
        
        self.client.willReconnect = {
            print_debug("Reconnecting to \(self.client.url)")
            return true
        }
        

        //        self.channel = client.create(LandingViewController.ChannelIdentifier)
         let room_identifier = ["room_id": "Ride_\(self.rideID)","ride_id":self.rideID,"role":"user"] as [String : Any]
        print_debug("RideID>>>>\(self.rideID)")
        self.channel = client.create(CheckingRidesViewController.ChannelIdentifier, identifier: room_identifier, autoSubscribe: true, bufferActions: true)
        
        self.channel?.onSubscribed = {
            print_debug("Subscribed to \(CheckingRidesViewController.ChannelIdentifier)")
        }
        
        self.startTimer()
        self.showTips()
        self.showDriverChekingView()

        
        self.channel?.onReceive = {(data: Any?, error: Error?) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
                        let JSONObject = JSON(data!)
            print_debug("status Json\(JSONObject)")
            
          let   statusKeyParser = StatusKeyParser(json: JSONObject)
            self.rideStatusParser = statusKeyParser
            if self.rideStatusParser.isAccepted {
                if !self.rideRequestStatus {
                    print_debug("Accept Broad cast  is working")

                self.rideRequestStatus = true
               self.showBookingConfirmedRideView()
                self.removeActionCable()
                }

            }else if (self.rideStatusParser.isDriverCancelled || self.rideStatusParser.isUserCancelled){
                self.rideRequestStatus = true

                self.showNoRideView()
                self.removeActionCable()

            }
            
        }
        
        
        
        self.client.connect()
    }
    
    
    
    
   
}







