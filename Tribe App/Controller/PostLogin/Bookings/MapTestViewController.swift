//
//  MapTestViewController.swift


import UIKit
import GoogleMaps
import SDWebImage
import GooglePlaces
import Firebase
import Alamofire
import SwiftyJSON


class MapTestViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawPath()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onclickback(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    func drawPath(){
        let url = "https://nominatim.openstreetmap.org/search.php?q=A%27ali&polygon_geojson=1&format=json&limit=1"
        let path = GMSMutablePath()
        AppSettings.shared.showLoader()
        Alamofire.request(url).responseJSON { response in
            AppSettings.shared.hideLoader()
            if let value = response.result.value {
                let json = JSON(value)
                print(json)
                for data in json.arrayValue{
                    if let classOfData = data["class"].string, classOfData == "boundary"{

                        if let geoJson = data["geojson"].dictionaryObject as Dictionary<String,AnyObject>?{
                            if let mypoints = geoJson["coordinates"] as? Array<Array<Array<Double>>>{
                                if let points = mypoints.first{
                                    for point in points{
                                        let coordinate = CLLocationCoordinate2D(latitude: point[1], longitude: point[0])
                                        path.add(coordinate)
                                    }
                                    let polyline = GMSPolyline(path: path)
                                    polyline.strokeWidth = 2
                                    polyline.geodesic = true
                                    polyline.strokeColor =  UIColor.red
                                    polyline.map = self.mapView
                                }
                            }
                        }
                    }
                }


            }
        }
    }
}




