//
//  RideScheduledViewController.swift


import UIKit
protocol RideScheduledViewControllerDelegate {
    func close(viewController:RideScheduledViewController)
}

class RideScheduledViewController: UIViewController {
    @IBOutlet weak var rideScheduleLabel: UILabel!
    @IBOutlet weak var rideDescLabel: UILabel!
    @IBOutlet weak var gotITButton: UIButton!


    var delegate:RideScheduledViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rideScheduleLabel.text = "RIDE SCHEDULED".localizedString
        self.rideDescLabel.text = "Your ride has been scheduled. Driver details will be shared 10 min before your pickup time.".localizedString
        self.gotITButton.setTitle("GOT IT".localizedString, for: .normal)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickClose(_ sender: UIButton){
        delegate?.close(viewController: self)
    }

}
