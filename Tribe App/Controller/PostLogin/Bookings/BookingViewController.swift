//
//  BookingViewController.swift

let Rs = "$ "

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class BookingViewController: UIViewController, GMSMapViewDelegate{
    @IBOutlet weak var googleMap : GMSMapView!
    @IBOutlet weak var addressPickerView : AddressPickerView!


    @IBOutlet weak var totalFareLabel : UILabel!

    @IBOutlet weak var enterDropOffButton : UIButton!
    @IBOutlet weak var enterDropOffLabel : UILabel!


    @IBOutlet weak var personalButton : UIButton!
    @IBOutlet weak var paymentButton : UIButton!
    @IBOutlet weak var couponButton : UIButton!
    @IBOutlet weak var confirmButton : UIButton!

    var selectedCategory : CarCategory!

    var pickUpCoordinates: CLLocationCoordinate2D!
    var dropOffCoordinates: CLLocationCoordinate2D!
    var pickUpAddress: String!
    var dropOffAddress: String!

    var rideDate : Date?
    var rideDateStr : String?
    var estimateCost : String?


    var rideID :String!
    var ride = Ride()

    var couponCode : String?
    var time:Double = 0.0
    var distance: Double = 0.0
    var rideFare : Double?
    var user: User!
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    weak var timer: Timer?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
//        self.confirmButton.setTitle("CONFIRM BOOKING".localizedString, for: .normal)
//        self.enterDropOffLabel.text = "".localizedString
//        self.totalFareLabel.text = "".localizedString
        self.googleMap.isMyLocationEnabled = true
        self.getEstimatedPrice()
        self.viewSetup()
        self.googleMap.mapStyle(withFilename: "mapStyle", andType: "json")
        self.googleMap.delegate = self
        self.addressPickerView.isUserInteractionEnabled = false
    }

    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
       // self.drawPath(path: self.path)
    }
    override func viewWillDisappear(_ animated: Bool) {
        if self.timer != nil{
            self.timer?.invalidate()
            self.timer = nil
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.perform(#selector(drawThePath), with: nil, afterDelay: 1.0)
    }
    func verifyPaymentMethodsAdded(){
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message)  in
            if success{
                if let someCards = resCards{
                    if someCards.count == 0{
                        AppSettings.shared.hideLoader()
//                        self.navigateToAddNewCard()
                    }else{
                        self.confirmRideRequest {(resRide) in
                            //nothing to do
                        }
                    }
                }else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    @objc func drawThePath(){
        self.drawPath(path: self.path)
    }

    func drawPath(path: GMSPath){
        self.polyline.path = path
        self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        self.polyline.strokeWidth = 3.0
        self.polyline.map = self.googleMap
//        let interVal:Double = 2.0/Double(self.path.count())
//        self.timer = nil
//        self.timer = Timer.scheduledTimer(timeInterval: interVal, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
    }

    @objc func animatePolylinePath() {
        DispatchQueue.main.async {
            if (self.i < self.path.count()) {
                self.animationPath.add(self.path.coordinate(at: self.i))
                self.animationPolyline.path = self.animationPath
                self.animationPolyline.strokeColor = UIColor.black
                self.animationPolyline.strokeWidth = 3
                self.animationPolyline.map = self.googleMap
                self.i += 1
            }
            else {
                self.i = 0
                self.animationPath = GMSMutablePath()
                self.animationPolyline.map = nil
            }
        }

    }

//    func navigateToAddNewCard(){
//        let addNewPaymentOptionVC = AppStoryboard.MyCards.viewController(AddCardViewController.self)
//        addNewPaymentOptionVC.shouldBackToBooking = true
    //        addNewPaymentOptionVC.deleg as! AddCardViewControllerDelegateate = self
//        let nav = AppSettings.shared.getNavigation(vc: addNewPaymentOptionVC)
//        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        nav.navigationBar.isHidden = false
//        self.present(nav, animated: true, completion: nil)
//    }

    @objc func stopAnimation(){
//        self.addressPickerView.stopAnimatingToRipples()
//        self.addressPickerView.stopAnimatingFromRipples()
    }

    func viewSetup(){
        var titleH = self.selectedCategory.categoryName
        if self.selectedCategory.time != ""{
            titleH = "\(titleH), \(self.selectedCategory.time)"
        }
        self.navigationItem.title = titleH
        addressPickerView.fromAddressLabel.text = pickUpAddress
        self.addPickUpMarker()
        addressPickerView.toAddressLabel.text = dropOffAddress
        self.addDropOffMarker()
        refreshMarkers()
        self.resetEnterDropOffButtonTarget()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getEstimatedPrice() -> Void {
        self.totalFareLabel.text = Rs+"..."
        self.enterDropOffLabel.text = "Estimating fare.."

        self.getTravelTimeAndDistance(from: self.pickUpCoordinates, to: self.dropOffCoordinates) { (time, distance) in
            self.time = time
            self.distance = distance

            if distance == 0.0 && time == 0.0{
                self.totalFareLabel.text = "Fare couldn't calculated!"
            }else{
               self.calculateFare(distance: distance, time: time, category: self.selectedCategory, completionBlock: {(responseCost) in
                if let estcost = responseCost{
                    let totalCost = estcost
                    let str = String(format: "%.2f", totalCost)
                    self.totalFareLabel.text = Rs+str
                    self.rideFare = estcost
                    self.estimateCost = Rs+str
                    self.enterDropOffLabel.text = "Total fare"
                    self.resetEnterDropOffButtonTarget()
                }else{
                    self.totalFareLabel.text = "Fare couldn't calculated!"
                }
                })
            }
        }
    }
    
//    func drawPath(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D)
//    {
//        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
//        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
//        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
//
//        Alamofire.request(url).responseJSON { response in
//            let json = JSON(response.data!)
//            let routes = json["routes"].arrayValue
//
//            for route in routes
//            {
//                let routeOverviewPolyline = route["overview_polyline"].dictionary
//                let points = routeOverviewPolyline?["points"]?.stringValue
//                self.path = GMSPath.init(fromEncodedPath: points!)!
//                let polyline = GMSPolyline.init(path: self.path)
//                polyline.strokeWidth = 4
//                polyline.strokeColor =  UIColor.black
//                polyline.map = self.googleMap
//            }
//        }
//    }



    func calculateFare(distance: Double,time:Double,category: CarCategory, completionBlock: @escaping (Double?)->Void){
        RideService.sharedInstance.getEstimatedCostForCategory(category.ID, distance: distance, time: time) { (success, responseCost, message) in
            if success{
                if let cost = responseCost{
                    completionBlock(cost)
                }else{
                    completionBlock(nil)
                }
            }else{
                completionBlock(nil)
            }
        }
    }

    func getTravelTimeAndDistance(from pickUpCoordinate:CLLocationCoordinate2D,to dropOffCoordinate: CLLocationCoordinate2D,completion:@escaping (_ time: Double,_ distance: Double)->Void)->Void{

                let origin = "\(pickUpCoordinate.latitude),\(pickUpCoordinate.longitude)"
                let destination = "\(dropOffCoordinate.latitude),\(dropOffCoordinate.longitude)"

                let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"
                Alamofire.request(url).responseJSON { response in
                    do{
                    let json = try JSON(data: response.data!)
                    let rows = json["rows"].arrayValue
                    if rows.count > 0{
                        let arrValues = rows.first!["elements"].arrayValue.first!
                        var distance = 0.0
                        if let distanceDict = arrValues["distance"].dictionaryObject as [String:AnyObject]?{
                            if let distancef = distanceDict["value"] as? Double{
                                distance = distancef
                            }
                        }
                        var duration = 0.0
                        if let durationDict = arrValues["duration"].dictionaryObject as [String:AnyObject]?{
                            if let durationf = durationDict["value"] as? Double{
                                duration = durationf
                            }
                        }
                        completion(duration,distance)
                    }
                    }catch{NKToastHelper.sharedInstance.showErrorAlert(self,message:"Error occurred while calculating time")}
                }
    }

    func resetEnterDropOffButtonTarget()-> Void {

        self.enterDropOffButton.removeTarget(nil, action: nil, for: .allEvents)
        if estimateCost == "" {
            self.enterDropOffButton.addTarget(self, action: #selector(BookingViewController.onEnterDroppOffAddressButton(_:)), for: .touchUpInside)
        }else{
            self.enterDropOffButton.addTarget(self, action: #selector(BookingViewController.onShowFareBreakUps(_:)), for: .touchUpInside)
        }
    }

    func refreshMarkers() -> Void {
        googleMap.clear()
        self.addPickUpMarker()
        self.addDropOffMarker()
        self.resetBoundOfMap()
//        self.drawPath(startLocation: self.pickUpCoordinates, endLocation: self.dropOffCoordinates)
    }
    func addPickUpMarker() -> Void {
        self.addMarkerforCoordinate(coordinate: self.pickUpCoordinates, image: #imageLiteral(resourceName: "uber_pin_green"), address: pickUpAddress )
    }
    func addDropOffMarker() -> Void {
        self.addMarkerforCoordinate(coordinate: self.dropOffCoordinates, image: #imageLiteral(resourceName: "pin"), address: dropOffAddress )
    }
    func addMarkerforCoordinate(coordinate : CLLocationCoordinate2D?,image: UIImage,address: String) {
        if let position = coordinate{
            let driverMarker = GMSMarker(position: position)
            driverMarker.icon = image
            driverMarker.isTappable = true
            driverMarker.title = address
            driverMarker.map = self.googleMap;
        }
    }

    func resetBoundOfMap() -> Void {
        let path = GMSMutablePath()
        path.add(self.pickUpCoordinates)
        path.add(self.dropOffCoordinates)
        let bounds = GMSCoordinateBounds(path: path)
        let camera = googleMap.camera(for: bounds, insets:UIEdgeInsets(top: 55, left: 15, bottom: 15, right: 15))
        googleMap.camera = camera!;
    }

    @IBAction func onEnterDroppOffAddressButton(_ sender: UIButton){
        self.getEstimatedPrice()
    }

    @IBAction func onShowFareBreakUps(_ sender: UIButton){
        if let rFare = self.rideFare as Double?{
            self.showFareBreakUpsDialog(rideFare: rFare)
        }else{
            self.getEstimatedPrice()
        }
    }

    func showFareBreakUpsDialog(rideFare: Double) -> Void {
//        let fareBreakUpsVC = AppStoryboard.Booking.viewController(FareBreakUpsViewController.self)
//        fareBreakUpsVC.fare = rideFare
//        fareBreakUpsVC.category = self.selectedCategory
//        fareBreakUpsVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
//        let nav = UINavigationController(rootViewController: fareBreakUpsVC)
//        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        nav.navigationBar.isHidden = true
//        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func showCouponDialog() -> Void {
        NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPersonalButton(_ sender: UIButton){
    }

    @IBAction func onClickCouponButton(_ sender: UIButton){
        showCouponDialog()
    }

    @IBAction func onClicConfirmBookingButton(_ sender: UIButton){
        self.verifyPaymentMethodsAdded()

    }

    func validateParams() -> (isValidate: Bool,message:String) {
        return(true,"")
    }

    func confirmRideRequest(completion:@escaping (Ride?)->Void) {

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        //
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rideRequest(self.user.ID,carCategory:self.selectedCategory.ID, pickUpAddress: self.pickUpAddress!, pickUpCoordinates: self.pickUpCoordinates!, dropOffAddress: dropOffAddress!, dropOffCoordinates: self.dropOffCoordinates!, time: self.time, distance: distance,scheduledDate: self.rideDate, paymentType: "cash", offerCode:"") { (success,resRide,message) in
        }
    }
}


extension BookingViewController: CheckingRidesViewControllerDelegate,RideScheduledViewControllerDelegate{


    func checkingRidesViewController(viewController: CheckingRidesViewController, shouldGotoTrackRideWithRide ride: Ride) {
        viewController.dismiss(animated: false, completion: nil)
       // self.openRideTracking(ride: ride)
    }

    func close(viewController: RideScheduledViewController) {
        viewController.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRoot(true)
    }

//    func openRideTracking(ride:Ride){
//        let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
//        rideTrackingVC.ride = ride
//        rideTrackingVC.fromHistory = false
//        self.navigationController?.pushViewController(rideTrackingVC, animated: true)
//    }

    func cancelRide(rideID: String,reason: String) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        RideService.sharedInstance.cancelRide(self.ride.ID, reason: reason) { (success,resRide,message)  in
            if success{
                if let aRide = resRide{
                    self.ride = aRide
                    NotificationCenter.default.post(name: .RIDE_CANCELLED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                    self.ride = Ride()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }


        }
    }

    func showChekingNearByDrivers(rideId:String) -> Void {
        let checkingVC = AppStoryboard.Booking.viewController(CheckingRidesViewController.self)
        checkingVC.delegate = self
        checkingVC.rideID = rideId
        checkingVC.pickupAddress = self.pickUpAddress
        self.present(checkingVC, animated: false, completion: {
        })
    }

    func showRideScheduled() -> Void {
        let rideScheduledVC = AppStoryboard.Booking.viewController(RideScheduledViewController.self)
        rideScheduledVC.delegate = self
        self.present(rideScheduledVC, animated: false, completion: {
        })
    }
}

