//
//  AddressPickerViewController.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 30/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import GooglePlaces
protocol AddressPickerViewControllerDelegate {
    func AddressPickerViewController(viewController:AddressPickerViewController, didSelect pickupCoordinate:CLLocationCoordinate2D?, pickupAddress:String?, dropCoordinate:CLLocationCoordinate2D?, dropAddress:String?)
}

class AddressPickerViewController: UIViewController, AddressPickerViewDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var centerImageView: UIImageView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var addressPickerView: AddressPickerView!
    @IBOutlet weak var addressHeaderView: UIView!

    var pickUpCoordinates: CLLocationCoordinate2D?
    var dropOffCoordinates: CLLocationCoordinate2D?
    var pickUpAddress: String?
    var dropOffAddress: String?
    var mapViewWillMove = false
    var mapViewWillMoveByLocationButton = false
    var mapViewMovedByGesture = false
    var selectedAddressType: AddressType = .from
    //var places = Array<Array<FavoritePlace>>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
       // self.places.append(Array<FavoritePlace>())
        //self.places.append(Array<FavoritePlace>())
//        self.getFavoritePlaces()
        _ = self.makeSearchViewControllerIfNeeded()
        self.addressPickerView.delegate = self
        self.addressPickerView.setNeedsLayout()
        self.addressPickerView.layoutIfNeeded()
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        self.mapView.delegate = self
        self.locationInitialSetup()
        self.mapView.alpha = 0.0
        self.centerImageView.alpha = 0.0
        self.doneButton.setTitle("Done".localizedString, for: .normal)
        self.addressPickerView.fromAddressLabel.text = "Enter pick up location".localizedString
        self.addressPickerView.toAddressLabel.text = "Where to?"
        
        if self.selectedAddressType == .from{
            guard let from = self.pickUpCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(from)
            guard let fromAddress = self.pickUpAddress else{return}
            self.addressPickerView.fromAddressLabel.text = fromAddress
            self.selectedAddressType = .to
        }else{
            guard let to = self.dropOffCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(to)
            guard let toAddress = self.dropOffAddress else{return}
            self.addressPickerView.toAddressLabel.text = toAddress
            self.selectedAddressType = .from
        }
    }



    func locationInitialSetup(){
        guard let from = self.pickUpCoordinates else{
            guard let to = self.dropOffCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(to)
            return
        }

        self.refreshCustomerLocationWithCoordinate(from)
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithRespectToHeight(self.doneButton, borderColor: .clear, borderWidth: 0)

        guard let headerView = addressHeaderView else {
            return
        }
//        CommonClass.makeCircularBottomRadius(self.addressPickerView, cornerRadius: 15)
//        headerView.dropShadow(shadowRadius:15,shadowOffset: CGSize(width: 0, height: 10))

        headerView.addshadow(top: false, left: false, bottom: true, right: false, shadowRadius: 10, shadowOpacity: 0.6, shadowColor: UIColor.black)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- AddressType selection handling
    func addressPickerView(addressPickerView: AddressPickerView, didTapOnAddressType addressType: AddressType) {
        self.selectedAddressType = addressType
        self.showGooglePlaceAutocomplete()
        if self.selectedAddressType == .from{
            guard let from = self.pickUpCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(from)
        }else{
            guard let to = self.dropOffCoordinates else{return}
            self.refreshCustomerLocationWithCoordinate(to)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.addButtonTapped(toTop: true)
        UIView.animate(withDuration: 1.0) {
            self.mapView.alpha = 1.0
            self.centerImageView.alpha = 1.0
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pop(false)
    }

    func releaseMapView(){
       // mapView.clear()
        //mapView.removeFromSuperview()
       // mapView = nil
        //addressPickerView.delegate = nil
//        addressPickerView.removeFromSuperview()
//        addressHeaderView.removeFromSuperview()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.removeButtonTapped()
    }
    @IBAction func onClickDoneButton(_ sender: UIButton){
        if self.shouldMoveForward(){
            let homeVC = AppStoryboard.Booking.viewController(HomeViewController.self)
            homeVC.pickUpCoordinates = self.pickUpCoordinates!
            homeVC.dropOffCoordinates = self.dropOffCoordinates!
            homeVC.pickUpAddress = self.pickUpAddress!
            homeVC.dropOffAddress = self.dropOffAddress!
            releaseMapView()
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }

    func getRecentVC() -> RecentSearchViewController? {
        let currentPullUpController = children
            .filter({ $0 is RecentSearchViewController })
            .first as? RecentSearchViewController
        if let currentPullUpController = currentPullUpController {
            return currentPullUpController
        }else{return nil}
    }

    private func makeSearchViewControllerIfNeeded() -> RecentSearchViewController {
        let currentPullUpController = children
            .filter({ $0 is RecentSearchViewController })
            .first as? RecentSearchViewController
        if let currentPullUpController = currentPullUpController {
            return currentPullUpController
        } else {
            return AppStoryboard.Booking.viewController(RecentSearchViewController.self)
        }
    }


    private func addPullUpController(toTop:Bool=false) {
        let pullUpController = makeSearchViewControllerIfNeeded()
        //pullUpController.places = self.places
        pullUpController.delegate = self
        addPullUpController(pullUpController, animated: true)
        if toTop{
            pullUpController.pullUpControllerMoveToVisiblePoint(pullUpController.pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
            pullUpController.updatePreferredFrameIfNeeded(animated: true)
        }
        self.view.bringSubviewToFront(self.addressHeaderView)
    }

    private func addButtonTapped(toTop:Bool=false) {
        self.doneButton.isHidden = true

        guard children.filter({ $0 is RecentSearchViewController }).count == 0 else {
            if toTop{
                let pullUpController = makeSearchViewControllerIfNeeded()
                //pullUpController.places = self.places
                pullUpController.delegate = self
                pullUpController.pullUpControllerMoveToVisiblePoint(pullUpController.pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
                pullUpController.updatePreferredFrameIfNeeded(animated: true)
            }
            return
        }
        addPullUpController(toTop: toTop)
    }

    private func removeButtonTapped() {
        let pullUpController = makeSearchViewControllerIfNeeded()
        removePullUpController(pullUpController, animated: true)
    }
}

extension AddressPickerViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.doneButton.isUserInteractionEnabled = false
        self.doneButton.alpha = 0.5
        if gesture{
            self.removeButtonTapped()
            self.doneButton.isHidden = false
        }
        let fromLocation = mapViewWillMoveByLocationButton
        mapViewWillMove = gesture || fromLocation
        self.mapViewWillMoveByLocationButton = false
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.doneButton.isUserInteractionEnabled = true
        self.doneButton.alpha = 1.0
        if !self.mapViewWillMove{
            self.mapViewWillMove = true
            return
        }
        if self.selectedAddressType == .to{
            dropOffCoordinates = position.target
            self.addressPickerView.toAddressLabel.text = "Getting address..".localizedString
            self.nameOfPlaceWithCoordinates(coordinate: position.target, complition: { (address) in
                self.dropOffAddress = address
                self.addressPickerView.toAddressLabel.text = address
            })

        }else{
            pickUpCoordinates = position.target
            //reset time to empty in categories array
            self.addressPickerView.fromAddressLabel.text = "Getting address..".localizedString

            self.nameOfPlaceWithCoordinates(coordinate: position.target, complition: { (address) in
                self.pickUpAddress = address
                self.addressPickerView.fromAddressLabel.text = address
            })
        }
    }


    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.mapView.mapType = .normal
        self.mapView.camera = camera
    }

    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        var currentAddress = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                let filteredLines = lines.filter({ (line) -> Bool in
                    return !line.isEmpty
                })
                currentAddress = filteredLines.joined(separator: ",")
                complition(currentAddress)
            }
        }
    }
}



extension AddressPickerViewController: GMSAutocompleteViewControllerDelegate{

    func showGooglePlaceAutocomplete(){
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        var fullAddress = ""
        fullAddress = place.name ?? ""
        if let formattedAddress = place.formattedAddress{
            if fullAddress.isEmpty{
                fullAddress = "\(formattedAddress)"
            }else{
                fullAddress = fullAddress + ", \(formattedAddress)"
            }
        }

        if selectedAddressType == .from{
            self.pickUpAddress = fullAddress
            self.pickUpCoordinates = place.coordinate
            self.addressPickerView.fromAddressLabel.text = fullAddress
        }else{
            self.dropOffAddress = fullAddress
            self.dropOffCoordinates = place.coordinate
            self.addressPickerView.toAddressLabel.text = fullAddress
        }
        refreshCustomerLocationWithCoordinate(place.coordinate)
        self.checkAndMove()
    }

    func checkAndMove(){
        if self.shouldMoveForward(){
            let homeVC = AppStoryboard.Booking.viewController(HomeViewController.self)
            homeVC.pickUpCoordinates = self.pickUpCoordinates!
            homeVC.dropOffCoordinates = self.dropOffCoordinates!
            homeVC.pickUpAddress = self.pickUpAddress!
            homeVC.dropOffAddress = self.dropOffAddress!
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        NKToastHelper.sharedInstance.showErrorAlert(nil, message: error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    func formattedAddress(from place:GMSPlace){

    }

}

extension AddressPickerViewController: RecentSearchViewControllerDelegate{
    func recentSearchViewController(viewController: RecentSearchViewController, didAskToSelectLocationOnMap selectLocationOnMap: Bool) {
        self.removeButtonTapped()
    }

    func recentSearchViewController(viewController: RecentSearchViewController, didRemoveFavoritePlace place: FavoritePlace) {}

    func recentSearchViewController(viewController: RecentSearchViewController, didAddFavoritePlace place: FavoritePlace) {

    }

    func recentSearchViewController(viewController: RecentSearchViewController, didSelectAddress place: FavoritePlace) {
        if self.selectedAddressType == .from{
            self.pickUpAddress = place.place
            self.pickUpCoordinates = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        }else{
            self.dropOffAddress = place.place
            self.dropOffCoordinates = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        }

        if self.shouldMoveForward(){
            let homeVC = AppStoryboard.Booking.viewController(HomeViewController.self)
            homeVC.pickUpCoordinates = self.pickUpCoordinates!
            homeVC.dropOffCoordinates = self.dropOffCoordinates!
            homeVC.pickUpAddress = self.pickUpAddress!
            homeVC.dropOffAddress = self.dropOffAddress!
            self.navigationController?.pushViewController(homeVC, animated: true)
        }

    }

    func shouldMoveForward() -> Bool {
        if self.pickUpCoordinates == nil {
            self.addressPickerView.onClickFromAddressButton(self.addressPickerView.fromAddressButton)
            return false
        }
        if self.pickUpAddress == nil {
            self.addressPickerView.onClickFromAddressButton(self.addressPickerView.fromAddressButton)
            return false
        }
        if self.dropOffCoordinates == nil {
            self.addressPickerView.onClickToAddressButton(self.addressPickerView.toAddressButton)
            return false
        }

        if self.dropOffAddress == nil {
            self.addressPickerView.onClickToAddressButton(self.addressPickerView.toAddressButton)
            return false
        }

        return ((self.pickUpCoordinates != nil) && (self.dropOffCoordinates != nil) && (self.pickUpAddress != nil) && (self.dropOffAddress != nil))
    }




}






