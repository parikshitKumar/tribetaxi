//
//  RecentSearchViewController.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 30/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//




import UIKit
import GooglePlaces
protocol RecentSearchViewControllerDelegate {
    func recentSearchViewController(viewController: RecentSearchViewController, didAskToSelectLocationOnMap selectLocationOnMap:Bool)
    func recentSearchViewController(viewController: RecentSearchViewController, didRemoveFavoritePlace place:FavoritePlace)
    func recentSearchViewController(viewController: RecentSearchViewController, didAddFavoritePlace place:FavoritePlace)
    func recentSearchViewController(viewController: RecentSearchViewController, didSelectAddress place:FavoritePlace)
}

class RecentSearchViewController: PullUpController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var topView: UIView!
    @IBOutlet private weak var secondView: UIView!
    var delegate: RecentSearchViewControllerDelegate?

    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero
    var isLoadingNewData = true
    var titleView : NavigationTitleView!

    var places = [Array<FavoritePlace>(),Array<FavoritePlace>()]{
        didSet{
            if self.tableView != nil{
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.registerCells()
        let navH: CGFloat = (UIDevice.isIphoneX ? 104 : 64)
        let minusH = 80+navH
        let totalH = UIScreen.main.bounds.height - minusH
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: totalH)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)
        tableView.attach(to: self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        willMoveToStickyPoint = { point in}
        didMoveToStickyPoint = { point in}
        onDrag = { point in }
        self.getFavoritePlaces()
        self.perform(#selector(updateFrames), with: nil, afterDelay: 0.5)
    }

    func registerCells(){
        self.tableView.register(UINib(nibName: "RecentPlaceCell", bundle: nil), forCellReuseIdentifier:"RecentPlaceCell")
        self.tableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
    }


    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.cornerRadius = 12
        let navH: CGFloat = (UIDevice.isIphoneX ? 84 : 64)
        let minusH = 80+navH
        let totalH = UIScreen.main.bounds.height - minusH

        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: totalH)
        self.view.addshadow(top: true, left: true, bottom: false, right: true)
        self.updatePreferredFrameIfNeeded(animated: true)
    }

    @objc func updateFrames(){
        self.updatePreferredFrameIfNeeded(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.updatePreferredFrameIfNeeded(animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        self.setupLeftBarButtons()
    }

    func setNavigationTitle() {
        self.navigationItem.title = ""
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x:-20, y: 0, width: self.view.frame.size.width - 120, height: 44)
        titleView.titleLabel.textAlignment = .left
        titleView.titleLabel.text = "Title"
        titleView.titleLabel.textColor = UIColor.black
        
        
    }
    
    func setupLeftBarButtons() {
        
        self.setNavigationTitle()
        let backButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        backButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        let origImage = UIImage(named: "back")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(tintedImage, for: .normal)
        backButton.addTarget(self, action: #selector(onclickBackButton(_:)), for: .touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        
        let titleItem = UIBarButtonItem(customView: titleView)
        
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        
        self.navigationItem.setLeftBarButtonItems([backBarButton, titleItem], animated: false)
    }
    
    @IBAction func onclickBackButton(_ sender : UIButton){
        
        self.navigationController?.pop(true)
    }

    // MARK: - PullUpController

    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }

    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return landscapeFrame
    }

    override var pullUpControllerPreviewOffset: CGFloat {
        return UIScreen.main.bounds.height/4
    }

    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        let navH: CGFloat = (UIDevice.isIphoneX ? 84 : 64)
        let minusH = 80+navH
        let totalH = UIScreen.main.bounds.height - minusH
        return [totalH]
    }

    override var pullUpControllerIsBouncingEnabled: Bool {
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension RecentSearchViewController: UITableViewDataSource, UITableViewDelegate,MakeAddressFavoriteViewControllerDelegate {
    func makeAddressFavoriteViewController(viewController: MakeAddressFavoriteViewController, didMakeAddressFavorites favPlace: FavoritePlace) {
        self.places[1].append(favPlace)
        for place in self.places[0]{
            if place.id == favPlace.id{
                place.isFavourite = true
                break
            }
        }
        self.tableView.reloadData()
        viewController.dismiss(animated: false, completion: nil)
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return places[section].count
        }else{
            return places[section].count+1
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{return nil}
        else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 25))
//            view.backgroundColor = appColor.blackLight
            let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.size.width-15, height: 25))
            label.font = fonts.OpenSans.bold.font(.xLarge)
            label.text = "Favorites".localizedString
            label.textColor = appColor.black
            view.addSubview(label)
            return view
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            if places.first?.count != 0{
                return 25
            }else{
                return 0
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        if section == 1 && indexPath.row == places[section].count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentPlaceCell", for: indexPath) as! RecentPlaceCell
            cell.placeIcon.image = #imageLiteral(resourceName: "select_location_map")
            cell.addressTitleLabel.text = "Set Location on Map".localizedString
            cell.addressDetailsLabel.text = ""
            cell.toggleButton.isHidden = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentPlaceCell", for: indexPath) as! RecentPlaceCell
            let place = places[indexPath.section][indexPath.row]
            cell.configure(place: place)
            cell.toggleButton.addTarget(self, action: #selector(onClickToggleButton(_:)), for: .touchUpInside)
            return cell
        }
    }

    @IBAction func onClickToggleButton(_ sender: UIButton){

        if let indexPath = sender.tableViewIndexPath(self.tableView){
            let place = self.places[indexPath.section][indexPath.row]
            if place.isFavourite{
                self.removePlaceFromFavorite(place)
            }else{
                //make address favorite
                self.openMakeFavorite(place: place)
            }
        }
    }

    func openMakeFavorite(place : FavoritePlace){
        let favVC = AppStoryboard.Booking.viewController(MakeAddressFavoriteViewController.self)
        favVC.placeAddress = place.place
        favVC.location = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
        favVC.delegate = self
        favVC.place = place
        favVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let nav = AppSettings.shared.getNavigation(vc: favVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: false, completion: nil)
    }

    func getFavoritePlaces(){
        PlaceService.sharedInstance.getPlaceForUser { (success, recentPlaces, favoritePlaces, message) in
            if success{
                self.places[0].removeAll()
                self.places[1].removeAll()

                if let rPlaces = recentPlaces{
                    self.places[0].removeAll()
                    self.places[0].append(contentsOf: rPlaces)
                }

                if let fPlaces = favoritePlaces{
                    self.places[1].removeAll()
                    self.places[1].append(contentsOf: fPlaces)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
            self.tableView.reloadData()
        }
    }

    func getIndexOf(place: FavoritePlace, inCollection array : Array<FavoritePlace>) -> Int?{
        var index: Int?
        for count in 0..<array.count{
            if place.id == array[count].id{
                index = count
                break
            }
        }
        return index
    }

    func removePlaceFromFavorite(_ place: FavoritePlace){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        PlaceService.sharedInstance.removePlace(place.id) {(success, message) in
            if success{
                guard let indexInRecent = self.getIndexOf(place: place, inCollection: self.places[0]) else{
                    guard let indexInFav = self.getIndexOf(place: place, inCollection: self.places[1]) else{
                        return
                    }
                    self.places[1].remove(at: indexInFav)
                    self.tableView.reloadData()
                    return
                }

                self.places[0].remove(at: indexInRecent)
                guard let indexInFav = self.getIndexOf(place: place, inCollection: self.places[1]) else{
                    return
                }
                self.places[1].remove(at: indexInFav)
                self.tableView.reloadData()

            }
        }
    }

    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == self.places[indexPath.section].count{
            delegate?.recentSearchViewController(viewController: self, didAskToSelectLocationOnMap: true)
        }else{
            let place = self.places[indexPath.section][indexPath.row]
            delegate?.recentSearchViewController(viewController: self, didSelectAddress: place)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}


