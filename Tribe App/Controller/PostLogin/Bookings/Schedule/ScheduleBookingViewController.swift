//
//  ScheduleBookingViewController.swift
//  Tribe App
//
//  Created by Parikshit on 28/08/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

protocol ScheduleBookingViewControllerDelegate{
    func selectScheduleDate(_ viewController: ScheduleBookingViewController, didSelectDate date:Date, didsuccess success:Bool,didSelectPaymentOption paymentOption:String)
}


class ScheduleBookingViewController: UIViewController {
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var showScheduleLabel:UILabel!
    @IBOutlet weak var dateButton:UIButton!
    @IBOutlet weak var timeButton:UIButton!
    @IBOutlet weak var confirmButton:UIButton!
    @IBOutlet weak var backView:UIView!

    var dateTextField : UITextField!
    var datePicker : UIDatePicker!
    var ridingTime = Date()
    var calenderDate = Date()
    var delegate: ScheduleBookingViewControllerDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        let stringDate = CommonClass.sharedInstance.formattedDateWith(Date(), format: "E, MMM d")
        self.calenderDate = Date()
        self.dateLabel.text = stringDate
        dateTextField = UITextField()
        self.view.addSubview(dateTextField)
        self.setUpDatePicker()
    }
    
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.confirmButton, borderColor: .clear, borderWidth: 2, cornerRadius: self.confirmButton.frame.height/2)
        CommonClass.makeCircularCornerNewMethodRadius(self.backView, cornerRadius: 15.0)


    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{return}
        guard let touchedView = touch.view else{return}
        if touchedView == self.view{
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func onClickselectDateButton(_ sender:UIButton) {
        self.navigateCalenderScreen()

    }
    @IBAction func onClickselectTimeButton(_ sender:UIButton) {
        if self.calenderDate.toMillis() <= Date().toMillis() {
            //            self.datePicker.minimumDate = Date.init(timeInterval: (2*60*60 + 10*60), since: Date())
            
            self.datePicker.minimumDate = Date.init(timeInterval: (30*60 + 5*60), since: Date())
            self.datePicker.date =  Date.init(timeInterval: (30*60 + 5*60), since: Date())
            self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        }else{
            self.datePicker.minimumDate = Date.init(timeInterval: -(24*60*60), since: Date())
            self.datePicker.date =  Date.init(timeInterval: 0, since: Date())
            self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        }
        
        
        dateTextField.becomeFirstResponder()
        
    }
    
    @IBAction func onClickConfirmButton(_ sender:UIButton) {
        self.navigateToPaymentSelectOption()

    }
    
    func navigateToPaymentSelectOption() {
        let categoryFareVC = AppStoryboard.Booking.viewController(SelectDefaultPayViewController.self)
        categoryFareVC.delegate = self
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        let nav = AppSettings.shared.getNavigation(vc: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }
        
        func convrtStringToDateFromCountDown(_ dateString: String) -> Date{
            let df  = DateFormatter()
            df.locale = Locale.autoupdatingCurrent
            df.timeZone = TimeZone.autoupdatingCurrent
            df.dateFormat = "YYYY-MM-dd HH:mm:ss"
            if let date = df.date(from: dateString){
                return date
            }
            return Date()
        }
    
    
    func navigateCalenderScreen(){
        let categoryFareVC = AppStoryboard.Booking.viewController(CalenderScheduleViewController.self)
        categoryFareVC.delegate = self
        categoryFareVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let nav = UINavigationController(rootViewController: categoryFareVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    func setUpDatePicker() -> Void {
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.time
        self.datePicker.minimumDate = Date.init(timeInterval: (30*60 + 5*60), since: Date())
        self.datePicker.date =  Date.init(timeInterval: (30*60 + 5*60), since: Date())
        self.datePicker.maximumDate =  Date.init(timeInterval: 30*24*60*60, since: Date())
        self.ridingTime = Date.init(timeInterval: (30*60 + 5*60), since: Date())
        self.setTimeinDateFormat()

        self.setTimeinDateFormat()

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = appColor.blueColor
        toolBar.sizeToFit()
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = appColor.blueColor
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localizedString, style: .plain, target: self, action: #selector(HomeViewController.doneClick))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localizedString, style: .plain, target: self, action: #selector(HomeViewController.cancelClick))
        cancelButton.tintColor = UIColor.white
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dateTextField.inputView = self.datePicker
        dateTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        self.ridingTime = datePicker.date//dateFormatter1.string(from: datePicker.date)
        dateTextField.text = dateFormatter1.string(from: datePicker.date)
        self.setTimeinDateFormat()
        dateTextField.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        dateTextField.resignFirstResponder()

    }
    
    func setTimeinDateFormat(){
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "h:mm a"
        dateTextField.text = dateFormatter1.string(from: self.ridingTime)
        dateTextField.resignFirstResponder()
        let temp = dateFormatter1.string(from: datePicker.date)
        self.timeLabel.text = temp
    }
    

}


extension ScheduleBookingViewController: CalenderScheduleViewControllerDelegate{
    func selectDate(_ viewController: CalenderScheduleViewController, didSelectDate date: Date) {
        let stringDate = CommonClass.sharedInstance.formattedDateWith(date, format: "E, MMM d")
        self.dateLabel.text = stringDate
        self.calenderDate = date
        
    }
    
    
}


extension ScheduleBookingViewController: SelectDefaultPayViewControllerDelegate {
    func selectPay(_ viewController: SelectDefaultPayViewController, didSelectIndex: Int, didSuccess success: Bool) {
        viewController.dismiss(animated: false, completion: nil)
        if success {
            
                    let stringDate = CommonClass.sharedInstance.formattedDateWith(calenderDate, format: "YYYY-MM-dd")
                    let stringDate2 = CommonClass.sharedInstance.formattedDateWith(ridingTime, format: "HH:mm:ss")
                    let ridingDate = self.convrtStringToDateFromCountDown(stringDate +  " " + stringDate2)
            if didSelectIndex == 0 {
                self.delegate?.selectScheduleDate(self, didSelectDate: ridingDate, didsuccess: true, didSelectPaymentOption: SelectPaymentOption.byWallet.rawValue)

                
            }else{
                self.delegate?.selectScheduleDate(self, didSelectDate: ridingDate, didsuccess: true, didSelectPaymentOption: SelectPaymentOption.byCash.rawValue)

            }
            
        }
    }
    
    
}


