//
//  CalenderScheduleViewController.swift
//  HLS Taxi
//
//  Created by Parikshit on 27/05/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import FSCalendar

let calenderTodayColor = UIColor.init(red: 233, green: 65, blue: 77, alpha: 1.0)

protocol CalenderScheduleViewControllerDelegate{
    func selectDate(_ viewController: CalenderScheduleViewController, didSelectDate date:Date)
}


class CalenderScheduleViewController: UIViewController,FSCalendarDataSource, FSCalendarDelegate  {
    
    
    @IBOutlet weak var selectDateCalendarView: FSCalendar!
    
    var delegate: CalenderScheduleViewControllerDelegate?
    var selectDate:Date!
    
    
    
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var okButton:UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    

    var calendar:Calendar!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        calendar = Calendar(identifier: .iso8601)
        self.selectDateCalendarView.dataSource = self
        self.selectDateCalendarView.delegate = self
        self.setupCalendar()
        self.selectDate = selectDateCalendarView.today
        let stringDate = CommonClass.sharedInstance.formattedDateWith(self.selectDate, format: "E, d MMM/yyyy")
        self.showCalenderDate(date:stringDate)
        self.reloadInfoView()
        // Do any additional setup after loading the view.
    }
    
    func reloadInfoView() {
        self.okButton.setTitle("OK".localizedString, for: .normal)
        self.cancelButton.setTitle("CANCEL".localizedString, for: .normal)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func showCalenderDate(date:String) {
        if date == ""  {
            return
        }
        
        if  let firstString = date.split(separator: "/").first {
            self.dateLabel.text = String(firstString)

        }
        if let secondString = date.split(separator: "/").last {
            self.yearLabel.text = String(secondString)

        }        
        
    }
    
    
    func setupCalendar(){
        
        selectDateCalendarView.calendarHeaderView.backgroundColor = UIColor.clear
        selectDateCalendarView.appearance.headerTitleColor = appColor.red
        selectDateCalendarView.shadow = false
        selectDateCalendarView.appearance.separators = .none
        selectDateCalendarView.appearance.todayColor = appColor.blueColor
//        self.selectDateCalendarView.appearance.titleFont = UIFont(name: fonts.OpenSans.regular.rawValue, size: fontSize.extraSmall.rawValue)
        self.selectDateCalendarView.appearance.headerDateFormat = "MMMM YYYY"
//        self.selectDateCalendarView.appearance.headerDateFormat = "E"
        
       // self.selectDateCalendarView.appearance.headerTitleFont = UIFont(name: fonts.OpenSans.semiBold.rawValue, size: fontSize.large.rawValue)
        self.selectDateCalendarView.headerHeight = 50
       // self.selectDateCalendarView.appearance.weekdayFont = UIFont(name: fonts.OpenSans.regular.rawValue, size: 14)
        self.selectDateCalendarView.appearance.borderRadius = 1.0
        self.selectDateCalendarView.appearance.headerMinimumDissolvedAlpha = 0.0
        self.selectDateCalendarView.appearance.selectionColor = appColor.blueColor
        self.selectDateCalendarView.appearance.weekdayTextColor = UIColor.black
        self.selectDateCalendarView.calendarWeekdayView.backgroundColor = UIColor.white
        
        
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Calendar.current.date(byAdding: .year, value:1, to: Date())!
        //return (calendar.c.date(byAdding: .year, value: 1, to: Date())!)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            selectDateCalendarView.setCurrentPage(date, animated: true)
        }
        
        self.selectDate = date
        
        let stringDate = CommonClass.sharedInstance.formattedDateWith(date, format: "E, d MMM/yyyy")
        print("hbnyfoidmutoi>>>" + stringDate)
        self.showCalenderDate(date:stringDate)

        if self.selectDate != selectDateCalendarView.currentPage{
            self.selectDateCalendarView.appearance.todayColor = UIColor.clear
            self.selectDateCalendarView.appearance.titleTodayColor = UIColor.black
            self.selectDateCalendarView.appearance.subtitleTodayColor =  UIColor.black
        }else{
            selectDateCalendarView.appearance.todayColor = UIColor.clear
            self.selectDateCalendarView.appearance.subtitleTodayColor = UIColor.clear
            self.selectDateCalendarView.appearance.titleTodayColor = UIColor.black
            
        }

        
    }

    
    

    
    @IBAction func onClickNextButton(_ sender:UIButton){
        let currentMonth: Date? = selectDateCalendarView?.currentPage
        let nextMonth: Date? = calendar.date(byAdding: .month, value: 1, to: currentMonth!)
        selectDateCalendarView?.setCurrentPage(nextMonth!, animated: true)
    }
    
    @IBAction func onClickPreviousButton(_ sender:UIButton){
        let currentMonth: Date? = selectDateCalendarView?.currentPage
        let nextMonth: Date? = calendar.date(byAdding: .month, value: -1, to: currentMonth!)
        selectDateCalendarView?.setCurrentPage(nextMonth!, animated: true)
    }
    
    
    
    @IBAction func onClickCancel(_ sender: UIButton){
        self.dismiss(animated: true) {
            return
        }
    }
    
    @IBAction func onClickDoneButton(_ sender: UIButton){
        if self.selectDate == nil{
            
        }else{
            self.dismiss(animated: true) {
                self.delegate?.selectDate(self, didSelectDate: self.selectDate)
            }
        }
    }
    
    


}
