//
//  MakeAddressFavoriteViewController.swift


import UIKit
import CoreLocation

protocol MakeAddressFavoriteViewControllerDelegate {
    func makeAddressFavoriteViewController(viewController: MakeAddressFavoriteViewController, didMakeAddressFavorites places: FavoritePlace)
}

class MakeAddressFavoriteViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var cancelButton: UIButton!
//   @IBOutlet weak var saveButton: UIButton!


    var delegate: MakeAddressFavoriteViewControllerDelegate?
    var category:String!
    var selectedCategory: AddressCategory = .home
    var placeAddress: String!
    var location : CLLocationCoordinate2D!
    var place : FavoritePlace!
    var addressFooterView: MakeAddressFooterView!

    override func viewDidLoad() {
        super.viewDidLoad()
        category = selectedCategory.rawValue
        if self.place.placeCategory != .other{
            self.category = self.place.category.capitalized
        }
       // self.saveButton.setTitle("Save".localizedString, for: .normal)
        //self.cancelButton.setTitle("Cancel".localizedString, for: .normal)

        self.registerCells()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.resetFooterView()
        self.tableView.reloadData()
        CommonClass.makeViewCircularWithCornerRadius(self.tableView, borderColor: .clear, borderWidth: 0, cornerRadius: 5)
    }

    
    
    func resetFooterView() {
        self.addressFooterView = MakeAddressFooterView.instanceFromNib()
        self.addressFooterView.frame = CGRect(x: 0, y:0, width: tableView.frame.size.width, height:50)
        self.addressFooterView.saveButton.setTitle("Save", for: .normal)
        self.addressFooterView.cancelButton.setTitle("Cancel", for: .normal)
        self.addressFooterView.saveButton.setTitleColor(appColor.blueColor, for: .normal)
        self.addressFooterView.cancelButton.setTitleColor(appColor.blueColor, for: .normal)

        self.addressFooterView.saveButton.addTarget(self, action: #selector(onClickSave(_:)), for: .touchUpInside)
        self.addressFooterView.cancelButton.addTarget(self, action: #selector(onClickCancel(_:)), for: .touchUpInside)
        self.tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.size.width, height: CGFloat(50))
        tableView.tableFooterView = self.addressFooterView
        self.tableView.tableHeaderView = UIView(frame: CGRect.zero)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let footerView = tableView.tableFooterView!
        footerView.setNeedsLayout()
        footerView.layoutIfNeeded()
        var frame = footerView.frame
        frame.size.height = 50
        footerView.frame = frame
        tableView.tableFooterView = footerView
    }
    
    
    func registerCells(){
        self.tableView.register(UINib(nibName: "MakeAddressFavoriteCell", bundle: nil), forCellReuseIdentifier:"MakeAddressFavoriteCell")
        self.tableView.register(UINib(nibName: "EnteredAddressCell", bundle: nil), forCellReuseIdentifier:"EnteredAddressCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickCancel(_ sender: UIButton){
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func onClickSave(_ sender: UIButton){
        if self.category.trimmingCharacters(in: .whitespaces).count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter some name".localizedString)
            return
        }
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        self.addPlaceToFavorite()//(address: self.placeAddress, category: self.category, coordinate: self.location)
    }

    func addPlaceToFavorite(){//}(address:String, category:String, coordinate:CLLocationCoordinate2D){
        PlaceService.sharedInstance.updateFavoritePlace(self.place.id,category: self.category, isFavorite: true) { (success, resPlace, message) in
            if success{
                if let place = resPlace{
                    self.delegate?.makeAddressFavoriteViewController(viewController: self, didMakeAddressFavorites: place)
                }
            }
        }
    }
}
extension MakeAddressFavoriteViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 50
        }else{
            if selectedCategory == .other{
                return 110
            }else{
                return 56
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MakeAddressFavoriteCell", for: indexPath) as! MakeAddressFavoriteCell
            cell.homeButton.setTitle("Home".localizedString, for: .normal)
            cell.workButton.setTitle("Work".localizedString, for: .normal)
            cell.otherButton.setTitle("Other".localizedString, for: .normal)

            cell.delegate = self
            cell.textField.isHidden = (self.selectedCategory != .other)
            cell.textField.delegate = self
            cell.textField.addTarget(self, action: #selector(textChange(_:)), for: .editingChanged)
            cell.textField.text = self.category
            cell.selectedCategory = self.selectedCategory
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EnteredAddressCell", for: indexPath) as! EnteredAddressCell
            cell.addressLabel.text = self.placeAddress
            return cell
        }
    }
}

extension MakeAddressFavoriteViewController: MakeAddressFavoriteCellDelegate, UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let text = textField.text else {return}
        self.category = text
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else {return}
        self.category = text
    }

    @IBAction func textChange(_ textField:UITextField){
        guard let text = textField.text else {return}
        self.category = text
    }

    func makeAddressFavoriteCell(cell: MakeAddressFavoriteCell, didSelectAddressType category: AddressCategory) {
        self.selectedCategory = category
        self.category = category.rawValue
        self.tableView.reloadData()
        if category == .other{
            UIView.animate(withDuration: 0.3) {
                self.tableViewHeight.constant = 216
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.tableViewHeight.constant = 166
            }
        }

    }


}
