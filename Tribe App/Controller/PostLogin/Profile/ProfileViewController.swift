//
//  ProfileViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    let placeholders = [["Full Name","Mobile","Email"]]
    var user: User!
    var titleView : NavigationTitleView!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "MY PROFILE".localizedString
        self.user = User.loadSavedUser()
        self.registerNIbs()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationViews()
        self.user = User.loadSavedUser()
        self.tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    
    func registerNIbs(){
        tableView.register(UINib(nibName: "UserImageCell", bundle: nil), forCellReuseIdentifier: "UserImageCell")
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "AlergiesCell", bundle: nil), forCellReuseIdentifier: "AlergiesCell")
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickMenu(_ sender:UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
        NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

    }

    @IBAction func onClickEditProfileButton(_ sender:UIButton){
        let editProfileVC = AppStoryboard.Profile.viewController(EditProfileViewController.self)
        editProfileVC.delegate = self
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }

   
}

extension ProfileViewController:EditProfileViewControllerDelegate{
    

    func editProfileViewController(viewController: EditProfileViewController, didUpdateProfile user: User) {
        self.user = user
        self.tableView.reloadData()
        NotificationCenter.default.post(name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil, userInfo: ["user":user])
    }
}

extension ProfileViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return placeholders.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeholders[section].count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        }else{
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            header.headerTitleLabel.text = "Parents Details"
            header.editButton.isHidden = false
            return header
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 0 : 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = self.userCell(tableView, cellForRowAt: indexPath)
            return cell
        }else{
            let cell = self.parentCell(tableView, cellForRowAt: indexPath)
            return cell
        }
    }

    func profileCell(tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UserImageCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserImageCell", for: indexPath) as! UserImageCell
        cell.userImageView.sd_setImage(with: URL(string:self.user.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:self.user.fullName))
        cell.userNameLabel.text = self.user.fullName.capitalized
        cell.editButton.setTitle("Edit Profile".localizedString, for: .normal)
        cell.editButton.addTarget(self, action: #selector(onClickEditProfileButton(_:)), for: .touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 68
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            height = 90
        case (1,placeholders[indexPath.section].count-1):
            height = 100
        default:
            height = 68
        }
        return height
    }

    func userCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell{
        if indexPath.row == 0{
            let cell = self.profileCell(tableView: tableView, cellForRowAt: indexPath)
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = "+"+user.contact
            cell.textField.placeholder = placeholders[indexPath.section][indexPath.row].localizedString
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = user.email
            cell.textField.placeholder = placeholders[indexPath.section][indexPath.row].localizedString
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)
            return cell
        }
    }

    func setUpTextColor(textField:FloatLabelTextField){
        textField.titleTextColour = appColor.black
        textField.titleActiveTextColour = appColor.black
    }
    func setUpTextColor(textField:FloatLabelTextView){
        textField.titleTextColour = appColor.black
        textField.titleActiveTextColour = appColor.black
    }
    func parentCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = placeholders[indexPath.section][indexPath.row].localizedString.capitalized
            cell.textField.text = user.parentName.capitalized
            self.setUpTextColor(textField: cell.textField)
            cell.textField.isUserInteractionEnabled = false
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.text = "+"+user.parentCountryCode+user.parentContact
            cell.textField.placeholder = placeholders[indexPath.section][indexPath.row].localizedString.capitalized
            cell.textField.isUserInteractionEnabled = false
            self.setUpTextColor(textField: cell.textField)

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlergiesCell", for: indexPath) as! AlergiesCell
            cell.textView.hint = placeholders[indexPath.section][indexPath.row].localizedString.capitalized
            cell.textView.text = user.parentAddress
            self.setUpTextColor(textField: cell.textView)
            cell.textView.isUserInteractionEnabled = false
            return cell
        }
    }
}











