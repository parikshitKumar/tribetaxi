//
//  EditProfileViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol EditProfileViewControllerDelegate {
    func editProfileViewController(viewController: EditProfileViewController, didUpdateProfile user:User)
}

class EditProfileViewController: UIViewController {
    var imagePickerController : UIImagePickerController!
    var userImage: UIImage?
    var user: User!
    var delegate: EditProfileViewControllerDelegate?
    var titleView : NavigationTitleView!

    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var emailTextField: FloatLabelTextField!
    @IBOutlet weak var fullNameTextField: FloatLabelTextField!
    @IBOutlet weak var phoneNumberTextField: FloatLabelTextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var userNameLabel:UILabel!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.user = User.loadSavedUser()
        self.setupTextFields()
        self.setupUserDetails()
        self.addGestureRecognizer()
        self.title = "EDIT PROFILE".localizedString
        self.submitButton.setTitle("SUBMIT".localizedString, for: .normal)
        self.fullNameTextField.placeholder = "Full Name".localizedString
        self.phoneNumberTextField.placeholder = "Phone Number".localizedString
        self.emailTextField.placeholder = "Email".localizedString
        self.userNameLabel.text = self.user.fullName


    }

    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        self.userIcon.isUserInteractionEnabled = true
        self.userIcon.addGestureRecognizer(profileTapGestureRecognizer)
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        self.showAlertToChooseAttachmentOption()
    }
    
    @IBAction func onClickCameraButton(_ sender :UIButton){
        self.showAlertToChooseAttachmentOption()
    }
    
    func setupUserDetails(){
        self.userIcon.sd_setImage(with: URL(string:self.user.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:self.user.fullName))
        self.fullNameTextField.text = self.user.fullName.capitalized
        self.phoneNumberTextField.text = "+"+self.user.contact
        self.emailTextField.text = self.user.email
        CommonClass.makeViewCircular(self.userIcon,borderColor: UIColor.clear,borderWidth: 0)
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(self.userIcon,borderColor: UIColor.clear,borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.submitButton, borderColor: UIColor.clear, borderWidth: 0)

    }
    override func viewWillAppear(_ animated: Bool) {
//        CommonClass.sharedInstance.setPlaceHolder(self.fullNameTextField, placeHolderString: "Full Name".localizedString, withColor: appColor.white)
//        CommonClass.sharedInstance.setPlaceHolder(self.phoneNumberTextField, placeHolderString: "Phone Number".localizedString, withColor: appColor.)
//        CommonClass.sharedInstance.setPlaceHolder(self.emailTextField, placeHolderString: "Email".localizedString, withColor: appColor.white)

        CommonClass.makeViewCircular(self.userIcon,borderColor: UIColor.clear,borderWidth: 0)
        setupNavigationViews()

    }
    override func viewDidAppear(_ animated: Bool) {
        CommonClass.makeViewCircular(self.userIcon,borderColor: UIColor.clear,borderWidth: 0)
    }


    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    
    func setupTextFields(){
        self.phoneNumberTextField.isUserInteractionEnabled = false
        self.fullNameTextField.titleFont = fonts.OpenSans.regular.font(.small)
        self.emailTextField.titleFont = fonts.OpenSans.regular.font(.small)
        self.phoneNumberTextField.titleFont = fonts.OpenSans.regular.font(.small)

    }
    
    func setUpTextColor(textField:FloatLabelTextField){
        textField.titleTextColour = appColor.white
        textField.titleActiveTextColour = appColor.white
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
        self.userImage = self.userIcon.image
        guard let fullName = self.fullNameTextField.text else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter full name".localizedString)
            return
        }

        guard let email = self.emailTextField.text else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter email".localizedString)
            return
        }

        let validation = self.validateParams(fullName: fullName.trimmingCharacters(in: .whitespaces), email: email.removingWhitespaces())
        if !validation.success{
            NKToastHelper.sharedInstance.showErrorAlert(self, message:validation.message)
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message:warningMessage.networkIsNotConnected.messageString())
            return
        }
        self.updateUserProfile(fullName: fullName, email: email)
    }

    func validateParams(fullName: String, email: String) -> (success:Bool,message:String){
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your full name".localizedString)
        }
        if !CommonClass.validateEmail(email){
            return (false,"Please enter a valid email".localizedString)
        }
        return (true,"")
    }

    func updateUserProfile(fullName: String, email:String){
        AppSettings.shared.showLoader(withStatus: "Updating..")
        LoginService.sharedInstance.updateProfileWith(fullName, email: email, attachmentImage: self.userImage) { (success, resUser, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let aUser = resUser{
                    self.user = aUser
                    self.setupUserDetails()
                    self.delegate?.editProfileViewController(viewController: self, didUpdateProfile: aUser)
                    self.navigationController?.pop(false)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

}

extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localizedString, style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery".localizedString, style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera".localizedString, style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage = info[.editedImage] as? UIImage{
            self.userImage = tempImage
            self.userIcon.image = self.userImage
        }else if let tempImage = info[.originalImage] as? UIImage{
            self.userImage = tempImage
            self.userIcon.image = self.userImage
        }
        picker.dismiss(animated: true) {}
    }
}

