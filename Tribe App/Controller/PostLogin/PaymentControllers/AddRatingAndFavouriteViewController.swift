//
//  AddRatingAndFavouriteViewController.swift
//  Rolling Rim
//
//  Created by Parikshit on 29/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit


protocol AddRatingAndFavouriteViewControllerDelegate {
    func addRatingAndFavouriteViewController(viewController:AddRatingAndFavouriteViewController, didTapClose done:Bool)
}

class AddRatingAndFavouriteViewController: UIViewController {
    
    @IBOutlet weak  var ratingBackView:UIView!
    @IBOutlet weak  var tipBackView:UIView!
    @IBOutlet weak var favDriverButton:UIButton!

    
    var ride:Ride!
    var user: User!
    var delegate: AddRatingAndFavouriteViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()

        self.tipBackView.isHidden = true
        self.ratingBackView.isHidden = false
        self.setRating(rating: Int(self.ride.rating))
        
//        if self.ride.driver.isFavouriteDriver {
//            self.favDriverButton.setImage(UIImage(named: "favorite_sel"), for: .normal)
//        }else{
//            self.favDriverButton.setImage(UIImage(named: "favourite-1"), for: .normal)
//
//        }
        

        // Do any additional setup after loading the view.
    }
    

    @IBAction func OnClickDoneButton(_ sender:UIButton)  {
        if !self.ratingBackView.isHidden {
            if !self.ride.driver.isFavouriteDriver{
            self.ratingBackView.isHidden = true
            self.tipBackView.isHidden = false
                
            }else{
                self.dismiss(animated: true, completion: nil)

            }
        }else{
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func OnClickCloseButton(_ sender:UIButton)  {
        
        if !self.ratingBackView.isHidden {
            if !self.ride.driver.isFavouriteDriver{
                self.ratingBackView.isHidden = true

                self.tipBackView.isHidden = false
                
            }else{
                self.dismiss(animated: true, completion: nil)
                
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }

        
    }
    
    @IBAction func onClickFavDriver(_ sender:UIButton) {
//        if ride.driver.isFavouriteDriver {
//            self.addFavouriteDriver(rideId: self.ride.ID, driverID: self.ride.driver.ID, isFavourite: false)
//
//        }else{
            self.addFavouriteDriver(rideId: self.ride.ID, driverID: self.ride.driver.ID, isFavourite: true)

       // }
    }
    
    
    
    @IBAction func onClickRating(_ sender:UIButton){
        let rating = sender.tag
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rating >= button.tag)
        }
        self.rateDriver(rideId: self.ride.ID, rating: Double(rating), userID: self.user.ID)
    }
    
    private func setRating(rating:Int){
        var rate = (rating < 1) ? 1 : rating
        rate = (rating > 5) ? 5 : rating
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rate >= button.tag)
        }
    }
    
    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.ride = aRide
                    self.setRating(rating: Int(aRide.rating))
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":aRide])
                    
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve your experience with us".localizedString,completionBlock:{

                    })
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    
    func addFavouriteDriver(rideId:String,driverID:String,isFavourite:Bool){
        AppSettings.shared.showLoader(withStatus: "Loading..")

        WalletService.sharedInstance.addFavouriteDriverForUser(rideId, driverId: driverID, isFavorite: isFavourite) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
//                if !self.ride.driver.isFavouriteDriver {
                    self.ride.driver.isFavouriteDriver = true
                    self.favDriverButton.setImage(UIImage(named: "favorite_sel"), for: .normal)
                
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Successfully add favourite driver".localizedString,completionBlock:{
                    self.dismiss(animated: true, completion: nil)
                })

                
//                }else{
//                self.ride.driver.isFavouriteDriver = false
//                self.favDriverButton.setImage(UIImage(named: "favourite-1"), for: .normal)
//                NKToastHelper.sharedInstance.showErrorAlert(self, message:"Successfully unfavourite driver" )
//                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
        
    }
}
