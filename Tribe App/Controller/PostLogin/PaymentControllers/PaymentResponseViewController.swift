//
//  PaymentResponseViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol PaymentResponseViewControllerDelegate {
    func paymentResponseViewController(viewController:PaymentResponseViewController, didTapClose done:Bool)
}

class PaymentResponseViewController: UIViewController {
    var payment:Payment!
    var delegate: PaymentResponseViewControllerDelegate?
    @IBOutlet weak var amountLabel:UILabel!
    @IBOutlet weak var showPaymentSuccessLabel:UILabel!
    @IBOutlet weak var showPaymentDescLabel:UILabel!
    @IBOutlet weak var showRateHereLabel:UILabel!
    
    
    @IBOutlet weak var FirstAmountButton: UIButton!
    @IBOutlet weak var secondAmountButton: UIButton!
    @IBOutlet weak var thirdAmountButton: UIButton!
    @IBOutlet weak var completeButton: UIButton!
    
    
    var tipAmount: String = ""


    var user: User!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.showPaymentSuccessLabel.text = "PAYMENT SUCCESSFULL".localizedString
        self.showPaymentDescLabel.text = "Your payment have successfully completed".localizedString
        self.showRateHereLabel.text = "RATE HERE".localizedString
        self.amountLabel.text = Rs + String(format: "%.2f", payment.amount)
        self.setRating(rating: Int(self.payment.ride.rating))
        self.unSelectedTipButton(button: FirstAmountButton)
        self.unSelectedTipButton(button: secondAmountButton)
        self.unSelectedTipButton(button: thirdAmountButton)
        self.FirstAmountButton.setTitle("1 " + Rs , for: .normal)
        self.secondAmountButton.setTitle("2 " + Rs , for: .normal)
        self.thirdAmountButton.setTitle("5 " + Rs , for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickClose(_ sender:UIButton){
        self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
    }

    @IBAction func onClickRating(_ sender:UIButton){
        let rating = sender.tag
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rating >= button.tag)
        }
        self.rateDriver(rideId: self.payment.ride.ID, rating: Double(rating), userID: self.user.ID)
    }

    private func setRating(rating:Int){
        var rate = (rating < 1) ? 1 : rating
        rate = (rating > 5) ? 5 : rating
        for index in 1...5{
            guard let button = self.view.viewWithTag(index) as? UIButton else{return}
            button.isSelected = (rate >= button.tag)
        }
    }

    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.payment.ride = aRide
                    self.setRating(rating: Int(aRide.rating))
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":aRide])

                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve your experience with us".localizedString,completionBlock:{
//                        self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
                    })
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    
    
    func selectTipButton(button: UIButton) {
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.clipsToBounds = true
        button.layer.backgroundColor = appColor.blueColor.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        
    }
    
    func unSelectedTipButton(button: UIButton) {
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.clipsToBounds = true
        button.layer.backgroundColor = UIColor.white.cgColor
        button.layer.borderColor = appColor.blueColor.cgColor
        button.setTitleColor(appColor.blueColor, for: .normal)
        button.layer.borderWidth = 1
    }
    
    
    @IBAction func onCLickFirstAmountButton(_ sender: UIButton) {
        self.selectTipButton(button: FirstAmountButton)
        self.unSelectedTipButton(button: secondAmountButton)
        self.unSelectedTipButton(button: thirdAmountButton)
        self.tipAmount = "1"
    }
    
    @IBAction func onCLickSecondAmountButton(_ sender: UIButton) {
        self.selectTipButton(button: secondAmountButton)
        self.unSelectedTipButton(button: FirstAmountButton)
        self.unSelectedTipButton(button: thirdAmountButton)
        
        self.tipAmount = "2"
        
    }
    @IBAction func onCLickThirdAmountButton(_ sender: UIButton) {
        self.selectTipButton(button: thirdAmountButton)
        self.unSelectedTipButton(button: FirstAmountButton)
        self.unSelectedTipButton(button: secondAmountButton)
        self.tipAmount = "5"
        
    }
    @IBAction func onCLickCompleteButton(_ sender: UIButton) {
        if self.tipAmount == "" {
            return
        }
        self.payForTip(rideId: self.payment.ride.ID, amount: self.tipAmount)
    }
    
    
    func payForTip(rideId:String,amount: String) {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.addTipFromUser(rideId, amount: amount) {(success,responceMessage) in
            AppSettings.shared.hideLoader()
            if success {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Successfully tip added", completionBlock: {
                    
                    self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
                    
                })
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: responceMessage)
                
            }
            
        }
    }
    
    
}
