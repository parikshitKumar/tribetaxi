//
//  AddNewPaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe
//import CardIO

class AddNewPaymentOptionViewController: UIViewController{
    @IBOutlet weak var cardNumberTextField : BKCardNumberField!
    @IBOutlet weak var cardHolderNameTextField : UITextField!
    @IBOutlet weak var ccvTextField : UITextField!
    @IBOutlet weak var expTextField : BKCardExpiryField!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var payButton : UIButton!
//    @IBOutlet weak var payableAmountLabel : UILabel!
    @IBOutlet weak var saveCardButton : UIButton!
    
    @IBOutlet weak var showCardNumberLabel: UILabel!
    @IBOutlet weak var showCardNameLabel: UILabel!
    @IBOutlet weak var showExpireDateLabel: UILabel!
    @IBOutlet weak var showCVVLabel: UILabel!
    
    var couponCode : String?
    var user : User!
    var ride:Ride!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ENTER CARD DETAILS".localizedString
        self.showCardNumberLabel.text = "Card No.".localizedString
        self.showCardNameLabel.text = "Name on Card".localizedString
        self.showExpireDateLabel.text = "Expiration Date".localizedString
        self.showCVVLabel.text = "CVV".localizedString
        CardIOUtilities.preload()
        self.setupAllView()
        self.navigateToScanCard()
    }
    func setupAllView(){
       // self.payableAmountLabel.text = "Enter Card Details"//"$ "+String(format: "%0.2lf", self.payableAmout)
        let totalAmount = self.ride.cost.totalPrice //+ (self.ride.cost.totalPrice*10.0/100.0)
        self.payButton.setTitle("PAY" + Rs + String(format: "%0.2lf", totalAmount), for: .normal)
//        self.saveCardButton.setTitle(" Save this card for faster check out".localizedString, for: .selected)

        cardNumberTextField.showsCardLogo = true
        
        cardNumberTextField.cardNumberFormatter.groupSeparater = " "
        self.saveCardButton.isSelected = true
        self.saveCardButton.setImage(#imageLiteral(resourceName: "checkbox_sel"), for: .selected)
        self.saveCardButton.setImage(#imageLiteral(resourceName: "checkbox_unsel"), for: .normal)

        self.saveCardButton.setTitle(" Save this card for faster check out".localizedString, for: .selected)
        self.saveCardButton.setTitle(" Save this card for faster check out".localizedString, for: .normal)

        CommonClass.makeViewCircularWithCornerRadius(self.containnerView, borderColor: appColor.brown, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardNumberTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardHolderNameTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.cardHolderNameTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.ccvTextField, borderColor: appColor.brown, borderWidth: 0, cornerRadius: 1)
        self.ccvTextField.setLeftPaddingPoints(5)
        self.ccvTextField.setRightPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.expTextField, borderColor: appColor.brown, borderWidth: 0, cornerRadius: 1)
        self.expTextField.setLeftPaddingPoints(5)

    }

    override func viewDidLayoutSubviews() {
    }
    @IBAction func onToggleSaveCardForFasterCheckOut(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        //validate card params
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validCardNumber.messageString())
            self.payButton.isEnabled = true

            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.messageString())
            self.payButton.isEnabled = true

            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.messageString())

            self.payButton.isEnabled = true

            return
        }

        guard let cardNumberFormatter = self.cardNumberTextField.cardNumberFormatter else{
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.messageString())
            return
        }

        guard let cardPattern = cardNumberFormatter.cardPatternInfo else {
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter a valid card number".localizedString)
            return
        }
        guard let brand = cardPattern.companyName else {
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.messageString())
            return
        }

        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.messageString())
            self.payButton.isEnabled = true

            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.messageString())
            self.payButton.isEnabled = true

            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expMonth.messageString())
            self.payButton.isEnabled = true

            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expYear.messageString())
            self.payButton.isEnabled = true

            return
        }

        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv)
        if !inputValidation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: inputValidation.message)
            self.payButton.isEnabled = true
            return
        }

        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv)
        if !success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.messageString())
            self.payButton.isEnabled = true

            return
        }


        self.createTokenAndProceedPayWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv)
    }

    func createTokenAndProceedPayWith(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String){

        AppSettings.shared.showLoader(withStatus: "Connecting..")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV

        STPAPIClient.shared().createToken(withCard: cardParams) { (stpToken, error) in
            if error != nil{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "\(String(describing: error!.localizedDescription))")
                self.payButton.isEnabled = true
                return
            }else{
                guard let token = stpToken else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.messageString())
                    self.payButton.isEnabled = true
                    return
                }

                AppSettings.shared.updateLoader(withStatus: "Paying..")
                let totalAmount = self.ride.cost.totalPrice //+ (self.ride.cost.totalPrice*10.0/100.0)
                self.payForRide(self.user, ride: self.ride, sourceToken: token.tokenId, amount: totalAmount, couponCode: self.couponCode)
            }

        }
    }


    func validteCardInputs(_ cardNumber:String,cardHolderName:String,brand:String,expMonth: Int?,expYear:Int?,CVV:String) -> (success:Bool,message:String) {
        if cardNumber.count == 0{
            return(false,warningMessage.validCardNumber.rawValue)
        }

        if cardHolderName.count == 0{
            return(false,warningMessage.cardHolderName.rawValue)
        }

        if let expMon = expMonth{
            if (expMon < 1) || (expMon > 12){return(false,warningMessage.validExpMonth.rawValue)}
        }else{
            return(false,warningMessage.expMonth.rawValue)
        }

        if let expY = expYear{
            if (expY < Date().year){return(false,warningMessage.validExpYear.rawValue)}
        }else{
            return(false,warningMessage.expYear.rawValue)
        }

        if !brand.lowercased().contains("maestro"){
            if (CVV.count < 3) || (CVV.count > 4){return(false,warningMessage.enterCVV.rawValue)}
        }
        return(true,"")

    }

    func validateCardParams(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String) -> Bool {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        let validationState = STPCardValidator.validationState(forCard: cardParams)
        return (validationState != .invalid)
    }



    func payForRide(_ user:User,ride:Ride,sourceToken:String, amount:Double, couponCode:String? = nil){
        PaymentService.sharedInstance.payForRide(userID: user.ID, cardID: "", stripeToken: sourceToken, rideID: ride.ID, amount: amount) { (success, resPayment, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let payment = resPayment {
                    self.ride.isPaid = true
                    self.ride.paymentStatus = true
                    self.ride = payment.ride
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":payment.ride])
                    //
                    //
                    self.showPaymentSuccess(payment)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


    func showPaymentSuccess(_ payment: Payment){
        let paymentSuccessVC = AppStoryboard.Billing.viewController(PaymentResponseViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.delegate = self
        self.present(paymentSuccessVC, animated: true, completion: nil)
    }



    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickScanCard(_ sender: UIButton){
        //open card scanner screen
        self.navigateToScanCard()
    }

    func navigateToScanCard(){
        let scanCardVC = AppStoryboard.MyCards.viewController(ScanCardViewController.self)
        scanCardVC.delegate = self
        let nav = AppSettings.shared.getNavigation(vc: scanCardVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }


}

extension AddNewPaymentOptionViewController: ScanCardViewControllerDelegate{
    func cardDidScan(viewcontroller: ScanCardViewController, withCardInfo cardInfo: CardIOCreditCardInfo) {
        if let cardNumber = cardInfo.cardNumber{
            self.cardNumberTextField.cardNumber = cardNumber
        }else{
            self.cardNumberTextField.cardNumber = ""
        }
        if let ccv = cardInfo.cvv{
            self.ccvTextField.text = ccv
        }else{
            self.ccvTextField.text = ""
        }
        if let cardholderName = cardInfo.cardholderName{
            self.cardHolderNameTextField.text = cardholderName
        }else{
            self.cardHolderNameTextField.text = ""
        }

        if let expiryMonth = cardInfo.expiryMonth as UInt?{
            if expiryMonth > 0{
                self.expTextField.dateComponents.month = Int(expiryMonth)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        if let expiryYear = cardInfo.expiryYear as UInt?{
            if expiryYear > 0{
                self.expTextField.dateComponents.year = Int(expiryYear)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
    }

    func cardDidScan(viewcontroller: ScanCardViewController, isUserChooseManual wantManual: Bool) {
        if wantManual{
            self.cardNumberTextField.cardNumber = ""
            self.ccvTextField.text = ""
            self.cardHolderNameTextField.text = ""
            self.expTextField.text = ""
        }
    }

}


extension AddNewPaymentOptionViewController:PaymentResponseViewControllerDelegate{
    func paymentResponseViewController(viewController: PaymentResponseViewController, didTapClose done: Bool) {
        viewController.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRoot(true)
    }
}


