//
//  BillViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit


class BillViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var payButton: UIButton!
    var fromHistory:Bool = false
    var ride:Ride!
    var user : User!
    var titleView : NavigationTitleView!
    var tipAmount: String = ""
    var isTipSuccessFull: Bool = false
    //    let heights = [92,90,200,120,200]
    //    let includeTipAndFavDriverHeight = [92,90,200,120,100,70,320]
    //    let includeOnlyDriver = [92,90,200,120,100,170]
    //    let includeOnlyTip = [92,90,200,120,100,320]
    
    //let tipAddedHeight = [165,170,122,120,320]
    let height = [106,200,122,80,70]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.ride.isPayTip = true
        //        self.ride.driver.isFavouriteDriver = false
        self.user = User.loadSavedUser()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "TipAddTableViewCell", bundle: nil), forCellReuseIdentifier:"TipAddTableViewCell")
        //self.setRightNavigationButton()
//        self.payButton.setTitle("....", for: .normal)
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        let status = self.ride.getCurrentStatus()
//        if status != .paymentPending {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.navigateToRatingView()
//            }
//        }
    }
    
    
    func setRightNavigationButton(){
        let button = UIButton()
        button.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        button.setImage(#imageLiteral(resourceName: "support_red"), for: .normal)
        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
        let navbutton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = navbutton
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let status = self.ride.getCurrentStatus()
//        let payButtonTitle = (status == .paymentPending) ? "Pay".localizedString : "Close".localizedString
//        self.payButton.setTitle(payButtonTitle, for: .normal)
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        AppSettings.shared.hideLoader()
    }
    
    override func viewWillLayoutSubviews() {
        self.tableView.reloadData()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    override func viewDidLayoutSubviews() {
//        CommonClass.makeViewCircularWithCornerRadius(payButton, borderColor: appColor.blueColor, borderWidth: 0, cornerRadius: 4)
        
    }
    
    
    
    
    @IBAction func onClickPay(_ sender: UIButton){
        let status = self.ride.getCurrentStatus()
        if status == .paymentPending{
            self.proceedToPayFor(ride: self.ride)
        }else{
            if fromHistory{
                self.navigationController?.pop(true)
            }else{
                self.navigationController?.popToRoot(true)
            }
        }
    }
    
    func proceedToPayFor(ride:Ride){
        let payForRide = AppStoryboard.Billing.viewController(PaymentOptionViewController.self)
        payForRide.user = self.user
        payForRide.ride = ride
        self.navigationController?.pushViewController(payForRide, animated: true)
    }
    
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        supportVC.ride = self.ride
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    //
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        if fromHistory{
            self.navigationController?.pop(true)
        }else{
            self.navigationController?.popToRoot(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension BillViewController: UITableViewDataSource, UITableViewDelegate,BillingRideRatingCellDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount: Int = 0
        let status = self.ride.getCurrentStatus()
        if status  == .paymentPending {
            rowCount = 4
        }else{
            if ride.paymentType.lowercased() == "cash"  {
                rowCount = 5
            }
            else{
                rowCount = 5

//                if ride.isPayTip {
//                    rowCount = 4
//
//                }else{
//                    rowCount = 5
//                }
            }
        }
        
        return rowCount
    }
    
    
    //    func calculateRowinCell()->Int {
    //        var rowCount: Int = 0
    //
    //        if ride.isPayTip && !ride.driver.isFavouriteDriver {
    //           rowCount = 6
    //        }else if !ride.isPayTip && ride.driver.isFavouriteDriver {
    //            rowCount = 6
    //        }else if ride.isPayTip && ride.driver.isFavouriteDriver {
    //            rowCount = 5
    //        }else{
    //            rowCount = 7
    //
    //        }
    //
    //        return rowCount
    //
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
                let cell =  self.showBillCellRemoveOfferView(tableView: tableView, indexPath: indexPath)
                return cell
           
            
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingRideAddressCell", for: indexPath) as! BillingRideAddressCell
            cell.pickUpAddressLabel.text = ride.startLocation
            cell.dropAddressLabel.text = ride.dropLocation
            cell.showFromLabel.text = "From".localizedString
            cell.showToLabel.text = "To".localizedString
            cell.rideLocationLabel.text = "RIDE LOCATION".localizedString.uppercased()
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingDriverDetailCell", for: indexPath) as! BillingDriverDetailCell
            cell.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
            cell.driverNameLabel.text = self.ride.driver.fullName.capitalized
            cell.carNameLabel.text = "\(self.ride.car.name) \(self.ride.car.number.uppercased())"
            cell.showHowRideLabel.text = "HOW WAS YOUR RIDE?".localizedString.uppercased()
            
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingRideRatingCell", for: indexPath) as! BillingRideRatingCell
            cell.showRateLabel.text = "RATE US".localizedString.uppercased()
            
            if self.ride.rating >= 1.0{
                cell.rating = Int(self.ride.rating)
            }
            cell.delegate = self
            return cell
        }else if indexPath.row == 4{
 
                let  cell =   self.doneBillingCell(tableView: tableView, indexPath: indexPath)
                return cell

        }
        
        return UITableViewCell()
    }
    
    
    func showBillCellRemoveOfferView(tableView:UITableView,indexPath:IndexPath) ->UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingPriceRemoveOfferPriceCell", for: indexPath) as! BillingPriceRemoveOfferPriceCell
        let tempDate = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        let  isEmptyDate =  CommonClass.sharedInstance.splitDateInLanguage(date: tempDate)
        if isEmptyDate != "" {
            let dateAndTime = CommonClass.sharedInstance.splitBetweenDateAndTimeInLanguage(date: tempDate)
            cell.dateTimeLabel.text = dateAndTime.0
            cell.timeLabel.text = dateAndTime.1
            
        }else{
            cell.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        }
        
        //cell.paymentModeLabel.text = (ride.paymentType == payedPaymenttype.wallet.rawValue) ? "Wallet" : "Cash"
        if ride.paymentType.lowercased() == payedPaymenttype.cash.rawValue {
            cell.paymentModeLabel.text = "Cash".localizedString
        }else if ride.paymentType.lowercased() == payedPaymenttype.wallet.rawValue{
            cell.paymentModeLabel.text = "Wallet".localizedString
            
        }else{
            cell.paymentModeLabel.text = "partial".localizedString
            
        }
        
        cell.amountLabel.text = String(format: "%0.2f",self.ride.cost.totalPrice)
        CommonClass.makeCircularBottomRadius(cell.backView, cornerRadius: 15.0)
        

        return cell
    }
    
    
    func doneBillingCell(tableView:UITableView,indexPath:IndexPath) ->UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingDoneTableViewCell", for: indexPath) as! BillingDoneTableViewCell
        let status = self.ride.getCurrentStatus()
        let payButtonTitle = (status == .paymentPending) ? "Pay".localizedString.uppercased() : "Close".localizedString.uppercased()
        cell.doneButton.setTitle(payButtonTitle, for: .normal)
        cell.doneButton.addTarget(self, action: #selector(onClickPay(_:)), for: .touchUpInside)
        return cell
    }
    
    @IBAction func onClickFavouriteDrive(_ sender:UIButton) {
        self.addFavouriteDriver(rideId: self.ride.ID, driverID: self.ride.driver.ID, isFavourite: true)
        
    }
    
    func cellBillingRideRatingCell(indexPath:IndexPath,tableView:UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingRideRatingCell", for: indexPath) as! BillingRideRatingCell
        cell.showRateLabel.text = "RATE US".localizedString
        if self.ride.rating >= 1.0{
            cell.rating = Int(self.ride.rating)
        }
        cell.delegate = self
        return cell
    }
    
//    func cellTipAddTableViewCell(indexPath:IndexPath,tableView:UITableView) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TipAddTableViewCell", for: indexPath) as! TipAddTableViewCell
//        cell.enterAmountTextField.text = "Enter a amount to choose".localizedString
//        cell.showAddTheTipForLabel.text = "Add the tip for".localizedString
//        cell.paytTipButtonButton.setTitle("PAY TIP".localizedString, for: .normal)
//        cell.firstAmountButton.addTarget(self, action: #selector(firstAmountButtonSelected(_:)), for: .touchUpInside)
//        cell.secondAmountButton.addTarget(self, action: #selector(secondAmountButtonSelected(_:)), for: .touchUpInside)
//        cell.thirdAmountButton.addTarget(self, action: #selector(thirdAmountButtonSelected(_:)), for: .touchUpInside)
//        cell.paytTipButtonButton.addTarget(self, action: #selector(onClickPayTip(_:)), for: .touchUpInside)
//        cell.firstAmountButton.setTitle("1 " + Rs , for: .normal)
//        cell.secondAmountButton.setTitle("2 " + Rs , for: .normal)
//        cell.thirdAmountButton.setTitle("5 " + Rs , for: .normal)
//
//        return cell
//    }
    
    
    
    
//    @IBAction func onClickPayTip(_ sender: UIButton) {
//        if self.tipAmount == "" {
//            return
//        }
//        self.payForTip(rideId: self.ride.ID, amount: self.tipAmount)
//    }
//
//    @IBAction func firstAmountButtonSelected(_ sender: UIButton) {
//        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
//
//            if let cell = tableView.cellForRow(at: indexPath) as? TipAddTableViewCell {
//                self.selectTipButton(button: cell.firstAmountButton)
//                self.unSelectedTipButton(button: cell.secondAmountButton)
//                self.unSelectedTipButton(button: cell.thirdAmountButton)
//                self.tipAmount = "1"
//
//            }
//
//
//        }
//    }
//
//    @IBAction func secondAmountButtonSelected(_ sender: UIButton) {
//        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
//            if let cell = tableView.cellForRow(at: indexPath) as? TipAddTableViewCell {
//                self.selectTipButton(button: cell.secondAmountButton)
//                self.unSelectedTipButton(button: cell.firstAmountButton)
//                self.unSelectedTipButton(button: cell.thirdAmountButton)
//                self.tipAmount = "2"
//            }
//
//
//        }
//    }
//
//    @IBAction func thirdAmountButtonSelected(_ sender: UIButton) {
//        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
//            if let cell = tableView.cellForRow(at: indexPath) as? TipAddTableViewCell {
//                self.selectTipButton(button: cell.thirdAmountButton)
//                self.unSelectedTipButton(button: cell.secondAmountButton)
//                self.unSelectedTipButton(button: cell.firstAmountButton)
//                self.tipAmount = "5"
//            }
//
//
//        }
//    }
//
//
//
//    func selectTipButton(button: UIButton) {
//        button.layer.cornerRadius = 0.5 * button.bounds.size.width
//        button.clipsToBounds = true
//        button.layer.backgroundColor = appColor.blueColor.cgColor
//        button.setTitleColor(UIColor.white, for: .normal)
//
//    }
//
//    func unSelectedTipButton(button: UIButton) {
//        button.layer.cornerRadius = 0.5 * button.bounds.size.width
//        button.clipsToBounds = true
//        button.layer.backgroundColor = UIColor.white.cgColor
//        button.layer.borderColor = appColor.blueColor.cgColor
//        button.setTitleColor(appColor.blueColor, for: .normal)
//        button.layer.borderWidth = 1
//    }
    
    
    
    
    
    func cell(cell: BillingRideRatingCell, didRate rating: Double) {
        self.rateDriver(rideId: self.ride.ID, rating: rating, userID: self.user.ID)
    }
    
    func rateDriver(rideId:String,rating: Double,userID:String) -> Void {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.rateDriverWithRide(rideId, rating: rating, userID:userID) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aRide = responseRide{
                    self.ride = aRide
                    self.tableView.reloadData()
                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride as Any])
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Thanks for your response. It'll help us to improve for experience with us".localizedString)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight: CGFloat = 0
        let status = self.ride.getCurrentStatus()
        if status == .paymentPending {
            rowHeight = CGFloat(self.height[indexPath.row])
            
        }else{
            
            if ride.paymentType.lowercased() == "cash"{
                rowHeight = CGFloat(self.height[indexPath.row])
                
            }else{
                rowHeight = heightInEveryCell(indexPath: indexPath)
                
            }
        }
        
        return rowHeight
        
    }
    
    func heightInEveryCell(indexPath:IndexPath) ->CGFloat {
        var rowHeight: CGFloat = 0
    rowHeight = CGFloat(self.height[indexPath.row])

        
//        if ride.isPayTip {
//            rowHeight = CGFloat(self.height[indexPath.row])
//
//        }else{
//            rowHeight = CGFloat(self.tipAddedHeight[indexPath.row])
//
//        }
        
        
        return rowHeight
        
    }
    
    
    func payForTip(rideId:String,amount: String) {
        AppSettings.shared.showLoader(withStatus: "Sending..")
        RideService.sharedInstance.addTipFromUser(rideId, amount: amount) {(success,responceMessage) in
            AppSettings.shared.hideLoader()
            if success {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Successfully tip added", completionBlock: {
                    self.ride.isPayTip = true
                    self.tableView.reloadData()
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    //                    self.delegate?.paymentResponseViewController(viewController: self, didTapClose: true)
                    
                })
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: responceMessage)
                
            }
            
        }
    }
    
    
    func addFavouriteDriver(rideId:String,driverID:String,isFavourite:Bool){
        
        WalletService.sharedInstance.addFavouriteDriverForUser(rideId, driverId: driverID, isFavorite: isFavourite) { (success,responseRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                self.ride.driver.isFavouriteDriver = true
                self.tableView.reloadData()
                //                    NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: ["ride":self.ride])
                NKToastHelper.sharedInstance.showErrorAlert(self, message:"Successfully add favourite driver" )
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
        
    }
    
    
    func navigateToRatingView() {
        let ratingVC = AppStoryboard.Billing.viewController(AddRatingAndFavouriteViewController.self)
        //        categoryFareVC.delegate = self
        ratingVC.ride = self.ride
        ratingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let nav = AppSettings.shared.getNavigation(vc: ratingVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        self.present(nav, animated: true, completion: nil)
    }
    
}






