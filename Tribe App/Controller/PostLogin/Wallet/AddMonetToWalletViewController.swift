//
//  AddMonetToWalletViewController.swift
//  JO CAB
//
//  Created by Parikshit on 26/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class AddMonetToWalletViewController: UITableViewController {
    @IBOutlet weak var transactionIDTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var uploadReceiptImage: UIImageView!
    @IBOutlet weak var uploadLabel: UILabel!
    
    @IBOutlet weak var transactionIDShowLabel: UILabel!
    @IBOutlet weak var amountShowLabel: UILabel!
    @IBOutlet weak var uploadShowReceipt: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
//    @IBOutlet weak var transactionIDView:UIView!
//    @IBOutlet weak var amountView: UIView!
//    @IBOutlet wea





    var titleView : NavigationTitleView!

    var screenShot : UIImage?
    var imagePickerController : UIImagePickerController!
    
    var amount: String = ""
    var transactionID :String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.decorateView()
        self.title = "WALLET".localizedString
        amountTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
        amountTextField.delegate = self
        transactionIDTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
        transactionIDTextField.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showAlertToChooseAttachmentOption))
          self.uploadReceiptImage.isUserInteractionEnabled = true
          self.uploadReceiptImage.addGestureRecognizer(tapGestureRecognizer)
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.setRightNavigationButton()
        self.transactionIDShowLabel.text = "Transaction ID".localizedString
        self.amountShowLabel.text = "Amount".localizedString
        self.uploadShowReceipt.text = "Upload Receipt".localizedString
        self.uploadLabel.text = "Upload your Receipt here".localizedString
        self.transactionIDTextField.placeholder = "Enter Transaction ID".localizedString
        self.amountTextField.placeholder = "Enter Amount".localizedString
        self.submitButton.setTitle("SUBMIT".localizedString, for: .normal)

        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.setRightNavigationButton()
        self.decorateView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func decorateView() {
        CommonClass.sharedInstance.setPlaceHolder(self.transactionIDTextField, placeHolderString: "Enter Transaction ID".localizedString, withColor: .white)
        CommonClass.sharedInstance.setPlaceHolder(self.amountTextField, placeHolderString: "Enter Amount".localizedString, withColor: .white)
        CommonClass.makeViewCircularWithCornerRadius(self.amountTextField, borderColor:.clear, borderWidth: 1, cornerRadius: 5)
        CommonClass.makeViewCircularWithCornerRadius(self.transactionIDTextField, borderColor: .clear, borderWidth: 1, cornerRadius: 5)

    }

    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
       self.navigationController?.pop(true)
    }
    
    func setRightNavigationButton(){
        let button = UIButton()
        button.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        button.setImage(#imageLiteral(resourceName: "support_red"), for: .normal)
        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
        let navbutton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = navbutton
    }
    

    
    func setImage() {
        if let img = self.screenShot{
            self.uploadReceiptImage.image = img
            self.uploadLabel.isHidden = true

        }else{
            self.uploadLabel.isHidden = false

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    
    @IBAction func onClickSubmitButton(_ sender: UIButton) {
        self.submitButton.isEnabled = false
        if !AppSettings.isConnectedToNetwork{
            self.submitButton.isEnabled = true

            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        

        
        if transactionID.trimmingCharacters(in: .whitespaces).count == 0{
            self.submitButton.isEnabled = true

            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the TransactionID".localizedString)
            return
        }
        
        if amount.trimmingCharacters(in: .whitespaces).count == 0{
            self.submitButton.isEnabled = true

            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the Amount".localizedString)
            return
        }
        
        guard let docImage =  self.uploadReceiptImage.image else {
            self.submitButton.isEnabled = true

            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please upload Receipt".localizedString)
            return
            
        }

        self.addTranction(transactionID, Amount: amount, screenShot:docImage )
    }
    
    func addTranction(_ transactionID:String, Amount: String,screenShot: UIImage?){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        self.submitButton.isEnabled = true

        WalletService.sharedInstance.addTransaction(transactionID, amount: Amount, image: screenShot) {(success,message) in
            AppSettings.shared.hideLoader()

            if success {
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your requested has been submitted, We are verifying your request, It may take some time. Come back later to see changes in wallet.".localizedString,completionBlock: {
                    self.submitButton.isEnabled = true

                    self.navigationController?.pop(true)
                })
            }else{
                self.submitButton.isEnabled = true
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)

            }
        }

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 100
        switch indexPath.row {
        case 0:
            height = 120
        case 1:
            height = 120
        case 2:
            height = 300
        default:
            break
        }
        return height
    }
    


}


extension AddMonetToWalletViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView) as IndexPath?{
            if indexPath.row == 0 {
                self.transactionID = textField.text!
            }else{
                self.amount = textField.text!

            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.tableView) as IndexPath?{
            if indexPath.row == 0 {
                self.transactionID = textField.text!
            }else{
                self.amount = textField.text!
                
            }
        }
        
    }
    
    @objc  func textDidChanged(_ textField: UITextField) -> Void {
        if let indexPath = textField.tableViewIndexPath(self.tableView) as IndexPath?{
            if indexPath.row == 0 {
                self.transactionID = textField.text!
            }else{
                self.amount = textField.text!
                
            }
        }
        
    }
}

extension AddMonetToWalletViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @objc func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localizedString, style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)
        
        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.setImage()
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage = info[.editedImage] as? UIImage{
            screenShot = tempImage
            self.setImage()
        }else if let tempImage = info[.originalImage] as? UIImage{
            screenShot = tempImage
            self.setImage()
        }
        picker.dismiss(animated: true) {}
    }
    
    
}





