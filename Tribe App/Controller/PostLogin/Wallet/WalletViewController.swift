//
//  WalletViewController.swift
//  JO CAB
//
//  Created by Parikshit on 26/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    @IBOutlet weak var walletTableView: UITableView!
    
    var titleView : NavigationTitleView!
    var bankDetail = Bank()
    var walletBalance : Double = 0.0
    var isFromMenu : Bool = false



    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "WALLET".localizedString

        self.walletTableView.dataSource = self
        self.walletTableView.delegate = self
        self.registerCells()
        self.getUserBankDetail()
        self.setRightNavigationButton()
        self.setUpLeftBarButton()

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        setUpLeftBarButton()
        self.setRightNavigationButton()
        self.walletTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)


    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menu") : #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
        
    }
    
    @IBAction func onClickBackButton(_ sender : UIBarButtonItem){
        if !isFromMenu{
            self.navigationController?.pop(true)
        }else{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
            NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

        }
        
        
    }
    
    
    func setRightNavigationButton(){
       let  button = UIButton(frame: CGRect(x:0, y:0, width:70, height:30))
        button.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        button.setImage(#imageLiteral(resourceName: "support_red"), for: .normal)

        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
        let viewWalletBarButton = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([viewWalletBarButton], animated: false)
    }
    
    
    func registerCells(){
        self.walletTableView.register(UINib(nibName: "AddMoneyWalletTableViewCell", bundle: nil), forCellReuseIdentifier:"AddMoneyWalletTableViewCell")
        self.walletTableView.register(UINib(nibName: "SendMoneyToTableViewCell", bundle: nil), forCellReuseIdentifier:"SendMoneyToTableViewCell")
        self.walletTableView.register(UINib(nibName: "ProcessMoneyTableViewCell", bundle: nil), forCellReuseIdentifier:"ProcessMoneyTableViewCell")
        self.walletTableView.register(UINib(nibName: "WalletContactTableViewCell", bundle: nil), forCellReuseIdentifier:"WalletContactTableViewCell")
        
        
    }
    
    
    @IBAction func addMoneyToButton(_ sender: UIButton) {
        let addMoneyVC = AppStoryboard.Wallet.viewController(AddMonetToWalletViewController.self)
        self.navigationController?.pushViewController(addMoneyVC, animated: true)
    }

    
    func getUserBankDetail() {
        AppSettings.shared.showLoader(withStatus: "Loading..")
        WalletService.sharedInstance.getUserWalletDetail() { (success, resBank, wallentBalance,message) in
            AppSettings.shared.hideLoader()
            if success{
                
                if let someBank = resBank{
                    self.bankDetail = someBank
                    self.walletBalance = wallentBalance
                    self.walletTableView.reloadData()
                }
                
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension WalletViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 100
        switch indexPath.row {
        case 0:
            height = 150
        case 1:
            height = 50
        case 2:
            height = 75
        case 3:
            height = 50
        case 4:
            height = 60
        case 5:
            height = 60
        case 6:
            height = 60
        default:
            break
        }
        return height
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoneyWalletTableViewCell", for: indexPath) as! AddMoneyWalletTableViewCell
            cell.showCurrentBalanceLabel.text = "Current Balance".localizedString
            cell.showAddMoneyLabel.text = "Add Money To Wallet".localizedString
            cell.AoaNumber.text = "  " + String(format: "%0.2f", self.walletBalance) + "  "
            cell.addMoneyButton.addTarget(self, action: #selector(addMoneyToButton(_:)), for: .touchUpInside)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComanWalletTableViewCell", for: indexPath) as! ComanWalletTableViewCell
            cell.titleLabel.text = "Send Money To".localizedString
            return cell
        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "SendMoneyToTableViewCell", for: indexPath) as! SendMoneyToTableViewCell
//            cell.nameLabel.text = bankDetail.accountHolderName
//            cell.AccNumber.text = bankDetail.last4
//            cell.ifscCode.text = bankDetail.accountID
//            cell.branch.text = bankDetail.bankName
//            cell.showBankDetailLabel.text = "Bank Details".localizedString
//            cell.showNameLabel.text = "Name:".localizedString
//            cell.showAccNumberLabel.text = "Acc Number:".localizedString
//            cell.showIfscCodeLabel.text = "IFSC Code:".localizedString
//            cell.showbranchLabel.text = "Branch:".localizedString
//            return cell
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "WalletContactTableViewCell", for: indexPath) as! WalletContactTableViewCell
//            cell.countryCodeLabel.text = bankDetail.countryCode
//            cell.contactLabel.text = bankDetail.contactNumber

            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComanWalletTableViewCell", for: indexPath) as! ComanWalletTableViewCell
            cell.titleLabel.text = "Process".localizedString

            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProcessMoneyTableViewCell", for: indexPath) as! ProcessMoneyTableViewCell
            cell.processImage.image = #imageLiteral(resourceName: "add_money")
            cell.processLabel.text = "Add Money".localizedString
             cell.descriptionLabel.text = "Add Money To given Account".localizedString
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProcessMoneyTableViewCell", for: indexPath) as! ProcessMoneyTableViewCell
            cell.processImage.image = #imageLiteral(resourceName: "upload_receipt")
            cell.processLabel.text = "Upload Receipt".localizedString
            cell.descriptionLabel.text = "You need to upload receipt and transition ID".localizedString
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProcessMoneyTableViewCell", for: indexPath) as! ProcessMoneyTableViewCell
            cell.processImage.image = #imageLiteral(resourceName: "wallet_money")
            cell.processLabel.text = "Wallet".localizedString
            cell.descriptionLabel.text = "Rolling Rim will add money in to your wallet hassel free Booking".localizedString
            return cell
        default:
            break
        }
        return UITableViewCell()
        
        

    }
    
    
}
