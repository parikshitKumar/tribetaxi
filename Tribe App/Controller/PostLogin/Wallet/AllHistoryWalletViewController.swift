//
//  AllHistoryWalletViewController.swift
//  Pepel Congo
//
//  Created by Parikshit on 10/04/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class AllHistoryWalletViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var aTableView: UITableView!
    var walletHistoryArray = [WalletHistoryModel]()

    
    var page = 1
    var perPage = 50
//    var totalPage = 0
   var isLoading = false
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.aTableView.addSubview(refreshControl)
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.aTableView.register(UINib(nibName: "HistoryWalletTableViewCell", bundle: nil), forCellReuseIdentifier:"HistoryWalletTableViewCell")
        self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")

        self.loadWalletHistoryFor(self.page, perPage: self.perPage)



    }
    

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        
        self.aTableView.tableFooterView = nil
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        self.isLoading = true
        WalletService.sharedInstance.walletUserHistory(keyWord: "all", pageNumber: self.page, perPage: self.perPage) { (success, resHistory, responseMessage,resTotalPage) in
            self.isLoading = false
            self.walletHistoryArray.removeAll()
            refreshControl.endRefreshing()
//            self.totalPage = resTotalPage
            if let someHistory = resHistory{
                self.walletHistoryArray.append(contentsOf: someHistory)
            }

            self.aTableView.reloadData()
        }
    }

    

    
    func loadWalletHistoryFor(_ page: Int,perPage:Int) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        WalletService.sharedInstance.walletUserHistory(keyWord: "all", pageNumber: self.page, perPage: self.perPage) { (success, resHistory, responseMessage,resTotalPage) in
            self.isLoading = false
            if let someHistory = resHistory{
                if someHistory.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.walletHistoryArray.append(contentsOf:someHistory)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
 
            self.aTableView.reloadData()
        }
    }

    


    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.walletHistoryArray.count == 0) ? 1 : self.walletHistoryArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if self.walletHistoryArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isLoading ? "Loading..".localizedString : "No history found\nPull down to refresh".localizedString
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryWalletTableViewCell", for: indexPath) as! HistoryWalletTableViewCell
            
            let historyWallet = self.walletHistoryArray[indexPath.row]


            if historyWallet.isAdd {
                let amount = String(format: "%0.2f", historyWallet.amount)
                //if historyWallet.paymentMode.isEmpty {
                    cell.titleLabel.text = Rs + amount + " " + "added to wallet".localizedString
//                }else{
//                    cell.titleLabel.text = Rs + amount + " " + "added to wallet".localizedString + " by " + historyWallet.paymentMode
//                }
                cell.titleLabel.textColor = appColor.blueColor
                cell.amountLabel.isHidden = true
                cell.imageIcon.image = UIImage(named: "wallet")
                cell.descLabel.text = CommonClass.formattedDateWithString(historyWallet.date, format: "MMM dd,yyyy")


            }else{
                cell.titleLabel.text = "Tribe Partner"
                    cell.descLabel.text = CommonClass.formattedDateWithString(historyWallet.date, format: "MMM dd,yyyy") + "." + "Paid by wallet".localizedString

                cell.titleLabel.textColor = appColor.black
                cell.amountLabel.isHidden = false
                cell.amountLabel.text = Rs + String(format: "%0.2f", historyWallet.amount)
                cell.imageIcon.image = UIImage(named: "car")
            }
            
            
            return cell
        }
    }
    
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if AppSettings.isConnectedToNetwork{
                        isLoading = true
                        self.page+=1
                        self.loadWalletHistoryFor(self.page, perPage: self.perPage)
                    }
                }
            }
        }
        
    }

    
    
    

}
