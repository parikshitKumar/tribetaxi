//
//  HistoryWalletViewController.swift
//  Pepel Congo
//
//  Created by Parikshit on 10/04/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class HistoryWalletViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var scrolling: UIScrollView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var addedButton: UIButton!
    @IBOutlet weak var paidButton: UIButton!
    @IBOutlet weak var moovingView :UIView!
    @IBOutlet weak var backView :UIView!

    var containnerVC : YSLContainerViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "WALLET HISTORY".localizedString
        self.setupNavigationViews()
        self.setRightNavigationButton()
        self.allButton.setTitle("All".localizedString, for: .normal)
        self.addedButton.setTitle("Added".localizedString, for: .normal)
        self.paidButton.setTitle("Paid".localizedString, for: .normal)

        self.scrolling.isPagingEnabled = true
        self.scrolling.bounces = false
        self.toggleRateButton(button: allButton)
        let allVc = AppStoryboard.Wallet.viewController(AllHistoryWalletViewController.self)
        self.addChild(allVc)
        let receivedVc = AppStoryboard.Wallet.viewController(ReceivedHistoryViewController.self)
        self.addChild(receivedVc)
        let paidVc = AppStoryboard.Wallet.viewController(PaidHistoryWalletViewController.self)
        self.addChild(paidVc)
        self.perform(#selector(LoadScollView), with: nil, afterDelay: 0.5)

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        CommonClass.makeCircularBottomRadius(self.backView, cornerRadius: 15)

        self.setRightNavigationButton()
    }

    
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func setRightNavigationButton(){
        let  button = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        button.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        button.setImage(#imageLiteral(resourceName: "support_red"), for: .normal)

        button.addTarget(self, action: #selector(onClickSupport(_:)), for: .touchUpInside)
        let viewWalletBarButton = UIBarButtonItem(customView: button)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([viewWalletBarButton], animated: false)
    }
    
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Support.viewController(SupportsViewController.self)
        supportVC.isFromMenu = false
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    

    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
//        self.navigationController?.pop(true)
       // NotificationCenter.default.post(name: .USER_NEW_PROFILE_DETAIL, object: nil, userInfo: nil)

        SlideNavigationController.sharedInstance().toggleLeftMenu()
        NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

    }
    
    
    func toggleRateButton(button:UIButton){
        if button == allButton{
            self.selectButton(button: allButton)
            self.unSelectButton(button: addedButton)
            self.unSelectButton(button: paidButton)


        }else if (button == addedButton){
            self.unSelectButton(button: allButton)
            self.selectButton(button: addedButton)
            self.unSelectButton(button: paidButton)

        }else{
            self.unSelectButton(button: allButton)
            self.unSelectButton(button: addedButton)
            self.selectButton(button: paidButton)
        }
    }
    
    func selectButton(button:UIButton) {
        button.isSelected = true
        button.setTitleColor(appColor.black, for: .normal)
        button.titleLabel?.font = fonts.OpenSans.bold.font(.lagreMedium)
    }
    
    
    func unSelectButton(button:UIButton) {
        button.isSelected = false
        button.setTitleColor(appColor.gray, for: .normal)
        button.titleLabel?.font = fonts.OpenSans.regular.font(.smallMedium)

    }

}


let kScreenWidth = UIScreen.main.bounds.size.width

extension HistoryWalletViewController: UIScrollViewDelegate {
    
    @IBAction func onClickAllButton(_ sender: UIButton){
        self.toggleRateButton(button: allButton)
        self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.allButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    @IBAction func onClickAddedButton(_ sender: UIButton){
        self.toggleRateButton(button: addedButton)
        self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.addedButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    @IBAction func onClickPaidButton(_ sender: UIButton){
        self.toggleRateButton(button: paidButton)
        self.raceScrollTo(CGPoint(x:2*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.paidButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    @objc func LoadScollView() {
        scrolling.delegate = nil
        scrolling.contentSize = CGSize(width:kScreenWidth * 3, height:scrolling.frame.size.height)
        for i in 0 ..< self.children.count {
            self.loadScrollViewWithPage(i)
        }
        scrolling.delegate = self
    }
    
    
    func loadScrollViewWithPage(_ page: Int) {
        if page < 0 {
            return
        }
        if page >= self.children.count {
            return
        }
        var allVC : AllHistoryWalletViewController
        var addedVC : ReceivedHistoryViewController
        var paidVC : PaidHistoryWalletViewController

        var frame: CGRect = scrolling.frame
        switch page {
        case 0:
            allVC = self.children[page] as! AllHistoryWalletViewController
            allVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            allVC.view.frame = frame
            scrolling.addSubview(allVC.view!)
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            
        case 1:
            addedVC = self.children[page] as! ReceivedHistoryViewController
            addedVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            addedVC.view.frame = frame
            scrolling.addSubview(addedVC.view!)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            addedVC.view.setNeedsLayout()
            addedVC.view.layoutIfNeeded()
            
            
        case 2:
            paidVC = self.children[page] as! PaidHistoryWalletViewController
            paidVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            paidVC.view.frame = frame
            scrolling.addSubview(paidVC.view!)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            paidVC.view.setNeedsLayout()
            paidVC.view.layoutIfNeeded()
            
            
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageCurrent = Int(floor(scrolling.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleRateButton(button: allButton)
            self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.allButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        case 1:
            self.toggleRateButton(button: addedButton)
            self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.addedButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
        case 2:
            self.toggleRateButton(button: paidButton)
            self.raceScrollTo(CGPoint(x:2*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.paidButton.frame.origin.x,y: 38), withSnapBack: false, delegate: nil, callbackmethod: nil)
        default:
            break
        }
    }
    
    func raceTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
        var stopPoint: CGPoint = destination
        if withSnapBack {
            let diffx = destination.x - moovingView.frame.origin.x
            let diffy = destination.y - moovingView.frame.origin.y
            if diffx < 0 {
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                stopPoint.x += 10.0
            }
            
            if diffy < 0 {
                stopPoint.y -= 10.0
            }
            else if diffy > 0 {
                stopPoint.y += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        moovingView.frame = CGRect(x:stopPoint.x, y:stopPoint.y, width: moovingView.frame.size.width,height: moovingView.frame.size.height)
        UIView.commitAnimations()
        let firstDelay = 0.1
        let startTime = firstDelay * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + startTime) {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            self.moovingView.frame = CGRect(x:destination.x, y:destination.y,width: self.moovingView.frame.size.width, height: self.moovingView.frame.size.height)
            UIView.commitAnimations()
        }
    }
    
    
    func raceScrollTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - scrolling.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        if isleft {
            scrolling.contentOffset = CGPoint(x:destination.x - 5, y:destination.y)
        }
        else {
            scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
        }
        
        UIView.commitAnimations()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            if isleft {
                self.scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
            }
            else {
                self.scrolling.contentOffset = CGPoint(x:destination.x - 5,y: destination.y)
            }
            UIView.commitAnimations()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0){() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.easeInOut)
                self.scrolling.contentOffset = CGPoint(x:destination.x, y:destination.y)
                UIView.commitAnimations()
            }
        }
    }
}

