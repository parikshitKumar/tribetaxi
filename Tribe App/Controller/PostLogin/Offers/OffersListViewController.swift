//
//  OffersListViewController.swift
//  Rolling Rim
//
//  Created by Parikshit on 30/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit
import SDWebImage



protocol OffersListViewControllerDelegate{
    func offersListViewController(viewController: OffersListViewController,  didselectOffer offer:OfferModel, didSuccess success: Bool, didSelectIndex selectIndex: Int)
    
}

class OffersListViewController: UIViewController {
    
    @IBOutlet weak var aTableView: UITableView!
    
    var page = 1
    var perPage = 100
    //    var totalPage = 0
    var isLoading = false
    var offers = [OfferModel]()
    var delegate:OffersListViewControllerDelegate?
    var selectOffer = OfferModel()
    var selectIndex:Int = 0
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "OFFERS LIST"
        self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")

        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.aTableView.addSubview(refreshControl)

        self.loadOffersList(self.page, perPage: self.perPage)

        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        
        self.aTableView.tableFooterView = nil
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        
        self.isLoading = true
        
        WalletService.sharedInstance.getAllOfferList(pageNumber: page, perPage: perPage) { (success, resDrivers, responseMessage) in
            self.isLoading = false
            self.offers.removeAll()
            refreshControl.endRefreshing()
            if let someDriver = resDrivers{
                self.offers.append(contentsOf: someDriver)
            }
            
            self.aTableView.reloadData()
        }
    }
    
    
    func loadOffersList(_ page: Int,perPage:Int) -> Void {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true

        WalletService.sharedInstance.getAllOfferList(pageNumber: page, perPage: perPage) { (success, resDrivers, responseMessage) in
            self.isLoading = false
            if let someDrivers = resDrivers{
                if someDrivers.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.offers = someDrivers
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            
            self.aTableView.reloadData()
        }
    }
    
    
    @IBAction func onClickBackButton(_ sender:UIButton) {
        if selectIndex == -1{
            
       self.delegate?.offersListViewController(viewController: self, didselectOffer: OfferModel(), didSuccess: false, didSelectIndex: -1)
            
        }else{
            self.navigationController?.pop(true)
        }
    }
    
}
    extension OffersListViewController:UITableViewDataSource,UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (self.offers.count == 0) ? 1 : self.offers.count
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 130
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if self.offers.count == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
                cell.messageLabel.text = isLoading ? "Loading..".localizedString : "No Offers found\nPull down to refresh".localizedString
                return cell
            }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OffersListTableViewCell", for: indexPath) as! OffersListTableViewCell
            let offer = self.offers[indexPath.row]
            cell.promoCodeTitle.text = offer.title
            cell.promCodeDescription.text = offer.descrption
            cell.promoImage.sd_setImage(with: URL(string: offer.image), placeholderImage: UIImage(named: "profile"))
            cell.offerExpireLabel.text = offer.expireAt
            cell.offerPercentageLabel.text = String(format: "%.2f",offer.offerPercentage) + " %"
            cell.promoCodeLabel.text =  " " + offer.coupanCode + " "
                cell.perUserCountLabel.text = "Max use: \(offer.usedOfferCount)/\(offer.perUserCount)"
            cell.selectOfferButton.addTarget(self, action: #selector(onClickSelectPromoButton(_:)), for: .touchUpInside)
            cell.selectOfferButton.isSelected = (self.selectIndex == indexPath.row)
//            print("Date>>" + offer.expireAt)
            return cell
                
            }
        }
        
        
        @IBAction func onClickSelectPromoButton(_ sender:UIButton) {
            if self.offers.count == 0 {
                return
            }
            
            if let indexpath = sender.tableViewIndexPath(self.aTableView) {
                
                let offer  = self.offers[indexpath.row]
                if self.selectIndex == indexpath.row {
                    self.selectIndex = -1
                    self.aTableView.reloadData()
                    return
                }
                
                self.selectIndex = indexpath.row
                self.aTableView.reloadData()
            self.delegate?.offersListViewController(viewController: self, didselectOffer: offer, didSuccess: true, didSelectIndex: self.selectIndex)
            }
            
        }

}

