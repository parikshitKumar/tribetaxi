//
//  SetupPasswordViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SetupPasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var user : User!
    var titleView : NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.title = "CHANGE PASSWORD".localizedString
        oldPasswordTextField.placeholder = "Current Password".localizedString
        newPasswordTextField.placeholder = "New Password".localizedString
        confirmedPasswordTextField.placeholder = "Confirm Password".localizedString
        self.doneButton.setTitle("Done".localizedString, for: .normal)
        //self.addViewOldPassword(textField: self.oldPasswordTextField)
        //self.addViewNewPassword(textField: self.newPasswordTextField)
       // self.addViewConfirmPassword(textField: self.confirmedPasswordTextField)

    }
    

    override func viewDidLayoutSubviews() {
//        CommonClass.sharedInstance.setPlaceHolder(self.oldPasswordTextField, placeHolderString: "Current Password".localizedString, withColor: appColor.white)
//        CommonClass.sharedInstance.setPlaceHolder(self.newPasswordTextField, placeHolderString: "New Password".localizedString, withColor: appColor.white)
//        CommonClass.sharedInstance.setPlaceHolder(self.confirmedPasswordTextField, placeHolderString: "Confirm Password".localizedString, withColor: appColor.white)

        self.doneButton.applyGradient(withColours: [appColor.blueColor,appColor.blueColor], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    ////Old Password
    
    func addViewOldPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showOldPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    
    @IBAction func showOldPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.oldPasswordTextField.isSecureTextEntry = !sender.isSelected
    }
    
    
    /////New Password
    
    func addViewNewPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showNewPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    
    @IBAction func showNewPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.newPasswordTextField.isSecureTextEntry = !sender.isSelected
    }
    
    
    
    func addViewConfirmPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showConfirmPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    
    @IBAction func showConfirmPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.confirmedPasswordTextField.isSecureTextEntry = !sender.isSelected
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
   
    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        
        self.navigationController?.pop(true)

    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        guard let oldPassword = oldPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.messageString())
            return
        }

        if oldPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.messageString())
            return
        }
        if oldPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.messageString())
            return
        }

        guard let newPassword = newPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.messageString())
            return
        }
        if newPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.messageString())
            return
        }
        if newPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.messageString())
            return
        }

        guard let confirmPassword = confirmedPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.messageString())
            return
        }

        if confirmPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.messageString())
            return
        }

        if confirmPassword != newPassword{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.passwordDidNotMatch.messageString())
            return
        }

        self.updatePassword(oldPassword: oldPassword, newPassword: newPassword)
    }


    func updatePassword(oldPassword:String,newPassword:String) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        LoginService.sharedInstance.setupPassword(self.user.contact, oldPassword: oldPassword, withNewPassword: newPassword) { (success, message) in
            if success{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                    self.navigationController?.pop(true)
                })
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


//    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
//        self.navigationController?.pop(true)
//    }


    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if sender == self.newPasswordTextField || sender == self.confirmedPasswordTextField{
            if let cnfrmPassword = self.confirmedPasswordTextField.text{
                if let newpswrd = self.newPasswordTextField.text{
                    // self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                    self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? appColor.lightGray : appColor.blueColor
                }
            }
        }
    }

}
