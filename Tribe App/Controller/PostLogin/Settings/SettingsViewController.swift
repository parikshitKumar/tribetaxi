//
//  SettingsViewController.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    let settings = [["Change Password","Emergency Contacts"],
                    ["Notifications"],
                    ["Privacy Policy","Term of Uses","Version"]
    ]
    

    let icons = [["change_password_setting","emergency_contact"],
                    ["notification_setting"],
                    ["privacy_policies","terms_condition","version"]
    ]

    @IBOutlet weak var tableView:UITableView!
    var titleView : NavigationTitleView!
    var contacts = Array<Contact>()
    var user:User!



    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.title = "SETTINGS".localizedString
        self.navigationController?.navigationBar.isHidden = false

        self.registerCells()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()

    }
    
    override func viewDidLayoutSubviews() {
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
        NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

    }

    func registerCells(){
        self.tableView.register(UINib(nibName: "NotificationSettingCell", bundle: nil), forCellReuseIdentifier:"NotificationSettingCell")
        self.tableView.register(UINib(nibName: "GeneralSettingCell", bundle: nil), forCellReuseIdentifier:"GeneralSettingCell")
        self.tableView.register(UINib(nibName: "ChangeLanguageTableViewCell", bundle: nil), forCellReuseIdentifier:"ChangeLanguageTableViewCell")

    }
}

extension SettingsViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settings.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 0 : 4
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.settings[section].count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationSettingCell", for: indexPath) as! NotificationSettingCell
            cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
            cell.settingNameLabel.text = settings[indexPath.section][indexPath.row].localizedString
            cell.settingSwitch.isOn = AppSettings.shared.isNotificationEnable
            cell.settingSwitch.addTarget(self, action: #selector(notificatonToggle(_:)), for: .valueChanged)
            return cell
        }
        //else if indexPath.section == 2 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeLanguageTableViewCell", for: indexPath) as! ChangeLanguageTableViewCell
//            cell.imageIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
//           cell.changeLbl.text = settings[indexPath.section][indexPath.row].localizedString
//            cell.englishButton.addTarget(self, action: #selector(onClickEnglishButton(_:)), for: .touchUpInside)
//            cell.portugueseButton.addTarget(self, action: #selector(onClickPortugueseButton(_:)), for: .touchUpInside)
//            self.changeLanghuageButton(cell: cell)
//            return cell
//
//        }
        
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralSettingCell", for: indexPath) as! GeneralSettingCell
            if (indexPath.section == settings.count-1) && (indexPath.row == settings[settings.count-1].count-1){
                cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
                cell.settingNameLabel.text = "Version".localizedString + " " + UIApplication.appVersion()
                cell.accIcon.isHidden = true
            }else{
                cell.settingIcon.image = UIImage(named:icons[indexPath.section][indexPath.row])
                cell.settingNameLabel.text = settings[indexPath.section][indexPath.row].localizedString
                cell.accIcon.isHidden = false
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            self.openResetPassword()
        case (0,1):
            self.openEmergencyContact()
        case (2,0):
            //self.openPrivacyPolicy()
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
        case (2,1):
//            self.openTermsOfUses()
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)

        default:
            return
        }
    }
    
    func changeLanghuageButton(cell: ChangeLanguageTableViewCell) {
//        let result = AppSettings.shared.selectedLanguageName
//        if result == SelectLanguage.Portuguese.rawValue {
//            cell.englishButton.backgroundColor = appColor.lightGray
//            cell.englishButton.setTitleColor(appColor.blackOne, for: .normal)
//            cell.portugueseButton.backgroundColor = appColor.yellow
//            cell.portugueseButton.setTitleColor(UIColor.white, for: .normal)
//        }else{
//            cell.portugueseButton.backgroundColor = appColor.lightGray
//            cell.portugueseButton.setTitleColor(appColor.blackOne, for: .normal)
//            cell.englishButton.backgroundColor = appColor.yellow
//            cell.englishButton.setTitleColor(UIColor.white, for: .normal)
//
//
//        }
        
    }
    

    
    func openResetPassword(){
        let resetPasswordVC = AppStoryboard.Settings.viewController(SetupPasswordViewController.self)
        self.navigationController?.pushViewController(resetPasswordVC, animated: true)
    }

    func openEmergencyContact(){
        let emergencyContactVC = AppStoryboard.Settings.viewController(EmergencyContactViewController.self)
        self.navigationController?.pushViewController(emergencyContactVC, animated: true)
    }

    func openPrivacyPolicy(){
        let privacyPolicyVC = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
        privacyPolicyVC.isPrivacyPolicy = true
        self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
    }

    func openTermsOfUses(){
        let termsOfUses = AppStoryboard.Settings.viewController(PrivacyPolicyViewController.self)
        termsOfUses.isPrivacyPolicy = false
        self.navigationController?.pushViewController(termsOfUses, animated: true)
    }

    @IBAction func notificatonToggle(_ sender: UISwitch){
        AppSettings.shared.isNotificationEnable = sender.isOn
    }
    
    @IBAction func onClickEnglishButton(_ sender: UIButton){
        AppSettings.shared.selectedLanguageCode = "en"
        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
                if let cell = tableView.cellForRow(at: indexPath) as? ChangeLanguageTableViewCell{
                    self.changeLanghuageButton(cell: cell)
            }
        }
        tableView.reloadData()
        NotificationCenter.default.post(name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil, userInfo: nil)


        
    }
    @IBAction func onClickPortugueseButton(_ sender: UIButton){
        AppSettings.shared.selectedLanguageCode = "pt-PT"
        if let indexPath = sender.tableViewIndexPath(self.tableView) as IndexPath?{
            if let cell = tableView.cellForRow(at: indexPath) as? ChangeLanguageTableViewCell{
                self.changeLanghuageButton(cell: cell)
            }
        }
        tableView.reloadData()
        NotificationCenter.default.post(name: .LANGUAGE_CHANGE_NOTIFICATION, object: nil, userInfo: nil)



    }
    
    
    

}

