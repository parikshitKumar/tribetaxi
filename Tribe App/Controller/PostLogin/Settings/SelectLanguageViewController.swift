//
//  SelectLanguageViewController.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 03/10/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit



protocol SelectLanguageViewControllerDelegate {
    func selectLanguage(_ viewController: SelectLanguageViewController, didSelectLanguage language:String)
}
class SelectLanguageViewController: UIViewController {
    var titleView : NavigationTitleView!
    var changeLanguageView: LanguageChangeView!
    var selectLanguageView: SelectLanguageView!
    var delegate : SelectLanguageViewControllerDelegate?

    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationViews()

      self.setupLeftBarButtons()
        self.setRightBarButtons()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
    override func viewDidLayoutSubviews() {
//        self.viewLanguages()

    }
    override func viewDidAppear(_ animated: Bool) {
            self.viewLanguages()

    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    
    
    func setNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x:-20, y: 0, width: self.view.frame.size.width - 120, height: 44)
        titleView.titleLabel.textAlignment = .left
        titleView.titleLabel.text = "LOG IN".localizedString
        titleView.titleLabel.textColor = UIColor.white
        
        
    }
    
    func setupLeftBarButtons() {
        
        self.setNavigationTitle()
        let menuButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        menuButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        let origImage = UIImage(named: "back")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        menuButton.setImage(tintedImage, for: .normal)
        menuButton.tintColor = .white
        menuButton.isHidden = true
        
        // menuButton.addTarget(self, action: #selector(onclickBackButton(_:)), for: .touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        
        let titleItem = UIBarButtonItem(customView: titleView)
        
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        
        self.navigationItem.setLeftBarButtonItems([menuBarButton, titleItem], animated: false)

    }
    

    
    
    func setRightBarButtons() {
        
        self.selectLanguageView = SelectLanguageView.instanceFromNib()
        self.selectLanguageView.frame = CGRect(x:self.view.frame.size.width - 140 ,y: 0, width:140, height: 44)
//        self.selectLanguageView.selectLanguageButton.addTarget(self, action: #selector(onClickSelectLanguage(_:)), for: .touchUpInside)
        self.prevSelectLanguage()

        let selectLanguageBarButton = UIBarButtonItem(customView: selectLanguageView)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([selectLanguageBarButton], animated: false)

    }
    
    
    @IBAction func onClickSelectLanguage(_ sender: UIButton) {
       // self.viewLanguages()
    }
    
    func viewLanguages() {
        self.changeLanguageView = LanguageChangeView.instanceFromNib()
        self.changeLanguageView.frame = CGRect(x:self.selectLanguageView.frame.origin.x,y: 60, width:self.selectLanguageView.frame.size.width, height: 55)
        self.changeLanguageView.englishFlag.addTarget(self, action: #selector(clickEnglishButton(_:)), for: .touchUpInside)
        self.changeLanguageView.portugueseFlag.addTarget(self, action: #selector(clickPortugueseButton(_:)), for: .touchUpInside)
        self.view.addSubview(self.changeLanguageView)
        //        self.view.bringSubview(toFront: changeLanguageView)
        
    }
    
    @IBAction func clickEnglishButton(_ sender: UIButton) {
       delegate?.selectLanguage(self, didSelectLanguage: SelectLanguage
        .English.rawValue)
    }
    
    @IBAction func clickPortugueseButton(_ sender: UIButton) {

//        delegate?.selectLanguage(self, didSelectLanguage: SelectLanguage.Portuguese.rawValue)

    }
    
    
    
    func prevSelectLanguage() {
//        if AppSettings.shared.selectedLanguageName == SelectLanguage.Portuguese.rawValue {
//
//            AppSettings.shared.selectedLanguageCode = "pt-PT"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "portuguese_flag")
//            self.selectLanguageView.selectLabel.text = "Portugues"
//
//    }else{
//
//            AppSettings.shared.selectedLanguageCode = "en"
//            self.selectLanguageView.selectImage.image = #imageLiteral(resourceName: "english_flag")
//            self.selectLanguageView.selectLabel.text = "English"
//    }
}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          self.dismiss(animated: false, completion: nil)
    }
    
   

}
