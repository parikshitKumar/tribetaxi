//
//  PrivacyPolicyViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var isPrivacyPolicy : Bool = true
    var isFromSignUp : Bool = false
    var titleView : NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.title =  isPrivacyPolicy ? "PRIVACY POLICY".localizedString :"TERMS AND CONDITION".localizedString
        //self.webView.delegate = self
        self.webView.backgroundColor = UIColor.white
        self.webView.tintColor  = UIColor.clear
//        let urlPath = isPrivacyPolicy ? "http://45.58.45.156:3002/privacy/policies" : "http://45.58.45.156:3002/user/terms"
//        if let targetUrl = URL(string: urlPath){
//            self.webView.delegate = self
//            self.webView.scalesPageToFit = false
//            let request = URLRequest(url: targetUrl)
//            self.webView.loadRequest(request)
//        }else{
//            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.messageString())
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

   
    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        
        if self.isFromSignUp{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.pop(true)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {

        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.showLoader(withStatus: "Loading..")
        }
    }

//    private func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationTypeUIWebView.NavigationType) -> Bool {
//        if navigationType == UIWebViewNavigationType.linkClicked {
//            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
//            return false
//        }
//        return true
//    }

}
