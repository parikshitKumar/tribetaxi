//
//  AddContactViewController.swift
//  CartingKidzsDriver
//
//  Created by admin on 19/07/18.
//  Copyright © 2018 Tecorb. All rights reserved.
//

import UIKit
protocol AddContactViewControllerDelegate {
    func addContactViewController(viewController:AddContactViewController, didAddContact contact:Contact)
}

class AddContactViewController: UIViewController {
    @IBOutlet weak var addContactButton: UIButton!
    @IBOutlet weak var fristNameTextField: FloatLabelTextField!
    @IBOutlet weak var lastNameTextField: FloatLabelTextField!
    @IBOutlet weak var contactTextField: FloatLabelTextField!
    @IBOutlet weak var driverContactImageView: UIImageView!
    @IBOutlet weak var addContactTitle: UILabel!

    

    var imagePickerController : UIImagePickerController!
    var attachmentImage : UIImage?
    var name:String = ""
    var contact:String = ""
    var user:User!
    var delegate: AddContactViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.user = User.loadSavedUser()
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        CommonClass.makeViewCircular(driverContactImageView, borderColor: .clear, borderWidth: 0.0)
        CommonClass.makeViewCircularWithCornerRadius(addContactButton, borderColor: .clear, borderWidth: 0.0, cornerRadius: 4.0)
        
        self.addContactTitle.text = "ADD CONTACT".localizedString
        self.fristNameTextField.placeholder = "First Name".localizedString
        self.lastNameTextField.placeholder = "Last Name".localizedString
        self.contactTextField.placeholder = "Contact".localizedString
        self.addContactButton.setTitle("SUBMIT".localizedString, for: .normal)
        
        self.fristNameTextField.text = self.name
        self.contactTextField.text = self.contact
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickCameraButton(_ sender :UIButton){
        self.showAlertToChooseAttachmentOption()
    }
    
    func selectImage() {
        let tappedOnImage = UITapGestureRecognizer(target: self, action: #selector(AddContactViewController.showAlertToChooseAttachmentOption))
        driverContactImageView.isUserInteractionEnabled = true
        driverContactImageView.addGestureRecognizer(tappedOnImage)
    }
    
    
    
    @IBAction func onClickDismissButton(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func onClickAddContactButton(_ sender: UIButton){
        guard let firstName = fristNameTextField.text else {return}
        let lastName = lastNameTextField.text ?? ""
        guard let contact = contactTextField.text else {return}
        let validationResult = self.validateParams(firstName,lastName: lastName, mobile: contact)
        if !validationResult.success{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validationResult.message)
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validationResult.message)

            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        self.addContact(firstName, lastName: lastName, contact: contact, image: self.attachmentImage)
    }

    func addContact(_ firstName:String, lastName:String, contact:String, image:UIImage?){
        AppSettings.shared.showLoader(withStatus: "Adding...")
        EmergencyContactsService.sharedInstance.addEmergencyContact(firstName, lastName: lastName, mobile: contact, profileImage: image, userID: self.user.ID) { (success, resContact, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let eContact = resContact{
                    self.delegate?.addContactViewController(viewController: self, didAddContact: eContact)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func validateParams(_ firstName:String,lastName:String,mobile:String) -> (success:Bool,message:String) {
        if firstName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"First name cann't be empty!".localizedString)
        }
//        if lastName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
//            return (success:false,message:"Last name cann't be empty!")
//        }
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Mobile number cann't be empty!".localizedString)
        }
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0{
            return (success:false,message:"Mobile number cann't be empty!".localizedString)
        }
        return (success:true,message:"")
    }
    
    

    
}
extension AddContactViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @objc func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localizedString, style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)
        
        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let tempImage = info[.editedImage] as? UIImage{
            self.attachmentImage = tempImage
            self.driverContactImageView.image = tempImage
        }else if let tempImage = info[.originalImage] as? UIImage{
            self.attachmentImage = tempImage
            self.driverContactImageView.image = tempImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
