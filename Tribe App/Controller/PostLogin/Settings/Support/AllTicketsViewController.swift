//
//  AllTicketsViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class AllTicketsViewController: UIViewController {
    @IBOutlet var ticketsTableView : UITableView!
    var user : User!
    var isNewDataLoading = true
    var tickets = Array<SupportQuery>()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AllTicketsViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()

   var titleView : NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SUPPORT TICKETS".localizedString
        self.user = User.loadSavedUser()
        self.ticketsTableView.addSubview(self.refreshControl)

        self.ticketsTableView.register(UINib(nibName: "TicketListCell", bundle: nil), forCellReuseIdentifier: "TicketListCell")
        self.ticketsTableView.register(UINib(nibName: "NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")

        self.ticketsTableView.dataSource = self
        self.ticketsTableView.delegate = self
        self.getAllTickets()
    }
    
    override func viewWillLayoutSubviews() {
        self.ticketsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    
    @IBAction func onclickBackButton(_ sender : UIBarButtonItem){
        
        self.navigationController?.pop(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getAllTickets()

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func getAllTickets(){
        self.isNewDataLoading = true
        self.ticketsTableView.reloadData()
        SupportService.sharedInstance.getAllQueriesForUser(userID: self.user.ID) { (success, resTickets, message) in
            self.tickets.removeAll()
            self.isNewDataLoading = false
            if self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }

            if success{
                if let someTickets = resTickets{
                    self.tickets.append(contentsOf: someTickets)
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
            self.ticketsTableView.reloadData()
        }
    }

}

extension AllTicketsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tickets.count > 0) ? tickets.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.tickets.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading..".localizedString : "No Query found\r\nPlease pull down to refresh".localizedString
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TicketListCell", for: indexPath) as! TicketListCell
            let ticket = self.tickets[indexPath.row]
            cell.ticketImageView.setShowActivityIndicator(true)
            cell.ticketImageView.setIndicatorStyle(.gray)
            cell.ticketImageView.sd_setImage(with: URL(string:ticket.image),placeholderImage: #imageLiteral(resourceName: "profile"))
            cell.statusLabel.text = ticket.status.uppercased()
//            cell.statusLabel.textColor = ticket.adminReplyStatus ? appColor.redColor : appColor.blackOne
            cell.statusLabel.textColor = TicketStatus(rawValue: ticket.status.lowercased()).getColor()

            cell.referenceNumberLabel.text = "xxxx" + ticket.id
            cell.queryTextLabel.text = ticket.message
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.tickets.count == 0{return}
        let ticket = self.tickets[indexPath.row]
        self.openSupportTicketDetails(ticket: ticket)
    }

    func openSupportTicketDetails(ticket: SupportQuery){
        let supportTicketDetailsVC = AppStoryboard.Support.viewController(SupportTicketViewController.self)
        supportTicketDetailsVC.ticket = ticket
        supportTicketDetailsVC.delegate = self
        self.navigationController?.pushViewController(supportTicketDetailsVC, animated: true)
    }
}

extension AllTicketsViewController: SupportTicketViewControllerDelegate{
    func ticket(_ ticket: SupportQuery, didUpdateStatusTo status: String) {
        self.tickets.filter { (tckt) -> Bool in
            tckt.id == ticket.id
            }.first?.status = status
        self.ticketsTableView.reloadData()
    }

}


