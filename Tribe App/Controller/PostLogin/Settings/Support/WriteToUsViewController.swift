//
//  WriteToUsViewController.swift
//  GPDock
//
//  Created by TecOrb on 10/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView

class WriteToUsViewController: UITableViewController {
    var user : User!
    var ride:Ride?
    @IBOutlet var messageTextView : RSKPlaceholderTextView!
    @IBOutlet weak var scnShotbutton : UIButton!
    @IBOutlet weak var userEmailLabel : UILabel!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    @IBOutlet weak var removeScreenShotbutton : UIButton!
    @IBOutlet weak var submitButton : UIButton!
    
    @IBOutlet weak var  showMesssageHereLabel: UILabel!
    @IBOutlet weak var showScreenShotRefrenceLabel: UILabel!
    @IBOutlet weak var showContactYouLabel: UILabel!
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showPhoneNumberLabel: UILabel!




    var screenShot : UIImage?
    var imagePickerController : UIImagePickerController!
    var titleView : NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "WRITE TO US".localizedString
        self.user = User.loadSavedUser()
        self.clearsSelectionOnViewWillAppear = true

//        self.messageTextView.placeholder = (AppSettings.shared.selectedLanguageName == SelectLanguage.Portuguese.rawValue) ? "Oi, estou enfrentando esse problema" : "Hi, I am facing this issue"
        self.userEmailLabel.text = self.user.fullName
        self.phoneNumberLabel.text = "+"+self.user.contact
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        if let myRide = self.ride{
            let temp = "Hi, I am facing issue with ride Id:".localizedString + "\(myRide.ID)"
            self.messageTextView.text = temp //"Hi, I am facing issue with ride Id: \(myRide.ID)"
        }
        self.showMesssageHereLabel.text = "Type your message here".localizedString
        self.showScreenShotRefrenceLabel.text = "Screenshot for reference".localizedString
        self.showContactYouLabel.text = "We'll contact you here".localizedString
        self.submitButton.setTitle("SUBMIT".localizedString, for: .normal)
        self.showNameLabel.text = "Name".localizedString
        self.showPhoneNumberLabel.text = "Phone Number".localizedString
    }

    override func viewWillLayoutSubviews() {
        CommonClass.makeViewCircular(self.scnShotbutton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircular(self.removeScreenShotbutton, borderColor: UIColor.clear, borderWidth: 0)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func refreshScreenShotButton() {
        if let img = self.screenShot{
            self.scnShotbutton.setImage(img, for: UIControl.State())
            self.removeScreenShotbutton.isHidden = false
        }else{
            self.scnShotbutton.setImage(#imageLiteral(resourceName: "camera"), for: UIControl.State())
            self.removeScreenShotbutton.isHidden = true
        }
    }
    
    
    
    
    
   

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }
        if self.messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the issue you are facing".localizedString)
            return
        }
        
        if self.messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 200{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter text with in 200 characters")
            return
        }

        if self.messageTextView.text.isEmpty{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter the issue you are facing".localizedString)
            return
        }

        if self.user.ID.isEmpty{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your session has expired. Please login again".localizedString)
            return
        }
        self.sendEmailToSupport(self.user.ID, message: self.messageTextView.text, screenShot: self.screenShot)
    }

    @IBAction func onClickRemoveScreenShot(_ sender: UIButton){
        self.screenShot = nil
        self.refreshScreenShotButton()
    }
    @IBAction func onClickAddScreenShot(_ sender: UIButton){
        self.showAlertToChooseAttachmentOption()
    }

    func sendEmailToSupport(_ userID:String, message: String,screenShot: UIImage?){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        SupportService.sharedInstance.sendEmailToSupport(userID, message: message, image: screenShot) { (success, resSupport,message) in
            AppSettings.shared.hideLoader()
            if success{
                if resSupport != nil{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: "Your message has been received and responded to as soon as possible".localizedString,completionBlock: {
                        self.navigationController?.pop(true)
                    })
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            if UIDevice.isIphoneX{
                return (self.view.frame.size.height-202)/3
            }else{
                return (self.view.frame.size.height-182)/3
            }
        }else{
            return 44
        }
    }
}

extension WriteToUsViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localizedString, style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera".localizedString, style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerController.SourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.refreshScreenShotButton()
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let tempImage = info[.editedImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }else if let tempImage = info[.originalImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }
        picker.dismiss(animated: true, completion: nil)
    }


}
