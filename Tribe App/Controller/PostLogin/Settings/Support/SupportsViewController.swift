//
//  SupportsViewController.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var faqs = Array<FAQModel>()
    var user : User!
    var ride:Ride?
    var isFromMenu : Bool = false
    var titleView : NavigationTitleView!

    @IBOutlet weak var supportTableView: UITableView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = appColor.appgrayBackground
        refreshControl.addTarget(self, action: #selector(SupportsViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "SUPPORT".localizedString
        self.user = User.loadSavedUser()
        self.supportTableView.register(UINib(nibName: "SupportTableViewCell", bundle: nil), forCellReuseIdentifier: "SupportTableViewCell")
        self.supportTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.supportTableView.addSubview(refreshControl)
        self.supportTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.setUpLeftBarButton()
        self.setupRightBarButtons()

        //        if CommonClass.isLoggedIn{
//            self.setupRightBarButtons()
//        }
        self.getFAQsFromServer()

    }

    override func viewDidLayoutSubviews() {
        self.setupRightBarButtons()
        self.setUpLeftBarButton()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationViews()
    }
    
    func setupNavigationViews(){
        self.navigationController?.navigationBar.isHidden = false
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }




    
    func setupRightBarButtons() {
        let viewTicketButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        viewTicketButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        viewTicketButton.setImage(#imageLiteral(resourceName: "ticket"), for: .normal)
        viewTicketButton.addTarget(self, action: #selector(viewTicketListButton(_:)), for: .touchUpInside)
        let viewTicketBarButton = UIBarButtonItem(customView: viewTicketButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([viewTicketBarButton], animated: false)
    }
    
    @IBAction func viewTicketListButton(_ sender: UIButton){
        let ticketsListVC = AppStoryboard.Support.viewController(AllTicketsViewController.self)
        self.navigationController?.pushViewController(ticketsListVC, animated: true)
        
    }


    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !AppSettings.isConnectedToNetwork{
            refreshControl.endRefreshing()
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.messageString())
            return
        }

        SupportService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            self.refreshControl.endRefreshing()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.removeAll()
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
    func getFAQsFromServer(){
        AppSettings.shared.showLoader(withStatus: "Loading..")
        SupportService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }
        }
    }

    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menu") : #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = appColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if !isFromMenu{
            self.navigationController?.pop(true)
        }else{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
            NotificationCenter.default.post(name: .WALLET_AMOUNT_UPDATE_NOTIFICATION, object: nil, userInfo: nil)

        }
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = appColor.brown
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return faqs.count + 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 100
        }else if indexPath.section == faqs.count+1 {
            return 127
        }else{
            return 70
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let faqVC = AppStoryboard.Support.viewController(FAQViewController.self)
            let faqModel = faqs[indexPath.section-1]
            faqVC.faqModel = faqModel
            self.navigationController?.pushViewController(faqVC, animated: true)
        }else{return}
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportUserNameTableViewCell", for: indexPath) as! SupportUserNameTableViewCell
            cell.userNameLabel.text = "Hi".localizedString + " " + self.user.fullName
            cell.showUserHelpLabel.text = "How can I help you?".localizedString
            return cell
        }else if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportTableViewCell", for: indexPath) as! SupportTableViewCell
            let faqModel = faqs[indexPath.section-1]
            cell.questionTextLabel.text = faqModel.category.capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportNotListedTableViewCell", for: indexPath) as! SupportNotListedTableViewCell
            cell.reachUsButton.setTitle(" REACH US ".localizedString, for: .normal)
            cell.showProblemNotListed.text = "Problem not listed?".localizedString
            cell.reachUsButton.addTarget(self, action: #selector(SupportsViewController.onClickReachUs(_:)), for: .touchUpInside)
            return cell
        }
    }

    @IBAction func onClickReachUs(_ sender: UIButton){

        self.navigateToWriteToUs()
    }

    func navigateToWriteToUs() {
        let writeToUsVC = AppStoryboard.Support.viewController(WriteToUsViewController.self)
        writeToUsVC.ride = self.ride
        self.navigationController?.pushViewController(writeToUsVC, animated: true)
    }
}

//extension SupportsViewController: SlideNavigationControllerDelegate{
//    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
//        return isFromMenu
//    }
//    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
//        return false
//    }
//}
