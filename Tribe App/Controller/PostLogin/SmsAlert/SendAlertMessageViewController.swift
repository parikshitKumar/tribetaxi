//
//  SendAlertMessageViewController.swift
//  Rolling Rim
//
//  Created by Parikshit on 24/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit
import MessageUI


class SendAlertMessageViewController: UIViewController {
    
    @IBOutlet weak var firstBackView:UIView!
    @IBOutlet weak var secondBackView:UIView!

    var user:User!
    var contacts = Array<Contact>()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.getContact()
        self.decorateViews()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.decorateViews()

    }
    
    func decorateViews(){
        CommonClass.makeViewCircularWithCornerRadius(self.firstBackView, borderColor: .clear, borderWidth: 1, cornerRadius: 10)
        
        CommonClass.makeViewCircularWithCornerRadius(self.secondBackView, borderColor: .clear, borderWidth: 1, cornerRadius: 10)
    }

    @IBAction func onClickBackButton(_ sender : UIBarButtonItem){

            SlideNavigationController.sharedInstance().toggleLeftMenu()
        
    }
    
    @IBAction func onClickCallDriver(_ sender: UIButton){
        self.dialPhone(phoneNumber: "100")
    }
    
    func dialPhone(phoneNumber:String){
        let dial = "telprompt://" + "\(phoneNumber)"
        self.showPhoneApp(dial: dial)
    }
    
    func showPhoneApp(dial: String)  {
        let url = URL(string:dial)!
        if (UIApplication.shared.canOpenURL(url)){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Cann't able to call right now\r\nPlease try later!".localizedString)
        }
    }
    
    
    @IBAction func onClickSendSmsAlert(_ sender:UIButton){
        displayMessageInterface()

    }
    

}

extension SendAlertMessageViewController:MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    
    func displayMessageInterface() {
        if self.contacts.count == 0 {
        NKToastHelper.sharedInstance.showErrorAlert(self, message: "Emergency contact are not present,Please add your emergency contact.")

            return
        }
        var StringContact = [String]()
        for tempContact in contacts {
            StringContact.append(tempContact.contact)
            
        }
        
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = StringContact
        composeVC.body = "I am riding in Rolling Rim."
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func getContact(){
        AppSettings.shared.showLoader(withStatus: "Loading..")
        EmergencyContactsService.sharedInstance.userContactList(userID: self.user.ID) { (success, resContacts, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someContacts = resContacts{
                    self.contacts.append(contentsOf: someContacts)
                }else{
                    //                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                //                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }
    
}





