//
//  FavoritePlace.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 02/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import SwiftyJSON

class FavoritePlace: NSObject {
    enum keys: String, CodingKey {
        case id = "id"
        case place = "place"
        case category = "category"
        case latitude = "latitude"
        case longitude = "longitude"
        case isFavourite = "is_favourite"
    }

    var id = ""
    var place = ""
    var category = ""
    var latitude = 0.0
    var longitude = 0.0
    var isFavourite = false
    var placeCategory: AddressCategory = .other
    override init() {
        super.init()
    }

    static func == (lhs: FavoritePlace, rhs: FavoritePlace) -> Bool {
        return lhs.id == rhs.id || (lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude)
    }

    init(json : JSON){
        if let id = json[keys.id.stringValue].int{
            self.id = "\(id)"
        }else if let id = json[keys.id.stringValue].string{
            self.id = id
        }

        if let place = json[keys.place.stringValue].string{
            self.place = place
        }

        if let category = json[keys.category.stringValue].string{
            self.category = category
            self.placeCategory = AddressCategory(rawValue: category)
        }

        if let latitude = json[keys.latitude.stringValue].double{
            self.latitude = latitude
        }else if let latitude = json[keys.latitude.stringValue].string{
            self.latitude = Double(latitude) ?? 0.0
        }

        if let longitude = json[keys.longitude.stringValue].double{
            self.longitude = longitude
        }else if let longitude = json[keys.longitude.stringValue].string{
            self.longitude = Double(longitude) ?? 0.0
        }

        if let isFavourite = json[keys.isFavourite.stringValue].bool{
            self.isFavourite = isFavourite
        }
        super.init()
    }

    init(dictionary : Dictionary<String,AnyObject>){
        if let id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dictionary[keys.id.stringValue] as? String{
            self.id = id
        }

        if let place = dictionary[keys.place.stringValue] as? String{
            self.place = place
        }

        if let category = dictionary[keys.category.stringValue] as? String{
            self.category = category
            self.placeCategory = AddressCategory(rawValue: category)
        }

        if let latitude = dictionary[keys.latitude.stringValue] as? Double{
            self.latitude = latitude
        }else if let latitude = dictionary[keys.latitude.stringValue] as? String{
            self.latitude = Double(latitude) ?? 0.0
        }

        if let longitude = dictionary[keys.longitude.stringValue] as? Double{
            self.longitude = longitude
        }else if let longitude = dictionary[keys.longitude.stringValue] as? String{
            self.longitude = Double(longitude) ?? 0.0
        }

        if let isFavourite = dictionary[keys.isFavourite.stringValue] as? Bool{
            self.isFavourite = isFavourite
        }
        super.init()
    }

}



class FavoritePlaceParser: NSObject {
    enum keys: String, CodingKey {
        case code = "code"
        case message = "message"
        case recentPlaces = "recent_places"
        case place = "recent_place"
        case favourites = "favourites"
        case lastTwoPlaces = "places"
    }

    var message = ""
    var recentPlaces = [FavoritePlace]()
    var favourites = [FavoritePlace]()
    var place = FavoritePlace()
    var lastTwoPlaces = [FavoritePlace]()
    var errorCode: ErrorCode = .failure
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let code = json[keys.code.stringValue].int{
            self.errorCode = ErrorCode(rawValue: code)
        }
        if let message = json[keys.message.stringValue].string{
            self.message = message
        }

        if let recentPlaces =  json[keys.recentPlaces.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for placeData in recentPlaces{
                let place = FavoritePlace(dictionary: placeData)
                self.recentPlaces.append(place)
            }
        }

        if let favourites =  json[keys.favourites.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for placeData in favourites{
                let place = FavoritePlace(dictionary: placeData)
                self.favourites.append(place)
            }
        }
        
        
        if let lastTwoRecentPlaces =  json[keys.lastTwoPlaces.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for placeData in lastTwoRecentPlaces{
                let place = FavoritePlace(dictionary: placeData)
                self.lastTwoPlaces.append(place)
            }
        }

        if let place =  json[keys.place.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.place = FavoritePlace(dictionary: place)
        }
    }
}



