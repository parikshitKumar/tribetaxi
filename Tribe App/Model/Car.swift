//
//  Ride.swift
//  TaxiApp
//
//  Created by TecOrb on 01/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class Car: NSObject {

    let kID  = "id"
    let kName  = "name"
    let kNumber  = "number"
    let kCapacity = "capacity"
    let kStatus  = "status"
    let kLatitude  = "latitude"
    let kLongitude  = "longitude"
    let kDistance  = "distance"
    let kTime  = "time"
    let kCarCategory  = "car_category"
    let kUserID = "user_id"
    let kCarCategoryID = "car_category_id"
    let kDriver = "driver"
    let kIsOnline = "is_online"
    let kImage = "image"

    var ID  = ""
    var name  = ""
    var number  = ""
    var capacity = "" //int or string
    var status = false //true of false
    var latitude:Double  = 0.0
    var longitude: Double  = 0.0
    var distance = ""
    var time = ""
    var carCategory  = CarCategory()
    var userID = ""
    var carCategoryID = ""
    var driver  = User()
    var isOnline = false
    var image = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }

        if let _userID = json[kUserID].int as Int?{
            self.userID = "\(_userID)"
        }else if let _userID = json[kUserID].string as String?{
            self.userID = _userID
        }

        if let _name = json[kName].string as String?{
            self.name = _name
        }
        if let _image = json[kImage].string as String?{
            self.image = _image
        }
        if let _number = json[kNumber].string as String?{
            self.number = _number
        }
        if let _capacity = json[kCapacity].int as Int?{
            self.capacity = "\(_capacity)"
        }else if let _capacity = json[kCapacity].string as String?{
            self.capacity = _capacity
        }

        if let _status = json[kStatus].bool as Bool?{
            self.status = _status
        }

        if let _isOnline = json[kIsOnline].bool as Bool?{
            self.isOnline = _isOnline
        }

        if let _distance = json[kDistance].string as String?{
            self.distance = _distance
        }
        if let _time = json[kTime].string as String?{
            self.time = _time
        }
        if let _carCategory = json[kCarCategory].dictionaryObject as [String:AnyObject]?{
            self.carCategory = CarCategory(dictionary: _carCategory)
        }
        if let _driver = json[kDriver].dictionaryObject as [String:AnyObject]?{
            self.driver = User(dictionary: _driver)
        }

        if let _catID = json[kCarCategoryID].int as Int?{
            self.carCategoryID = "\(_catID)"
        }else if let _catID = json[kCarCategoryID].string as String?{
            self.carCategoryID = _catID
        }

        if let _lat = json[kLatitude].double as Double?{
            self.latitude = _lat
        }else if let _lat = json[kLatitude].string as String?{
            self.latitude = Double(_lat) ?? 0.0
        }

        if let _long = json[kLongitude].double as Double?{
            self.longitude = _long
        }else if let _long = json[kLongitude].string as String?{
            self.longitude = Double(_long) ?? 0.0
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _userID = dictionary[kUserID] as? Int{
            self.userID = "\(_userID)"
        }else if let _userID = dictionary[kUserID] as? String{
            self.userID = _userID
        }

        if let _name = dictionary[kName] as? String{
            self.name = _name
        }
        if let _number = dictionary[kNumber] as? String{
            self.number = _number
        }
        if let _capacity = dictionary[kCapacity] as? Int{
            self.capacity = "\(_capacity)"
        }else if let _capacity = dictionary[kCapacity] as? String{
            self.capacity = _capacity
        }

        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }

        if let _isOnline = dictionary[kIsOnline] as? Bool{
            self.isOnline = _isOnline
        }

        if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }
        if let _time = dictionary[kTime] as? String{
            self.time = _time
        }
        if let _carCategory = dictionary[kCarCategory] as? [String:AnyObject]{
            self.carCategory = CarCategory(dictionary: _carCategory)
        }
        if let _driver = dictionary[kDriver] as? [String:AnyObject]{
            self.driver = User(dictionary: _driver)
        }
        if let _catID = dictionary[kCarCategoryID] as? Int{
            self.carCategoryID = "\(_catID)"
        }else if let _catID = dictionary[kCarCategoryID] as? String{
            self.carCategoryID = _catID
        }

        if let _lat = dictionary[kLatitude] as? Double{
            self.latitude = _lat
        }else if let _lat = dictionary[kLatitude] as? String{
            self.latitude = Double(_lat) ?? 0.0
        }

        if let _long = dictionary[kLongitude] as? Double{
            self.longitude = _long
        }else if let _long = dictionary[kLongitude] as? String{
            self.longitude = Double(_long) ?? 0.0
        }
        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }
        super.init()
    }



}

class CarParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kCars = "cars"

    var responseMessage = ""
    var cars = [Car]()

    override init() {
        super.init()
    }

    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let carArray = json[kCars].arrayObject as? [[String:AnyObject]]{
            for carDict in carArray{
                let car = Car(dictionary: carDict)
                self.cars.append(car)
            }
        }
        super.init()
    }
}



class CostEstimationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kEstimatedFair = "estimated_fair"
    let kTotalCost = "fair_cost"
    let kCar = "car"

    var responseMessage = ""
    var estimatedCost:Double = 0.0

    override init() {
        super.init()
    }
    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let estimation = json[kEstimatedFair].dictionaryObject as [String:AnyObject]?{
            if let _totalcost = estimation[kTotalCost] as? Double{
                self.estimatedCost = _totalcost
            }
        }else if let estimation = json[kCar].dictionaryObject as [String:AnyObject]?{
            if let _totalcost = estimation[kTotalCost] as? Double{
                self.estimatedCost = _totalcost
            }
        }
        
        super.init()
    }
}

