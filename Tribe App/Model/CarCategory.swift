//
// CarCategory.swift
//  MyLaundryApp
//
//  Created by TecOrb on 21/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON



class CarCategory: NSObject {

    let kID = "id"
    let kCategoryName = "category_name"
    let kBaseFair = "base_fair"
    let kBaseMile = "base_mile"
    let kPricePerMile = "price_per_mile"
    let kWaitingPricePerMile = "waiting_price_per_min"

    let kCancellationFee = "cancellation_fee"
    let kImage = "image"
    let kTime = "time"
    let kDistance = "distance"
    let kMaxSpeed = "maxspeed"
    let kRideFairPerMinute = "ride_time_fair"

    let kNearestCarLatitude = "nearest_car_latitude"
    let kNearestCarLongitude = "nearest_car_longitude"
    let kCarsName = "cars_name"
    let kCapecity = "capacity"
    let kCategoryType = "category_type"
    

    var ID : String = ""
    var categoryName: String = ""
    var baseFair : Double = 0.0
    var baseMile:Double = 0.0
    var pricePerMile : Double = 0.0
    var waitingPricePerMinute :Double = 0.0


    var cancellationFee = 0.0
    var image = ""
    var maxSpeed = 0.0
    var rideFairPerMinute = 0.0


    var time : String = ""
    var distance: String = ""
    var nearestCarLatitude : Double = 0.0
    var nearestCarLongitude: Double = 0.0
    var carsName = ""
    var capecity: Int = 3

    var approxFare: Double = 0.0
    var categoryType: String = ""

    override init() {
        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let _Id = dictionary[kID] as? Int{
            self.ID = "\(_Id)"
        }else if let _Id = dictionary[kID] as? String{
            self.ID = _Id
        }

        if let _name = dictionary[kCategoryName] as? String{
            self.categoryName = _name
        }

        if let _carsname = dictionary[kCarsName] as? String{
            self.carsName = _carsname
        }

        if let _baseFair = dictionary[kBaseFair] as? String{
            self.baseFair = Double(_baseFair) ?? 0.0
        }else if let _baseFair = dictionary[kBaseFair] as? Double{
            self.baseFair = _baseFair
        }

        if let _baseMile = dictionary[kBaseMile] as? String{
            self.baseMile = Double(_baseMile) ?? 0.0
        }else if let _baseMile = dictionary[kBaseMile] as? Double{
            self.baseMile = _baseMile
        }

        if let _cancellationFee = dictionary[kCancellationFee] as? String{
            self.cancellationFee = Double(_cancellationFee) ?? 0.0
        }else if let _cancellationFee = dictionary[kCancellationFee] as? Double{
            self.cancellationFee = _cancellationFee
        }
        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }
        if let _maxSpeed = dictionary[kMaxSpeed] as? String{
            self.maxSpeed = Double(_maxSpeed) ?? 0.0
        }else if let _maxSpeed = dictionary[kMaxSpeed] as? Double{
            self.maxSpeed = _maxSpeed
        }

        if let _rideTimeFair = dictionary[kRideFairPerMinute] as? String{
            self.rideFairPerMinute = Double(_rideTimeFair) ?? 0.0
        }else if let _rideTimeFair = dictionary[kRideFairPerMinute] as? Double{
            self.rideFairPerMinute = _rideTimeFair
        }


        if let _pricePerMile = dictionary[kPricePerMile] as? String{
            self.pricePerMile = Double(_pricePerMile) ?? 0.0
        }else if let _pricePerMile = dictionary[kPricePerMile] as? Double{
            self.pricePerMile = _pricePerMile
        }

        if let _waitingPricePerMile = dictionary[kWaitingPricePerMile] as? String{
            self.waitingPricePerMinute = Double(_waitingPricePerMile) ?? 0.0
        }else if let _waitingPricePerMile = dictionary[kWaitingPricePerMile] as? Double{
            self.waitingPricePerMinute = _waitingPricePerMile
        }



        if let _time = dictionary[kTime] as? String{
            self.time = _time
        }else if let _time = dictionary[kTime] as? Double{
            self.time = "\(_time)"
        }

        if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }else if let _distance = dictionary[kDistance] as? Double{
            self.distance = "\(_distance)"
        }


        //nearest Car location
        if let _lat = dictionary[kNearestCarLatitude] as? String{
            self.nearestCarLatitude = Double(_lat) ?? 0.0
        }else if let _lat = dictionary[kNearestCarLatitude] as? Double{
            self.nearestCarLatitude = _lat
        }

        if let _longitude = dictionary[kNearestCarLongitude] as? String{
            self.nearestCarLongitude = Double(_longitude) ?? 0.0
        }else if let _longitude = dictionary[kNearestCarLongitude] as? Double{
            self.nearestCarLongitude = _longitude
        }

        if let _capecity = dictionary[kCapecity] as? Int{
            self.capecity = _capecity
        }else if let _capecity = dictionary[kCapecity] as? String{
            self.capecity = Int(_capecity) ?? 4
        }
        
        if let _categoryType = dictionary[kCategoryType] as? String{
            self.categoryType = _categoryType
        }


        super.init()
    }

     init(json: JSON) {

        if let _Id = json[kID].int as Int?{
            self.ID = "\(_Id)"
        }else if let _Id = json[kID].string as String?{
            self.ID = _Id
        }
        if let _capecity = json[kCapecity].int{
            self.capecity = _capecity
        }else if let _capecity = json[kCapecity].string{
            self.capecity = Int(_capecity) ?? 4
        }

        if let _name = json[kCategoryName].string as String?{
            self.categoryName = _name
        }

        if let _carsname = json[kCarsName].string as String?{
            self.carsName = _carsname
        }

        if let _baseFair = json[kBaseFair].string as String?{
            self.baseFair = Double(_baseFair) ?? 0.0
        }else if let _baseFair = json[kBaseFair].double as Double?{
            self.baseFair = _baseFair
        }

        if let _baseMile = json[kBaseMile].string as String?{
            self.baseMile = Double(_baseMile) ?? 0.0
        }else if let _baseMile = json[kBaseMile].double as Double?{
            self.baseMile = _baseMile
        }

        if let _pricePerMile = json[kPricePerMile].string as String?{
            self.pricePerMile = Double(_pricePerMile) ?? 0.0
        }else if let _pricePerMile = json[kPricePerMile].double as Double?{
            self.pricePerMile = _pricePerMile
        }

        if let _waitingPricePerMile = json[kWaitingPricePerMile].string as String?{
            self.waitingPricePerMinute = Double(_waitingPricePerMile) ?? 0.0
        }else if let _waitingPricePerMile = json[kWaitingPricePerMile].double as Double?{
            self.waitingPricePerMinute = _waitingPricePerMile
        }

        if let _time = json[kTime].string as String?{
            self.time = _time
        }else if let _time = json[kTime].double as Double?{
            self.time = "\(_time)"
        }

        if let _distance = json[kDistance].string as String?{
            self.distance = _distance
        }else if let _distance = json[kDistance].double as Double?{
            self.distance = "\(_distance)"
        }


        if let _cancellationFee = json[kCancellationFee].string as String?{
            self.cancellationFee = Double(_cancellationFee) ?? 0.0
        }else if let _cancellationFee = json[kCancellationFee].double as Double?{
            self.cancellationFee = _cancellationFee
        }
        if let _image = json[kImage].string as String?{
            self.image = _image
        }
        if let _maxSpeed = json[kMaxSpeed].string as String?{
            self.maxSpeed = Double(_maxSpeed) ?? 0.0
        }else if let _maxSpeed = json[kMaxSpeed].double as Double?{
            self.maxSpeed = _maxSpeed
        }

        if let _rideTimeFair = json[kRideFairPerMinute].string as String?{
            self.rideFairPerMinute = Double(_rideTimeFair) ?? 0.0
        }else if let _rideTimeFair = json[kRideFairPerMinute].double as Double?{
            self.rideFairPerMinute = _rideTimeFair
        }


        //nearest Car location
        if let _lat = json[kNearestCarLatitude].string as String?{
            self.nearestCarLatitude = Double(_lat) ?? 0.0
        }else if let _lat = json[kNearestCarLatitude].double as Double?{
            self.nearestCarLatitude = _lat
        }

        if let _longitude = json[kNearestCarLongitude].string as String?{
            self.nearestCarLongitude = Double(_longitude) ?? 0.0
        }else if let _longitude = json[kNearestCarLongitude].double as Double?{
            self.nearestCarLongitude = _longitude
        }
        
        if let _categoryType = json[kCategoryType].string as String?{
            self.categoryType = _categoryType
        }

        super.init()
    }

}

class CarCategoryParser: NSObject {
    let kResponseMessage = "message"
    let kResponseCode = "code"
    let kCategories = "car_categories"

    var responseMessage = ""
    var responseCode: Int = 0
    var categories = [CarCategory]()
    override init() {
        super.init()
    }

    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _responseMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _responseMessage
        }
        
        if let _categoryArray = json[kCategories].arrayObject as? [[String:AnyObject]]{
            self.categories.removeAll()
            for _cate in _categoryArray{
                let cate = CarCategory(dictionary: _cate)
                self.categories.append(cate)
            }
        }
        super.init()
    }

}





//approx_fares

//"capacity" : null,
//"image" : null,
//"base_mile" : "15",
//"waiting_price_per_min" : "0.65",
//"ride_time_fair" : "2",
//"base_fair" : "8",
//"cancellation_fee" : null,
//"price_per_mile" : "3.5",
//"maxspeed" : 120


class ApproxFare: NSObject{

    enum keys: String, CodingKey {
        case id = "id"
        case fare = "fair"
        case minimumFare = "minimum_price"
        case categoryName = "category_name" // : "Mercedes",
        case capacity = "capacity"
        case image = "image"
        case baseKM = "base_mile"
        case waitingPricePerMin = "waiting_price_per_min"
        case rideTimeFair = "ride_time_fair"
        case baseFair = "base_fair"
        case cancellationFee = "cancellation_fee"
        case pricePerKM = "price_per_mile"
        case maxspeed = "maxspeed"
        case offerPrice = "offer_price"
    }

    //values
    var categoryID = ""
    var fare : Double = 0.0
    var categoryName = ""

    var capacity: Int = 0
    var image = ""
    var baseKM: Double = 0.0
    var waitingPricePerMin:Double = 0.0
    var rideTimeFair:Double = 0.0
    var baseFair : Double = 0.0
    var minimumFare : Double = 0.0
    var cancellationFee:Double = 0.0
    var pricePerKM:Double = 0.0
    var maxspeed:Double = 0.0
    var offerPrice:Double = 0.0

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? String{
            self.categoryID = _id
        }else if let _id = dictionary[keys.id.stringValue] as? Int{
            self.categoryID = "\(_id)"
        }

        if let price = dictionary[keys.fare.stringValue] as? Double{
            self.fare = price
        }else if let price = dictionary[keys.fare.stringValue] as? String{
            self.fare = Double(price) ?? 0.0
        }

        if let minprice = dictionary[keys.minimumFare.stringValue] as? Double{
            self.minimumFare = minprice
        }else if let minprice = dictionary[keys.minimumFare.stringValue] as? String{
            self.minimumFare = Double(minprice) ?? 0.0
        }

        if let catName = dictionary[keys.categoryName.stringValue] as? String{
            self.categoryName = catName
        }

        if let capacity = dictionary[keys.capacity.stringValue] as? Int{
            self.capacity = capacity
        }else if let capacity = dictionary[keys.capacity.stringValue] as? String{
            self.capacity = Int(capacity) ?? 0
        }

        if let baseKM = dictionary[keys.baseKM.stringValue] as? Double{
            self.baseKM = baseKM
        }else if let baseKM = dictionary[keys.baseKM.stringValue] as? String{
            self.baseKM = Double(baseKM) ?? 0.0
        }

        if let waitingPricePerMin = dictionary[keys.waitingPricePerMin.stringValue] as? Double{
            self.waitingPricePerMin = waitingPricePerMin
        }else if let waitingPricePerMin = dictionary[keys.waitingPricePerMin.stringValue] as? String{
            self.waitingPricePerMin = Double(waitingPricePerMin) ?? 0.0
        }

        if let rideTimeFair = dictionary[keys.rideTimeFair.stringValue] as? Double{
            self.rideTimeFair = rideTimeFair
        }else if let rideTimeFair = dictionary[keys.rideTimeFair.stringValue] as? String{
            self.rideTimeFair = Double(rideTimeFair) ?? 0.0
        }

        if let baseFair = dictionary[keys.baseFair.stringValue] as? Double{
            self.baseFair = baseFair
        }else if let baseFair = dictionary[keys.baseFair.stringValue] as? String{
            self.baseFair = Double(baseFair) ?? 0.0
        }

        if let cancellationFee = dictionary[keys.cancellationFee.stringValue] as? Double{
            self.cancellationFee = cancellationFee
        }else if let cancellationFee = dictionary[keys.cancellationFee.stringValue] as? String{
            self.cancellationFee = Double(cancellationFee) ?? 0.0
        }

        if let pricePerKM = dictionary[keys.pricePerKM.stringValue] as? Double{
            self.pricePerKM = pricePerKM
        }else if let pricePerKM = dictionary[keys.pricePerKM.stringValue] as? String{
            self.pricePerKM = Double(pricePerKM) ?? 0.0
        }

        if let maxspeed = dictionary[keys.maxspeed.stringValue] as? Double{
            self.maxspeed = maxspeed
        }else if let maxspeed = dictionary[keys.maxspeed.stringValue] as? String{
            self.maxspeed = Double(maxspeed) ?? 0.0
        }
        if let _offerPrice = dictionary[keys.offerPrice.stringValue] as? Double{
            self.offerPrice = _offerPrice
        }else if let _offerPrice = dictionary[keys.offerPrice.stringValue] as? String{
            self.offerPrice = Double(_offerPrice) ?? 0.0
        }

        super.init()
    }
}



class Wallet: NSObject {
    
    let kId = "id"
    let kPoint = "point"
    
    
    var ID = ""
    var point : Double = 0.0
    
    override init() {
        super.init()
    }
    
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _id = dictionary[kId] as? String{
            self.ID = _id
        }else if let _id = dictionary[kId] as? Int{
            self.ID = "\(_id)"
        }
        
        if let _point = dictionary[kPoint] as? Double{
            self.point = _point
        }else if let _point = dictionary[kPoint] as? String{
            self.point = Double(_point) ?? 0.0
        }
        
        super.init()

    }
    
    
    
}


class ApproxFareParser: NSObject {

    enum keys: String, CodingKey {
        case message = "message"
        case code = "code"
        case approxFares = "approx_fares"
        case wallet = "wallet"
    }

    var message = ""
    var approxFares = [ApproxFare]()
    var errorCode : ErrorCode = .failure
    var wallet =  Wallet()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _message = json[keys.message.stringValue].string as String?{
            self.message = _message
        }
        
        if let _wallet =  json[keys.wallet.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.wallet = Wallet(dictionary: _wallet)
        }

        if let _fareArray = json[keys.approxFares.stringValue].arrayObject as? [[String:AnyObject]]{
            self.approxFares.removeAll()
            for _fare in _fareArray{
                let fare = ApproxFare(dictionary: _fare)
                self.approxFares.append(fare)
            }
        }
        super.init()
    }
}


