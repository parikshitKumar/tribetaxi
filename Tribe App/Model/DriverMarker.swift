//
//  DriverMarker.swift
//  TaxiApp
//
//  Created by TecOrb on 10/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps


class DriverMarker: GMSMarker {
    var driver : ActionCableDriver!
    var timeBeforeUpdated:Double = 1.6
    var type: String!
    init(driver: ActionCableDriver) {
        self.driver = driver
        super.init()
        self.position = CLLocationCoordinate2D(latitude: driver.latitude, longitude: driver.longitude)

            self.icon = UIImage(named: "tribe_car")

        groundAnchor = CGPoint(x: 0.5, y: 0.5)
        self.isFlat = true
    }
}


//class DriverMarkerPrevious: GMSMarker {
//    var driver : Driver!
//    var timeBeforeUpdated:Double = 1.6
//    var type: String!
//    init(driver: Driver) {
//        self.driver = driver
//        super.init()
//        self.position = CLLocationCoordinate2D(latitude: driver.location.latitude, longitude: driver.location.longitude)
//        if driver.type == "bike" {
//            self.icon = UIImage(named: "jocab_bike")
//            
//        }else{
//            self.icon = UIImage(named: "memanga_car")
//            
//        }
//        groundAnchor = CGPoint(x: 0.5, y: 0.5)
//        self.isFlat = true
//    }
//}


