//
//  SupportModels.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

/*
 {
 "id": 8,
 "image": "",
 "message": "Check My issues",
 "reference_no": "U27R0670",
 "status": false,
 "support_chats": []
 }
 */

class SupportQuery: NSObject {
    enum keys:String, CodingKey{
        case id = "id"
        case image = "image"
        case message = "description"
        case referenceNumber = "reference_no"
        case status = "status"
        case supportChats = "ticket_chats"
        case adminReplyStatus = "admin_reply_status"
    }

    var id = ""
    var image = ""
    var message = ""
    var referenceNumber = ""
    var status = ""
    var adminReplyStatus = true
    var supportChats = Array<SupportMessage>()

    override init(){
        super.init()
    }

    init(dict: Dictionary<String,AnyObject>){
        if let id = dict[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dict[keys.id.stringValue] as? String{
            self.id = id
        }

        if let imageUrl = dict[keys.image.stringValue] as? String{
            self.image = imageUrl
        }
        if let message = dict[keys.message.stringValue] as? String{
            self.message = message
        }
        if let referenceNumber = dict[keys.referenceNumber.stringValue] as? String{
            self.referenceNumber = referenceNumber
        }

        if let adminStatus = dict[keys.adminReplyStatus.stringValue] as? Bool{
            self.adminReplyStatus = adminStatus
        }
        
        if let status = dict[keys.status.stringValue] as? String{
            self.status = status
        }

        if let supportChats = dict[keys.supportChats.stringValue] as? Array<Dictionary<String,AnyObject>>{
            for chat in supportChats{
                let message = SupportMessage(dict: chat)
                self.supportChats.append(message)
            }
        }
        super.init()
    }
}


/*
 {
 "id": 8,
 "reply": "Great",
 "reply_by": "admin"
 }
 */

enum ReplyBy {
    case you
    case admin
}

class SupportMessage: NSObject {
    enum keys:String, CodingKey{
        case id = "id"
        case reply = "reply"
        case replyBy = "reply_by"
    }

    var id = ""
    var reply = ""
    var replyBy : ReplyBy = .you

    override init(){
        super.init()
    }
    init(dict:Dictionary<String,AnyObject>) {
        if let id = dict[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dict[keys.id.stringValue] as? String{
            self.id = id
        }
        if let reply = dict[keys.reply.stringValue] as? String{
            self.reply = reply
        }

        if let replyBy = dict[keys.replyBy.stringValue] as? String{
            self.replyBy = (replyBy == "admin") ? .admin : .you
        }
        super.init()
    }
}


class SupportTicketParser: NSObject {
    enum keys:String, CodingKey{
        case code = "code"
        case message = "message"
        case supports = "tickets"
        case support = "ticket"
        case comment = "chat"
    }

    var message = ""
    var support = SupportQuery()
    var supports = Array<SupportQuery>()
    var comment = SupportMessage()
    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let message = json[keys.message.stringValue].string{
            self.message = message
        }
        if let sQuery = json[keys.support.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.support = SupportQuery(dict: sQuery)
        }
        if let comment = json[keys.comment.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.comment = SupportMessage(dict: comment)
        }

        if let supportQueries = json[keys.supports.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for query in supportQueries{
                let sq = SupportQuery(dict: query)
                self.supports.append(sq)
            }
        }


    }
}
