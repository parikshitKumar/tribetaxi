//
//  Offer.swift
//  GPDock
//
//  Created by Parikshit on 09/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

//"id": 2,
//"offer_percentage": 89,
//"offer_code": "67",
//"title": "alok",
//"description": "oiewuow",
//"expire_at": "2019-05-31T12:12:29.000Z",
//"max_usage_count": 65,
//"per_user_count": 87,
//"image": "http://res.cloudinary.com/rollingrim-com/image/upload/v1559052837/offers/d9ub8mghgq1dlwk5ygmr.jpg",
//"role": "user",
//"user_id": null,
//"is_private": false



class OfferModel: NSObject {
    let kID = "id"
    let kOfferPercentage = "offer_percentage"
    let kCoupanCode = "offer_code"
    let kImage = "image"
    let kTitle = "title"
    let kDescription = "description"
    let kExpireAt = "expire_at"
    let kMaxUsagesCount = "max_usage_count"
    let kPerUserCount = "per_user_count"
    let kIsPrivate = "is_private"
    let kUsedOfferCount = "used_count"
    
    var ID:Int = 0
    var offerPercentage:Double = 0.0
    var coupanCode = ""
    var image = ""
    var title = ""
    var descrption = ""
    var expireAt = ""
    var isPrivare = false
    var maxUsagesCount:Int = 0
    var perUserCount:Int = 0
    var usedOfferCount:Int = 0

    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int {
            self.ID = _ID
        }else if let _ID = dictionary[kID] as? String {
            self.ID = Int(_ID) ?? 0
        }

        if let _offerPercentage = dictionary[kOfferPercentage] as? Double {
            self.offerPercentage = _offerPercentage
        }else if let _offerPercentage = dictionary[kOfferPercentage] as? String {
            self.offerPercentage = Double(_offerPercentage) ?? 0.0
        }
        if let _coupanCode = dictionary[kCoupanCode] as? String {
            self.coupanCode = _coupanCode
        }
        if let _image  = dictionary[kImage] as? String {
            self.image = _image
        }
        if let _title = dictionary[kTitle] as? String {
            self.title = _title
        }
        if let _description = dictionary[kDescription] as? String {
            self.descrption = _description
        }
        if let _expireAt = dictionary[kExpireAt] as? String {
            self.expireAt = CommonClass.formattedDateWithString(_expireAt, format: "dd-MM-YYYY")//dateWithString(_expireAt)
        }

        
        if let _maxUsagesCount = dictionary[kMaxUsagesCount] as? Int {
            self.maxUsagesCount = _maxUsagesCount
        }else if let _maxUsagesCount = dictionary[kMaxUsagesCount] as? String {
            self.maxUsagesCount = Int(_maxUsagesCount) ?? 0
        }
        if let _perUserCount = dictionary[kPerUserCount] as? Int {
            self.perUserCount = _perUserCount
        }else if let _perUserCount = dictionary[kPerUserCount] as? String {
            self.perUserCount = Int(_perUserCount) ?? 0
        }

        if let _usedOfferCount = dictionary[kUsedOfferCount] as? Int {
            self.usedOfferCount = _usedOfferCount
        }else if let _usedOfferCount = dictionary[kUsedOfferCount] as? String {
            self.usedOfferCount = Int(_usedOfferCount) ?? 0
        }

      super.init()
    }
    
    init(json:JSON) {
        if let _ID = json[kID].int {
            self.ID = _ID
        }else if let _ID = json[kID].string {
            self.ID = Int(_ID) ?? 0
        }

        if let _offerPercentage = json[kOfferPercentage].double as Double? {
            self.offerPercentage = _offerPercentage
        } else if let _offerPercentage = json[kOfferPercentage].string {
            self.offerPercentage = Double(_offerPercentage) ?? 0.0
        }

        if let _coupanCode = json[kCoupanCode].string {
            self.coupanCode = _coupanCode
        }
        if let _image = json[kImage].string {
            self.image = _image
        }
        if let _title = json[kTitle].string {
            self.title = _title
        }
        if let _description = json[kDescription].string {
            self.descrption = _description
        }
        if let _expireAt = json[kExpireAt].string {
            self.expireAt = CommonClass.formattedDateWithString(_expireAt, format: "dd-MM-YYYY")
        }

        if let _maxUsagesCount = json[kMaxUsagesCount].int as Int? {
            self.maxUsagesCount = _maxUsagesCount
        } else if let _maxUsagesCount = json[kMaxUsagesCount].string {
            self.maxUsagesCount = Int(_maxUsagesCount) ?? 0
            
        }
        if let _perUserCount = json[kPerUserCount].int as Int? {
            self.perUserCount = _perUserCount
        } else if let _perUserCount = json[kPerUserCount].string {
            self.perUserCount = Int(_perUserCount) ?? 0
            
        }
        if let _perUserCount = json[kPerUserCount].int as Int? {
            self.perUserCount = _perUserCount
        } else if let _perUserCount = json[kPerUserCount].string {
            self.perUserCount = Int(_perUserCount) ?? 0
            
        }
        
        
        if let _usedOfferCount = json[kUsedOfferCount].int as Int? {
            self.usedOfferCount = _usedOfferCount
        }else if let _usedOfferCount = json[kUsedOfferCount].string {
            self.usedOfferCount = Int(_usedOfferCount) ?? 0
        }

       
       super.init()
    }
    
}
class OfferParser: NSObject {
    let kResponseCode = "code"
    let kTotalPages = "total_pages"
    let kTotalRecord = "total_records"
    let kOffers = "offers"
    let kOffer = "offers"
    let kResponseMessage = "message"

    
    var totalPages:Int = 0
    var totalRecord:Int = 0
    var offers = [OfferModel]()
    var offer = OfferModel()
    var responseMessage = ""
    var errorCode: ErrorCode = .failure

    override init() {
        super.init()
    }
    init(json:JSON) {
        
        if let _message = json[kResponseMessage].string{
            self.responseMessage = _message
        }
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _totalPages = json[kTotalPages].int as Int?{
            self.totalPages = _totalPages
        }
        if let _totalrecord = json[kTotalRecord].int as Int?{
            self.totalRecord = _totalrecord
        }
        
        if let offerArray = json[kOffers].arrayObject as? [[String:AnyObject]]{
            for offerDict in offerArray{
                let offer = OfferModel(dictionary: offerDict)
                self.offers.append(offer)
            }
        }
        
        
        if let offerDict = json[kOffer].dictionaryObject as [String:AnyObject]?{
            self.offer = OfferModel(dictionary: offerDict)
        }
        
        super.init()
    }
}


















