//
//  FavouriteDrivers.swift
//  Rolling Rim
//
//  Created by Parikshit on 23/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit
import SwiftyJSON

class FavouriteDrivers: NSObject {

    
    let kID = "id"
    let kisFavourite = "is_favourite"
    let kDriver = "driver"
    let kRideID = "ride_id"
    
    var ID = ""
    var isFavourite:Bool = false
    var driver = User()
    var rideID = ""
    
    override init() {
        super.init()
    }
    
    
    init(json : JSON){
        if let _userId = json[kID].int as Int?{
            self.ID = "\(_userId)"
        }else if let _userId = json[kID].string as String?{
            self.ID = _userId
        }
        if let _rideID = json[kRideID].int as Int?{
            self.rideID = "\(_rideID)"
        }else if let _rideID = json[kRideID].string as String?{
            self.rideID = _rideID
        }
        if let isFav = json[kisFavourite].bool as Bool?{
            self.isFavourite = isFav
        }

        if let _driver = json[kDriver].dictionary as Dictionary<String,AnyObject>?{
            self.driver = User(dictionary: _driver)
        }
        super.init()

    }
    
    
    init(dictionary : [String:AnyObject]){
        
        if let _userId = dictionary[kID] as? NSInteger{
            self.ID = "\(_userId)"
        }else if let _userId = dictionary[kID] as? String{
            self.ID = _userId
        }
        if let _rideID = dictionary[kRideID] as? NSInteger{
            self.rideID = "\(_rideID)"
        }else if let _rideID = dictionary[kRideID] as? String{
            self.rideID = _rideID
        }
        
        if let isFav = dictionary[kisFavourite] as? Bool{
            self.isFavourite = isFav
        }
        
        if let _driver = dictionary[kDriver] as? Dictionary<String,AnyObject>{
            self.driver = User(dictionary: _driver)
        }
        super.init()

    }
}






class FavouriteDriverParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kDrivers = "fav_driver"
    let kDriver = "fav_driver"
    var responseMessage = ""
    var drivers = [FavouriteDrivers]()
    var driver = FavouriteDrivers()
    //var payment =
    var errorCode: ErrorCode = .failure
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _message = json[kResponseMessage].string{
            self.responseMessage = _message
        }
        
        
        if let _driver =  json[kDriver].dictionaryObject as Dictionary<String,AnyObject>?{
            self.driver = FavouriteDrivers(dictionary: _driver)
        }
        
        if let _drivers  = json[kDriver].arrayObject as? [[String: AnyObject]]{
            drivers.removeAll()
            for u in _drivers{
                let driver = FavouriteDrivers(dictionary: u)
                drivers.append(driver)
            }
        }
    }
    
}

