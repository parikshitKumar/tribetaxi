//
//  Ride.swift
//  TaxiApp
//
//  Created by TecOrb on 03/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

enum RideCurrentStatus:String{
    case onGoing
    case completed
    case cancelled
    case paymentPending
    case scheduled

}

enum RideType:String {
    case instant
    case scheduled

    init(rawValue: String){
        if rawValue == "scheduled"{
            self = .scheduled
        }else{
            self = .instant
        }
    }
}

class Ride: NSObject {
    let kID = "id"//: 2,
    let kStartLocation = "start_location"//: "Fortis Hospital, Sector 62, Noida, India",
    let kStartLatitude = "start_lat"//: "28.6189",
    let kStartLongitude = "start_lng"//: "77.3725",
    let kEndLocation = "end_location"//: "H block, Sector 63, Noida",
    let kEndLatitude = "end_lat"//: "28.628454",
    let kEndLongitude = "end_lng"//: "-86.8459904",
    let kDistance = "distance"//: "10.5",
    let kRequestStatus = "request_status"//: true,
    let kRideStatus = "ride_status"//: true,
    let kIsStarted = "is_started"//: true,
    let kIsDriverReached = "driver_reached"//: true,

    let kIsSeen = "seen"//: false,
    let kIsPaid = "paid"//: false,
    let kEstimatedTime = "estimated_time"//: null,
    let kStartTime = "start_time"//: 1492674026.436616,
    let kEndTime = "end_time"//: 1492758817.979886,
    let kRating = "rating"//: null,
    let kDriver = "driver"//driver model
    let kUser = "user"//user model
    let kCar = "car"
    let kCarCategory = "car_category"
    let kDriverReject = "driver_reject"//: false,
    let kDriverCount = "driver_count"//: null,
    let kTotalPrice = "total_price"//: null,
    let kCost = "actual"
    let kIsCancel = "is_cancel"
    let kCancelCount = "cancel_count"
    let kOTP = "otp"
    let kCoupon = "coupon"
    let kCreatedAt = "created_at"
    let kEstimatedCost = "estimated"

    let kDropLocation = "drop_location" //: "Fortis noida sec 63",
    let kDropLatitude = "drop_lat"// : "28.6189",
    let kDropLongitude = "drop_lng"// : "77.3725",
    let kPaymentStatus = "paid"
    let kRideType = "ride_type"
    let kScheduleTime = "schedule_time"
    let kWallet = "wallet"
    let kCash = "cash"
    let kPaymentType = "payment_type"
    let kIsPayTip = "tip_status"
    let kDropChangedAt = "change_ride_location"
    let kIsCashPayment = "is_cash_payment"



    var ID = ""//: 2,
    var startLocation = ""//: "Fortis Hospital, Sector 62, Noida, India",
    var startLatitude:Double = 0.0//: "28.6189",
    var startLongitude:Double = 0.0//: "77.3725",
    var endLocation = ""//: "H block, Sector 63, Noida",
    var endLatitude:Double = 0.0//: "28.628454",
    var endLongitude:Double = 0.0//: "-86.8459904",
    var dropLocation = "" //: "Fortis noida sec 63",
    var dropLatitude:Double = 0.0// : "28.6189",
    var dropLongitude:Double = 0.0
    var scheduleTime:Double = 0


    var totalPrice:Double = 0.0//: "233",
    var paymentStatus = false
    var distance = "" //: "10.5",
    var requestStatus = false//: true,
    var rideStatus = false//: true,
    var isStarted = false//: true,
    var isDriverReached = false
    var isSeen = false//: false,
    var isPaid = false//: false,
    var estimatedTime = 0.0//: null,
    var startTime = ""//:Double = 0.0//: 1492674026.436616,
    var endTime = ""//:Double = 0.0//: 1492758817.979886,
    var rating:Double = 0//: null,
    var driver = User()//driver model
    var user = User()//user model
    var car = Car()//car model
    var carCategory = CarCategory()
    var isDriverReject = false
    var driverCount:Int = 0
    var cost = Cost()
    var isUserCancelled = false
    var isDriverCancelled = false

    var userCancelCount:Int = 0
    var OTP = ""
    var coupon = ""
    var createdAt = ""
    var scheduledAt = ""
    var rideType: RideType = .instant
    var estimatedCost = Cost()
    var cash: Double = 0.0
    var wallet: Double = 0.0
    var paymentType: String = ""
    var isPayTip: Bool = false
    var dropChangedAt = DropChangedAt()
    var isCashPayment: Bool = false


    override init() {
        super.init()
    }


    func getRideMapUrl(_ size: CGSize) -> URL{
        
        //let iconUrl = "http://res.cloudinary.com/hlsapp/image/upload/v1536133985/pin.png"
        let pickUpIcon = "https://res.cloudinary.com/dhgdau8v2/image/upload/v1542435278/app%20images/pin_green_3x.png"
        let dropUpIcon = "https://res.cloudinary.com/dhgdau8v2/image/upload/v1542435228/app%20images/pin.png"
        let scale = UIScreen.main.scale
        let status = self.getCurrentStatus()
        let endLocation = (status == .cancelled || status == .scheduled) ? "\(self.endLatitude),\(self.endLongitude)" : "\(self.dropLatitude),\(self.dropLongitude)"
        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=icon:\(pickUpIcon)|\(self.startLatitude),\(self.startLongitude)&markers=icon:\(dropUpIcon)|\(endLocation)&\("size=\(Int(scale*size.width))x\(Int(scale*size.height))")&key=\(GOOGLE_API_KEY)"
        let url = URL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
        return url
    }

    func isCancelled()->Bool{
        return self.isUserCancelled || (self.isDriverCancelled && self.requestStatus && !self.isStarted) || (self.isDriverCancelled && !self.requestStatus && !self.isStarted)
    }

    func getCurrentStatus() -> RideCurrentStatus{
        if self.isUserCancelled{
            return .cancelled
        }else if(self.isDriverCancelled && self.requestStatus && !self.isStarted){
            return .cancelled
        }else if(self.isDriverCancelled && !self.requestStatus && !self.isStarted){
            return .cancelled
        }else if self.rideStatus && self.paymentStatus{
            return .completed
        }else if self.rideStatus && !self.paymentStatus{
            return .paymentPending
        }else{
            if rideType == .scheduled{
                if (self.requestStatus == false) || (self.scheduleTime > Date().toMillis()){
                    return .scheduled
                }else{
                    return .onGoing
                }
            }else{
                return .onGoing
            }
        }
    }

    func mailInvoice(_ email:String, completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){
        RideService.sharedInstance.mailInvoice(for: self.ID,email: email) { (success, message) in
            completionBlock(success,message)
        }
    }
    
    init(json:JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }

        if let _startLat = json[kStartLatitude].double as Double?{
            self.startLatitude = _startLat
        }else if let _startLat = json[kStartLatitude].string as String?{
            self.startLatitude = Double(_startLat) ?? 0.0
        }

        if let _startLong = json[kStartLongitude].double as Double?{
            self.startLongitude = _startLong
        }else if let _startLong = json[kStartLongitude].string as String?{
            self.startLongitude = Double(_startLong) ?? 0.0
        }



        if let _endLat = json[kEndLatitude].double as Double?{
            self.endLatitude = _endLat
        }else if let _endLat = json[kEndLatitude].string as String?{
            self.endLatitude = Double(_endLat) ?? 0.0
        }

        if let _endLong = json[kEndLongitude].double as Double?{
            self.endLongitude = _endLong
        }else if let _endLong = json[kEndLongitude].string as String?{
            self.endLongitude = Double(_endLong) ?? 0.0
        }
        if let _startLocation = json[kStartLocation].string as String?{
            self.startLocation = _startLocation
        }
        if let _endLocation = json[kEndLocation].string as String?{
            self.endLocation = _endLocation
        }
        //updated for drop location
        if let _dropLat = json[kDropLatitude].double as Double?{
            self.dropLatitude = _dropLat
        }else if let _dropLat = json[kDropLatitude].string as String?{
            self.dropLatitude = Double(_dropLat) ?? 0.0
        }

        if let _dropLong = json[kDropLongitude].double as Double?{
            self.dropLongitude = _dropLong
        }else if let _dropLong = json[kDropLongitude].string as String?{
            self.dropLongitude = Double(_dropLong) ?? 0.0
        }
        if let _dropLocation = json[kDropLocation].string as String?{
            self.dropLocation = _dropLocation
        }
        ////End update for drop location

        if let _distance = json[kDistance].int as Int?{
            self.distance = "\(_distance)"
        }else if let _distance = json[kDistance].string as String?{
            self.distance = _distance
        }
        if let _paymentStatus = json[kPaymentStatus].bool as Bool?{
            self.paymentStatus = _paymentStatus
        }

        if let _requestStatus = json[kRequestStatus].bool as Bool?{
            self.requestStatus = _requestStatus
        }
        if let _rideStatus = json[kRideStatus].bool as Bool?{
            self.rideStatus = _rideStatus
        }
        if let _isStarted = json[kIsStarted].bool as Bool?{
            self.isStarted = _isStarted
        }
        if let _driverRejected = json[kDriverReject].bool as Bool?{
            self.isDriverCancelled = _driverRejected
        }
        if let _isReached = json[kIsDriverReached].bool as Bool?{
            self.isDriverReached = _isReached
        }
        if let _isPaid = json[kIsPaid].bool as Bool?{
            self.isPaid = _isPaid
        }
        if let _isSeen = json[kIsSeen].bool as Bool?{
            self.isSeen = _isSeen
        }

//        if let _startTime = json[kStartTime].double as Double?{
//            self.startTime = _startTime
//        }else if let _startTime = json[kStartTime].string as String?{
//            self.startTime = Double(_startTime) ?? 0.0
//        }

        if let _startTime = json[kStartTime].string as String?{
            self.startTime = CommonClass.formattedDateWithString(_startTime, format: "hh:mm a")
        }

        if let _totalPrice = json[kTotalPrice].double as Double?{
            self.totalPrice  = _totalPrice
        }else if let _totalPrice = json[kTotalPrice].string as String?{
            self.totalPrice = Double(_totalPrice) ?? 0.0
        }

        if let _endTime = json[kEndTime].string as String?{
            self.endTime = CommonClass.formattedDateWithString(_endTime, format: "hh:mm a")
        }

        if let _estimatedTime = json[kEstimatedTime].double as Double?{
            self.estimatedTime = _estimatedTime
        }else if let _estimatedTime = json[kEstimatedTime].string as String?{
            self.estimatedTime = Double(_estimatedTime) ?? 0.0
        }
        if let _rating = json[kRating].double as Double?{
            self.rating = _rating
        }else if let _rating = json[kRating].string as String?{
            self.rating = Double(_rating) ?? 0.0
        }

        if let _driver = json[kDriver].dictionaryObject as [String:AnyObject]?{
            self.driver = User(dictionary: _driver)
        }
        if let _user = json[kUser].dictionaryObject as [String:AnyObject]?{
            self.user = User(dictionary: _user)
        }
        if let _car = json[kCar].dictionaryObject as [String:AnyObject]?{
            self.car = Car(dictionary: _car)
        }
        if let _carCategory = json[kCarCategory].dictionaryObject as [String:AnyObject]?{
            self.carCategory = CarCategory(dictionary: _carCategory)
        }

        if let _driverCount = json[kDriverCount].int as Int?{
            self.driverCount = _driverCount
        }else if let _driverCount = json[kDriverCount].string as String?{
            self.driverCount = Int(_driverCount) ?? 0
        }
        if let _isDriverReject = json[kDriverReject].bool as Bool?{
            self.isDriverReject = _isDriverReject
        }
        if let _totalCost = json[kCost].dictionaryObject as [String:AnyObject]?{
            self.cost = Cost(dictionary: _totalCost)
        }
        if let _estimatedCost = json[kEstimatedCost].dictionaryObject as [String:AnyObject]?{
            self.estimatedCost = Cost(dictionary: _estimatedCost)
        }

        if let _cancelCount = json[kCancelCount].int as Int?{
            self.userCancelCount = _cancelCount
        }else if let _cancelCount = json[kCancelCount].string as String?{
            self.userCancelCount = Int(_cancelCount) ?? 0
        }
        if let _isCancelled = json[kIsCancel].bool as Bool?{
            self.isUserCancelled = _isCancelled
        }

        if let _otp = json[kOTP].int as Int?{
            let rideOtp = "\(_otp)"
            if rideOtp.count >= 4{
                self.OTP = rideOtp.last4
            }else{
                self.OTP = "\(_otp)"
            }
        }else if let _otp = json[kOTP].string as String?{
            let rideOtp = _otp
            if rideOtp.count >= 4{
                self.OTP = rideOtp.last4
            }else{
                self.OTP = "\(_otp)"
            }
        }

        if let _coupon = json[kCoupon].int as Int?{
            self.coupon = "\(_coupon)"
        }else if let _coupon = json[kCoupon].string as String?{
            self.coupon = _coupon
        }

        if let _createdAt = json[kCreatedAt].string as String?{
            self.createdAt = CommonClass.dateWithString(_createdAt)
        }
        if let _rideType = json[kRideType].string as String?{
            self.rideType = RideType(rawValue: _rideType)
        }

        if let _scheduledTime = json[kScheduleTime].double{
            self.scheduleTime = _scheduledTime
            self.scheduledAt = CommonClass.scheduleTimeString(_scheduledTime)
        }else if let _scheduledTime = json[kScheduleTime].string{
            let time = Double(_scheduledTime) ?? 0
            self.scheduleTime = time
            self.scheduledAt = CommonClass.scheduleTimeString(time)
        }
        
        if let _wallet = json[kWallet].double as Double?{
            self.wallet  = _wallet
        }else if let _wallet = json[kWallet].string as String?{
            self.wallet = Double(_wallet) ?? 0.0
        }
        
        if let _cash = json[kCash].double as Double?{
            self.cash  = _cash
        }else if let _cash = json[kCash].string as String?{
            self.cash = Double(_cash) ?? 0.0
        }

        if let _paymentType = json[kPaymentType].string as String?{
            self.paymentType = _paymentType
        }
        
        if let _isPayTip = json[kIsPayTip].bool as Bool?{
            self.isPayTip = _isPayTip
        }
        
        
        if let dropChangedAtDict = json[kDropChangedAt].dictionaryObject as Dictionary<String,AnyObject>?{
            self.dropChangedAt = DropChangedAt(dictionary: dropChangedAtDict)
        }
        
        
        if let _isCashPayment = json[kIsCashPayment].bool as Bool?{
            self.isCashPayment = _isCashPayment
        }

        super.init()
    }

    init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _startLat = dictionary[kStartLatitude] as? Double{
            self.startLatitude = _startLat
        }else if let _startLat = dictionary[kStartLatitude] as? String{
            self.startLatitude = Double(_startLat) ?? 0.0
        }

        if let _totalPrice = dictionary[kTotalPrice] as? Double{
            self.totalPrice  = _totalPrice
        }else if let _totalPrice = dictionary[kTotalPrice] as? String{
            self.totalPrice = Double(_totalPrice) ?? 0.0
        }

        if let _startLong = dictionary[kStartLongitude] as? Double{
            self.startLongitude = _startLong
        }else if let _startLong = dictionary[kStartLongitude] as? String{
            self.startLongitude = Double(_startLong) ?? 0.0
        }

        if let _endLat = dictionary[kEndLatitude] as? Double{
            self.endLatitude = _endLat
        }else if let _endLat = dictionary[kEndLatitude] as? String{
            self.endLatitude = Double(_endLat) ?? 0.0
        }

        if let _endLong = dictionary[kEndLongitude] as? Double{
            self.endLongitude = _endLong
        }else if let _endLong = dictionary[kEndLongitude] as? String{
            self.endLongitude = Double(_endLong) ?? 0.0
        }
        if let _startLocation = dictionary[kStartLocation] as? String{
            self.startLocation = _startLocation
        }
        if let _endLocation = dictionary[kEndLocation] as? String{
            self.endLocation = _endLocation
        }


        //updated for drop location
        if let _dropLat = dictionary[kDropLatitude] as? Double{
            self.dropLatitude = _dropLat
        }else if let _dropLat = dictionary[kDropLatitude] as? String{
            self.dropLatitude = Double(_dropLat) ?? 0.0
        }

        if let _dropLong = dictionary[kDropLongitude] as? Double{
            self.dropLongitude = _dropLong
        }else if let _dropLong = dictionary[kDropLongitude] as? String{
            self.dropLongitude = Double(_dropLong) ?? 0.0
        }
        if let _dropLocation = dictionary[kDropLocation] as? String{
            self.dropLocation = _dropLocation
        }
        ////End update for drop location


        if let _distance = dictionary[kDistance] as? Int{
            self.distance = "\(_distance)"
        }else if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }

        if let _requestStatus = dictionary[kRequestStatus] as? Bool{
            self.requestStatus = _requestStatus
        }
        if let _rideStatus = dictionary[kRideStatus] as? Bool{
            self.rideStatus = _rideStatus
        }
        if let _isStarted = dictionary[kIsStarted] as? Bool{
            self.isStarted = _isStarted
        }
        if let _driverRejected = dictionary[kDriverReject] as? Bool{
            self.isDriverCancelled = _driverRejected
        }
        
        if let _isReached = dictionary[kIsDriverReached] as? Bool{
            self.isDriverReached = _isReached
        }
        if let _isPaid = dictionary[kIsPaid] as? Bool{
            self.isPaid = _isPaid
        }
        if let _isSeen = dictionary[kIsSeen] as? Bool{
            self.isSeen = _isSeen
        }
        if let _paymentStatus = dictionary[kPaymentStatus] as? Bool{
            self.paymentStatus = _paymentStatus
        }
//        if let _startTime = dictionary[kStartTime] as? Double{
//            self.startTime = _startTime
//        }else if let _startTime = dictionary[kStartTime] as? String{
//            self.startTime = Double(_startTime) ?? 0.0
//        }

        if let _startTime = dictionary[kStartTime] as? String{
            self.startTime = CommonClass.formattedDateWithString(_startTime, format: "hh:mm a")
        }
//        if let _endTime = dictionary[kEndTime] as? Double{
//            self.endTime = _endTime
//        }else if let _endTime = dictionary[kEndTime] as? String{
//            self.endTime = Double(_endTime) ?? 0.0
//        }

        if let _endTime = dictionary[kEndTime] as? String{
            self.endTime = CommonClass.formattedDateWithString(_endTime, format: "hh:mm a")
        }
        if let _estimatedTime = dictionary[kEstimatedTime] as? Double{
            self.estimatedTime = _estimatedTime
        }else if let _estimatedTime = dictionary[kEstimatedTime] as? String{
            self.estimatedTime = Double(_estimatedTime) ?? 0.0
        }
        if let _rating = dictionary[kRating] as? Double{
            self.rating = _rating
        }else if let _rating = dictionary[kRating] as? String{
            self.rating = Double(_rating) ?? 0.0
        }
        if let _driver = dictionary[kDriver] as? [String:AnyObject]{
            self.driver = User(dictionary: _driver)
        }


        if let _user = dictionary[kUser] as? [String:AnyObject]{
            self.user = User(dictionary: _user)
        }
        if let _car = dictionary[kCar] as? [String:AnyObject]{
            self.car = Car(dictionary: _car)
        }
        if let _carCategory = dictionary[kCarCategory] as? [String:AnyObject]{
            self.carCategory = CarCategory(dictionary: _carCategory)
        }

        if let _driverCount = dictionary[kDriverCount] as? Int{
            self.driverCount = _driverCount
        }else if let _driverCount = dictionary[kDriverCount] as? String{
            self.driverCount = Int(_driverCount) ?? 0
        }
        if let _isDriverReject = dictionary[kDriverReject] as? Bool{
            self.isDriverReject = _isDriverReject
        }
        if let _totalCost = dictionary[kCost] as? [String:AnyObject]{
            self.cost = Cost(dictionary: _totalCost)
        }
        if let _estimatedCost = dictionary[kEstimatedCost] as? [String:AnyObject]{
            self.estimatedCost = Cost(dictionary: _estimatedCost)
        }

        if let _cancelCount = dictionary[kCancelCount] as? Int{
            self.userCancelCount = _cancelCount
        }else if let _cancelCount = dictionary[kCancelCount] as? String{
            self.userCancelCount = Int(_cancelCount) ?? 0
        }
        if let _isCancelled = dictionary[kIsCancel] as? Bool{
            self.isUserCancelled = _isCancelled
        }

        if let _otp = dictionary[kOTP] as? Int{
            let rideOtp = "\(_otp)"
            if rideOtp.count >= 4{
                self.OTP = rideOtp.last4
            }else{
                self.OTP = "\(_otp)"
            }
        }else if let _otp = dictionary[kOTP] as? String{
            let rideOtp = _otp
            if rideOtp.count >= 4{
                self.OTP = rideOtp.last4
            }else{
                self.OTP = "\(_otp)"
            }
        }

        if let _coupon = dictionary[kCoupon] as? Int{
            self.coupon = "\(_coupon)"
        }else if let _coupon = dictionary[kCoupon] as? String{
            self.coupon = _coupon
        }

        if let _createdAt = dictionary[kCreatedAt] as? String{
            self.createdAt = CommonClass.dateWithString(_createdAt)
        }
        if let _rideType = dictionary[kRideType] as? String{
            self.rideType = RideType(rawValue: _rideType)
        }

        if let _scheduledTime = dictionary[kScheduleTime] as? Double{
            self.scheduleTime  = _scheduledTime
            self.scheduledAt = CommonClass.scheduleTimeString(_scheduledTime)
        }else if let _scheduledTime = dictionary[kScheduleTime] as? String{
            let time = Double(_scheduledTime) ?? 0
            self.scheduleTime = time
            self.scheduledAt = CommonClass.scheduleTimeString(time)
        }
        
        if let _wallet = dictionary[kWallet] as? Double{
            self.wallet  = _wallet
        }else if let _wallet = dictionary[kWallet] as? String{
            self.wallet = Double(_wallet) ?? 0.0
        }
        
        if let _cash = dictionary[kCash] as? Double{
            self.cash  = _cash
        }else if let _cash = dictionary[kCash] as? String{
            self.cash = Double(_cash) ?? 0.0
        }
        
        
        if let _paymentType = dictionary[kPaymentType] as? String{
            self.paymentType = _paymentType
        }
        if let _isPayTip = dictionary[kIsPayTip] as? Bool{
            self.isPayTip = _isPayTip
        }
        
        
        if let dropChangedAtDict = dictionary[kDropChangedAt] as? Dictionary<String,AnyObject>{
            self.dropChangedAt = DropChangedAt(dictionary: dropChangedAtDict)
        }
        
        if let _isCashPayment = dictionary[kIsCashPayment] as? Bool{
            self.isCashPayment = _isCashPayment
        }

        super.init()
    }
}


class Cost: NSObject {
    let kDistance = "distance"
    let kTime = "time"
    let kTotalPrice = "cost"
    let kOfferPrice = "offer_price"
    let kTax = "tax"

    var distance:Double = 0.0
    var time:Double = 0.0
    var totalPrice:Double = 0.0
    var offerPrice:Double = 0.0
    var tax:Double = 0.0

    override init() {
        super.init()
    }

    init(dictionary:[String:AnyObject]) {
        if let _dist = dictionary[kDistance] as? Double{
            self.distance = _dist
        }else if let _dist = dictionary[kDistance] as? String{
            self.distance = Double(_dist) ?? 0.0
        }

        if let _time = dictionary[kTime] as? Double{
            self.time = _time
        }else if let _time = dictionary[kTime] as? String{
            self.time = Double(_time) ?? 0.0
        }
        if let _totalPrice = dictionary[kTotalPrice] as? Double{
            self.totalPrice = _totalPrice
        }else if let _totalPrice = dictionary[kTotalPrice] as? String{
            self.totalPrice = Double(_totalPrice) ?? 0.0
        }
        if let _OfferPrice = dictionary[kOfferPrice] as? Double{
            self.offerPrice = _OfferPrice
        }else if let _OfferPrice = dictionary[kOfferPrice] as? String{
            self.offerPrice = Double(_OfferPrice) ?? 0.0
        }
        if let _tax = dictionary[kTax] as? Double{
            self.tax = _tax
        }else if let _tax = dictionary[kTax] as? String{
            self.tax = Double(_tax) ?? 0.0
        }

        super.init()
    }

}

class RideParser: NSObject {
    let kResponseCode = "response_code"
    let kResponseMessage = "message"
    let kRides = "rides"
    let kRide = "ride"
    let kPendingRide = "pending_ride"
    let kScheduleRide = "schedule_ride"
    let kTotalPages = "total_records"
    let kBookings = "result"

    var errorCode : ErrorCode = .failure
    var responseMessage = ""
    var rides = [Ride]()
    var ride = Ride()
    var totalPages:Int = 0

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }else if let _rCode = json["code"].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _totalPages = json[kTotalPages].int as Int?{
            self.totalPages = _totalPages
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }else if let _rMessage = json["message"].string as String?{
            self.responseMessage = _rMessage
        }

        if let rideArray = json[kBookings].arrayObject as? [[String:AnyObject]]{
            for rideDict in rideArray{
                let ride = Ride(dictionary: rideDict)
                self.rides.append(ride)
            }
        }

        if let rideArray = json[kRides].arrayObject as? [[String:AnyObject]]{
            for rideDict in rideArray{
                let ride = Ride(dictionary: rideDict)
                self.rides.append(ride)
            }
        }

        if let _ride = json[kRide].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ride = Ride(dictionary: _ride)
        }
        if let _ride = json[kPendingRide].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ride = Ride(dictionary: _ride)
        }
        if let _ride = json[kScheduleRide].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ride = Ride(dictionary: _ride)
            self.ride.rideType = .scheduled
        }
        super.init()
    }
}

class BookingParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRides = "rides"
    let kBookings = "result"
    let kRide = "ride"
    let kBooking = "booking"
    let kTotalPages = "total_records"

    var responseMessage = ""
    var rides = [Ride]()
    var ride = Ride()
    var totalPages:Int = 0

    override init() {
        super.init()
    }
    var errorCode : ErrorCode = .failure
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let _totalPages = json[kTotalPages].int as Int?{
            self.totalPages = _totalPages
        }

        if let rideArray = json[kBookings].arrayObject as? [[String:AnyObject]]{
            for rideDict in rideArray{
                let ride = Ride(dictionary: rideDict)
                self.rides.append(ride)
            }
        }

        if let _ride = json[kBooking].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ride = Ride(dictionary: _ride)
        }

        super.init()
    }
}



class RideLocationModel: NSObject {
    let kID = "id"
    let kLatitude = "latitude" //: 28.6293576,
    let kLongitude = "longitude" //: 77.3841646,
    let kPreviousLatitude = "previous_lat" //: 28.6293576,
    let kPreviousLongitude = "previous_lng" //: 77.3841646,
    let kDistance = "distance"//: "1.6 km",
    let kTime = "time"//: "6 mins",
    let kWearing = "wearing"
    
    var ID = ""
    var latitude:Double = 0.0 //: 28.6293576,
    var wearing:Double = 0.0 //: 28.6293576,
    var longitude:Double = 0.0 //: 77.3841646,
    var previousLatitude:Double = 0.0 //: 28.6293576,
    var previousLongitude:Double = 0.0 //: 77.3841646,
    var distance = ""//: "1.6 km",
    var time = ""//: "6 mins",

    override init() {
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        
        if let _wearing = dictionary[kWearing] as? Double{
            self.wearing = _wearing
        }else if let _wearing = dictionary[kWearing] as? String{
            self.wearing = Double(_wearing) ?? 0.0
        }

        if let _Lat = dictionary[kLatitude] as? Double{
            self.latitude = _Lat
        }else if let _Lat = dictionary[kLatitude] as? String{
            self.latitude = Double(_Lat) ?? 0.0
        }

        if let _Longi = dictionary[kLongitude] as? Double{
            self.longitude = _Longi
        }else if let _Longi = dictionary[kLongitude] as? String{
            self.longitude = Double(_Longi) ?? 0.0
        }

        if let _pLat = dictionary[kPreviousLatitude] as? Double{
            self.previousLatitude = _pLat
        }else if let _pLat = dictionary[kPreviousLatitude] as? String{
            self.previousLatitude = Double(_pLat) ?? 0.0
        }

        if let _pLongi = dictionary[kPreviousLongitude] as? Double{
            self.previousLongitude = _pLongi
        }else if let _pLongi = dictionary[kPreviousLongitude] as? String{
            self.previousLongitude = Double(_pLongi) ?? 0.0
        }
        if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }

        if let _time = dictionary[kTime] as? String{
            self.time = _time
        }
        super.init()
    }
}





class CancelReasonParser: NSObject {
    enum keys: String, CodingKey {
        case cancelReasons = "cancel_reasons"
        case code = "code"
        case message = "message"
    }
    
    var message = ""
    var reasons = [CancelReason]()
    var errorCode : ErrorCode = .failure
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        
        if let message = json[keys.message.stringValue].string as String?{
            self.message = message
        }
        
        
        if let reasonsArray = json[keys.cancelReasons.stringValue].arrayObject as? [[String:AnyObject]]{
            for reasonDict in reasonsArray{
                let reason = CancelReason(dictionary: reasonDict)
                self.reasons.append(reason)
            }
        }
        
        super.init()
    }
    
}

class CancelReason: NSObject {
    enum keys: String, CodingKey {
        case id = "id"
        case reason = "reason"
        case role = "role"
    }
    
    var id = ""
    var reason = ""
    var role = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        if let id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dictionary[keys.id.stringValue] as? String{
            self.id = id
        }
        
        if let reason = dictionary[keys.reason.stringValue] as? String{
            self.reason = reason
        }
        
        if let role = dictionary[keys.role.stringValue] as? String{
            self.role = role
        }
        
        super.init()
    }
}




class DropChangedAt: NSObject {
    enum keys: String,CodingKey {
        case address = "start_location"
        case latitude = "start_lat"
        case longitude = "start_lng"
    }
    
    private var latitude:Double = 0
    private var longitude:Double = 0
    var address = ""
    
    var coordinate2D : CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        if let latitude = dictionary[keys.latitude.stringValue] as? Double{
            self.latitude = latitude
        }else if let latitude = dictionary[keys.latitude.stringValue] as? String{
            self.latitude = Double(latitude) ?? 0.0
        }
        
        if let longitude = dictionary[keys.longitude.stringValue] as? Double{
            self.longitude = longitude
        }else if let longitude = dictionary[keys.longitude.stringValue] as? String{
            self.longitude = Double(longitude) ?? 0.0
        }
        
        if let address = dictionary[keys.address.stringValue] as? String{
            self.address = address
        }
        
        super.init()
    }
}
