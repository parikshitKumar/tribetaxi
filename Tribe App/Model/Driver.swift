//
//  Driver.swift
//  TaxiApp
//
//  Created by tecorb on 3/28/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps

class TimeMarker: GMSMarker {
    var time: String!
    init(time:String) {
        self.time = time
        super.init()
        self.icon = imageForMarker()
    }
    
    

    func  imageForMarker() -> UIImage {
        let myview = MarkerView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 40, height: 71)
        myview.priceLabel.text = time
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }

    func imageWithView(view : UIView,scale:CGFloat) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let myimg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return myimg
    }
}







class Driver: NSObject {
    let kLocation  = "l"
    let kPLocation  = "l"
    let driverID = ""
    
    var location = Coordinate()
    var pLocation = PreviousCoordinate()
    var id : String = ""
    var type: String = "car"
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _location = json[kLocation].dictionaryObject as [String:AnyObject]?{
            self.location = Coordinate(dictionary: _location)
        }
        if let _pLocation = json[kPLocation].dictionaryObject as [String:AnyObject]?{
            self.pLocation = PreviousCoordinate(dictionary: _pLocation)
        }
        if let _id = json[driverID].string as String?{
            self.id = _id
        }

        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _location = dictionary[kLocation] as? [String:AnyObject]{
            self.location = Coordinate(dictionary: _location)
        }
        if let _pLocation = dictionary[kPLocation] as? [String:AnyObject]{
            self.pLocation = PreviousCoordinate(dictionary: _pLocation)
        }
        if let _id = dictionary[driverID] as? String{
            self.id = _id
        }
        super.init()
    }
}

class Coordinate: NSObject {
    let kLat = "0"
    let kLong = "1"
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _latitude = json[kLat].double as Double?{
            self.latitude = _latitude
        }else if let _latitude = json[kLat].string as String?{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.longitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _latitude = dictionary[kLat] as? Double{
            self.latitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.longitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
}

class PreviousCoordinate: NSObject {
    let kLat = "0"
    let kLong = "1"
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _latitude = json[kLat].double as Double?{
            self.latitude = _latitude
        }else if let _latitude = json[kLat].string as String?{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.longitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _latitude = dictionary[kLat] as? Double{
            self.latitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.longitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.longitude = Double(_longitude) ?? 0.0
        }
        super.init()
    }
}



class AddressMarker: GMSMarker {
    var address: String!
    var markerImage: UIImage!
    var infoMode: String
    init(address:String,image: UIImage, infoMode: String ) {
        self.address = address
        self.markerImage = image
        self.infoMode = infoMode
        super.init()
        if infoMode == "left" {
            self.icon = leftImageForMarker()

        }else{
        self.icon = imageForMarker()
        }
    }
    
    
    
    func  imageForMarker() -> UIImage {
        let myview = MarkerInfoView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 300, height: 100)
        myview.infoLabel.text = address
        myview.markerImage.image = markerImage
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }
    
    
    func leftImageForMarker() -> UIImage {
        
        let myview = LeftMarkerInfo.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 300, height: 100)
        myview.leftInfoLabel.text = address
        myview.markerImage.image = markerImage
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }
    
    func imageWithView(view : UIView,scale:CGFloat) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let myimg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return myimg
    }
}




class ActionCableDriver: NSObject {
    let kLat  = "latitude"
    let kLong  = "longitude"
    let kDriverID = "driver_id"
    let KIsAddedDriver = "is_added"

    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var previousLatitude: Double = 0.0
    var previousLongitude: Double = 0.0
    var driverID = ""
    var isAddedDriver: Bool = false

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _latitude = json[kLat].double as Double?{
            self.latitude = _latitude
        }else if let _latitude = json[kLat].string as String?{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.longitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.longitude = Double(_longitude) ?? 0.0
        }
        
        if let _id = json[kDriverID].string as String?{
            self.driverID = _id
        }else if let _id = json[kDriverID].int as Int?{
            self.driverID = "\(_id)"
        }
        
        if let _longitude = json[kLat].double as Double?{
            self.previousLatitude = _longitude
        }else if let _longitude = json[kLat].string as String?{
            self.previousLatitude = Double(_longitude) ?? 0.0
        }
        if let _longitude = json[kLong].double as Double?{
            self.previousLongitude = _longitude
        }else if let _longitude = json[kLong].string as String?{
            self.previousLongitude = Double(_longitude) ?? 0.0
        }
        
        if let _id = json[kDriverID].string as String?{
            self.driverID = _id
        }else if let _id = json[kDriverID].int as Int?{
            self.driverID = "\(_id)"
        }
        
        
        if let _isAddedDriver = json[KIsAddedDriver].bool as Bool?{
            self.isAddedDriver = _isAddedDriver
        }

        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _latitude = dictionary[kLat] as? Double{
            self.latitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.latitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.longitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.longitude = Double(_longitude) ?? 0.0
        }
        if let _latitude = dictionary[kLat] as? Double{
            self.previousLatitude = _latitude
        }else if let _latitude = dictionary[kLat] as? String{
            self.previousLatitude = Double(_latitude) ?? 0.0
        }
        if let _longitude = dictionary[kLong] as? Double{
            self.previousLongitude = _longitude
        }else if let _longitude = dictionary[kLong] as? String{
            self.previousLongitude = Double(_longitude) ?? 0.0
        }
        if let _id = dictionary[kDriverID] as? String{
            self.driverID = _id
        }else if let _id = dictionary[kDriverID] as? Int{
            self.driverID = "\(_id)"
        }
        if let _isAddedDriver = dictionary[KIsAddedDriver] as? Bool{
            self.isAddedDriver = _isAddedDriver
        }
        
        super.init()
    }
}


class DriverParser: NSObject {
    
    var errorCode = ErrorCode.failure
    var responseMessage = ""
    var drivers = Array<ActionCableDriver>()
    var driver = ActionCableDriver()
    
    var trackDriver = ActionCableDriver()
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {

        
        if let _cards = json["drivers"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            drivers.removeAll()
            for _cardDict in _cards{
                let card = ActionCableDriver(dictionary: _cardDict)
                self.drivers.append(card)
            }
        }
        
        if let _card = json["drivers"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.driver = ActionCableDriver(dictionary: _card)
        }
        
        if let _card = json.dictionaryObject as Dictionary<String,AnyObject>?{
            self.trackDriver = ActionCableDriver(dictionary: _card)
        }
        
    }
    


}

//
class StatusKeyParser: NSObject {

       let  kIsAccepted = "is_accepted"
       let kIsUserCancelled = "is_user_cancelled"
       let  kIsDriverCancelled = "is_driver_cancelled"
       let kIsStarted = "is_started"
       let kIsEnded = "is_ended"
       let kIsReached = "is_reached"
       let  kTitle = "title"
       let kIsAdminCancelled = "is_admin_cancelled"



    var isAccepted:Bool = false
    var isUserCancelled:Bool = false
    var isDriverCancelled:Bool = false
    var isStarted:Bool = false
    var isEnded:Bool = false
    var isReached:Bool = false
    var title = ""
    var isAdminCancelled:Bool = false

    override init() {
        super.init()
    }

    init(json: JSON) {
        


        if let _isAccepted = json[kIsAccepted].bool as Bool?{
            self.isAccepted = _isAccepted
        }
        if let _isUserCancelled = json[kIsUserCancelled].bool as Bool? {
            self.isUserCancelled = _isUserCancelled
        }
        if let _isDriverCancelled = json[kIsDriverCancelled].bool as Bool?{
            self.isDriverCancelled = _isDriverCancelled
        }
        if let _isStarted = json[kIsStarted].bool as Bool?{
            self.isStarted = _isStarted
        }
        if let _isEnded = json[kIsEnded].bool as Bool?{
            self.isEnded = _isEnded
        }
        if let _isReached = json[kIsReached].bool as Bool?{
            self.isReached = _isReached
        }
        if let _title = json[title].string as String?{
            self.title = _title
        }
        
        if let _isAdminCancelled = json[kIsAdminCancelled].bool as Bool?{
            self.isAdminCancelled = _isAdminCancelled
        }

    }



}
