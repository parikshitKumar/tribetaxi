//
//  WalletModel.swift
//  JO CAB
//
//  Created by Parikshit on 27/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
import SwiftyJSON
//


class Bank:NSObject{

    let kID = "id"
    let KAccountID = "account_id"
    let kAccountHolderName = "account_holder_name"
    let kAccountType = "account_type"
    let kBankName = "bank_name"
    let kLast4 = "last4"
    let kCountryCode = "country_code"
    let kContactNumber = "contact_no"
    
    var ID = ""
    var accountID : String = ""
    var accountHolderName : String = ""
    var accountType : String = ""
    var bankName : String = ""
    var last4 : String = ""
    var countryCode = ""
    var contactNumber = ""
    
    override init() {
        super.init()
    }
    
    init(params:Dictionary<String,AnyObject>) {
        
        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }
        
        if let _accountID = params[KAccountID] as? Int{
            self.accountID = "\(_accountID)"
        }else if let _accountID = params[KAccountID] as? String{
            self.accountID = _accountID
        }
        
        if let _accountHolderName = params[kAccountHolderName] as? String{
            self.accountHolderName = _accountHolderName
        }
        if let _accountType = params[kAccountType] as? String{
            self.accountType = _accountType
        }
        if let _bankName = params[kBankName] as? String{
            self.bankName = _bankName
        }
        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _countrtCode = params[kCountryCode] as? String{
            self.countryCode = _countrtCode
        }
        if let _contactNumber = params[kContactNumber] as? String{
            self.contactNumber = _contactNumber
        }

        super.init()
    }
}


class WalletParser : NSObject {
    
    var errorCode = ErrorCode.failure
    var responseMessage = ""
    var banks = Array<Bank>()
    var bank = Bank()
    var walletBalance : Double = 0.0

    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        if let _code = json["code"].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }
        
        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }
                
        
        if let walletBalance = json["wallet_balance"].double as Double?{
            self.walletBalance = walletBalance
        }else if let walletBalance = json["wallet_balance"].string as String?{
            self.walletBalance = Double(walletBalance) ?? 0.0
        }
        
        
        if let _banks = json["bank_detail"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _bankDict in _banks{
                let bank = Bank(params: _bankDict)
                self.banks.append(bank)
            }
        }
        
        if let _bank = json["bank_detail"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.bank = Bank(params: _bank)
        }
        
    }
    
}
    
    
    class WalletHistoryModel: NSObject{
        
        let kAmount = "amount"
        let kDate = "date"
        let kStatus = "status"
        let kIsAdd = "is_add"
        let kIsPaid = "is_paid"
        let kPaymentMode = "payment_mode"
        
        var amount: Double = 0.0
        var date = ""
        var status: Bool = false
        var isAdd: Bool = false
        var isPaid: Bool = false
        var paymentMode = ""
        
        override init() {
            super.init()
        }
        
        init(params:Dictionary<String,AnyObject>) {
            
            if let _amount = params[kAmount] as? Double{
                self.amount = _amount
            }else if let _amount = params[kAmount] as? String{
                self.amount = Double(_amount) ?? 0.0
            }
            
            
            if let _date = params[kDate] as? String{
                self.date = _date
            }
            
            if let _status = params[kStatus] as? Bool{
                self.status = _status
            }
            
            if let _isAdded = params[kIsAdd] as? Bool{
                self.isAdd = _isAdded
            }
            
            if let _isPaid = params[kIsPaid] as? Bool{
                self.isPaid = _isPaid
            }
            
            if let _paymentMode = params[kPaymentMode] as? String{
                self.paymentMode = _paymentMode
            }
            super.init()
        }
    }
    
    class HistoryWalletParser : NSObject {
        
        
        var errorCode = ErrorCode.failure
        var responseMessage = ""
        var walletHistories = Array<WalletHistoryModel>()
        var walletHistory = WalletHistoryModel()
        var walletBalance : Double = 0.0
        var totalPages: Int = 0
        
        
        
        
        override init() {
            super.init()
        }
        
        init(json: JSON) {
            if let _code = json["code"].int{
                self.errorCode = ErrorCode(rawValue: _code)
            }
            
            if let _totalPages = json["remaining_count"].int as Int?{
                self.totalPages = _totalPages
            }
            
            
            if let _msg = json["message"].string as String?{
                self.responseMessage = _msg
            }
            
            
            if let _banks = json["wallet_history"].arrayObject as? Array<Dictionary<String,AnyObject>>{
                for _bankDict in _banks{
                    let bank = WalletHistoryModel(params: _bankDict)
                    self.walletHistories.append(bank)
                }
            }
            
            if let _bank = json["wallet_history"].dictionaryObject as Dictionary<String,AnyObject>?{
                self.walletHistory = WalletHistoryModel(params: _bank)
            }
            
        }
}
