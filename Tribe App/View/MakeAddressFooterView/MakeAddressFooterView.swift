//
//  MakeAddressFooterView.swift
//  HLS Taxi
//
//  Created by Parikshit on 23/12/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class MakeAddressFooterView: UIView {
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    

    class func instanceFromNib() -> MakeAddressFooterView {
        
        return UINib(nibName: "MakeAddressFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MakeAddressFooterView
    }
    
}
