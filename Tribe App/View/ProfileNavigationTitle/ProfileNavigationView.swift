//
//  ProfileNavigationView.swift
//  JO CAB
//
//  Created by Parikshit on 24/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class ProfileNavigationView: UIView {
    @IBOutlet weak var profileImage :UIImageView!
    @IBOutlet weak var titleLabel :UILabel!
    @IBOutlet weak var profileButton :UIButton!
    
    @IBOutlet weak var showHelloLabel: UILabel!


    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.profileImage, borderColor: appColor.red , borderWidth: 1)
    }


    
    class func instanceFromNib() -> ProfileNavigationView {
        return UINib(nibName: "ProfileNavigationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProfileNavigationView
    }

}
