//
//  CategoryCell.swift
//  TaxiApp
//
//  Created by TecOrb on 13/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    //@IBOutlet weak var fareLabel:UILabel!
    @IBOutlet weak var containner:UIView!
    @IBOutlet weak var categoryIcon:UIImageView!
    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 5)
    }

}




