//
//  SupportBarButtonItem.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SupportBarButtonItem: UIView {
    @IBOutlet weak var supportButton:UIButton!
    class func instanceFromNib() -> SupportBarButtonItem {
        return UINib(nibName: "SupportBarButtonItem", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SupportBarButtonItem
    }
}

