//
//  FavouriteDriversCell.swift
//  Rolling Rim
//
//  Created by Parikshit on 23/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class FavouriteDriversCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var favButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        CommonClass.makeViewCircular(self.userImageView, borderColor: appColor.greenColor, borderWidth: 1)
        
    }
    
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.userImageView, borderColor: appColor.blueColor, borderWidth: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
