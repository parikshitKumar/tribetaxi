//
//  ChangeLanguageTableViewCell.swift
//  HLSDriver
//
//  Created by admin on 07/12/18.
//  Copyright © 2018 Tecorb. All rights reserved.
//

import UIKit

class ChangeLanguageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var changeLbl:UILabel!
    @IBOutlet weak var englishButton:UIButton!
    @IBOutlet weak var portugueseButton:UIButton!
    @IBOutlet weak var imageIcon:UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
