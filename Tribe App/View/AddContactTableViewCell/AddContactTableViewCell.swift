//
//  AddContactTableViewCell.swift
//  TaxiApp
//
//  Created by tecorb on 3/14/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class AddContactTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
