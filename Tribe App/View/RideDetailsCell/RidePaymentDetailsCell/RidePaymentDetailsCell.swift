//
//  RidePaymentDetailsCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 30/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RidePaymentDetailsCell: UITableViewCell {
    @IBOutlet weak var yourTripLabel: UILabel!
    @IBOutlet weak var taxesLabel: UILabel!
    @IBOutlet weak var totalBillLabel: UILabel!
    @IBOutlet weak var paymentModeLabel: UILabel!
    
    @IBOutlet weak var showBillDetailsLabel: UILabel!
    @IBOutlet weak var showYourTripLabel: UILabel!
    @IBOutlet weak var showIncludingTaxLabel: UILabel!
    @IBOutlet weak var showTotalBillLabel: UILabel!
    @IBOutlet weak var showPaymentModeLabel: UILabel!
//    @IBOutlet weak var offerPrice: UILabel!



    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
