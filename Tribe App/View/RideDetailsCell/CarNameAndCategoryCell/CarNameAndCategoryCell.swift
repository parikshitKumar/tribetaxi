//
//  CarNameAndCategoryCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 24/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class CarNameAndCategoryCell: UITableViewCell {
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.carIcon, borderColor: appColor.blueColor, borderWidth: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
