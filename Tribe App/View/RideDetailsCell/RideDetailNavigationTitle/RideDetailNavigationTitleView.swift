//
//  RideDetailNavigationTitleView.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 31/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RideDetailNavigationTitleView: UIView {
    @IBOutlet weak var crnLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    class func instanceFromNib() -> RideDetailNavigationTitleView {
        return UINib(nibName: "RideDetailNavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RideDetailNavigationTitleView
    }
}
