//
//  SelectLanguageView.swift
//  JO CAB
//
//  Created by Parikshit on 15/12/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class SelectLanguageView: UIView {

//    @IBOutlet weak var selectLanguageButton: UIButton!
    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var selectLanguageTextField: UITextField!

    class func instanceFromNib() -> SelectLanguageView {
        return UINib(nibName: "SelectLanguageView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SelectLanguageView
    }

}
