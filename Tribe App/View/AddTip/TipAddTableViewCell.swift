//
//  TipAddTableViewCell.swift
//  HLS Taxi
//
//  Created by Parikshit on 22/12/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class TipAddTableViewCell: UITableViewCell {
    @IBOutlet weak var showAddTheTipForLabel: UILabel!
    @IBOutlet weak var oneCHFButton: UIButton!
    @IBOutlet weak var twoCHFButton: UIButton!
    @IBOutlet weak var fiveCHFButton: UIButton!
    @IBOutlet weak var enterCHFTextField: UITextField!
    @IBOutlet weak var paytTipButtonButton: UIButton!
    @IBOutlet weak var oneCHFLabel: UILabel!
    @IBOutlet weak var twoCHFLabel: UILabel!
    @IBOutlet weak var fiveCHFLabel: UILabel!
    @IBOutlet weak var staticOneCHFLabel: UILabel!
    @IBOutlet weak var staticTwoCHFLabel: UILabel!
    @IBOutlet weak var staticFiveCHFLabel: UILabel!
    @IBOutlet weak var oneCHFView: UIView!
    @IBOutlet weak var twoCHFView: UIView!
    @IBOutlet weak var fiveCHFView: UIView!

//    @IBOutlet weak var completeButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        drawTipView()
        
    }
    
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.paytTipButtonButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.paytTipButtonButton.frame.size.height/2)
        CommonClass.makeViewCircularWithCornerRadius(self.enterCHFTextField, borderColor: .clear, borderWidth: 0, cornerRadius: self.paytTipButtonButton.frame.size.height/2)

        CommonClass.makeCircularTopRadius(self, cornerRadius: 15.0)

        
    }
    
    
    func drawTipView() {
        oneCHFView.layer.cornerRadius = 0.5 * oneCHFView.bounds.size.width
        oneCHFView.clipsToBounds = true
        oneCHFView.layer.borderColor = appColor.red.cgColor
        oneCHFView.layer.borderWidth = 1
        twoCHFView.layer.cornerRadius = 0.5 * twoCHFView.bounds.size.width
        twoCHFView.clipsToBounds = true
        twoCHFView.layer.borderColor = appColor.red.cgColor
        twoCHFView.layer.borderWidth = 1
        fiveCHFView.layer.cornerRadius = 0.5 * fiveCHFView.bounds.size.width
        fiveCHFView.clipsToBounds = true
        fiveCHFView.layer.borderColor = appColor.red.cgColor
        fiveCHFView.layer.borderWidth = 1
        
        
        
    }
    
//    override func layoutSubviews() {
//        self.separatorImage.image = UIImage.drawDottedSeparatorImage(width: separatorImage.frame.size.width, height: 1, color: .lightGray)
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
