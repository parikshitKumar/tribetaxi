//
//  CompetedRideCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
class CompletedRideCell: UITableViewCell {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var carCategoryLabel: UILabel!
    @IBOutlet weak var crnLabel: UILabel!
    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var showFromLabel: UILabel!
    @IBOutlet weak var showToLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var backView: UIView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

//        backView.backgroundColor = UIColor.white
//        backView.layer.shadowOffset = CGSize.zero
//        backView.layer.shadowOpacity = 1.0
//        backView.layer.shadowRadius = 30.0
//        backView.layer.masksToBounds = false
//        CommonClass.makeCircularBottomRadius(backView, cornerRadius: 15.0)
//        backView.addshadow(top: false, left: false, bottom: true, right: false, shadowRadius: 30, shadowOpacity: 0.2, shadowColor: appColor.lightGray)
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(driverIcon, borderColor: appColor.red, borderWidth: 1)
//        CommonClass.makeViewCircularWithCornerRadius(self.backView, borderColor: .lightGray, borderWidth: 0, cornerRadius: 20)
//        self.backView.dropShadow(shadowRadius:10,shadowOffset: CGSize(width: 0, height: 10))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
