//
//  InCompetedRideCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class InCompletedRideCell: UITableViewCell {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var carCategoryLabel: UILabel!
    @IBOutlet weak var crnLabel: UILabel!
    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var rideStatusLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var showFromLabel: UILabel!
    @IBOutlet weak var showToLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var showAmountCurrency: UILabel!
    @IBOutlet weak var backView: UIView!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(driverIcon, borderColor: appColor.red, borderWidth: 1)
//        CommonClass.makeCircularBottomRadius(backView, cornerRadius: 15.0)
//        backView.backgroundColor = UIColor.white
//        backView.layer.shadowOffset = CGSize.zero
//        backView.layer.shadowOpacity = 1.0
//        backView.layer.shadowRadius = 30.0
//        backView.layer.masksToBounds = false
    

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//
//        // add shadow on cell
//        backgroundColor = UIColor.red // very important
//        layer.masksToBounds = false
//        layer.shadowOpacity = 4.0
//        layer.shadowRadius = 4
//        layer.shadowOffset = CGSize(width: 0, height: 0)
//        layer.shadowColor = UIColor.black.cgColor
//
//        // add corner radius on `contentView`
//        contentView.backgroundColor = .green
//        contentView.layer.cornerRadius = 8
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    

    
    
}


