//
//  RecentPlaceCell.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 02/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class RecentPlaceCell: UITableViewCell {
    @IBOutlet weak var placeIcon : UIImageView!
    @IBOutlet weak var toggleButton : UIButton!
    @IBOutlet weak var addressTitleLabel : UILabel!
    @IBOutlet weak var addressDetailsLabel : UILabel!

    override func prepareForReuse() {
        self.addressTitleLabel.text = ""
        self.addressDetailsLabel.text = ""
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.toggleButton.setImage(#imageLiteral(resourceName: "fovurite"), for: .normal)
        self.toggleButton.setImage(#imageLiteral(resourceName: "remove"), for: .selected)
    }

    func configure(place:FavoritePlace){
        let placeIcon = place.isFavourite ? (UIImage(named: place.category.lowercased()) ?? UIImage(named:"favorite_sel")) : UIImage(named:"history_unsel")
        self.placeIcon.image = placeIcon
        self.toggleButton.isHidden = false
        self.toggleButton.isSelected = place.isFavourite
        self.addressTitleLabel.text = place.isFavourite ? place.category.uppercased() : place.place
        self.addressDetailsLabel.text = place.isFavourite ? place.place : ""
    }
    
    func configureWith(place:FavoritePlace,section:Int){
        
        if section == 0 {
            let placeIcon =  UIImage(named:"history_unsel")
            self.placeIcon.image = placeIcon
        }else{
        let placeIcon = place.isFavourite ? (UIImage(named: place.category.lowercased()) ?? UIImage(named:"favorite_sel")) : UIImage(named:"history_unsel")
            self.placeIcon.image = placeIcon

        }
        self.toggleButton.isHidden = false
        self.toggleButton.isSelected = place.isFavourite
        self.addressTitleLabel.text = place.isFavourite ? place.category.uppercased() : place.place
        self.addressDetailsLabel.text = place.isFavourite ? place.place : ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
