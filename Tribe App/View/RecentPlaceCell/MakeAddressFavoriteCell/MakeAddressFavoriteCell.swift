//
//  MakeAddressFavoriteCell.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 03/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol MakeAddressFavoriteCellDelegate {
    func makeAddressFavoriteCell(cell: MakeAddressFavoriteCell, didSelectAddressType category: AddressCategory)
}

class MakeAddressFavoriteCell: UITableViewCell {
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var workButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var textField: FloatLabelTextField!
    var selectedCategory: AddressCategory = .home{
        didSet{
            if selectedCategory == .home{
                self.homeButton.isSelected = true
                self.workButton.isSelected = false
                self.otherButton.isSelected = false
                self.textField.text = "Home"
            }else if selectedCategory == .work{
                self.homeButton.isSelected = false
                self.workButton.isSelected = true
                self.otherButton.isSelected = false
                self.textField.text = "Work"
            }else{
                self.homeButton.isSelected = false
                self.workButton.isSelected = false
                self.otherButton.isSelected = true
                self.textField.text = ""
            }
        }
    }
    var delegate: MakeAddressFavoriteCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.backgroundColor = appColor.red
        self.homeButton.setImage(#imageLiteral(resourceName: "radio_sel"), for: .selected)
        self.homeButton.setImage(#imageLiteral(resourceName: "radio_unsel"), for: .normal)

        self.workButton.setImage(#imageLiteral(resourceName: "radio_sel"), for: .selected)
        self.workButton.setImage(#imageLiteral(resourceName: "radio_unsel"), for: .normal)

        self.otherButton.setImage(#imageLiteral(resourceName: "radio_sel"), for: .selected)
        self.otherButton.setImage(#imageLiteral(resourceName: "radio_unsel"), for: .normal)
        // Initialization code
    }

    @IBAction func onClickHome(_ sender: UIButton){
        self.homeButton.isSelected = true
        self.workButton.isSelected = false
        self.otherButton.isSelected = false
        self.textField.text = "Home"
        self.delegate?.makeAddressFavoriteCell(cell: self, didSelectAddressType: .home)
    }
    @IBAction func onClickWork(_ sender: UIButton){
        self.homeButton.isSelected = false
        self.workButton.isSelected = true
        self.otherButton.isSelected = false
        self.textField.text = "Work"
        self.delegate?.makeAddressFavoriteCell(cell: self, didSelectAddressType: .work)
    }
    @IBAction func onClickOthers(_ sender: UIButton){
    CommonClass.sharedInstance.setPlaceHolder(self.textField, placeHolderString: "Enter name".localizedString, withColor: appColor.boxGray)

        self.homeButton.isSelected = false
        self.workButton.isSelected = false
        self.otherButton.isSelected = true
        self.textField.text = ""
        self.delegate?.makeAddressFavoriteCell(cell: self, didSelectAddressType: .other)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
