//
//  ConfirmRideView.swift
//  Pepel Congo
//
//  Created by Parikshit on 25/02/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class ConfirmRideView: UIView {
//    @IBOutlet weak var pickUpAddressLabel: UILabel!
//    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var cateroryCarLabel: UILabel!
    @IBOutlet weak var wallletAmountLabel: UILabel!
    @IBOutlet weak var fairDetailsButton:UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var showWalletLabel:UILabel!
//    @IBOutlet weak var dotedImageView: UIImageView!
    @IBOutlet weak var offerButton: UIButton!

     
    class func instanceFromNib() -> ConfirmRideView {
        return UINib(nibName: "ConfirmRideView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ConfirmRideView
    }
    override func layoutSubviews() {
       // self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: appColor.redColor)
    }


}
