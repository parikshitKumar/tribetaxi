//
//  NavigationTitleView.swift
//  GPDock
//
//  Created by TecOrb on 29/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    @IBOutlet weak var titleLabel :UILabel!

    //let gradient = CAGradientLayer()
    var shouldAttributtedMiddle : Bool = false{
        didSet{
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }


    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }


    class func instanceFromNib() -> NavigationTitleView {
        return UINib(nibName: "NavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigationTitleView
    }

}
