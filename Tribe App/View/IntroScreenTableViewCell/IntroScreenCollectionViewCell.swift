//
//  IntroScreenCollectionViewCell.swift
//  Rolling Rim
//
//  Created by Parikshit on 08/04/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class IntroScreenCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
