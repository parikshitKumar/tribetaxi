//
//  DriverDetailOnConfirmRideView.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 21/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol DriverDetailOnConfirmRideViewDelegate {
    func onTap(view: DriverDetailOnConfirmRideView)
}

class DriverDetailOnConfirmRideView: UIView {
    var delegate : DriverDetailOnConfirmRideViewDelegate?
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var registrationAndOTP: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!

    class func instanceFromNib() -> DriverDetailOnConfirmRideView {
        return UINib(nibName: "DriverDetailOnConfirmRideView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverDetailOnConfirmRideView
    }

    override func layoutSubviews() {
        self.backgroundColor = .clear
        CommonClass.makeViewCircular(self.driverImage, borderColor: appColor.blueColor, borderWidth: 1)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.delegate?.onTap(view: self)
    }
}
