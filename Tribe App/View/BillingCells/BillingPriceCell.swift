//
//  BillingPriceCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 26/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class BillingPriceCell: UITableViewCell {
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var paymentModeLabel: UILabel!
    @IBOutlet weak var offerPrice: UILabel!
    @IBOutlet weak var actualTotalPrice: UILabel!
    @IBOutlet weak var showOfferPrice: UILabel!
    @IBOutlet weak var showActualTotalPrice: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var supportButtton: UIButton!
    @IBOutlet weak var backView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.supportButtton.roundCorners(corners: [.topLeft,.bottomLeft], radius: 17.5)
        self.backView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}





class BillingPriceRemoveOfferPriceCell: UITableViewCell {
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var paymentModeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var supportButtton: UIButton!
    @IBOutlet weak var backView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.supportButtton.roundCorners(corners: [.topLeft,.bottomLeft], radius: 17.5)
//        CommonClass.makeCircularBottomRadius(self.backView, cornerRadius: 15.0)

        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
