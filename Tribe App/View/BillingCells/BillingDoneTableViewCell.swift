//
//  BillingDoneTableViewCell.swift
//  HLS Taxi
//
//  Created by Parikshit on 01/08/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class BillingDoneTableViewCell: UITableViewCell {
    @IBOutlet weak var doneButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
     CommonClass.makeViewCircularWithCornerRadius(self.doneButton, borderColor: .clear, borderWidth: 0, cornerRadius: self.doneButton.frame.size.height/2)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
