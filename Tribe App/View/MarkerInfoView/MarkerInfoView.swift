//
//  MarkerInfoView.swift
//  JO CAB
//
//  Created by Parikshit on 30/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var markerImage: UIImageView!




    
    class func instanceFromNib() -> MarkerInfoView {
        return UINib(nibName: "MarkerInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MarkerInfoView
    }

}
