//
//  AddressNavigationView.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 28/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class AddressNavigationView: UIView {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addPikerButton: UIButton!

    class func instanceFromNib() -> AddressNavigationView {
        return UINib(nibName: "AddressNavigationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AddressNavigationView
    }
}
