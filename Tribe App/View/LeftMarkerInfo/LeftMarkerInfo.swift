//
//  LeftMarkerInfo.swift
//  JO CAB
//
//  Created by Parikshit on 21/11/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class LeftMarkerInfo: UIView {
    @IBOutlet weak var leftInfoView: UIView!
    @IBOutlet weak var leftInfoLabel: UILabel!
    @IBOutlet weak var markerImage: UIImageView!
    
    
    
    
    
    class func instanceFromNib() -> LeftMarkerInfo {
        return UINib(nibName: "LeftMarkerInfo", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LeftMarkerInfo
    }
    
}
