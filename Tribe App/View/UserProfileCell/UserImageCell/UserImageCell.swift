//
//  UserImageCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 07/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class UserImageCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
//        CommonClass.makeViewCircular(self.userImageView, borderColor: appColor.blueColor, borderWidth: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.editButton, borderColor: .clear, borderWidth: 0, cornerRadius: 5)

    }
    
    override func layoutSubviews() {
        self.editButton.layer.backgroundColor = appColor.blueColor.cgColor

        CommonClass.makeViewCircularWithCornerRadius(self.editButton, borderColor: .clear, borderWidth: 0, cornerRadius: 5)

        CommonClass.makeViewCircular(self.userImageView, borderColor: UIColor.clear, borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
