//
//  ProfileImageAndNameCell.swift
//  TaxiApp
//
//  Created by TecOrb on 25/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ProfileImageAndNameCell: UITableViewCell {
    @IBOutlet weak var coverPhoto : UIImageView!
    @IBOutlet weak var profileImage : UIImageView!
//    @IBOutlet weak var nameLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.profileImage, borderColor: UIColor(patternImage: #imageLiteral(resourceName: "background")), borderWidth: 1)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ProfileDetailCell: UITableViewCell {
    @IBOutlet weak var nameTextField : FloatLabelTextField!
    @IBOutlet weak var mobileNumberTextField : FloatLabelTextField!
    @IBOutlet weak var emailTextField : FloatLabelTextField!
    @IBOutlet weak var profileImage : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.profileImage, borderColor: UIColor.clear, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

