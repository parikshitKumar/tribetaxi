//
//  LanguageChangeView.swift
//  JO CAB
//
//  Created by Parikshit on 15/12/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class LanguageChangeView: UIView {
    @IBOutlet weak var englishFlag: UIButton!
    @IBOutlet weak var portugueseFlag: UIButton!

    class func instanceFromNib() -> LanguageChangeView {
        return UINib(nibName: "LanguageChangeView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LanguageChangeView
    }

}
