//
//  SupportQuestionTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 13/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportQuestionTableViewCell: UITableViewCell {
    @IBOutlet weak var questionTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

