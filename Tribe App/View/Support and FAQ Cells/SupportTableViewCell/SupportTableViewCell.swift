//
//  SupportTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportTableViewCell: UITableViewCell {
    @IBOutlet weak var questionTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}



class SupportUserNameTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var showUserHelpLabel : UILabel!

    //@IBOutlet weak var separatorView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


class SupportNotListedTableViewCell: UITableViewCell {
    @IBOutlet weak var showProblemNotListed : UILabel!
    @IBOutlet weak var reachUsButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        CommonClass.makeViewCircularWithCornerRadius(self.reachUsButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 3)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


