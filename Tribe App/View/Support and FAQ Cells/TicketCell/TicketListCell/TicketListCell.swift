//
//  TicketListCell.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TicketListCell: UITableViewCell {
    @IBOutlet weak var ticketImageView : UIImageView!
    @IBOutlet weak var referenceNumberLabel : UILabel!
    @IBOutlet weak var queryTextLabel : UILabel!
    @IBOutlet weak var statusLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }



    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
