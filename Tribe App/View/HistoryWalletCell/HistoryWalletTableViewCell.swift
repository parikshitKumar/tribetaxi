//
//  HistoryWalletTableViewCell.swift
//  Pepel Congo
//
//  Created by Parikshit on 10/04/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class HistoryWalletTableViewCell: UITableViewCell {
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var backView: UIView!

    

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }
    
    
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.backView, borderColor: .clear, borderWidth: 1, cornerRadius: 10)
        self.backView.backgroundColor = appColor.white
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
