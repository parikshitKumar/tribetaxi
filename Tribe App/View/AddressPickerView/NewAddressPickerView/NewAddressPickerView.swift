//
//  NewAddressPickerView.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 16/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
protocol NewAddressPickerViewDelegate {
    func addressPickerTextFieldDidBeginEditing(_ text: String,forAddressType addressType: AddressType)
    func addressPickerTextFieldDidEndEditing(_ text: String,forAddressType addressType: AddressType)
    func addressPickerTextFieldDidChange(_ text: String,forAddressType addressType: AddressType)
}

class NewAddressPickerView: UIView,UITextFieldDelegate {
    @IBOutlet weak var dotedImageView: UIImageView!

    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    var selectedAddressType : AddressType = .from

    var delegate: NewAddressPickerViewDelegate?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.fromTextField.delegate = self
        self.toTextField.delegate = self
        self.fromTextField.addTarget(self, action: #selector(textFieldChangeEdit(_:)), for: .editingChanged)
        self.toTextField.addTarget(self, action: #selector(textFieldChangeEdit(_:)), for: .editingChanged)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.fromTextField.delegate = self
        self.toTextField.delegate = self
        self.fromTextField.addTarget(self, action: #selector(textFieldChangeEdit(_:)), for: .editingChanged)
        self.toTextField.addTarget(self, action: #selector(textFieldChangeEdit(_:)), for: .editingChanged)
    }

    override func layoutSubviews() {
        self.dotedImageView.image = UIImage.drawDottedImage(width: 3, height: dotedImageView.frame.size.height, color: UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 1.0))
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let text = textField.text else{return}
        self.selectedAddressType = (textField == fromTextField) ? .from : .to
        delegate?.addressPickerTextFieldDidBeginEditing(text, forAddressType: selectedAddressType)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else{return}
        self.selectedAddressType = (textField == fromTextField) ? .from : .to
        delegate?.addressPickerTextFieldDidBeginEditing(text, forAddressType: selectedAddressType)
    }

    @IBAction func textFieldChangeEdit(_ textField: UITextField) {
        guard let text = textField.text else{return}
        self.selectedAddressType = (textField == fromTextField) ? .from : .to
        delegate?.addressPickerTextFieldDidBeginEditing(text, forAddressType: selectedAddressType)
    }
    
}
