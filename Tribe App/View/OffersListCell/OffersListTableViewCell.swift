//
//  OffersListTableViewCell.swift
//  Rolling Rim
//
//  Created by Parikshit on 30/05/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class OffersListTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var promCodeDescription: UILabel!
    @IBOutlet weak var promoCodeTitle : UILabel!
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var offerPercentageLabel : UILabel!
    @IBOutlet weak var offerExpireLabel : UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var selectOfferButton: UIButton!
    @IBOutlet weak var perUserCountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

    
    

