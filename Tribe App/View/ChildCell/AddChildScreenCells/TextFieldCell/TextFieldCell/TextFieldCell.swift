//
//  TextFieldCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    @IBOutlet weak var textField: FloatLabelTextField!
    @IBOutlet weak var containner: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textField.titleFont = fonts.OpenSans.regular.font(.small)
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



class RegisterButtonCell: UITableViewCell {
    @IBOutlet weak var registerButton : UIButton!
    @IBOutlet weak var supportButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.registerButton, borderColor: UIColor.clear, borderWidth: 1, cornerRadius: self.registerButton.frame.size.height/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



class ReferralCodeCell: UITableViewCell {
    @IBOutlet weak var gotReferralCodeButton : UIButton!
    @IBOutlet weak var helpButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        // CommonClass.makeViewCircularWithCornerRadius(self.registerButton, borderColor: UIColor.white, borderWidth: 1, cornerRadius: self.registerButton.frame.size.height/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


