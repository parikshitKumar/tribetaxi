//
//  DOBCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

protocol DOBCellDelegate {
    func dateOfBirth(dobCell cell: DOBCell, didSelectDate date: Date)
}

class DOBCell: UITableViewCell {
    @IBOutlet weak var dobTextField: FloatLabelTextField!
    @IBOutlet weak var containner: UIView!
    var dob: Date!
    var datePicker : UIDatePicker!
    var delegate: DOBCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.dobTextField.titleFont = fonts.OpenSans.regular.font(.small)
        self.setUpDatePicker()
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)

        if dob != nil{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            self.dobTextField.text = dateFormatter.string(from: dob)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpDatePicker() -> Void {
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        self.datePicker.minimumDate =  Date.init(timeInterval: -(30*12*30*24*60*60), since: Date())
        self.datePicker.maximumDate = Date()
        self.datePicker.date = (self.dob == nil) ? Date() : self.dob
        self.datePicker.addTarget(self, action: #selector(onDateChange(_:)), for: .valueChanged)
        // ToolBar

        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = appColor.red
        toolBar.sizeToFit()
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = appColor.red
        
        // Adding Button ToolBar
        
        //new
        
       // let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeViewController.doneClick))
        
        
        //doneButton.tintColor = UIColor.white
        //let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        
       // let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(HomeViewController.cancelClick))
        
       // cancelButton.tintColor = UIColor.white

        //toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        dobTextField.inputView = self.datePicker
        dobTextField.inputAccessoryView = toolBar
    }

    @IBAction func onDateChange(_ datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        self.dob = datePicker.date//dateFormatter1.string(from: datePicker.date)
        self.dobTextField.text = dateFormatter.string(from: datePicker.date)
        self.delegate?.dateOfBirth(dobCell: self, didSelectDate: self.dob)
    }

    @objc func doneClick() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        self.dob = datePicker.date//dateFormatter1.string(from: datePicker.date)
        self.dobTextField.text = dateFormatter.string(from: datePicker.date)
        self.delegate?.dateOfBirth(dobCell: self, didSelectDate: self.dob)
        dobTextField.resignFirstResponder()
    }

    @objc func cancelClick() {
        dobTextField.resignFirstResponder()
    }


}
