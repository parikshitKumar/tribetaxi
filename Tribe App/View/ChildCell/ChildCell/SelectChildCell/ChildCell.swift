//
//  ChildCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 13/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class ChildCell: UITableViewCell {
    @IBOutlet weak var childNameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var selectionTick: UIButton!
    @IBOutlet weak var childImage: UIImageView!
    @IBOutlet weak var containner: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionTick.setImage(#imageLiteral(resourceName: "check"), for: .selected)
        self.selectionTick.setImage(#imageLiteral(resourceName: "check_sel"), for: .normal)
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.childImage, borderColor: appColor.blueColor, borderWidth: 1)
        //CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
