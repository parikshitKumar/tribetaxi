//
//  ContactCell.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 16/07/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit
import CountryPickerView

protocol ContactCellDelegate {
    func contact(contactCell cell:ContactCell, didSelectCountry selectedCountry:Country)
}

class ContactCell: UITableViewCell {
    var delegate: ContactCellDelegate?
    @IBOutlet weak var countryCodeLabel : UILabel!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    @IBOutlet weak var phoneNumberTextField : UITextField!
    @IBOutlet weak var containner : UIView!

    var viewController:UIViewController!
    var cpv : CountryPickerView!
    var selectedCountry: Country!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.countryPickerSetUp()
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClickCountryCodeButton(_ sender: UIButton){
        if self.viewController != nil{
            cpv.showCountriesList(from: viewController)
        }
    }
}


extension ContactCell: CountryPickerViewDataSource,CountryPickerViewDelegate{

    func countryPickerSetUp(with country:Country? = nil) {
        if cpv != nil{
            cpv.removeFromSuperview()
        }

        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.OpenSans.regular.font(.medium)
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        cpv.dataSource = self
        cpv.delegate = self

        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])

        if let selCountry = country{
            self.selectedCountry = selCountry
            cpv.setCountryByCode(selCountry.code)
        }else{
            self.selectedCountry = cpv.selectedCountry
            delegate?.contact(contactCell: self, didSelectCountry: self.selectedCountry)
        }
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
    }

    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }


    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil
        //        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: nil, action: nil)
        //        barButton.tintColor = appColor.blue
        //        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.countryCodeLabel.text = "(\(self.selectedCountry.code))"+self.selectedCountry.phoneCode
        delegate?.contact(contactCell: self, didSelectCountry: self.selectedCountry)
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }


}
