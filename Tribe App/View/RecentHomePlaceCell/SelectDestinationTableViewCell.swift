//
//  SelectDestinationTableViewCell.swift
//  HLS Taxi
//
//  Created by Parikshit on 31/07/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class SelectDestinationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel :UILabel!
    @IBOutlet weak var categoryLabel :UILabel!
    @IBOutlet weak var categoryImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
