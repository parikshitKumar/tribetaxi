//
//  LandingDestinationTableViewCell.swift
//  HLS Taxi
//
//  Created by Parikshit on 31/07/19.
//  Copyright © 2019 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class LandingDestinationTableViewCell: UITableViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var destinationButton:UIButton!
    @IBOutlet weak var searchImage:UIImageView!
    @IBOutlet weak var destinationView:UIView!
    @IBOutlet weak var destinationLabel:UILabel!
    @IBOutlet weak var scheduleButton:UIButton!
    @IBOutlet weak var scheduleView:UIView!
    @IBOutlet weak var scheduleLabel:UILabel!
    @IBOutlet weak var userNameLabel:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code


    }
    
    override func layoutSubviews() {
        CommonClass.makeCircularCornerNewMethodRadius(self.destinationView, cornerRadius: 15.0)

    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
