//
//  OnGoingRideView.swift
//  HLS Taxi
//
//  Created by Nakul Sharma on 25/09/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class OnGoingRideView: UIView {
    @IBOutlet weak var driverIcon: UIImageView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var carCategoryLabel: UILabel!
    @IBOutlet weak var crnLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!

    @IBOutlet weak var pickUpAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var containner: UIView!


    class func instanceFromNib() -> OnGoingRideView {
        return UINib(nibName: "OnGoingRideView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OnGoingRideView
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupRideDetails(ride:Ride){
        self.dateTimeLabel.text = (ride.rideType == .instant) ? ride.createdAt : ride.scheduledAt
        self.carCategoryLabel.text = ride.carCategory.categoryName
        self.crnLabel.text = "CRN\(ride.ID)"
        if ride.driver.ID != ""{
            self.driverIcon.sd_setImage(with: URL(string:ride.driver.profileImage) ?? URL(string: api.base.url())!, placeholderImage: AppSettings.shared.userAvatarImage(username:ride.driver.fullName))
        }else{
            self.driverIcon.image = UIImage(named: ride.carCategory.categoryName.lowercased())
        }
        self.pickUpAddressLabel.text = ride.startLocation
        self.dropAddressLabel.text = ride.endLocation
        CommonClass.makeViewCircular(self.driverIcon, borderColor: appColor.blueColor, borderWidth: 1)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircular(driverIcon, borderColor: appColor.blueColor, borderWidth: 1)
    }


}
