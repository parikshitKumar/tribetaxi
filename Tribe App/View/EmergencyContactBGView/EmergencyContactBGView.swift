//
//  EmergencyContactBGView.swift
//  Carting Kidzs
//
//  Created by Nakul Sharma on 02/08/18.
//  Copyright © 2018 TecOrb Technologies Pvt. Ltd. All rights reserved.
//

import UIKit

class EmergencyContactBGView: UIView {
    @IBOutlet weak  var firstLabel:  UILabel!
    @IBOutlet weak  var secondLabel:  UILabel!
    @IBOutlet weak  var thirdLabel:  UILabel!
    class func instanceFromNib() -> EmergencyContactBGView {
        return UINib(nibName: "EmergencyContactBGView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmergencyContactBGView
    }
}
