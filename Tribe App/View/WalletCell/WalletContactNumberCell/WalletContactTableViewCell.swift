//
//  WalletContactTableViewCell.swift
//  Pepel Congo
//
//  Created by Parikshit on 12/03/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class WalletContactTableViewCell: UITableViewCell {
      @IBOutlet weak var countryCodeLabel :UILabel!
      @IBOutlet weak var contactLabel :UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
