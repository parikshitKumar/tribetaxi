//
//  SendMpneyToTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 26/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class SendMoneyToTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var AccNumber: UILabel!
    @IBOutlet weak var ifscCode: UILabel!
    @IBOutlet weak var branch: UILabel!
    
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showAccNumberLabel: UILabel!
    @IBOutlet weak var showIfscCodeLabel: UILabel!
    @IBOutlet weak var showbranchLabel: UILabel!
    @IBOutlet weak var showBankDetailLabel: UILabel!

    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
