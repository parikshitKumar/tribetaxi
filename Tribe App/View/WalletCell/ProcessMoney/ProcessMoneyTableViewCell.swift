//
//  ProcessMoneyTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 26/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class ProcessMoneyTableViewCell: UITableViewCell {
    @IBOutlet weak var processImage: UIImageView!
    @IBOutlet weak var processLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
