//
//  AddMoneyWalletTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 26/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class AddMoneyWalletTableViewCell: UITableViewCell {
    @IBOutlet weak var AoaNumber: UILabel!
    @IBOutlet weak var addMoneyView: UIView!
    @IBOutlet weak var addMoneyButton: UIButton!
    @IBOutlet weak var showCurrentBalanceLabel: UILabel!
    @IBOutlet weak var showAddMoneyLabel: UILabel!



    
    override func layoutSubviews() {

//        self.addMoneyView.backgroundColor = appColor.blackLight
        CommonClass.makeViewCircularWithCornerRadius(self.addMoneyView, borderColor: .groupTableViewBackground, borderWidth: 1, cornerRadius: 5)
        CommonClass.makeViewCircularWithRespectToHeight(self.AoaNumber, borderColor: .clear, borderWidth: 0)

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
