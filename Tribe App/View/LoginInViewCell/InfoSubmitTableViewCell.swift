//
//  InfoSubmitTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 25/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class InfoSubmitTableViewCell: UITableViewCell {
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var alreadyAUserLabel: UILabel!
    @IBOutlet weak var logInLabel: UILabel!
//    @IBOutlet weak var selectCheckBoxButton: UIButton!
//    @IBOutlet weak var termsAndConditionButton: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func layoutSubviews() {
//        self.backgroundColor = appColor.blackOne
        CommonClass.makeViewCircularWithCornerRadius(self.signUpButton, borderColor: .groupTableViewBackground, borderWidth: 0, cornerRadius: 25)
        CommonClass.makeCircularTopRadius(self, cornerRadius: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

