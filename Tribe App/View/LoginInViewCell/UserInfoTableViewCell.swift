//
//  UserInfoTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 25/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
import CountryPickerView

protocol UserContactCellDelegate {
    func contact(contactCell cell:UserInfoTableViewCell, didSelectCountry selectedCountry:Country)
}

class UserInfoTableViewCell: UITableViewCell {
    var delegate: UserContactCellDelegate?

    @IBOutlet weak var countryCodeLabel : UILabel!
    @IBOutlet weak var countryCodeButton : UIButton!

    @IBOutlet weak var phoneNumberTextField : UITextField!
    @IBOutlet weak var fullNameTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var countryPickerView: UIView!
    
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showPasswordLabel: UILabel!
    @IBOutlet weak var showEmailLabel: UILabel!
    @IBOutlet weak var showMobileLabel: UILabel!
    @IBOutlet weak var countryImage: UIImageView!


    var viewController:UIViewController!
    var cpv : CountryPickerView!
    var selectedCountry: Country!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
//        self.backgroundColor = appColor.blackOne
//        CommonClass.sharedInstance.setPlaceHolder(self.fullNameTextField, placeHolderString: "User name".localizedString, withColor: .white)
//        CommonClass.sharedInstance.setPlaceHolder(self.emailTextField, placeHolderString: "Email".localizedString, withColor: .white)
//        CommonClass.sharedInstance.setPlaceHolder(self.phoneNumberTextField, placeHolderString: "Mobile number".localizedString, withColor: .white)
//        CommonClass.sharedInstance.setPlaceHolder(self.passwordTextField, placeHolderString: "Password".localizedString, withColor: .white)
        self.addViewPassword(textField: passwordTextField)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickCountryCodeButton(_ sender: UIButton){
        if self.viewController != nil{
            cpv.showCountriesList(from: viewController)
        }
    }
    
    
    func addViewPassword(textField: UITextField){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .selected)
        button.setImage(UIImage(named: "hide_password"), for: .normal)
        let height = textField.frame.size.height
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - height), y: CGFloat(0), width: height, height: height)
        button.addTarget(self, action: #selector(showPassword(_:)), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    @IBAction func showPassword(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.passwordTextField.isSecureTextEntry = !sender.isSelected
    }

}



extension UserInfoTableViewCell: CountryPickerViewDataSource,CountryPickerViewDelegate{
    
    func countryPickerSetUp(with country:Country? = nil) {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.OpenSans.regular.font(.medium)
        cpv.countryDetailsLabel.textColor = UIColor.clear
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        cpv.dataSource = self
        cpv.delegate = self
        
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        
        if let selCountry = country{
            self.selectedCountry = selCountry
            cpv.setCountryByCode(selCountry.code)
        }else{
            self.selectedCountry = cpv.selectedCountry
            delegate?.contact(contactCell: self, didSelectCountry: self.selectedCountry)
        }
        self.countryCodeLabel.text = self.selectedCountry.phoneCode
//        self.countryImage.image = self.selectedCountry.flag
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }
    
    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        return nil
        //        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: nil, action: nil)
        //        barButton.tintColor = appColor.blue
        //        return barButton
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }
    
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        self.countryCodeLabel.text = self.selectedCountry.phoneCode
//        self.countryImage.image = self.selectedCountry.flag
        delegate?.contact(contactCell: self, didSelectCountry: self.selectedCountry)
    }
    
    
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    
}

