//
//  ImageTableViewCell.swift
//  JO CAB
//
//  Created by Parikshit on 25/10/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var backView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeCircularTopRadius(self.backView, cornerRadius: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
