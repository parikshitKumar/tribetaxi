//
//  AnimateHeaderView.swift
//  Pepel Congo
//
//  Created by Parikshit on 12/03/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit

class AnimateHeaderView: UIView {
    @IBOutlet weak var pickAddressLabel: UILabel!
    @IBOutlet weak var dropAddressLabel: UILabel!
    @IBOutlet weak var pickAddressButton: UIButton!
    @IBOutlet weak var dropAddressButton: UIButton!
    @IBOutlet weak var pickAddressView: UIView!
    @IBOutlet weak var dropAddressView: UIView!

    class func instanceFromNib() -> AnimateHeaderView {
        
        return UINib(nibName: "AnimateHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AnimateHeaderView
    }
    
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.pickAddressView, borderColor: .clear, borderWidth: 1, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.dropAddressView, borderColor: .clear, borderWidth: 1, cornerRadius: 2)

    }
    
    func setPickAndDropAddress(pick:String,drop:String){
        pickAddressLabel.text = pick
        dropAddressLabel.text = drop
        
    }
}
