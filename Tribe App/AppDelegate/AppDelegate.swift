//
//  AppDelegate.swift
//  Tribe App
//
//  Created by Parikshit on 26/08/19.
//  Copyright © 2019 Parikshit. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces
import SystemConfiguration
import SwiftyJSON
import Stripe
import CoreLocation



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var player: AVAudioPlayer?
    var user: User!
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!

    
    func getLoggedInUser(){
        self.user = User.loadSavedUser()
        
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: .MessagingRegistrationTokenRefreshed, object: nil)

        FirebaseApp.configure()
        STPPaymentConfiguration.shared().publishableKey = stripeKey
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        self.getLoggedInUser()
        if AppSettings.shared.isFirstLaunchAfterReset{
            AppSettings.shared.resetOnFirstAppLaunch()
        }
        self.registerForRemoteNotification()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledToolbarClasses = [SupportTicketViewController.self]
        
        //        if !isDebugEnabled {
        //            self.logCrashlyticsUser()
        //        }
        self.launchApplicaton(userInfo: nil)
        if AppSettings.shared.isLoggedIn{
//            LoginService.sharedInstance.updateDeviceTokeOnServer()
            self.getOnGoingRides()
       }
        
        LoginService.sharedInstance.updateDeviceTokeOnServer()

        //self.updateLocationInBackground()

        
        // Override point for customization after application launch.
        return true
    }
    
    
    func launchApplicaton(userInfo:Dictionary<String,AnyObject>?){
        if AppSettings.shared.isLoggedIn{
            AppSettings.shared.proceedToHome()
        }else{
            //AppSettings.shared.proceedToLoginModule()
            AppSettings.shared.proceedToLoginModule()        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Rolling_Rim")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}



extension AppDelegate : MessagingDelegate{
    
    
    
    
    func logCrashlyticsUser() {
        if AppSettings.shared.isLoggedIn{
            let user = User.loadSavedUser()
            Crashlytics.sharedInstance().setUserEmail(user.email)
            Crashlytics.sharedInstance().setUserIdentifier(user.ID)
            Crashlytics.sharedInstance().setUserName(user.fullName)
        }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        kUserDefaults.set(fcmToken, forKey: kDeviceToken)
        LoginService.sharedInstance.updateDeviceTokeOnServer()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        if let message = remoteMessage.appData as? Dictionary<String,AnyObject> {
            print_debug("Message ID from firebase: \(message)")
        }
    }
}

/*=============================NOTIFICATION HANDLING==============================*/


extension AppDelegate{
    class func getAppDelegate() -> AppDelegate{
        
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        kUserDefaults.set(deviceTokenString, forKey: kDeviceToken)
        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
        }
    }
    
    
    func registerForRemoteNotification() {
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
            if error == nil{
                DispatchQueue.main.async {
                    Messaging.messaging().delegate = self
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    
    
    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            kUserDefaults.set(refreshedToken, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print_debug("Device token for push notifications: FAIL -- ")
        print_debug(error.localizedDescription)
    }
    
    //Called when a notification is delivered to a foreground app.
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        if let userInfo = notification.request.content.userInfo as? Dictionary<String,AnyObject>{
            let notificationType = self.getNotificationType(from: userInfo)
            if notificationType == .RIDE_CANCELLED_BY_DRIVER_NOTIFICATION{
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: nil)
            }else if (notificationType == .SINGLE_USER_NOTIFICATION) || (notificationType == .BULK_USER_NOTIFICATION) || (notificationType == .OFFER_USER_NOTIFICATION){
                
                if AppSettings.shared.isNotificationEnable {
                    completionHandler([.alert, .badge, .sound])
                    return
                }
                
            }else if notificationType == .DROP_CHANGE_OLD_USER_NOTIFICATION {
                NotificationCenter.default.post(name: .DROP_CHANGE_OLD_USER_NOTIFICATION, object: nil, userInfo: nil)
                NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: nil)
                
                if AppSettings.shared.isNotificationEnable {
                    completionHandler([.alert, .badge, .sound])
                    return
                }
                
            }
            
        }
        if !AppSettings.shared.isNotificationEnable{
            completionHandler([])
            return
        }
        
        if (self.window?.rootViewController?.topViewController is RideTrackingViewController) || (self.window?.rootViewController?.topViewController is CheckingRidesViewController) {
            completionHandler([])
        } else {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    //MARK:- Incall status bar handling
    func application(_ application: UIApplication, willChangeStatusBarFrame newStatusBarFrame: CGRect) {
        self.window?.layoutIfNeeded()
        self.window?.updateConstraintsIfNeeded()
    }
    
    func application(_ application: UIApplication, didChangeStatusBarFrame oldStatusBarFrame: CGRect) {
        self.window?.layoutIfNeeded()
        self.window?.updateConstraintsIfNeeded()
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfo = response.notification.request.content.userInfo as? Dictionary<String,AnyObject>{
            if AppSettings.shared.isLoggedIn{
                self.handleNotifications(with: userInfo)
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            self.handleNotifications(with: userInfoDict)
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            self.handleNotifications(with: userInfoDict)
        }
    }
    
    func getNotificationType(from userInfo: Dictionary<String,AnyObject>) -> Notification.Name {
        let json = JSON.init(userInfo)
        print_debug("nitification json: \r\n \(json)")
        
        guard let notificationType = userInfo[kNotificationType] as? String else {
            return NSNotification.Name("none")
        }
        
        if notificationType == kRideAcceptedNotification{
            NotificationCenter.default.post(name: .RIDE_ACCEPTED_NOTIFICATION, object: nil, userInfo: nil)
            
            return Notification.Name.RIDE_ACCEPTED_NOTIFICATION
        }
        else if notificationType == kRideEndedNotification{
            NotificationCenter.default.post(name: .RIDE_END_NOTIFICATION, object: nil, userInfo: nil)
            
            return Notification.Name.RIDE_END_NOTIFICATION
        }
        else if notificationType == kDriverReachedNotification{
            NotificationCenter.default.post(name: .DRIVER_REACHED_NOTIFICATION, object: nil, userInfo: nil)
            
            return Notification.Name.DRIVER_REACHED_NOTIFICATION
        }
        else if notificationType == kRideStartedNotification{
            NotificationCenter.default.post(name: .RIDE_START_NOTIFICATION, object: nil, userInfo: nil)
            return Notification.Name.RIDE_START_NOTIFICATION
            
        }else if notificationType == kDriverCancelledNotification{
            NotificationCenter.default.post(name: .RIDE_CANCELLED_BY_DRIVER_NOTIFICATION, object: nil, userInfo: nil)
            //            NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: nil)
            
            return Notification.Name.RIDE_CANCELLED_BY_DRIVER_NOTIFICATION
        }
        else if notificationType == kUserBlockedByAdmin{
            userBlockByAdminText(userInfo: userInfo)
            return NSNotification.Name("none")
        }
        else if notificationType == kUserUnBlockedByAdmin{
            userBlockByAdminText(userInfo: userInfo)
            return NSNotification.Name("none")
        }else if notificationType == kDropChangeNewUserNotification {
            return Notification.Name.DROP_CHANGE_OLD_USER_NOTIFICATION
        }else if notificationType == kSingleUserNotification {
            return Notification.Name.SINGLE_USER_NOTIFICATION
            
        }else if notificationType == kBulkUserNotification {
            return Notification.Name.BULK_USER_NOTIFICATION
            
        }else if notificationType == kOfferUserNotification {
            return Notification.Name.OFFER_USER_NOTIFICATION
            
        }
//        else if notificationType == kScheduleRideAccept {
//            NotificationCenter.default.post(name: .RIDE_UPDATED_NOTIFICATION, object: nil, userInfo: nil)
//            NotificationCenter.default.post(name: .REMOVE_SCHEDULE_RIDE_NOTIFICATION, object: nil, userInfo: nil)
//            
//            return Notification.Name.SCHEDULE_RIDE_ACCEPTED_NOTIFICATION
//            
//        }
        else{
            return NSNotification.Name("none")
        }
    }
    
    func handleNotifications(with userInfo : Dictionary<String,AnyObject>){
        var rideID = ""
        if let rID = userInfo["ride_id"] as? String{
            rideID = rID
        }else  if let rID = userInfo["ride_id"] as? Int{
            rideID = "\(rID)"
        }
        
        if rideID != ""{
            self.getRideBookingDetails(rideId: rideID)
            
        }
    }
    
    
    func userBlockByAdminText(userInfo : Dictionary<String,AnyObject>) {
        
        if let appsInfo = userInfo ["aps"] as? Dictionary<String,AnyObject> {
            if let alertInfo = appsInfo["alert"] as? Dictionary<String,AnyObject>{
                var tempBody = ""
                if let body = alertInfo["body"] as? String {
                    tempBody = body
                    
                }
                NKToastHelper.sharedInstance.showErrorAlert(nil, message:tempBody)
                
            }
            
        }
        
        
    }
    
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "carLock", withExtension: "mp3") else {
            print_debug("url not found")
            return
        }
        do {
            /// this codes for making this app ready to takeover the device audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            /// change fileTypeHint according to the type of your audio file (you can omit this)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            // no need for prepareToPlay because prepareToPlay is happen automatically when calling play()
            player!.play()
            self.perform(#selector(stopAudio), with: nil, afterDelay: 3)
        } catch let error{
            print_debug("error: \(error.localizedDescription)")
        }
    }
    
    @objc func stopAudio() {
        if player != nil {
            player?.stop()
            player = nil
        }
    }
    
    
    func getOnGoingRides(){
        RideService.sharedInstance.getOnGoingRides { (success, resRides, count, message) in
            if var rides = resRides,rides.count > 0{
                rides.sort(by: { (ride1, ride2) -> Bool in
                    return ride1.ID < ride2.ID
                })
                //                if (self.window?.rootViewController?.topViewController is RideTrackingViewController){return}
                guard let currentRide = rides.last else{return}
                self.processRide(ride: currentRide)
            }
        }
    }
    
    func getRideBookingDetails(rideId:String){
        if !AppSettings.isConnectedToNetwork {
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        //AppSettings.shared.showLoader(withStatus: "Please wait..")
        RideService.sharedInstance.getRideDetails(rideId) { (success,resRide,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let ride = resRide{
                    AppSettings.shared.hideLoader()
                    self.processRide(ride: ride)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
        }
    }
    
    
    
    
    
    func processRide(ride:Ride){
        
        print("rideuserID>>>>\(ride.user.ID)")
        print("userID>>>>\(self.user.ID)")
        
        if ride.user.ID != self.user.ID{
            // NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Bad Request!")
            return
        }
        let currentStatus = ride.getCurrentStatus()
        switch currentStatus{
        case .cancelled:
            if ride.rideType == .scheduled{
                self.openScheduledRideDetails(ride: ride)
            }else{
                self.openRideDetails(ride: ride)
            }
        case .completed:
            self.openPaymentScreen(ride: ride)
        case .paymentPending:
            self.openPaymentScreen(ride: ride)
        case .onGoing:
            self.openRideTracking(ride: ride)
        case .scheduled:
            self.openScheduledRideDetails(ride: ride)
        }
    }
    
    func getWindowNavigation()-> UINavigationController?{
        guard let window = self.window else {
            return nil
        }
        guard let nav = window.rootViewController as? UINavigationController else {
            return nil
        }
        return nav
    }
    
    func openRideTracking(ride:Ride){
        if (self.window?.rootViewController?.topViewController is RideTrackingViewController) {
            return
        }else{
            let rideTrackingVC = AppStoryboard.History.viewController(RideTrackingViewController.self)
            rideTrackingVC.ride = ride
            rideTrackingVC.fromHistory = false
            rideTrackingVC.isFromAppDelegate = true
            guard let navigationController = self.getWindowNavigation() else{return}
            navigationController.pushViewController(rideTrackingVC, animated: true)
        }
    }
    
    func openPaymentScreen(ride:Ride){
        if (self.window?.rootViewController?.topViewController is BillViewController) {
            return
        }else{
            let billingVC = AppStoryboard.Billing.viewController(BillViewController.self)
            billingVC.ride = ride
            billingVC.fromHistory = false
            guard let navigationController = self.getWindowNavigation() else{return}
            navigationController.pushViewController(billingVC, animated: true)
        }
    }
    
    func openRideDetails(ride:Ride){
        if (self.window?.rootViewController?.topViewController is RideDetailsViewController) {
            return
        }else{
            let rideDetailsVC = AppStoryboard.History.viewController(RideDetailsViewController.self)
            rideDetailsVC.ride = ride
            guard let navigationController = self.getWindowNavigation() else{return}
            navigationController.pushViewController(rideDetailsVC, animated: true)
        }
        
    }
    func openScheduledRideDetails(ride:Ride){
        if (self.window?.rootViewController?.topViewController is ScheduledRideDetailsViewController) {
            return
        }else{
            let rideDetailsVC = AppStoryboard.History.viewController(ScheduledRideDetailsViewController.self)
            rideDetailsVC.ride = ride
            guard let navigationController = self.getWindowNavigation() else{return}
            navigationController.pushViewController(rideDetailsVC, animated: true)
        }
    }
}


///////////////


extension AppDelegate : CLLocationManagerDelegate {
    

    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
        case .authorizedAlways:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
        case .restricted:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: nil) { (done) in
                
            }
            
        case .denied:
              CommonClass.sharedInstance.showGotoLocationSettingAlert(in: nil) { (done) in
                
            }
    }
    }
    

    func startReceivingSignificantLocationChanges() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedWhenInUse {
            return
        }
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            self.currentLocation = location

        }
    }
    
    
    func updateLocationInBackground(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.allowsBackgroundLocationUpdates = false
        
        //updated by Nakul
        locationManager.pausesLocationUpdatesAutomatically = false
        //End of update
        //locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        //self.startReceivingSignificantLocationChanges()
//        locationManager.distanceFilter = 10
        
    }
    
}



extension AppDelegate{

    class func locationManager() -> CLLocationManager{
        return (UIApplication.shared.delegate as! AppDelegate).locationManager
    }
}
